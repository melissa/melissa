from setuptools import setup


with open("VERSION", "r") as f:
    __version__ = f.read().strip()


setup(
    name='melissa',
    version=__version__,
    python_requires='>=3.8',
    package_data={'melissa': ['utility/bash_scripts/*.sh']}
)
