"""This script defines helper classes server-launcher connection."""
import ctypes
from typing import Any, List, Union
from enum import Enum

import numpy as np
from numpy.typing import NDArray

# C prototypes
c_char_ptr = ctypes.POINTER(ctypes.c_char)
c_double_ptr = ctypes.POINTER(ctypes.c_double)
c_void_ptr_ptr = ctypes.POINTER(ctypes.c_void_p)


# This should maybe go into a constants file
MELISSA_MAX_NODE_NAME_LENGTH = 128


# def get_str(buff: Union[bytes, bytearray]) -> str:
#     """Extracts and decodes the first null-terminated string from a byte buffer.

#     ### Parameters
#     - **buff (Union[bytes, bytearray]): The byte buffer to decode.

#     ### Returns
#     - **str**: The decoded string up to the first null byte (`\x00`)."""
#     return buff.split(b'\x00')[0].decode('utf-8')


# def encode(msg_parts: List[str]) -> Iterable[bytes]:
#     """Encodes a list of strings into an iterable of bytes.

#     ### Parameters
#     - **msg_parts (List[str]): A list of strings to be encoded.

#     ### Returns
#     - **Iterable[bytes]**: An iterable where each string is encoded to bytes."""
#     return map(lambda x: bytes(x), msg_parts)


class MessageType(Enum):
    """Message types for launcher-server communication."""
    HELLO = 0
    JOB = 1
    DROP = 2
    STOP = 3
    TIMEOUT = 4
    SIMU_STATUS = 5
    SERVER = 6
    ALIVE = 7
    CONFIDENCE_INTERVAL = 8
    OPTIONS = 9


class ServerNodeName:
    """Represents a server node with a rank and node name for encoding messages.

    ### Attributes
    - **rank** (`int`): The rank of the server node.
    - **node_name** (`str`): The name of the server node."""
    def __init__(self, rank: int, node_name: str) -> None:

        self.msg_prefix = ctypes.c_int32(MessageType.SERVER.value)
        self.rank = ctypes.c_int32(rank)
        self.node_name = node_name.encode('utf-8')

    def encode(self) -> bytes:
        """Encodes the server node information into a bytes object.

        ### Returns
        - `bytes`: Encoded representation of the server node."""

        return bytes(self.msg_prefix) + bytes(self.rank) + self.node_name

    def recv(self) -> None:
        """Placeholder method for receiving data. Not yet implemented."""


class ConnectionRequest:
    """Represents a connection request with a simulation id and communicator size.

    ### Attributes
    - **simulation_id** (`int`): The id of the simulation making the request.
    - **comm_size** (`int`): The size of the communicator for the connection."""
    def __init__(self, simulation_id: int, comm_size: int) -> None:

        self.simulation_id = simulation_id
        self.comm_size = comm_size

    def encode(self, buff: bytes) -> bytes:
        """Encodes the connection request into a byte buffer.

        ### Parameters
        - **buff** (bytes): The buffer containing data to encode.

        ### Returns
        - `bytes`: The encoded connection request."""

        parts = [
            ctypes.c_int32.from_buffer_copy(buff[:4]),
            ctypes.c_int32.from_buffer_copy(buff[4:])
        ]
        return b''.join(map(bytes, parts))

    @classmethod
    def recv(cls, buff: bytes) -> "ConnectionRequest":
        """Decodes a byte buffer to create a `ConnectionRequest` instance.

        ### Parameters
        - **buff** (bytes): The buffer containing the encoded connection request.

        ### Returns
        - **`ConnectionRequest`**: A new instance with the decoded values."""

        comm_size = ctypes.c_int32.from_buffer_copy(buff[:4])
        simulation_id = ctypes.c_int32.from_buffer_copy(buff[4:])
        return cls(simulation_id.value, comm_size.value)


class ConnectionResponse(ctypes.Structure):
    """
    Represents a response to a connection request,
    including communication details and configuration.

    ### Attributes
    - **comm_size** (`int`): Size of the communicator for the connection.
    - **sobol_op** (`int`): Indicates if Sobol operations are enabled.
    - **learning** (`int`): Specifies the learning configuration.
    - **nb_parameters** (`int`): Number of parameters used in the simulation.
    - **verbose_lvl** (`int`): Verbosity level for debugging or logging.
    - **port_names** (`List[str]`): List of port names padded to the maximum node name length."""
    def __init__(self,
                 comm_size: int,
                 sobol_op: int,
                 learning: int,
                 nb_parameters: int,
                 verbose_lvl: int,
                 port_names: List[str]) -> None:

        self.comm_size = ctypes.c_int32(comm_size)
        self.sobol_op = ctypes.c_int32(sobol_op)
        self.learning = ctypes.c_int32(learning)
        self.nb_parameters = ctypes.c_int32(nb_parameters)
        self.verbose_lvl = ctypes.c_int32(verbose_lvl)

        print('port_names: ', port_names)
        self.port_names = b''.join(
            [
                port.ljust(MELISSA_MAX_NODE_NAME_LENGTH, '\x00').encode('utf-8')
                for port in port_names
            ]
        )

    def encode(self) -> bytes:
        """Encodes the connection response into a byte buffer.

        ### Returns
        - `bytes`: The encoded connection response as a byte string."""

        parts: List[Any] = [
            self.comm_size, self.sobol_op, self.learning,
            self.nb_parameters, self.verbose_lvl, self.port_names
        ]
        return b''.join(map(lambda x: bytes(x), parts))


class JobDetails:
    """Represents details of a job, including its simulation id, job id, and associated parameters.

    ### Attributes
    - **simulation_id** (`int`): id of the simulation associated with the job.
    - **job_id** (`str`): Unique identifier for the job.
    - **parameters** (`Union[List, NDArray]`): Array of parameters associated with the job."""
    def __init__(self,
                 simulation_id: int,
                 job_id: str,
                 parameters: Union[List, NDArray]) -> None:

        self.simulation_id = simulation_id
        self.job_id = job_id
        self.parameters = parameters

    @classmethod
    def from_msg(cls, msg: bytes, nb_parameters: int) -> "JobDetails":
        """Creates a `JobDetails` instance from a received message.

        ### Parameters
        - **msg** (`bytes`): The byte-encoded message containing job details.
        - **nb_parameters** (`int`): Number of parameters expected in the message.

        ### Returns
        - `JobDetails`: An instance populated with details extracted from the message."""

        simulation_id = ctypes.c_int32.from_buffer_copy(msg[4:8])
        job_id = msg[8: (-nb_parameters * 8)].decode('utf-8')

        # Extract parameters from the message.
        params = msg[(-nb_parameters * 8):]
        parameters = np.frombuffer(params, dtype=np.double)

        return cls(simulation_id.value, job_id, parameters)


class Stop:
    """Represents a STOP message to be sent in the communication protocol."""
    def encode(self) -> bytes:
        """Encodes the STOP message.

        ### Returns
        - `bytes`: The encoded STOP message as a byte sequence."""
        return bytes(ctypes.c_int32(MessageType.STOP.value))


class SimulationStatusMessage:
    """Represents a message containing the status of a simulation.

    ### Attributes
    - **simulation_id** (`int`): The id of the simulation.
    - **status** (`Any`): The status of the simulation."""
    def __init__(self, simulation_id: int, status: Any) -> None:

        self.status = ctypes.c_int32(status.value)
        self.simulation_id = ctypes.c_int32(simulation_id)

    def encode(self) -> bytes:
        """Encodes the simulation status message.

        ### Returns
        - `bytes`: The encoded message as a byte sequence."""

        parts = [ctypes.c_int32(MessageType.SIMU_STATUS.value),
                 self.simulation_id,
                 self.status]
        msg = b''.join(map(bytes, parts))
        return msg
