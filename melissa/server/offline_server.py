from typing import Dict, Any

from melissa.server.sensitivity_analysis import SensitivityAnalysisServer


class OfflineServer(SensitivityAnalysisServer):
    """`OfflineServer` class extends the `SensitivityAnalysisServer` but
    activates offline mode to disable reception of data. This is useful when the user
    requires to sample the parameters and generate client scripts to store their data instead
    of sending it to the server."""
    def __init__(self, config_dict: Dict[str, Any]) -> None:

        config_dict["sa_config"] = {
            "checkpoint_interval": 0
        }
        super().__init__(config_dict)
        self.make_study_offline()
