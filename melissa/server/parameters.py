"""This script consists of helper classes for implementing random sampling
strategies for the simulation parameters."""

import random
import copy
from enum import Enum, EnumMeta, unique
from abc import ABC, abstractmethod
from typing import (
    Any, List, Union, TypeVar,
    Type, Optional, Iterable
)

import cloudpickle
import numpy as np
from numpy.typing import NDArray, DTypeLike
from scipy.stats import qmc


ParameterSamplerClass = TypeVar('ParameterSamplerClass', bound='BaseExperiment')


# Dynamic parameter generation (the default approach where no parameter gets stored)
class BaseExperiment(ABC):
    """A base class for both static and dynamic parameter generation strategies for simulations.

    - This class serves as the common parent for different methods of sampling simulation
    parameters.
    The `draw()` method, which is implemented by subclasses, is called from the server when creating
    client scripts to generate parameter values.
    - The `sample()` method samples parameters in their raw form that must be post-processed.

    ### Attributes
    - **nb_params** (`int`): The number of parameters to be generated.
    - **nb_sims** (`int`): The number of simulations to be run.
    - **l_bounds** (`List[Union[float, int]]`): The lower bounds for the parameter values.
    - **u_bounds** (`List[Union[float, int]]`): The upper bounds for the parameter values.
    - **seed** (`int`): The seed value for random number generation to
    ensure reproducibility (Default is `None`).
    - **dtype** (`DTypeLike`): The data type for the generated parameters.
    (Default is `np.float64`)
    - **rng** `Generator`: A NumPy random number generator initialized with either
    the given seed or a random seed."""
    def __init__(self,
                 nb_params: int,
                 nb_sims: int,
                 l_bounds: List[Union[float, int]],
                 u_bounds: List[Union[float, int]],
                 seed: Optional[int] = None,
                 dtype: DTypeLike = np.float64) -> None:

        self.seed = seed
        self.nb_sims = nb_sims
        self.nb_params = nb_params
        self.dtype = dtype
        self.set_seeds()

        assert len(l_bounds) == len(u_bounds), "Lengths of boundary lists must be the same."
        if len(l_bounds) == 1:
            self.l_bounds = np.full((self.nb_params,), l_bounds[0])
            self.u_bounds = np.full((self.nb_params,), u_bounds[0])
        else:
            self.l_bounds = np.array(l_bounds)
            self.u_bounds = np.array(u_bounds)

    def set_seeds(self) -> None:
        """ Sets the random seeds for ensuring reproducibility across experiments.

        This method initializes the random seed for,

        -Python's `random` module
        - NumPy's random number generator
        - Default random number generator (RNG) using NumPy's `default_rng`.

        ### Behavior
        - If `self.seed` is specified, it sets the same seed across `random`,
        NumPy's random module, and `np.random.default_rng` for consistent
        results.
        - If `self.seed` is not specified (`None` or `0`), it initializes
        `self.rng` with a random seed provided by the system."""

        if self.seed:
            random.seed(self.seed)
            np.random.seed(self.seed)
            self.rng = np.random.default_rng(self.seed)
        else:
            self.rng = np.random.default_rng()

    def generator(self) -> Any:
        """Generates the parameters for each simulation.

        ### Returns
        - `Any`: An iterator that yields the generated parameters."""
        for _ in range(self.nb_sims):
            x = self.draw()
            if isinstance(x, np.ndarray):
                x = x.astype(self.dtype)
            yield x

    @abstractmethod
    def draw(self) -> Iterable[Any]:
        """Draws the next set of parameters. This method must call `sample(nb_samples)`
        that returns the parameters in their raw format. But, user can perform
        post-processing here.

        Make sure that you return an iterable and not a single type.
        The returned parameters will be used by server's client script generation phase
        which converts each parameter in this returned iterable into a string to be attached.
        Therefore, the return type must have defined `__str__`.

        ### Returns
        - `List[Any]`: The generated set of parameters."""
        return self.sample(1)

    @abstractmethod
    def sample(self, nb_samples: int) -> Any:
        """Samples the specified number of parameter sets.

        ### Parameters
        - **nb_samples** (`int`): The number of parameter sets to generate.

        ### Returns
        - `NDArray`: The generated set of parameters."""
        raise NotImplementedError("sample method must be defined.")


class StaticExperiment(BaseExperiment):
    """`StaticExperiment` class generates and stores fixed parameters for simulations.
    It overrides the `draw()` method to provide parameters sequentially,
    and stores them in the `parameters`, indexed by simulation id.

    When instantiated it will set `parameters` of shape `(nb_sims, nb_params)`.

    ### Attributes
    - **_current_idx** (`int`): The current index for selecting the next set of parameters.
    - **parameters** (`NDArray`): The pre-generated parameter values stored
    as a NumPy array for each simulation."""
    def __init__(self,
                 nb_params: int,
                 nb_sims: int,
                 l_bounds: List[Union[float, int]],
                 u_bounds: List[Union[float, int]],
                 seed: Optional[int] = None,
                 dtype=np.float64) -> None:

        super().__init__(nb_params, nb_sims, l_bounds, u_bounds, seed, dtype)
        self._current_idx = 0
        self.parameters: NDArray = np.array([])

    def reset_index(self) -> None:
        self._current_idx = 0

    @property
    def current_index(self) -> int:
        return self._current_idx

    @current_index.setter
    def current_index(self, val: int):
        self._current_idx = val

    def get_current_parameters(self) -> NDArray:
        return self.parameters

    def draw(self) -> NDArray:
        """Draws the next set of static parameters.

        ### Returns
        - `NDArray`: The next set of parameters from the pre-generated list."""

        if len(self.parameters) == 0:
            self.set_parameters(self.sample(self.nb_sims))

        params = self.parameters[self._current_idx]
        self._current_idx += 1
        return params

    def set_parameters(self, parameters: Union[List, NDArray]):
        """Sets the static parameters.

        ### Parameters
        - **parameters** (`Union[List, NDArray]`): A list or array of parameters to set.

        ### Raises
        - `ValueError`: If the parameters list is empty."""

        if len(parameters) > 0:
            self.parameters = np.array(parameters, dtype=np.float32)
        else:
            raise ValueError("Invalid parameters.")

    def checkpoint_state(self) -> None:
        """Saves the current state for checkpointing."""

        self.checkpoint_data.update({
            "parameters": self.parameters.tolist(),
            "current_idx": self._current_idx
        })

        with open("checkpoints/active_sampling.pkl", "wb") as f:
            cloudpickle.dump(self.checkpoint_data, f)

    def restart_from_checkpoint(self) -> None:
        """Restores the state from a checkpoint."""

        with open("checkpoints/active_sampling.pkl", "rb") as f:
            self.checkpoint_data = cloudpickle.load(f)

        self._current_idx = self.checkpoint_data["current_idx"]
        self.set_parameters(self.checkpoint_data["parameters"])


class PickFreezeMixIn:
    """Mixin class for implementing pick-freeze sampling method.

    This method is useful for generating parameter sets for multiple clients
    (or a group of clients), often used in Sobol groups for sensitivity analysis.
    The class provides functionality to apply the pick-freeze technique,
    which combines two sets of samples by freezing one parameter from one set
    while changing others based on the second set.

    ### Attributes
    - **nb_params** (`int`): The number of parameters in the sample.
    - **apply_pick_freeze** (`bool`): A flag to enable or disable the pick-freeze method.
    - **second_order** (`bool`): A flag to indicate if second-order interactions."""
    nb_params: int

    def __init__(self,
                 apply_pick_freeze: bool = False,
                 second_order: bool = False) -> None:

        self.apply_pick_freeze = apply_pick_freeze
        self.second_order = second_order

    def pick_freeze(self, sample_a: NDArray, sample_b: NDArray) -> NDArray:
        """Applies the pick-freeze method to construct the set of input parameters.

        This method generates a new set of samples by combining two input samples
        (sample_a and sample_b). It freezes one parameter from each sample and
        then combines them with the others.

        ### Parameters
        - **sample_a** (`NDArray`): The first set of sample parameters.
        - **sample_b** (`NDArray`): The second set of sample parameters.

        ### Returns
        - `NDArray`: A new set of generated samples using the pick-freeze method."""

        sample = np.vstack((sample_a, sample_b))
        for k in range(self.nb_params):
            sample_ek = copy.deepcopy(sample_a)
            sample_ek[k] = sample_b[k]
            sample = np.vstack((sample, sample_ek))

        if self.second_order:
            for k in range(self.nb_params):
                sample_ck = copy.deepcopy(sample_b)
                sample_ck[k] = sample_a[k]
                sample = np.vstack((sample, sample_ck))
        return sample

    def __pf_draw(self) -> NDArray:
        """Generates dynamic parameters for the current simulation using the pick-freeze method.

        If the pick-freeze method is enabled, two samples are generated and
        combined using the pick-freeze technique.
        Otherwise, a single sample is drawn.

        ### Returns
        - `NDArray`: The generated parameter set for the current simulation."""

        nb_samples = 1
        sample_a = self.sample(nb_samples)
        if self.apply_pick_freeze:
            sample_b = self.sample(nb_samples)
            return self.pick_freeze(sample_a, sample_b)
        return sample_a

    def draw(self) -> Any:
        """Draws the next set of parameters, applying the pick-freeze method if enabled.

        If the pick-freeze method is enabled, it calls the `__pf_draw`
        method to generate the parameter set. Otherwise, it uses the base
        class method to generate parameters.

        ### Returns
        - `NDArray`: The drawn parameter set."""

        if self.apply_pick_freeze:
            return self.__pf_draw()
        return super().draw()  # type: ignore

    @abstractmethod
    def sample(self, nb_samples: int) -> NDArray:
        """Abstract method to sample parameters.

        ### Parameters
        - **nb_samples** (`int`): The number of samples to generate.

        ### Returns
        - `NDArray`: The drawn samples as a numpy array."""
        raise NotImplementedError("sample method must be defined.")


class SobolBaseExperiment(PickFreezeMixIn, BaseExperiment):
    """A Base class with pick-freeze methods and dynamic parameters."""
    def __init__(self,
                 nb_params: int,
                 nb_sims: int,
                 l_bounds: List[Union[float, int]],
                 u_bounds: List[Union[float, int]],
                 seed: Optional[int] = None,
                 dtype=np.float64,
                 apply_pick_freeze: bool = False,
                 second_order: bool = False) -> None:

        BaseExperiment.__init__(self, nb_params, nb_sims, l_bounds, u_bounds, seed, dtype)
        PickFreezeMixIn.__init__(self, apply_pick_freeze, second_order)


class RandomUniformSamplerMixIn:
    """This class defines `sample()` method for random sampling with numpy."""
    nb_params: int
    l_bounds: NDArray
    u_bounds: NDArray

    def sample(self, nb_samples: int) -> NDArray:
        """Generates random samples uniformly distributed between specified bounds.

        ### Parameters
        - **nb_samples** (`int`): The number of samples to generate.

        ### Returns
        - `NDArray`: A numpy array of shape `(nb_samples, nb_params)`
        containing the sampled parameters."""

        return np.random.uniform(
            self.l_bounds, self.u_bounds,
            size=(nb_samples, self.nb_params)
        ).squeeze()


class RandomUniform(RandomUniformSamplerMixIn, SobolBaseExperiment):
    """Random uniform sampling experiment that integrates Sobol and uniform sampling methods."""
    def __init__(self,
                 nb_params: int,
                 nb_sims: int,
                 l_bounds: List[Union[float, int]],
                 u_bounds: List[Union[float, int]],
                 seed: Optional[int] = None,
                 dtype=np.float64,
                 apply_pick_freeze: bool = False,
                 second_order: bool = False) -> None:

        SobolBaseExperiment.__init__(self, nb_params, nb_sims, l_bounds, u_bounds, seed, dtype,
                                     apply_pick_freeze, second_order)
        RandomUniformSamplerMixIn.__init__(self)


class RandomUniformStatic(RandomUniformSamplerMixIn, StaticExperiment):
    """Random uniform sampling experiment that integrates Static and uniform sampling methods.
    This class generates and stores uniformly sampled parameters - it is needed
    for reproducibile experiments when the random seed is set."""
    def __init__(self,
                 nb_params: int,
                 nb_sims: int,
                 l_bounds: List[Union[float, int]],
                 u_bounds: List[Union[float, int]],
                 seed: Optional[int] = None,
                 dtype=np.float64) -> None:

        RandomUniformSamplerMixIn.__init__(self)
        StaticExperiment.__init__(self, nb_params, nb_sims, l_bounds, u_bounds, seed, dtype)


class QMCSamplerMixIn:
    """Mixin class for samplers based on `scipy.stats.qmc` methods,
    such as Halton and Latin Hypercube.

    This class defines the `sample()` method specifically for generating samples
    using quasi-Monte Carlo methods, including Halton and Latin Hypercube samplers"""
    l_bounds: NDArray
    u_bounds: NDArray

    def __init__(self, sampler: Union[qmc.Halton, qmc.LatinHypercube]) -> None:
        self._sampler = sampler

    def sample(self, nb_samples: int) -> NDArray:
        """Generates scaled samples using the chosen quasi-Monte Carlo sampler.

        ### Returns
        - `NDArray`: A numpy array of shape `(nb_samples, nb_params)`
        containing the sampled parameters."""
        return qmc.scale(
            self._sampler.random(nb_samples),
            self.l_bounds, self.u_bounds
        ).squeeze()


class HaltonSamplerMixIn(QMCSamplerMixIn):
    """MixIn class for `scipy.qmc.Halton` sampler."""
    nb_params: int
    seed: Optional[int]

    def __init__(self) -> None:
        QMCSamplerMixIn.__init__(
            self,
            qmc.Halton(d=self.nb_params, scramble=True, seed=self.seed)
        )


class LatinHypercubeSamplerMixIn(QMCSamplerMixIn):
    """MixIn class for `scipy.qmc.LatinHypercube` sampler."""
    nb_params: int
    seed: Optional[int]

    def __init__(self):
        QMCSamplerMixIn.__init__(
            self,
            qmc.LatinHypercube(d=self.nb_params, scramble=False, seed=self.seed)
        )


class HaltonGenerator(HaltonSamplerMixIn, SobolBaseExperiment):
    """Deterministic sample generator based on scipy Halton sequence
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.qmc.Halton.html"""
    def __init__(self,
                 nb_params: int,
                 nb_sims: int,
                 l_bounds: List[Union[float, int]],
                 u_bounds: List[Union[float, int]],
                 seed: Optional[int] = None,
                 dtype=np.float64,
                 apply_pick_freeze: bool = False,
                 second_order: bool = False) -> None:

        SobolBaseExperiment.__init__(self, nb_params, nb_sims, l_bounds, u_bounds, seed, dtype,
                                     apply_pick_freeze, second_order)
        HaltonSamplerMixIn.__init__(self)


class LHSGenerator(LatinHypercubeSamplerMixIn, SobolBaseExperiment):
    """Non-deterministic sample generator based on scipy LHS sampling
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.qmc.LatinHypercube.html"""
    def __init__(self,
                 nb_params: int,
                 nb_sims: int,
                 l_bounds: List[Union[float, int]],
                 u_bounds: List[Union[float, int]],
                 seed: Optional[int] = None,
                 dtype=np.float64,
                 apply_pick_freeze: bool = False,
                 second_order: bool = False) -> None:

        SobolBaseExperiment.__init__(self, nb_params, nb_sims, l_bounds, u_bounds, seed, dtype,
                                     apply_pick_freeze, second_order)
        LatinHypercubeSamplerMixIn.__init__(self)


class MyGetItem(EnumMeta):
    def __getitem__(self, name):
        try:
            return super().__getitem__(name.upper().strip())
        except KeyError as e:
            if (
                "random" in name.lower().strip()
                or "uniform" in name.lower().strip()
            ):
                return super().__getitem__("RANDOM_UNIFORM")
            raise e


@unique
class ParameterSamplerType(Enum, metaclass=MyGetItem):
    """Enum to choose the paramter sampler type.

    - RANDOM_UNIFORM = 0
    - HALTON = 1
    - LHS = 2
    - DEFAULT_BREED = 3"""
    RANDOM_UNIFORM = 0
    HALTON = 1
    LHS = 2
    DEFAULT_BREED = 3


def make_parameter_sampler(sampler_t: Union[ParameterSamplerType, Type[ParameterSamplerClass]],
                           **kwargs) -> BaseExperiment:
    """Creates and returns an instance of a parameter sampler based on the specified sampler type.

    This function supports both predefined sampler types from `ParameterSamplerType` and custom
    sampler classes passed directly. It instantiates the appropriate sampler class with the
    provided keyword arguments.

    ### Parameters:
    - **sampler_t** (`Union[ParameterSamplerType, Type[ParameterSamplerClass]]`):
        - `ParameterSamplerType`: An enum value from `ParameterSamplerType`.
        (`RANDOM_UNIFORM`, `HALTON`, `LHS`)
        - `Type[ParameterSamplerClass]`: A predefined or a custom class type to be instantiated
        (Not an object).
    - **kwargs**: Additional keyword arguments passed to the sampler class constructor.

    ### Returns:
    - `BaseExperiment`: An instance of the chosen parameter sampler.

    ### Raises:
    - `ValueError`: If `sampler_t` is not one of the supported enum values or
    a valid subclass of `BaseExperiement`."""

    if isinstance(sampler_t, ParameterSamplerType):
        if sampler_t is ParameterSamplerType.RANDOM_UNIFORM:
            return RandomUniform(**kwargs)
        elif sampler_t is ParameterSamplerType.HALTON:
            return HaltonGenerator(**kwargs)
        elif sampler_t is ParameterSamplerType.LHS:
            return LHSGenerator(**kwargs)
        else:
            raise ValueError("ParameterSamplerType must be one of "
                             "`RANDOM_UNIFORM`, `HALTON`, or `LHS`.")
    elif isinstance(sampler_t, type) and issubclass(sampler_t, BaseExperiment):
        return sampler_t(**kwargs)
    else:
        raise ValueError("Invalid sampler type. Must be a ParameterSamplerType enum "
                         "or a custom sampler class.")
