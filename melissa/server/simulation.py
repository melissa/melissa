"""This script defines classes associated with simulations, groups, and their metadata."""

import logging
import struct
import time
from dataclasses import dataclass
from enum import Enum
from typing import Any, Dict, List, Optional, Union, Tuple

import numpy as np
from numpy.typing import NDArray, ArrayLike

logger = logging.getLogger(__name__)


@dataclass
class SimulationData:
    """Stores data related to a specific simulation.

    ### Attributes
    - **simulation_id** (`int`): The id of the simulation.
    - **time_step** (`int`): The time step for the simulation.
    - **data** (`Dict[str, Any]`): The data associated with the simulation per field.
    - **parameters** (`list`): The list of parameters for the simulation."""

    simulation_id: int
    time_step: int
    data: Dict[str, Any]
    parameters: List

    def __repr__(self) -> str:
        """Returns a string representation of the `SimulationData` object."""
        s = (
            f"<{self.__class__.__name__} "
            f"sim-id={self.simulation_id} "
            f"time-step={self.time_step}>"
        )
        return s


class PartialSimulationData:
    """Stores partial data for a specific simulation,
    including information on time step, client rank, and field.

    ### Attributes
    - **time_step** (`int`): The time step for the simulation.
    - **simulation_id** (`int`): The id of the simulation.
    - **client_rank** (`int`): The rank of the client submitting the data.
    - **data_size** (`int`): The size of the data associated with the simulation.
    - **field** (`str`): The field to which the data belongs.
    - **data** (`Any`): The actual data associated with the simulation."""

    MAX_FIELD_NAME_SIZE = 128

    def __init__(
        self,
        time_step: int,
        simulation_id: int,
        client_rank: int,
        data_size: int,
        field: str,
        data: Any,
    ) -> None:

        self.time_step = time_step
        self.simulation_id = simulation_id
        self.client_rank = client_rank
        self.data_size = data_size
        self.field = field
        self.data = data

    @classmethod
    def from_msg(cls,
                 msg: bytes,
                 learning: int) -> "PartialSimulationData":

        """Class method to deserialize a message and create an instance of the class.

        ### Parameters
        - **msg** (`bytes`): The serialized message in bytes format to be deserialized.
        - **learning** (`int`): A flag or parameter used during the deserialization process.

        ### Returns
        - `PartialSimulationData`: A new instance of the class created from the
        deserialized message."""

        time_step: int
        simulation_id: int
        client_rank: int
        data_size: int
        field: bytearray

        # unpack metadata
        size_metadata: int = 4 * 4 + cls.MAX_FIELD_NAME_SIZE
        time_step, simulation_id, client_rank, data_size, field = struct.unpack(
            f"4i{cls.MAX_FIELD_NAME_SIZE}s", msg[: size_metadata]
        )
        field_name: str = field.split(b"\x00")[0].decode("utf-8")

        # unpack data array (float32 for DL and float64 for SA)
        dtype = np.dtype(np.float32 if learning > 0 else np.float64)
        data: ArrayLike = np.frombuffer(
            msg,
            offset=size_metadata,
            dtype=dtype
        )

        return cls(time_step, simulation_id, client_rank, data_size, field_name, data)

    def __repr__(self) -> str:
        """Returns a string representation of the `PartialSimulationData` object."""
        return (
            f"<{self.__class__.__name__}: sim-id={self.simulation_id}, "
            f"time-step={self.time_step}, field=\"{self.field}\""
        )


class SimulationDataStatus(Enum):
    """Enum class representing the possible statuses of simulation data."""
    PARTIAL = 0
    COMPLETE = 1
    ALREADY_RECEIVED = 2
    EMPTY = 3


class SimulationStatus(Enum):
    """Enum class representing the possible statuses of a simulation's state."""
    CONNECTED = 0
    RUNNING = 1
    FINISHED = 2


class Simulation:
    """Represents a single simulation with associated metadata. Each object corresponds
    to a unique simulation or client and contains information necessary for tracking
    the simulation's state, parameters, and fault tolerance.

    ### Attributes
    - **id_** (`int`): The unique identifier for the simulation.
    - **nb_time_steps** (`int`): The total number of time steps for the simulation.
    - **fields** (`List[str]`): A list of fields that will be sent.
    - **parameters** (`List[Any]`): A list of parameters used in the simulation."""
    def __init__(self,
                 id_: int,
                 nb_time_steps: int,
                 fields: List[str],
                 parameters: List[Any]) -> None:

        self.id = id_
        self.nb_time_steps = nb_time_steps
        self.fields = fields
        self.received_simulation_data: Dict[
            int, Dict[int, Dict[str, Optional[Union[int, PartialSimulationData]]]]
        ] = {}  # client_rank, time_step, field, int (SA) or PartialSimulationData (DL)
        self.received_time_steps: Dict[int, NDArray[np.bool_]] = {}
        self.parameters: List[Any] = parameters
        self.nb_received_time_steps: int = 0
        self.nb_failures: int = 0
        self.last_message: Optional[float] = None
        self.duration: float = -1.
        self.t0: float = -1.

    def init_structures(self, client_rank: int, time_steps_known: bool = True) -> None:
        """Initializes data structures to track received simulation data for a given client rank.

        ### Parameters
        - **client_rank** (`int`): The rank of the client whose data is being tracked.
        - **time_steps_known** (`bool`, optional): if the total number of time steps
        is known in advance. Default is `True`."""

        # ensure client rank is initialized
        if client_rank not in self.received_simulation_data:
            self.received_simulation_data[client_rank] = {}
            # Known sample count
            shape = (len(self.fields), self.nb_time_steps if time_steps_known else 1)
            self.received_time_steps[client_rank] = np.zeros(
                shape=shape,
                dtype=bool
            )

        if self.t0 < 0:
            self.t0 = time.time()

    def init_data_storage(self, client_rank: int, time_step: int) -> None:
        """Prepares storage for tracking field-level data
        for a specific time step of a client rank.

        ### Parameters
        - **client_rank** (`int`): The rank of the client whose data is being tracked.
        - **time_step** (`int`): The time step for which data storage is initialized."""

        if time_step not in self.received_simulation_data[client_rank]:
            self.received_simulation_data[client_rank][time_step] = {
                f: None for f in self.fields
            }

    def time_step_expansion(self, client_rank: int, time_step: int) -> None:
        """Dynamically expands the `received_time_steps` matrix for a client rank
        if the time step exceeds current capacity.

        ### Parameters
        - **client_rank** (`int`): The rank of the client whose matrix needs expansion.
        - **time_step** (`int`): The time step that triggered the need for expansion."""

        if time_step > self.received_time_steps[client_rank].shape[1] - 1:
            self.received_time_steps[client_rank] = np.concatenate(
                [
                    self.received_time_steps[client_rank],
                    np.zeros((len(self.fields), 1), dtype=bool),
                ],
                axis=1,
            )

    def update(self,
               client_rank: int,
               time_step: int,
               field: str,
               data: Optional[Union[int, PartialSimulationData]]) -> None:
        """Updates the data associated with a specific field and time step for a client rank.

        ### Parameters
        - **client_rank** (`int`): The rank of the client whose data is being updated.
        - **time_step** (`int`): The time step associated with the data.
        - **field** (`str`): The specific field being updated.
        - **data** (`Optional[Union[int, PartialSimulationData]]`):
        The new data to store for the field."""
        self.received_simulation_data[client_rank][time_step][field] = data

    def get_data(self,
                 client_rank: int,
                 time_step: int) -> Dict[str, Optional[Union[int, PartialSimulationData]]]:
        """Retrieves the data for all fields associated with a specific time step of a client rank.

        ### Parameters
        - **client_rank** (`int`): The rank of the client whose data is being retrieved.
        - **time_step** (`int`): The time step for which data is being fetched.

        ### Returns
        - `Dict[str, Optional[Union[int, PartialSimulationData]]]`:
        A dictionary containing field names as keys and their corresponding data as values."""
        return self.received_simulation_data[client_rank][time_step]

    def clear_data(self,
                   client_rank: int,
                   time_step: int) -> None:
        """Clears the data for all fields associated with a specific time step of a client rank.
        Useful when the complete data is given to the server for post-processing and no longer needs
        to be in this object. This is to avoid duplications in checkpointing.

        ### Parameters
        - **client_rank** (`int`): The rank of the client whose data is being cleaned.
        - **time_step** (`int`): The time step for which data is being cleaned."""

        ref = self.received_simulation_data[client_rank][time_step]
        for field in ref.keys():
            ref[field] = None

    # TODO: remove
    def crashed(self) -> bool:
        """Unused."""
        return False

    def has_already_received(self,
                             client_rank: int,
                             time_step: int,
                             field: str) -> bool:
        """Checks if the given time step has already been received
        for the specified client and field, helping to avoid duplication of data.

        ### Parameters
        - **client_rank** (`int`): The rank of the client whose data is being retrieved.
        - **time_step** (`int`): The time step for which the data is being checked.
        - **field** (`str`): The field associated with the data being checked.

        ### Returns
        - `bool`: if the data has already been received."""

        field_idx = self.fields.index(field)
        return self.received_time_steps[client_rank][field_idx, time_step]

    def is_complete(self, time_step: int) -> bool:
        """Checks if the given time step has data for all the defined fields.

        ### Parameters
        - **time_step** (`int`): The time step to check for completeness.

        ### Returns
        - `bool`: if data has been received for all defined fields."""

        for _, client_data in self.received_simulation_data.items():
            if (
                time_step not in client_data
                or len(
                    [field for field in client_data[time_step].values() if field is not None]
                ) != len(self.fields)
            ):
                return False
        return True

    def mark_as_received(self,
                         client_rank: int,
                         time_step: int,
                         field: str) -> None:
        """Marks the given time step as received for the specified field.

        ### Parameters
        - **client_rank** (`int`): The rank of the client sending the data.
        - **time_step** (`int`): The time step for which the data is received.
        - **field** (`str`): The field associated with the data being marked as received."""

        field_idx = self.fields.index(field)
        self.received_time_steps[client_rank][field_idx, time_step] = True
        self.last_message = time.time()

    def finished(self) -> bool:
        """Checks whether all time steps for the simulation have been received.

        ### Returns
        - `bool`: `True` if all time steps have been received."""

        has_finished = self.nb_received_time_steps == self.nb_time_steps

        if has_finished:
            self.duration = time.time() - self.t0

        return has_finished


class Group:
    """Represents a Sobol group with all the relevant simulations.

    This class maintains a caching mechanism for storing corresponding
    time steps received for the current group.

    ### Attributes
    - **group_id** (`int`): The id of the group.
    - **sobol_** (`bool`): A flag indicating whether the group uses Sobol cache.
    (default is False)."""
    def __init__(self,
                 group_id: int,
                 sobol_: bool = False) -> None:

        self.group_id = group_id
        self.simulations: Dict[int, Simulation] = {}
        self.nb_failures: int = 0
        self.sobol_on: bool = sobol_
        self.sobol_cache: Dict[Tuple[str, int, int], Dict[int, np.ndarray]] = {}
        if self.sobol_on:
            self.sobol_cache = {}

    def __len__(self) -> int:
        """Returns length of the current group."""
        return len(self.simulations)

    def cache(self, pdata: PartialSimulationData) -> None:
        """Caches the received simulation data for the
        specified field, simulation id, client rank, and time step.

        ### Parameters
        - **pdata** (`PartialSimulationData`): The simulation data to be cached."""

        key = (pdata.field, pdata.client_rank, pdata.time_step)
        if key not in self.sobol_cache:
            self.sobol_cache[key] = {}
        self.sobol_cache[key][pdata.simulation_id] = pdata.data

    def get_cached(self,
                   field: str,
                   client_rank: int,
                   time_step: int) -> NDArray[np.float64]:
        """Retrieves cached group data for the
        specified field, client rank, and time step.

        ### Parameters
        - **field** (`str`): The field associated with the simulation data.
        - **client_rank** (`int`): The rank of the client whose data is being retrieved.
        - **time_step** (`int`): The time step of the simulation.

        ### Returns
        - `NDArray[np.float64]`: The cached data sorted
        by simulation ids."""

        key = (field, client_rank, time_step)
        if (
            key not in self.sobol_cache
            and len(self.sobol_cache[key]) != len(self)
        ):
            return np.array([])

        return np.array([
            self.sobol_cache[key][sim_id]
            for sim_id in sorted(self.sobol_cache[key].keys())
        ])
