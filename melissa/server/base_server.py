"""This script defines the parent class for melissa server."""

from datetime import timedelta
import logging
import os
import socket
import threading
import datetime
import time
from abc import ABC, abstractmethod
from enum import Enum
from pathlib import Path
from typing import (
    Any, Dict, Optional, Set,
    Tuple, Union, List, Type
)

import psutil
import numpy as np
from numpy.typing import DTypeLike
import zmq
from mpi4py import MPI
from mpi4py.util.dtlib import to_numpy_dtype
import cloudpickle
import rapidjson
try:
    import debugpy
except ModuleNotFoundError:
    pass


from iterative_stats.iterative_moments import IterativeMoments
from melissa.scheduler.job import State
from melissa.launcher import config, message
from melissa.scheduler import job
from melissa.server.fault_tolerance import FaultTolerance
from melissa.server.parameters import (
    BaseExperiment, StaticExperiment, ParameterSamplerType,
    ParameterSamplerClass, make_parameter_sampler
)
from melissa.utility.message import Message
from melissa.server.message import ConnectionRequest, ConnectionResponse
from melissa.server.simulation import (
    Group, PartialSimulationData,
    Simulation, SimulationData,
    SimulationDataStatus
)
from melissa.utility.networking import (
    LengthPrefixFramingDecoder,
    LengthPrefixFramingEncoder,
    connect_to_launcher,
    select_protocol,
    is_port_in_use,
    UnsupportedProtocol
)
from melissa.utility.timer import Timer


logger = logging.getLogger(__name__)


MPI2NP_DT: Dict[str, DTypeLike] = {
    "int": to_numpy_dtype(MPI.INT),
    "float": to_numpy_dtype(MPI.FLOAT),
    "double": to_numpy_dtype(MPI.DOUBLE),
}


class ServerStatus(Enum):
    """Server status enum."""
    CHECKPOINT = 1
    TIMEOUT = 2


class UnsupportedConfiguration(Exception):
    """Errors from the configuration file."""
    def __init__(self, msg) -> None:
        self.msg = msg

    def __str__(self) -> str:
        return f"Unsupported Configuration: {self.msg}"


class ReceptionError(Exception):
    """Errors from the reception loop."""
    def __init__(self, msg) -> None:
        self.msg = msg

    def __str__(self) -> str:
        return f"Training Error: {self.msg}"


def bytes_to_readable(total_bytes: int) -> str:
    """Returns human-readable byte representation."""
    final_value = float(total_bytes)
    unit = "bytes"
    if total_bytes >= 1024:
        final_value /= 1024
        unit = "KB"
    if total_bytes >= pow(1024, 2):
        final_value /= 1024
        unit = "MB"
    if total_bytes >= pow(1024, 3):
        final_value /= 1024
        unit = "GB"
    if unit == "bytes":
        return f"{int(final_value)} {unit}"
    return f"{final_value:.3f} {unit}"


class BaseServer(ABC):
    """`BaseServer` class that handles the following tasks:

    - Manages connections with the launcher and clients.
    - Generates client scripts for simulations.
    - Encodes and decodes messages between the server and clients.
    - Provides basic checkpointing functionality to save and restore states.

    ### Parameters
    - **config_dict** (`Dict[str, Any]`): A dictionary containing configuration settings for
    initializing the server.
    - **checkpoint_file** (`str`, optional): The filename for the checkpoint file
    (default is `"checkpoint.pkl"`). This file is used for saving and restoring the server's state.
    - **restart** (`bool`, optional): A flag indicating whether to restart from a checkpoint
    (default is `False`).

    ### Attributes
    - **comm** (`MPI.Intracomm`): The MPI communicator for inter-process communication.
    - **rank** (`int`): The rank of the current process in the MPI communicator.
    - **comm_size** (`int`): The total number of server processes in the MPI communicator.
    - **client_comm_size** (`int`): The total number of client processes.
    - **server_processes** (`int`): Synonym for `comm_size`.
    - **connection_port** (`int`): The server port to establish request-response connection with
    the clients.
    - **data_puller_port** (`int`): The server port to establish data pulling with the clients.

    - **_offline_mode** (`bool`): Internal flag indicating offline mode where no sending operation
    takes place. Useful when running multiple clients to produce datasets.
    Call `self.make_study_offline` to enable.
    - **_learning** (`int`): Internal flag indicating the learning state (initially 0).
    - **__t0** (`float`): The timestamp marking the initialization time of the object.
    - **__launcher_job_submissions** (`Set[Tuple[int, State]]`): A set to manage job submissions
    for the launcher.

    - **_catch_error** (`bool`): Flag indicating whether to catch configuration-related errors.
    - **_restart** (`int`): Flag indicating if the system is in a restart state; initialized
    from the `MELISSA_RESTART` environment variable.
    - **_consistency_lock** (`threading.RLock`): Reentrant lock to ensure thread-safe operations
    on shared resources.
    - **_is_receiving** (`bool`): Flag indicating whether data reception is ongoing.
    - **_is_online** (`bool`): Flag indicating if the system is in an online operational mode.
    - **_sobol_op** (`bool`): Flag indicating whether Sobol operations are being performed.
    - **_total_bytes_recv** (`int`): Tracks the total number of bytes received over the network.
    - **_active_sim_ids** (`set`): Set of active simulation ids currently being managed.
    - **__max_sim_id** (`int`): Tracks the maximum simulation id observed so far.

    - **_groups** (`Dict[int, Group]`): Dictionary mapping group ids to `Group` objects.
    - **_parameter_sampler** (`Optional[BaseExperiment]`): Sampler for generating parameter values
    for simulations.
    - **__parameter_generator** (`Any`): Internal generator object for producing parameters.

    - **verbose_level** (`int`): Determines the verbosity level for logging and debugging output.
    - **config_dict** (`Dict[str, Any`]): Configuration dictionary provided during initialization.
    - **checkpoint_file** (`str`): File name used for storing checkpoint data.

    - **crashes_before_redraw** (`int`): Number of simulation crashes allowed before
    redrawing parameters.
    - **max_delay** (`Union[int, float]`): Maximum allowed delay for simulations, in seconds.
    - **rm_script** (`bool`): Indicates whether client scripts should be removed after execution.
    - **group_size** (`int`): Number of simulations grouped together for batch processing.
    - **zmq_hwm** (`int`): High-water mark for ZeroMQ communication.

    - **fields** (`List[str]`): List of field names used in the study.
    - **nb_parameters** (`int`): Number of parameters in the parameter sweep study.
    - **nb_time_steps** (`int`): Number of time steps in each simulation.
    - **nb_clients** (`int`): Total number of clients participating in the parameter sweep study.

    - **nb_groups** (`int`): Total number of groups, derived from the number of clients
    and group size.
    - **nb_submitted_simulations** (`int`): Tracks the number of simulations submitted so far.
    - **nb_finished_simulations** (`int`): Tracks the number of completed simulations.
    - **nb_connected_simulations** (`int`): Tracks the number of currently connected simulations.
    - **mtt_simulation_completion** (`float`): Iteratively keeps track of mean of simulation
    durations.

    - **no_fault_tolerance** (`bool`): Indicates whether fault tolerance is disabled,
    based on the `MELISSA_FAULT_TOLERANCE` environment variable.
    - **__ft** (`FaultTolerance`): Fault tolerance object managing simulation
    crashes and retries."""
    def __init__(self,
                 config_dict: Dict[str, Any],
                 comm: MPI.Comm = MPI.COMM_WORLD,
                 checkpoint_file: str = "checkpoint.pkl",
                 restart: bool = False) -> None:

        # MPI initialization
        self.comm: MPI.Comm = comm
        self.rank: int = self.comm.Get_rank()
        self.comm_size: int = self.comm.Get_size()
        self.server_processes: int = self.comm_size
        self.client_comm_size: int = 0
        self.__connection_port: int = 2003
        self.__data_puller_port: int = 5000

        self._offline_mode: bool = False
        self._learning: int = 0
        self.__t0: float = time.time()
        self.__launcher_job_submissions: Set[Tuple[int, State]] = set()

        self._catch_error: bool = False
        self._restart: int = int(os.environ["MELISSA_RESTART"])
        self._consistency_lock: threading.RLock = threading.RLock()
        self._is_receiving: bool = False
        self._is_online: bool = False
        self._sobol_op: bool = False
        self._total_bytes_recv: int = 0
        self._active_sim_ids: set = set()
        self.__max_sim_id: int = -1
        self._groups: Dict[int, Group] = {}
        self._parameter_sampler: Optional[BaseExperiment] = None
        self.__parameter_generator: Any = None

        self.verbose_level: int = 0
        self.config_dict: Dict[str, Any] = config_dict
        self.checkpoint_file: str = checkpoint_file
        study_options: Dict[str, Any] = self.config_dict["study_options"]

        # Scan study options dictionary
        self.crashes_before_redraw: int = study_options.get("crashes_before_redraw", 1)
        self.max_delay: Union[int, float] = study_options.get("simulation_timeout", 60)
        self.rm_script: bool = study_options.get("remove_client_scripts", False)
        self.group_size: int = study_options.get("group_size", 1)
        self.zmq_hwm: int = study_options.get("zmq_hwm", 0)
        try:
            self.fields: List[str] = study_options["field_names"]
            self.nb_parameters: int = study_options["nb_parameters"]
            self.nb_time_steps: int = study_options["nb_time_steps"]
            self.nb_clients: int = study_options["parameter_sweep_size"]
        except KeyError as e:
            logger.error(f"[INCORRECT] Key not found in the configuration: {e}")
            self._catch_error = True

        self.nb_groups: int = self.nb_clients // self.group_size
        self.nb_submitted_simulations: int = 0
        self.nb_finished_simulations: int = 0
        self.nb_connected_simulations: int = 0
        self.mtt_simulation_completion: IterativeMoments = IterativeMoments(max_order=1, dim=1)

        # Fault-Tolerance initialization
        self.no_fault_tolerance: bool = os.environ["MELISSA_FAULT_TOLERANCE"] == "OFF"

        self.ignore_client_death: bool = config_dict.get("ignore_client_death", False)
        if self.ignore_client_death:
            assert self.no_fault_tolerance, \
                "Client deaths cannot be ignored if Fault-tolerance is ON"
            is_direct_scheduler: bool = config_dict["launcher_config"]["scheduler"] == "openmpi"
            if not is_direct_scheduler:
                assert self.comm_size == 1, \
                    "Client deaths cannot be ignored for indirect schedulers "
                "since the launcher only reports job updates to server rank 0. "
                "Enabling this feature would require additional synchronization across "
                "all server ranks, which is not currently implemented."

        logger.info("fault-tolerance " + ("OFF" if self.no_fault_tolerance else "ON"))
        self.__ft: FaultTolerance = FaultTolerance(
            self.no_fault_tolerance,
            self.max_delay,
            self.crashes_before_redraw,
            self.nb_groups,
        )

        os.makedirs(f"checkpoints/{self.rank}", exist_ok=True)
        if restart:
            # TODO implement generic restart/checkpoint
            raise NotImplementedError("[ERROR] this needs to be implemented")

    @property
    def offline_mode(self) -> bool:
        return self._offline_mode

    def make_study_offline(self) -> None:
        self._offline_mode = True
        logger.warning(
            f"Rank {self.rank}>> Currently running with offline mode. "
            "No reception will take place."
        )

    @property
    def time_steps_known(self) -> bool:
        """Time steps are known prior study or not."""
        return True

    @property
    def learning(self) -> int:
        """Deep learning activated?
        Required when establishing a connection with clients."""
        return self._learning

    @property
    def consistency_lock(self) -> threading.RLock:
        """Useful for active sampling."""
        return self._consistency_lock

    @property
    def is_receiving(self) -> bool:
        return self._is_receiving

    @is_receiving.setter
    def is_receiving(self, value: bool):
        self._is_receiving = value

    @property
    def is_online(self) -> bool:
        return self._is_online

    @is_online.setter
    def is_online(self, value: bool):
        self._is_online = value

    @property
    def sobol_op(self) -> bool:
        return self._sobol_op

    @sobol_op.setter
    def sobol_op(self, value: bool):
        self._sobol_op = value

    @property
    def parameter_sampler(self) -> Optional[BaseExperiment]:
        return self._parameter_sampler

    @parameter_sampler.setter
    def parameter_sampler(self, value: Optional[BaseExperiment]):
        self._parameter_sampler = value

    def _save_base_state(self) -> None:
        """Checkpoints all common attributes in the server class to preserve the current state."""

        if self.no_fault_tolerance:
            return

        # make a directory for the checkpoint files if one does not exist
        if not os.path.exists("checkpoints"):
            os.makedirs("checkpoints", exist_ok=True)

        # save some state metadata to be reloaded later
        with self.consistency_lock:
            metadata = {
                "nb_groups": self.nb_groups,
                "nb_submitted_simulations": self.nb_submitted_simulations,
                "nb_finished_simulations": self.nb_finished_simulations,
                "nb_connected_simulations": self.nb_connected_simulations,
                "max_sim_id": self.__max_sim_id,
                "groups": self._groups,
                "t0": self.__t0,
                "total_bytes_recv": self._total_bytes_recv
            }

            with open(f'checkpoints/{self.rank}/metadata.pkl', 'wb') as f:
                cloudpickle.dump(metadata, f)

        if self.rank == 0:
            metadata = {"MELISSA_RESTART": int(os.environ["MELISSA_RESTART"])}
            with open('checkpoints/restart_metadata.json', 'wb') as f:
                # write the metadata to json
                rapidjson.dump(metadata, f)

    def _load_base_state(self) -> None:
        """Loads all common attributes in the server class from a checkpoint or saved state."""

        try:
            # load the metadata
            with open(f'checkpoints/{self.rank}/metadata.pkl', 'rb') as f:
                metadata = cloudpickle.load(f)
        except FileNotFoundError as e:
            logger.exception(f"Rank {self.rank}>> Fault-tolerance must be set for checkpointing.")
            raise FileNotFoundError from e

        self.nb_groups = metadata["nb_groups"]
        self.nb_submitted_simulations = metadata["nb_submitted_simulations"]
        self.nb_finished_simulations = metadata["nb_finished_simulations"]
        self.nb_connected_simulations = metadata["nb_connected_simulations"]
        self.__max_sim_id = metadata["max_sim_id"]
        self._groups = metadata["groups"]
        self.__t0 = metadata["t0"]
        self._total_bytes_recv = metadata["total_bytes_recv"]

    def __initialize_ports(self,
                           connection_port: int = 2003,
                           data_puller_port: int = 5000) -> None:
        """Assigns port numbers for connection and data pulling as class attributes.
        If the specified ports are already in use, likely due to multiple servers running
        on the same node, the function attempts to find available ports by incrementing the
        base port values and rechecking their availability.

        _Note: When multiple independent `melissa-server` jobs are running simultaneously
        on the same node, there is a chance that a port may incorrectly appear as available,
        leading to potential conflicts._

        ### Parameters
        - **connection_port** (`int`, optional): The port number used for establishing the main
        connection (default is `2003`).
        - **data_puller_port** (`int`, optional): The port number used for pulling data
        (default is `5000`).

        ### Raises
        - `RuntimeError`: If no ports were found after given number of attempts."""

        # Ports initialization
        logger.info(f"Rank {self.rank}>> Initializing server...")
        self.node_name = socket.gethostname()

        attempts = max(10, self.comm_size)
        if self.rank == 0:
            self.__connection_port = connection_port
            i = 0
            while is_port_in_use(self.__connection_port) and i < attempts:
                logger.warning(
                    f"Rank {self.rank}>> Connection port {self.__connection_port} in use. "
                    "Trying another..."
                )
                self.__connection_port += 1
                i += 1

            if i == attempts:
                logger.error(
                    f"{self.rank}>> Could not find an available connection port after "
                    f"{attempts} attempts."
                )
                raise RuntimeError

        # Set data puller port
        self.__data_puller_port = data_puller_port + (self.rank * (attempts + 1))
        i = 0
        while is_port_in_use(self.__data_puller_port) and i < attempts:
            logger.warning(f"Rank {self.rank}>> Data puller port {self.__data_puller_port} in use. "
                           "Trying another...")
            self.__data_puller_port += 1
            i += 1

        if i == attempts:
            logger.error(f"{self.rank}>> Could not find an available data puller port after "
                         f"{attempts} attempts.")
            raise RuntimeError

        self.__data_puller_port_name = f"tcp://{self.node_name}:{self.__data_puller_port}"
        self._port_names = self.comm.allgather(self.__data_puller_port_name)
        logger.debug(f"port_names {self._port_names}")

    def __connect_to_launcher(self) -> None:
        """Establishes a connection with the launcher and sends metadata about the study."""

        self._launcherfd: socket.socket
        # Setup communication instances
        self.__protocol, prot_name = select_protocol()
        logger.info(f"Server/launcher communication protocol: {prot_name}")
        if self.rank == 0:
            self._launcherfd = connect_to_launcher()
            logger.debug(f"Rank {self.rank}>> Launcher fd set up: { self._launcherfd.fileno()}")
            self._launcherfd.send(self._encode_msg(message.CommSize(self.comm_size)))
            logger.debug(f"Rank {self.rank}>> Comm size {self.comm_size} sent to launcher")
            self._launcherfd.send(self._encode_msg(message.GroupSize(self.group_size)))
            logger.debug(f"Rank {self.rank}>> Group size {self.group_size} sent to launcher")
        # synchronize non-zero ranks after rank 0 connection to make sure
        # these ranks only connect after comm_size is known to the launcher
        self.comm.Barrier()
        if self.rank > 0:
            self._launcherfd = connect_to_launcher()
            logger.debug(f"Rank {self.rank}>> Launcher fd set up: {self._launcherfd.fileno()}")

        # if an error was caught earlier the connection is immediately closed
        # this will prevent the server to be relaunched infinitely
        if self._catch_error:
            raise UnsupportedConfiguration(
                "Please make sure you have used correct keys and values."
            )

    def __setup_sockets(self) -> None:
        """Sets up ZeroMQ (ZMQ) sockets over a given TCP connection port for communication."""

        self.__zmq_context = zmq.Context()
        # Simulations (REQ) <-> Server (REP)
        self._connection_responder = self.__zmq_context.socket(zmq.REP)
        if self.rank == 0:
            addr1 = f"tcp://*:{self.__connection_port}"
            try:
                self._connection_responder.bind(addr1)
            except Exception as e:
                logger.exception(str(e))
                raise e
            logger.info(
                f"Rank {self.rank}>> Binding to {addr1} successful."
            )

        # Simulations (PUSH) -> Server (PULL)
        self.__data_puller = self.__zmq_context.socket(zmq.PULL)
        self.__data_puller.setsockopt(zmq.RCVHWM, self.zmq_hwm)
        self.__data_puller.setsockopt(zmq.LINGER, -1)
        addr2 = f"tcp://*:{self.__data_puller_port}"
        try:
            self.__data_puller.bind(addr2)
        except Exception as e:
            logger.exception(str(e))
            raise e
        logger.info(
            f"Rank {self.rank}>> Data puller to {addr2} successful."
        )

        # Time-out checker (creates thread)
        self.__timerfd_0, self.__timerfd_1 = socket.socketpair(
            socket.AF_UNIX, socket.SOCK_STREAM
        )
        timer = Timer(self.__timerfd_1, timedelta(seconds=self.max_delay))
        self.__t_timer = threading.Thread(target=timer.run, daemon=True)
        self.__t_timer.start()

    def __setup_poller(self) -> None:
        """This method sets up the polling mechanism by registering three important sockets:
        - **Data Socket**: Handles data communication.
        - **Timer Socket**: Manages timing events.
        - **Launcher Socket**: Facilitates communication with the launcher."""

        self.__zmq_poller = zmq.Poller()
        self.__zmq_poller.register(self.__data_puller, zmq.POLLIN)
        self.__zmq_poller.register(self.__timerfd_0, zmq.POLLIN)
        self.__zmq_poller.register(self._launcherfd, zmq.POLLIN)
        if self.rank == 0:
            self.__zmq_poller.register(self._connection_responder, zmq.POLLIN)

    def __start_debugger(self) -> None:
        """Launches the Visual Studio Code (VSCode) debugger for debugging purposes."""

        # 5678 is the default attach port that we recommend users to set
        # in the documentation.
        debugpy.listen(5678)
        logger.warning("Waiting for debugger attach, please start the "
                       "debugger by navigating to debugger pane ctrl+shift+d "
                       "and selecting\n"
                       "Python: Remote Attach")
        debugpy.wait_for_client()
        logger.info("Debugger successfully attached.")
        # send message to launcher to ensure debugger doesnt timeout
        snd_msg = self._encode_msg(message.StopTimeoutMonitoring())
        self._launcherfd.send(snd_msg)

    def initialize_connections(self) -> None:
        """Initializes socket connections for communication."""

        self.__initialize_ports()
        self.__connect_to_launcher()
        self.__setup_sockets()
        self.__setup_poller()

        if self.config_dict.get("vscode_debugging", False):
            self.__start_debugger()

    def _get_group_id_by_simulation(self, sim_id: int) -> int:
        """Returns group id of the given simulation id."""
        return sim_id // self.group_size

    def _get_sim_id_list_by_group(self, group_id: int) -> List[int]:
        """Returns a list of all simulation ids for a given group id.
        Required for gathering messages by group id"""
        return list(self._groups[group_id].simulations.keys())

    def _get_all_sim_ids(self):
        """Yields all simulation ids across all groups."""
        for group_id in self._groups.keys():
            yield from self._get_sim_id_list_by_group(group_id)

    def _verify_and_update_sampler_kwargs(self, sampler_t, **kwargs) -> Dict[str, Any]:
        """Updates the parameters that were not provided by the user when
        creating a sampler using `set_parameter_sampler` method. It also ensures whether
        the seed is given or not for a parallel server."""

        # if not provided by the users
        if "nb_params" not in kwargs:
            kwargs["nb_params"] = self.nb_parameters
        if "nb_sims" not in kwargs:
            kwargs["nb_sims"] = self.nb_clients
        if "seed" not in kwargs:
            if hasattr(self, "seed"):
                kwargs["seed"] = self.seed
            elif (
                self.comm_size > 1
                and isinstance(sampler_t, type)
                and issubclass(sampler_t, StaticExperiment)
            ):
                self._catch_error = True
                raise UnsupportedConfiguration(
                    "`seed` must be passed to the `set_parameter_sampler` "
                    "when working with a parallel server and a static parameter sampler."
                )

        return kwargs

    def set_parameter_sampler(self,
                              sampler_t: Union[ParameterSamplerType, Type[ParameterSamplerClass]],
                              **kwargs) -> None:
        """Sets the defined parameter sampler type. This dictates how parameters are sampled
        for experiments. This sampler type can either be pre-defined or customized
        by inheriting a pre-defined sampling class.

        ### Parameters
        - **sampler_t** (`Union[ParameterSamplerType, Type[ParameterSamplerClass]]`):
            - `ParameterSamplerType`: Enum specifying pre-defined samplers.
            - `Type[ParameterSamplerClass]`: A class type to instantiate.
        - **kwargs** (`Dict[str, Any]`): Dictionary of keyword arguments.
        Useful to pass custom parameter as well as strict parameter such as
        `l_bounds`, `u_bounds`, `apply_pick_freeze`, `second_order`, `seed=0`, etc."""
        kwargs = self._verify_and_update_sampler_kwargs(sampler_t, **kwargs)
        self._parameter_sampler = make_parameter_sampler(sampler_t, **kwargs)

    def _update_parameter_generator(self) -> None:
        """Creates a generator from the given parameter sampler.
        The generated object must define the `draw()` method for sampling parameters."""

        if self._parameter_sampler:
            self.__parameter_generator = self._parameter_sampler.generator()

    def _launch_first_groups(self, first_id: int = 0) -> None:
        """Launches the study groups for the very first run.
        This process involves generating the client scripts and ensures that
        no restart has occurred in the case of fault tolerance.

        ### Parameters
        - **first_id** (`int`, optional): The identifier for the first group to launch
        (default is `0`)."""

        # Get current working directory containing the client script template
        client_script = os.path.abspath("client.sh")

        if not os.path.isfile(client_script):
            raise FileNotFoundError("error client script not found")

        self._update_parameter_generator()
        # Generate all client scripts
        self._generate_client_scripts(first_id, self.nb_clients)

        # Launch every group
        # note that the launcher message stacking feature does not work
        for group_id in range(first_id, first_id + self.nb_groups):
            self._launch_group(group_id)

    def _get_next_parameters(self) -> List:
        """Retrieves the next set of parameters from the parameter generator.

        ### Returns
        - `List`: A list containing the next set of parameters."""

        try:
            return list(next(self.__parameter_generator))
        except StopIteration:
            self._update_parameter_generator()
            return list(next(self.__parameter_generator))

    def _generate_client_scripts(self,
                                 first_id: int,
                                 nb_scripts: int,
                                 default_parameters: Optional[List[Any]] = None,
                                 create_new_group: bool = False) -> None:
        """Creates all required client scripts (e.g., `client.X.sh`),
        and sets up a dictionary for fault tolerance.

        ### Parameters
        - **first_id** (`int`): The identifier for the first client script to be generated.
        - **nb_scripts** (`int`): The total number of client scripts to create.
        - **default_parameters** (`list`, optional):
        A list of default parameters to be used in the client scripts.
        (default is `None`), the default values are used.
        - **create_new_group** (`bool`, optional): Flag indicating whether to
        create a new group of clients (default is `False`)."""

        for sim_id in range(first_id, first_id + nb_scripts):
            parameters = []
            # ping, if scripts generation takes too long
            if nb_scripts > 10000 and (sim_id - first_id) % 10000 == 0:
                self._launcherfd.send(self._encode_msg(message.Ping()))
            if default_parameters is not None:
                parameters = default_parameters
            elif self.__parameter_generator:
                parameters = self._get_next_parameters()

            if self.rank == 0:
                self.__generate_client_script(sim_id, parameters)

            logger.info(
                f"Rank {self.rank}>> Created client.{sim_id}.sh with parameters {parameters}"
            )

            # Fault-tolerance dictionary creation and update
            # create_new_group is specifically for active sampling
            group_id = self._get_group_id_by_simulation(sim_id)
            if create_new_group or group_id not in self._groups:
                self._groups[group_id] = Group(group_id, self.sobol_op)
            if sim_id not in self._groups[group_id].simulations:
                if not create_new_group:
                    self.nb_submitted_simulations += 1
                simulation = Simulation(
                    sim_id, self.nb_time_steps, self.fields, parameters
                )
                self._groups[group_id].simulations[sim_id] = simulation

    def __generate_client_script(self,
                                 sim_id: int,
                                 parameters: List[Any]) -> None:
        """Generates a single client script for a given simulation id and parameters.

        ### Parameters
        - **sim_id** (`int`): The simulation id associated with the client script.
        - **parameters** (`list`): The list of parameters.
        of parameters to be used in the client script."""

        client_script_i = os.path.abspath(f"./client_scripts/client.{str(sim_id)}.sh")
        Path('./client_scripts').mkdir(parents=True, exist_ok=True)
        with open(client_script_i, "w") as f:
            print("#!/bin/sh", file=f)
            print("exec env \\", file=f)
            print(f"  MELISSA_SIMU_ID={sim_id} \\", file=f)
            print(f"  MELISSA_SERVER_NODE_NAME={self.node_name} \\", file=f)
            print(f"  MELISSA_SERVER_PORT={self.__connection_port} \\", file=f)
            # str() conversion causes problems with scientific notations
            # and should not be used
            if self.rm_script:
                print(
                    "  "
                    + " ".join(
                        [os.path.join(os.getcwd(), "client.sh")]
                        + [
                            np.format_float_positional(x) if not isinstance(x, str)
                            else x for x in parameters
                        ]
                    )
                    + " &",
                    file=f,
                )
                print("  wait", file=f)
                print('  rm "$0"', file=f)
            else:
                print(
                    "  "
                    + " ".join(
                        [os.path.join(os.getcwd(), "client.sh")]
                        + [
                            np.format_float_positional(x) if not isinstance(x, str)
                            else x for x in parameters
                        ]
                    ),
                    file=f,
                )

        os.chmod(client_script_i, 0o744)

    def _launch_group(self, group_id: int) -> None:
        """Submits a request to the launcher to run a given group id.
        For non-Sobol studies, the group id and simulation id are the same.

        ### Parameters
        - **group_id** (`int`): The unique identifier of the group to be launched."""

        if self.rank == 0:
            # Job submission message to launcher (initial_id,num_jobs)
            snd_msg = self._encode_msg(message.JobSubmission(group_id, 1))
            self._launcherfd.send(snd_msg)
            logger.debug(
                f"Rank {self.rank}>> group "
                f"{group_id + 1}/{self.nb_groups} "
                "submitted to launcher"
            )

    def _kill_group(self, group_id: int) -> None:
        """Submits a request to the launcher to terminate a given group id.

        ### Parameters
        - **group_id** (`int`): The unique identifier of the group to be terminated."""

        if self.rank == 0:
            logger.warning(f"Server crashed, restarting incomplete job {group_id}")
            snd_msg = self._encode_msg(message.JobCancellation(group_id))
            self._launcherfd.send(snd_msg)

    def _relaunch_group(self, group_id: int, create_new_group: bool) -> None:
        """Relaunches a failed group with or without new parameters,
        depending on the fault tolerance configuration.

        ### Parameters
        - **group_id** (`int`): The unique identifier of the group to be relaunched.
        - **create_new_group** (`bool`): A flag indicating whether to create a new group
        with new parameters."""

        if create_new_group:
            del self._groups[group_id]
            # nb_submitted_simulations needs to be decremented after each group deletion
            # to compensate for generate_client_scripts which will increment it group_size times
            self.nb_submitted_simulations -= self.group_size
            for sim in range(self.group_size):
                self._generate_client_scripts(
                    group_id * self.group_size + sim, 1, None
                )
        else:
            for sim in range(self.group_size):
                self._generate_client_scripts(
                    group_id * self.group_size + sim,
                    1,
                    self._groups[group_id].simulations[group_id * self.group_size + sim].parameters,
                )

        self._launch_group(group_id)

    def _handle_simulation_connection(self, msg: bytes) -> str:
        """Handles an incoming connection request from a submitted simulation.
        This method is executed by rank 0 only.

        ### Parameters
        - **msg** (`bytes`): The message received from the simulation requesting a connection.

        ### Returns
        - `str`: A response message indicating the status of the connection request."""

        request = ConnectionRequest.recv(msg)
        self.client_comm_size = request.comm_size
        sim_id = request.simulation_id
        logger.debug(
            f"Rank {self.rank}>> [Connection] received connection message "
            f"from sim-id={sim_id} with client-comm-size={self.client_comm_size}."
        )
        logger.debug(
            f"Rank {self.rank}>> [Connection] sending response to sim-id={sim_id}"
            f" with learning={self._learning}"
        )
        response = ConnectionResponse(
            self.comm_size,
            int(self._sobol_op),
            self._learning,
            self.nb_parameters,
            self.verbose_level,
            self._port_names,
        )
        self._connection_responder.send(response.encode())
        self.nb_connected_simulations += 1
        logger.info(
            f"Rank {self.rank}>> [Connection] sim-id={sim_id} established."
        )
        with self.consistency_lock:
            self.__max_sim_id = max(self.__max_sim_id, sim_id)

        return "Connection"

    def _kill_and_restart_simulations(self) -> None:
        """Kills and restarts simulations that were running when the server crashed."""

        for group_id in self._groups.keys():
            for sim_id in self._groups[group_id].simulations.keys():
                simulation = self._groups[group_id].simulations[sim_id]
                if simulation.nb_received_time_steps < simulation.nb_time_steps:
                    # generate the client script just in-case a client finished, removed
                    # its own script, but before the server checkpointed itself. This
                    # is a corner case.
                    self.__generate_client_script(sim_id, simulation.parameters)
                    # relaunch the group
                    self._launch_group(group_id)

    def poll_sockets(self,
                     timeout: int = 10,
                     ) -> Optional[Union[ServerStatus, SimulationData, PartialSimulationData]]:
        """Performs polling over the registered socket descriptors to monitor various events,
        including timer, launcher messages, new client connections, and data readiness.

        ### Parameters
        - **timeout** (`int`, optional): The maximum time (in seconds) to wait for a socket
        event before returning. Default is `10` seconds.

        ### Returns
        - `Optional[Union[ServerStatus, SimulationData, PartialSimulationData]]`:
            - `ServerStatus` if the event is related to server status.
            - `SimulationData` if new simulation data is received.
            - `PartialSimulationData` if partial data from a simulation is received."""

        # 1. Poll sockets
        # ZMQ sockets
        sockets = dict(self.__zmq_poller.poll(timeout))
        if not sockets:
            return ServerStatus.TIMEOUT

        if self.rank == 0:
            # 2.  Look for connections  from new simulations (just at rank 0)
            if (
                self._connection_responder in sockets
                and sockets[self._connection_responder] == zmq.POLLIN
            ):
                msg = self._connection_responder.recv()
                logger.debug(f"Rank {self.rank}>> Handle client connection request")
                self._handle_simulation_connection(msg)

        # 3. Handle launcher message
        # this is a TCP/SCTP socket not a zmq one, so handled differently
        if self._launcherfd.fileno() in sockets:
            logger.debug(f"Rank {self.rank}>> Handle launcher message")
            self.__handle_fd()

        # 4. Handle simulation data message
        if (
            not self.offline_mode
            and self.__data_puller in sockets
            and sockets[self.__data_puller] == zmq.POLLIN
        ):
            logger.debug(f"Rank {self.rank}>> Handle simulation data")
            msg = self.__data_puller.recv()
            self._total_bytes_recv += len(msg)
            return self._handle_simulation_data(msg)

        # 5. Handle timer message
        if self.__timerfd_0.fileno() in sockets:
            logger.debug(f"Rank {self.rank}>> Handle timer message")
            self.__handle_timerfd()

        return None

    def __handle_timerfd(self) -> None:
        """Handles timer messages."""

        self.__timerfd_0.recv(1)
        try:
            self.__ft.check_time_out(self._groups)
        except Exception as e:
            if not self.ignore_client_death:
                raise e
            else:
                self.nb_finished_simulations += 1

        if not self.ignore_client_death:
            for group_id, create_new_group in self.__ft.restart_group.items():
                self._relaunch_group(group_id, create_new_group)

        self.__ft.restart_group = {}

    def __handle_fd(self) -> None:
        """Handles the launcher's messages."""

        bs = self._launcherfd.recv(256)
        rcvd_msg = self._decode_msg(bs)

        for msg in rcvd_msg:
            # 1. Launcher sent JOB_UPDATE message (msg.job_id <=> group_id)
            if isinstance(msg, message.JobUpdate):
                group = self._groups[msg.job_id]
                group_id = group.group_id
                if msg.job_state in [job.State.RUNNING, job.State.WAITING]:
                    self.__launcher_job_submissions.add((msg.job_id, msg.job_state))
                # React to simulation status
                elif msg.job_state in [job.State.ERROR, job.State.FAILED]:
                    logger.debug("Launcher indicates job failure")
                    # TODO: fix the code below. it is a dirty way to ignore the client death.
                    try:
                        create_new_group = self.__ft.handle_failed_group(group_id, group)
                    except Exception as e:
                        if not self.ignore_client_death:
                            raise e
                        else:
                            self.nb_finished_simulations += 1
                    if not self.ignore_client_death:
                        self._relaunch_group(group_id, create_new_group)
                elif self.offline_mode and msg.job_state is job.State.TERMINATED:
                    self.nb_finished_simulations += 1
                    logger.info(f"Rank {self.rank}>> [Offline] sim-id={group_id} has terminated.")

        # 2. Server sends PING
        if self.rank == 0:
            logger.debug(
                "Server got message from launcher and sends PING back"
            )
            snd_msg = self._encode_msg(message.Ping())
            self._launcherfd.send(snd_msg)

# =====================================Active sampling=====================================
    def _get_most_recent_sim_id(self) -> int:
        """Retrieves the highest simulation id among those that have
        successfully established a connection with the server.
        This is primarily used in adaptive sampling to determine
        the starting point for modifying scripts of clients that
        have not yet been submitted by the launcher."""
        with self.consistency_lock:
            return self.__max_sim_id if self.__max_sim_id > -1 else -1

    def _get_launcher_view(self) -> Set[Tuple[int, State]]:
        """Returns the set consisting of all job submissions
        made by the launcher for which the server has received an update.
        This information is helpful for deciding which future clients are likely
        to have their scripts updated after adaptive sampling.

        ### Returns
        - `Set[Tuple[int, State]]`: A set containing job submissions."""
        return self.__launcher_job_submissions

    def _clear_launcher_view(self) -> None:
        """Clears the Set.
        Needed when we want to avoid considering old clients' status."""
        with self.consistency_lock:
            self.__launcher_job_submissions.clear()

# =====================================Active sampling=====================================

    def _decode_msg(self, byte_stream: bytes) -> List[Message]:
        """Deserializes a message based on the specified protocol.

        ### Parameters
        - **byte_stream** (`bytes`): The byte stream to be deserialized,
        representing the encoded message.

        ### Returns
        - `List[Message]`: A list of byte sequences representing
        the deserialized message components."""

        msg_list = []
        if self.__protocol == socket.IPPROTO_TCP:
            packets = LengthPrefixFramingDecoder(
                config.TCP_MESSAGE_PREFIX_LENGTH
            ).execute(byte_stream)
            for p in packets:
                msg_list.append(message.deserialize(p))
            logger.debug(f"Decoded launcher messages {msg_list}")
            return msg_list
        if self.__protocol == socket.IPPROTO_SCTP:
            msg_list.append(message.deserialize(byte_stream))
            logger.debug(f"Decoded launcher messages {msg_list}")
            return msg_list
        raise UnsupportedProtocol(f"{self.__protocol} not supported for decoding.")

    def _encode_msg(self, msg: Message) -> bytes:
        """Serializes message based on the specified protocol.

        ### Parameters
        - **msg** (`Message`): The message to be serialized,
        typically a byte sequence that needs encoding.

        ### Returns
        - `bytes`: The serialized byte stream representing the encoded message."""

        if self.__protocol == socket.IPPROTO_TCP:
            encoded_packet = LengthPrefixFramingEncoder(
                config.TCP_MESSAGE_PREFIX_LENGTH
            ).execute(msg.serialize())
            return encoded_packet
        if self.__protocol == socket.IPPROTO_SCTP:
            return msg.serialize()
        raise UnsupportedProtocol(f"{self.__protocol} not supported for encoding.")

    def _all_done(self) -> bool:
        """Checks whether all clients' data has been received and
        unregisters the timer socket if completed.

        ### Returns
        - `bool`: if all clients' data has been successfully received."""

        if self.nb_finished_simulations == self.nb_submitted_simulations:
            # join thread and close timer sockets
            logger.info(f"Rank {self.rank}>> closes timer sockets.")
            self.__timerfd_0.close()
            self.__zmq_poller.unregister(self.__timerfd_0)
            self.__t_timer.join(timeout=1)
            if self.__t_timer.is_alive():
                logger.warning("timer thread did not terminate")
            else:
                self.__timerfd_1.close()
            return True

        return False

    def close_connection(self, exit_: int = 0) -> None:
        """Signals to the launcher that the study has ended with a specified exit status.

        ### Parameters
        - `exit_` (`int`, optional): The exit status code to be sent to the launcher.
        Defaults to `0`, indicating successful completion."""

        snd_msg = self._encode_msg(message.Exit(exit_))
        # 1. Syn server processes
        self.comm.Barrier()

        # 2. Send msg to the launcher
        if self.rank == 0:
            if exit_ > 0:
                logger.error("The server failed with an error")
            else:
                logger.info(f"Rank {self.rank}>> turns the launcher off.")
            self._launcherfd.send(snd_msg)

    def get_memory_info_in_gb(self) -> Tuple[float, float]:
        """Returns a `Tuple[float, float]` containing memory consumed and
        the total main memory in GB."""

        memory = psutil.virtual_memory()
        consumed = memory.used / (1024 ** 3)
        total = memory.total / (1024 ** 3)

        return consumed, total

    def _show_insights(self) -> None:
        """Logs information gathered from clients, and server processing."""

        seconds = self.mtt_simulation_completion.get_mean()[0]
        mean_hms = str(
            datetime.timedelta(
                seconds=int(seconds)
            )
        )
        t = mean_hms if seconds > 1 else f"{seconds:.2} sec."
        logger.info(
            f"Rank {self.rank}>> [Insight] "
            f"Average simulation completion={t}"
        )

        consumed, total = self.get_memory_info_in_gb()
        logger.info(
            f"Rank {self.rank}>> [Insight] Memory consumption={consumed:.2f}/{total:.2f} GB."
        )

    def _write_final_report(self) -> None:
        """Write miscellaneous information about the analysis."""

        # Total time
        total_time = time.time() - self.__t0
        total_time = self.comm.allreduce(total_time, op=MPI.SUM)
        # Total MB received
        total_b = self.comm.allreduce(self._total_bytes_recv, op=MPI.SUM)
        if self.rank == 0:
            msg_bytes = bytes_to_readable(total_b)
            total_hms = datetime.timedelta(seconds=total_time // self.comm_size)
            logger.info(" - Number of finished simulations: "
                        f"{self.nb_finished_simulations}/{self.nb_submitted_simulations}")
            logger.info(f" - Number of server ranks: {self.comm_size}")
            logger.info(f" - Total time: {str(total_hms)}")
            logger.info(f" - Total data received: {msg_bytes}")

    def _server_finalize(self, exit_: int = 0) -> None:
        """Finalizes the server operations.

        ### Parameters
        - `exit_` (`int`, optional): The exit status code indicating the outcome
        of the server's operations. Defaults to `0`, which signifies a successful termination."""

        self._write_final_report()
        logger.info(f"Server finalizing with status {exit_}.")
        self.close_connection(exit_)

    def setup_environment(self) -> None:
        """Optional. A method that sets up the environment or initialization.
        Any necessary setup methods go here.
        For example, Melissa DL study needs `dist.init_process_group` to be called."""
        return None

    def _check_simulation_data(self,
                               simulation: Simulation,
                               simulation_data: PartialSimulationData
                               ) -> Tuple[
                                   SimulationDataStatus, Union[
                                       Optional[SimulationData],
                                       Optional[PartialSimulationData]]]:
        """Tracks and validates incoming simulation data.

        1. **Client Rank Initialization**:
        Ensures `simulation_data` structures are initialized per client rank.
        2. **Dynamic Matrix Expansion**:
        Handles unknown sizes dynamically as new time steps are encountered.
        3. **Duplicate Data Detection**:
        Discards messages if the data for the specified field and time step has
        already been received.
        4. **Time Step Completion**:
            - Checks if all fields for a specific time step have been received and processes them
            into a `SimulationData` object.
            - Handles cases where the data is empty.
        5. **Partial Data Handling**: Tracks fields received so far and waits for completion.

        ### Parameters
        - **simulation** (`Simulation`): Tracks the state and received data of the simulation.
        - **simulation_data** (`PartialSimulationData`): The incoming data message from
        the simulation.

        ### Returns
        - `SimulationDataStatus`: Status of the simulation data
        (`COMPLETE`, `PARTIAL`, `ALREADY_RECEIVED`, `EMPTY`).
        - `Union[Optional[SimulationData], Optional[PartialSimulationData]]`:
            - Sensitivity Analysis:
                - A `PartialSimulationData` object regardless of it being incomplete
                as SA can be computed independently.
            - Deep Learning:
                - A `SimulationData` object if all fields for the time step are complete.
                - `None` if the data is incomplete or invalid."""

        client_rank, time_step, field = (
            simulation_data.client_rank,
            simulation_data.time_step,
            simulation_data.field,
        )

        # lock needed when training thread might checkpoint on the same data
        # that is being updated below
        with self.consistency_lock:
            simulation.init_structures(
                client_rank=client_rank,
                time_steps_known=self.time_steps_known
            )
            # following expansion is conditional based on
            # the current shape and the given time step
            simulation.time_step_expansion(client_rank, time_step)

            # check for duplicate data
            if simulation.has_already_received(client_rank, time_step, field):
                return SimulationDataStatus.ALREADY_RECEIVED, None

            # initialize storage for all fields
            simulation.init_data_storage(client_rank, time_step)
            # update received data
            simulation.update(client_rank, time_step, field, simulation_data)

            simulation.mark_as_received(client_rank, time_step, field)

            # check if the time step is complete
            if simulation.is_complete(time_step):
                simulation.nb_received_time_steps += 1

                # handle empty data scenario
                if simulation_data.data_size == 0:
                    return SimulationDataStatus.EMPTY, None

                return (
                    SimulationDataStatus.COMPLETE,
                    self._process_complete_data_reception(
                        simulation,
                        simulation_data
                    )
                )

            # Partial data received
            return (
                SimulationDataStatus.PARTIAL,
                self._process_partial_data_reception(
                    simulation,
                    simulation_data
                )
            )

    def __deserialize_message(self, msg: bytes) -> PartialSimulationData:
        """Deserializes a byte stream into a `PartialSimulationData` object.

        ### Parameters
        - **msg** (`bytes`): Serialized message containing simulation data.

        ### Returns
        - `PartialSimulationData`: Data objet."""

        data = PartialSimulationData.from_msg(msg, self.learning)
        logger.debug(
            f"Rank {self.rank}>> received message "
            f"from sim-id={data.simulation_id}, client-rank={data.client_rank}, "
            f"vect-size={len(data.data)}"
        )
        return data

    def _validate_data(self, simulation_data: PartialSimulationData) -> bool:
        """Validates the time step and field of the received simulation data.

        ### Parameters
        - **simulation_data** (`PartialSimulationData`): The data to validate.

        ### Returns
        - `bool`: if the data is valid."""

        sim_id = simulation_data.simulation_id
        time_step = simulation_data.time_step
        field = simulation_data.field

        if not 0 <= time_step < self.nb_time_steps:
            logger.warning(f"Rank {self.rank}>> [BAD] sim-id={sim_id}, time-step={time_step}")
            return False
        if field not in self.fields:
            logger.warning(f"Rank {self.rank}>> [BAD] sim-id={sim_id}, field=\"{field}\"")
            return False
        return True

    def __determine_and_process_simulation_data(self, simulation_data: PartialSimulationData
                                                ) -> Optional[Union[SimulationData,
                                                                    PartialSimulationData]]:
        """Determines the status of the simulation data and handles actions accordingly.

        ### Parameters
        - **simulation_data** (`PartialSimulationData`): The incoming simulation data to process.

        ### Returns
        - `Optional[Union[SimulationData, PartialSimulationData]]`:
        return of the `_check_simulation_data` method."""

        sim_id = simulation_data.simulation_id
        time_step = simulation_data.time_step
        group_id = self._get_group_id_by_simulation(sim_id)
        group = self._groups[group_id]
        simulation = group.simulations[sim_id]

        # check simulation data status
        sim_status, sim_data = self._check_simulation_data(simulation, simulation_data)
        if sim_status is SimulationDataStatus.COMPLETE:
            logger.debug(
                f"Rank {self.rank}>> sim-id={sim_id}, time-step={time_step} assembled."
            )
        elif sim_status is SimulationDataStatus.ALREADY_RECEIVED:
            logger.warning(
                f"Rank {self.rank}>> [Duplicated] sim-id={sim_id}, time-step={time_step}"
            )
        if sim_status in [SimulationDataStatus.COMPLETE, SimulationDataStatus.EMPTY]:
            if simulation.finished():
                logger.info(f"Rank {self.rank}>> sim-id={sim_id} has finished sending.")
                self.nb_finished_simulations += 1
                self.mtt_simulation_completion.increment(simulation.duration)
                if self.nb_finished_simulations % 100 == 0:
                    self._show_insights()

        return sim_data

    def _handle_simulation_data(self,
                                msg: bytes) -> Optional[
                                    Union[SimulationData,
                                          PartialSimulationData]]:
        """This method handles the following tasks:
        1. **Deserialization**: Converts the incoming byte stream
        into a `PartialSimulationData` object.
        2. **Validation**: Ensures the data is valid based on:
            - Time step being within the allowed range.
            - Field name being recognized.
        3. **Simulation Data Handling**:
            - Updates the status of the simulation based on the received data.
            - Detects and logs duplicate messages.
        4. **Completion Check**:
            - Marks the simulation as finished if all data is received.
            - Updates the count of finished simulations.

        ### Parameters
        - **msg** (`bytes`): A serialized message containing simulation data.

        ### Returns
        - `Optional[PartialSimulationData]`:
            - `PartialSimulationData`, if successful.
            - `None`, if the message fails validation."""

        data = self.__deserialize_message(msg)
        if self._validate_data(data):
            return self.__determine_and_process_simulation_data(data)
        return None

# =====================================Abstract Methods=====================================

    @abstractmethod
    def _server_online(self) -> None:
        """An abstract method where user controls the data handling while server is online.
        Unique to melissa flavors."""
        raise NotImplementedError("Subclasses must override this method.")

    @abstractmethod
    def _check_group_size(self) -> None:
        """An abstract method that checks if the group size was correctly set.
        Unique to melissa flavors."""
        raise NotImplementedError("Subclasses must override this method.")

    @abstractmethod
    def _process_partial_data_reception(self,
                                        simulation: Simulation,
                                        simulation_data: PartialSimulationData
                                        ) -> Optional[PartialSimulationData]:
        """Returns a value when data has been partially received.
        Unique to melissa flavors."""
        raise NotImplementedError("Subclass must override this method.")

    @abstractmethod
    def _process_complete_data_reception(self,
                                         simulation: Simulation,
                                         simulation_data: PartialSimulationData
                                         ) -> Union[PartialSimulationData,
                                                    SimulationData]:
        """Returns a value when data has been completely received.
        Unique to melissa flavors."""
        raise NotImplementedError("Subclass must override this method.")

    @abstractmethod
    def _receive(self) -> None:
        """Handles data coming from the server object.
        Unique to melissa flavors."""
        raise NotImplementedError("Subclasses must override this method.")

    @abstractmethod
    def start(self) -> None:
        """The high level organization of server events.
        Unique to melissa flavors."""
        raise NotImplementedError("Subclasses must override this method.")

    @abstractmethod
    def checkpoint_state(self) -> None:
        """Checkpoints the server object at the current state.
        Unique to melissa flavors."""
        return None

    @abstractmethod
    def _restart_from_checkpoint(self) -> None:
        """Restarts the server object from a checkpoint.
        Unique to melissa flavors."""
        raise NotImplementedError("Subclasses must override this method.")
