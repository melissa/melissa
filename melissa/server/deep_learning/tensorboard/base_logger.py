import logging
from abc import ABC, abstractmethod
from typing import Any, Optional, Union, List
from pathlib import Path
from matplotlib.figure import Figure

try:
    import pandas as pd
    import tensorflow as tf
    TB_TO_DF = True
except ModuleNotFoundError:
    TB_TO_DF = False


logger = logging.getLogger(__name__)


class TensorboardLogger(ABC):
    def __init__(self, disable: bool = False, debug: bool = False):

        self.disable: bool = disable
        self.debug: bool = debug
        self._writer: Any = None

    @property
    def writer(self) -> Any:
        assert not self.disable
        return self._writer

    @abstractmethod
    def log_scalar(self, tag: str, scalar_value: Any, step: int):
        """Logs scalar to tensorboard logger.

        ### Parameters
        - **tag** (`str`): Metric tag.
        - **scalar_value** (`Any`): Value to log.
        - **step** (`int`): Step number."""
        pass

    def log_scalars(self, main_tag: str, tag_scalar_dict: dict, step: int):
        """Logs several scalars with multi-tag to tensorboard logger.

        ### Parameters
        - **main_tag** (`str`): The parent name for the tags.
        - **tag_scalar_dict** (`dict`): Key-value pair storing the tag and corresponding values.
        - **step** (`int`): Step number."""
        pass

    @abstractmethod
    def log_scalar_dbg(self, tag: str, scalar_value: Any, step: int):
        """Logs debugging-related scalar to tensorboard logger.

        ### Parameters
        - **tag** (`str`): Metric tag.
        - **scalar_value** (`Any`): Value to log.
        - **step** (`int`): Step number."""
        pass

    def log_histogram(self, tag: str, values: Any, step: Optional[int] = None):
        """Logs histograms to tensorboard logger.

        ### Parameters
        - **tag** (`str`): Metric tag.
        - **values** (`Any`): Values to log.
        - **step** (`Optional[int]`): Step number."""
        pass

    def log_figure(
        self,
        tag: str,
        figure: Union[Figure, List[Figure]],
        step: Optional[int] = None,
        close: bool = True,
    ):
        """Render matplotlib figure into an image and add it to summary.

        ### Parameters
        - **tag** (`str`): Figure identifier.
        - **figure** (`Union[Figure, List[Figure]]`): Figure or a list of figures.
        - **step** (`Optional[int]`): Step number.
        - **close** (`bool`) : Flag to automatically close the figure."""
        pass

    def close(self):
        """Flushes and closes tensorboard logger."""
        pass


def convert_tb_logs_to_df(rank: int) -> None:
    """Converts local TensorBoard data into Pandas DataFrame.
    Saves the pandas dataframe as a pickle file inside
    out_dir/tensorboard."""

    if not TB_TO_DF:
        logger.error(
            "Unable to import dependencies for log. "
            "Please install `pandas` and `tensorflow-cpu`."
        )
        return

    def convert_tfevent(filepath):
        records = []
        for raw_record in tf.data.TFRecordDataset(filepath):
            event = tf.compat.v1.Event.FromString(raw_record.numpy())
            if event.summary and len(event.summary.value) > 0:
                records.append(parse_tfevent(event))
        return pd.DataFrame(records)

    def parse_tfevent(tfevent):
        return {
            "wall_time": tfevent.wall_time,
            "name": tfevent.summary.value[0].tag,
            "step": tfevent.step,
            "value": float(tfevent.summary.value[0].simple_value),
        }

    columns_order = ["wall_time", "name", "step", "value"]

    out = []
    for folder in Path("tensorboard").iterdir():
        if f"gpu_{rank}" in str(folder):
            for file in folder.iterdir():
                if "events.out.tfevents" not in str(file):
                    continue
                if f"rank_{rank}" not in str(file):
                    continue
                logger.info(f"Parsing {str(file)}")
                out.append(convert_tfevent(str(file)))

    if out:
        all_df = pd.concat(out)[columns_order]
        all_df.reset_index(drop=True, inplace=True)
        all_df.to_pickle(f"./tensorboard/data_rank_{rank}.pkl")
    else:
        logger.warning("No valid TensorBoard event files found.")
