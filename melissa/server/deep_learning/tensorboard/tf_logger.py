import io

import numpy as np
from typing import Any, Union, List, Optional
import tensorflow as tf
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
from PIL import Image

from melissa.server.deep_learning.tensorboard import TensorboardLogger


class TfTensorboardLogger(TensorboardLogger):
    def __init__(
        self,
        rank: int = 0,
        logdir: str = "tensorboard",
        disable: bool = False,
        debug: bool = False,
    ) -> None:
        super().__init__(disable, debug)
        if not self.disable:
            self._writer = tf.summary.create_file_writer(
                logdir + f"/gpu_{rank}", filename_suffix=f"rank_{rank}"
            )

    def log_scalar(self, tag: str, scalar_value: Any, step: int):
        if not self.disable and self._writer is not None:
            with self._writer.as_default():
                tf.summary.scalar(name=tag, data=scalar_value, step=step)

    def log_scalars(self, main_tag: str, tag_scalar_dict: dict, step: int):
        if not self.disable and self._writer is not None:
            with self._writer.as_default():
                for key, value in tag_scalar_dict.items():
                    tf.summary.scalar(name=f"{main_tag}/{key}", data=value, step=step)

    def log_histogram(self, tag: str, values: Any, step: Optional[int] = None):
        if not self.disable and self._writer is not None:
            with self._writer.as_default():
                tf.summary.histogram(name=tag, data=values, step=step)

    def log_scalar_dbg(self, tag: str, scalar_value: Any, step: int):
        if not self.disable and self._writer is not None and self.debug:
            with self._writer.as_default():
                tf.summary.scalar(name=tag, data=scalar_value, step=step)

    def __figure_to_image(self, figures: Union[Figure, List[Figure]], close: bool = True):
        """Converts matplotlib figure(s) to a TensorFlow image tensor.
        Inspired from `torch.utils.tensorboard._utils.figure_to_image`."""

        def render_to_rgb(figure):
            buf = io.BytesIO()
            figure.savefig(buf, format='png')
            buf.seek(0)
            image = Image.open(buf)
            image = np.array(image.convert("RGB"))
            image = np.moveaxis(image, source=2, destination=0)  # Convert to CHW format
            buf.close()
            if close:
                plt.close(figure)
            return image

        if isinstance(figures, list):
            images = np.stack([render_to_rgb(fig) for fig in figures])
        else:
            images = render_to_rgb(figures)

        images = tf.convert_to_tensor(images, dtype=tf.float32) / 255.0
        images = tf.expand_dims(images, axis=0) if len(images.shape) == 3 else images

        return images

    def log_figure(
        self,
        tag: str,
        figure: Union[Figure, List[Figure]],
        step: Optional[int] = None,
        close: bool = True,
    ):
        if not self.disable and self._writer is not None:
            image = self.__figure_to_image(figure, close)
            tf.summary.image(tag, image, step=step)

    def close(self):
        if not self.disable and self._writer is not None:
            self._writer.flush()
            self._writer.close()
