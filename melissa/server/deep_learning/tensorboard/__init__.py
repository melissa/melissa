from melissa.server.deep_learning.tensorboard.base_logger import (
    TensorboardLogger,
    convert_tb_logs_to_df
)
from melissa.server.deep_learning import FrameworkType


__all__ = [
    "TensorboardLogger",
    "convert_tb_logs_to_df",
    "make_tb_logger"
]


def make_tb_logger(framework_t: FrameworkType,
                   rank: int = 0,
                   logdir: str = "tensorboard",
                   disable: bool = False,
                   debug: bool = False) -> TensorboardLogger:

    """Factory function to create a TensorBoard logger based on the specified deep learning
    framework.

    ### Parameters
    - **framework_t** (`FrameworkType`): The type of framework (`TORCH`, `TENSORFLOW`,
    or `DEFAULT`).
    - **rank** (`int`, optional): Rank of the process (used for distributed training).
    Defaults to `0`.
    - **logdir** (`str`, optional): Directory where TensorBoard logs are stored.
    Defaults to `"tensorboard"`.
    - **disable** (`bool`, optional): If `True`, disables logging. Defaults to `False`.
    - **debug** (`bool`, optional): If `True`, enables debug mode for the logger.
    Defaults to `False`.

    ### Returns
    - `TensorboardLogger`: An instance of the appropriate TensorBoard logger
    (`TorchTensorboardLogger` or `TfTensorboardLogger`).

    ### Raises
    - `ModuleNotFoundError`: If `DEFAULT` is selected but neither PyTorch nor TensorFlow
    loggers are available.
    - `ValueError`: If an unsupported framework type is provided."""

    if framework_t is FrameworkType.TORCH:
        from melissa.server.deep_learning.tensorboard.torch_logger import TorchTensorboardLogger
        return TorchTensorboardLogger(rank, logdir, disable, debug)

    elif framework_t is FrameworkType.TENSORFLOW:
        from melissa.server.deep_learning.tensorboard.tf_logger import TfTensorboardLogger
        return TfTensorboardLogger(rank, logdir, disable, debug)

    elif framework_t is FrameworkType.DEFAULT:
        try:
            from melissa.server.deep_learning.tensorboard.torch_logger import TorchTensorboardLogger
            return TorchTensorboardLogger(rank, logdir, disable, debug)
        except ModuleNotFoundError:
            pass

        try:
            from melissa.server.deep_learning.tensorboard.tf_logger import TfTensorboardLogger
            return TfTensorboardLogger(rank, logdir, disable, debug)
        except ModuleNotFoundError:
            pass

        raise ModuleNotFoundError("Neither Torch nor TensorFlow TensorBoard loggers are available.")

    else:
        raise ValueError(f"Unsupported framework type: {framework_t}")
