from typing import Any, Optional, Union, List
from torch.utils.tensorboard import SummaryWriter
from melissa.server.deep_learning.tensorboard.base_logger import TensorboardLogger
from matplotlib.figure import Figure


class TorchTensorboardLogger(TensorboardLogger):
    def __init__(
        self,
        rank: int = 0,
        logdir: str = "tensorboard",
        disable: bool = False,
        debug: bool = False,
    ) -> None:

        super().__init__(disable, debug)
        if not self.disable:
            self._writer = SummaryWriter(
                f"{logdir}/gpu_{rank}", filename_suffix=f"rank_{rank}"
            )
            layout = {
                "Server stats": {
                    "loss": ["Multiline", ["Loss/train", "Loss/valid"]],
                    "put_get_time": ["Multiline", ["put_time", "get_time"]],
                },
            }
            self.writer.add_custom_scalars(layout)

    def log_scalar(self, tag: str, scalar_value: Any, step: int):
        if not self.disable and self.writer is not None:
            self.writer.add_scalar(tag, scalar_value, step)

    def log_scalars(self, main_tag: str, tag_scalar_dict: dict, step: int):
        if not self.disable and self.writer is not None:
            self.writer.add_scalars(main_tag, tag_scalar_dict, step)

    def log_histogram(self, tag: str, values: Any, step: Optional[int] = None):
        if not self.disable and self.writer is not None:
            self.writer.add_histogram(tag, values, step)

    def log_scalar_dbg(self, tag: str, scalar_value: Any, step: int):
        if not self.disable and self.writer is not None and self.debug:
            self.writer.add_scalar(tag, scalar_value, step)

    def log_figure(
        self,
        tag: str,
        figure: Union[Figure, List[Figure]],
        step: Optional[int] = None,
        close: bool = True,
    ):
        if not self.disable and self.writer is not None:
            self.writer.add_figure(tag, figure, step, close=close)

    def close(self):
        if not self.disable and self.writer is not None:
            self.writer.flush()
            self.writer.close()
