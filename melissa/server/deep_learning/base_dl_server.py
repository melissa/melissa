"""This script defines a base class for deep learning."""

import sys
import logging
import os
from threading import Thread
import time
from abc import abstractmethod
from functools import wraps
from typing_extensions import override
from typing import Any, Callable, Dict, Optional, Tuple

from mpi4py import MPI

import cloudpickle
from melissa.launcher import message
from melissa.server.main import ServerError
from melissa.server.base_server import (
    ReceptionError,
    BaseServer,
    UnsupportedConfiguration,
)
from melissa.server.deep_learning import FrameworkType
from melissa.server.deep_learning.train_workflow import TrainingWorkflowMixin
from melissa.server.deep_learning.experimental_monitoring import (  # type: ignore
    ExperimentalMonitoringMixin
)
from melissa.server.deep_learning.dataset import (
    MelissaIterableDataset,
    make_dataset,
    make_dataloader,
)
from melissa.server.deep_learning.reservoir import BaseQueue, make_buffer, BufferType
from melissa.server.deep_learning.tensorboard import (
    TensorboardLogger,
    convert_tb_logs_to_df,
    make_tb_logger
)
from melissa.server.simulation import PartialSimulationData, Simulation, SimulationData
from melissa.utility.networking import get_rank_and_num_server_proc
from melissa.utility.idr_torch import SlurmEnvironment


logger = logging.getLogger(__name__)


class BaseDLServerError(Exception):
    """Any base dl server non-training error."""

    def __init__(self, msg) -> None:
        self.msg = msg

    def __str__(self) -> str:
        return f"OtherError: {self.msg}"


class TrainingError(Exception):
    """Errors from the training loop."""

    def __init__(self, msg) -> None:
        self.msg = msg

    def __str__(self) -> str:
        return f"Training Error: {self.msg}"


# TODO: Remove ExperimentalMonitoringMixIn the future.
class DeepMelissaServer(
    BaseServer,
    ExperimentalMonitoringMixin,
    TrainingWorkflowMixin,
):
    """`DeepMelissaServer` is designed for studies involving deep learning workflows.
    It manages data buffering, logging, and configuration necessary for distributed training.

    ### Parameters
    - **config_dict** (`Dict[str, Any]`): A dictionary containing configuration settings for
    initializing the server.

    ### Attributes
    - **dl_config** (`Dict[str, Any]`): Dictionary containing deep learning-specific configurations.
    - **batch_size** (`int`): The size of batches used for training.
    - **per_server_watermark** (`int`): Watermark level to determine data buffering thresholds.
    - **buffer_size** (`int`): Total size of the buffer to store training data.
    - **pseudo_epochs** (`int`): Number of pseudo-epochs to replicate epoch-based training
    in an online setting.
    - **current_sample_count** (`int`): Counter for the number of samples received.
    - **nb_expected_batches** (`int`): Number of expected batches; deterministic for FIFO and FIRO.
    - **nb_batches_update** (`int`): Number of batches after which updates are triggered during
    training (For example, for validation).
    - **checkpoint_interval** (`int`): Number of batches after which checkpointing is triggered.
    Defaults to `nb_batches_update`.
    - **setup_slurm_ddp** (`bool`): Boolean flag to configure SLURM-based
    distributed data parallel training.
    - **_model** (`Any`): Model to be trained.
    - **_optimizer** (`Any`): Training optimizer.
    - **__buffer** (`BaseQueue`): Instance of a buffer object (FIFO, FIRO, or Reservoir) to
    manage training data.
    - **__dataset** (`Dataset`): Dataset interface to provide training data, integrates with the
    buffer and allows transformations via `process_simulation_data`.
    - **_framework_t** (`FrameworkType`): Framework type for the iterable dataset to be
    instantiated with `make_dataset`. It can either be `DEFAULT`, `TORCH`, or `TENSORFLOW`.
    - **_tb_logger** (`TensorboardLogger`): Logger to handle logging of metrics for
    visualization during training."""

    def __init__(self, config_dict: Dict[str, Any], **kwargs) -> None:

        super().__init__(config_dict, **kwargs)

        # connection request requires this to be set
        self._learning = 2
        self._check_group_size()

        # this lets us ping the launcher periodically
        # ensuring the launcher does not assume the server is dead.
        self.__run_handler_thread: bool = False
        self.__pinger_thread: Thread = Thread(name="pinger", target=self.__loop_pings)
        self.__receiver_thread: Thread = Thread(target=self._receive)
        self.__first_completion: bool = False

        self.dl_config: Dict[str, Any] = config_dict["dl_config"]
        self.batch_size: int = self.dl_config["batch_size"]
        self.per_server_watermark: int = self.dl_config["per_server_watermark"]
        self.buffer_size: int = self.dl_config["buffer_size"]
        self.pseudo_epochs: int = self.dl_config.get("pseudo_epochs", 1)
        self.current_sample_count: int = 0
        self.nb_expected_batches: int = 1
        self.nb_expected_time_steps: int = 1
        self.nb_batches_update: int = self.dl_config["nb_batches_update"]
        self.checkpoint_interval: int = self.dl_config.get(
            "checkpoint_interval",
            self.nb_batches_update
        )

        slurm_env = SlurmEnvironment()
        self.setup_slurm_ddp: bool = (
            slurm_env.nnodes > 1
            and len(slurm_env.gpu_ids) >= 1
        )

        self.batch_offset: int = 0
        self._model: Any = None
        self._optimizer: Any = None

        # str -> enum
        self._tb_logger: Optional[TensorboardLogger] = None
        self._framework_t: FrameworkType = FrameworkType.DEFAULT

        self._valid_dataloader: Any = None

    def __configure_data_collection(self) -> None:
        """Instantiates the data collection i.e buffer, dataset, and dataloader.
        Users must implement `process_simulation_data`."""

        buffer_t: BufferType = BufferType[self.dl_config.get("buffer", "FIRO")]
        # initialize tensorboardLogger
        self._tb_logger = make_tb_logger(
            framework_t=self._framework_t,
            rank=self.rank,
            disable=not self.dl_config["tensorboard"],
            debug=self.verbose_level >= 3
        )

        self._buffer: BaseQueue = make_buffer(
            buffer_size=self.buffer_size,
            buffer_t=buffer_t,
            per_server_watermark=self.per_server_watermark,
            pseudo_epochs=self.pseudo_epochs,
        )

        self.__dataset: MelissaIterableDataset = make_dataset(
            framework_t=self._framework_t,
            buffer=self._buffer,
            tb_logger=self._tb_logger,
            config_dict=self.config_dict,
            transform=self.process_simulation_data,
        )

        self._train_dataloader = make_dataloader(
            framework_t=self._framework_t,
            iter_dataset=self.dataset,
            batch_size=self.batch_size,
            num_workers=0,
            drop_last=True,
        )

    @property
    def time_steps_known(self) -> bool:
        return self.nb_expected_batches != 0

    @property
    def tb_logger(self) -> TensorboardLogger:
        assert self._tb_logger is not None
        return self._tb_logger

    @property
    def buffer(self) -> BaseQueue:
        return self._buffer

    @property
    def optimizer(self) -> Any:
        if self._optimizer is None:
            raise AttributeError(
                "Parent classes rely on `self.optimizer`. It must be set by the user."
            )
        return self._optimizer

    @optimizer.setter
    def optimizer(self, optimizer: Any) -> None:
        self._optimizer = optimizer

    @property
    def model(self) -> Any:
        if self._model is None:
            raise AttributeError(
                "Parent classes rely on `self.model`. It must be set by the user."
            )
        return self._model

    @model.setter
    def model(self, model: Any) -> None:
        self._model = model

    @property
    def dataset(self) -> MelissaIterableDataset:
        return self.__dataset

    @dataset.setter
    def dataset(self, dataset: MelissaIterableDataset) -> None:
        self.__dataset = dataset

    @property
    def valid_dataloader(self) -> Any:
        return self._valid_dataloader

    @valid_dataloader.setter
    def valid_dataloader(self, dataloader: Any) -> None:
        if self.rank == 0:
            logger.info(f"Rank {self.rank}>> Setting valid_dataloader.")
            self._valid_dataloader = dataloader

    @override
    def _check_group_size(self) -> None:
        """Checks if the group size was correctly set."""

        if self.group_size > 1 and self.nb_clients % self.group_size != 0:
            m = "Incorrect group_size, please remove or adjust this option"
            logger.error(m)
            self._catch_error = True
            raise UnsupportedConfiguration(m)

    def __loop_pings(self) -> None:
        """Maintains communication with the launcher to ensure it
        does not assume the server has become unresponsive."""

        while self.__run_handler_thread:
            self._launcherfd.send(self._encode_msg(message.Ping()))
            logger.debug(f"Rank {self.rank}>> pinging launcher.")
            time.sleep(5)

    def __start_pinger_thread(self) -> None:
        """Starts the pinger thread and set the flag."""

        self.__run_handler_thread = True
        self.__pinger_thread.start()

    def __stop_pinger_thread(self) -> None:
        """Stops the pinger thread and unsets the flag."""

        self.__run_handler_thread = False
        if self.__pinger_thread.is_alive():
            self.__pinger_thread.join(timeout=1.0)

    @override
    def start(self) -> None:
        """The main entrypoint for the server events."""

        try:
            self.__configure_data_collection()
            if not self._restart:
                self._launch_first_groups()
            if not self.setup_slurm_ddp:
                self.setup_environment()
            else:
                self._setup_environment_slurm()
            if not self._restart:
                self.model, self.optimizer = self.prepare_training_attributes()
            else:
                # the reinitialization from checkpoint occurs here
                logger.info(
                    f"Rank {self.rank}>> Continuing from checkpoint restart-count={self._restart}"
                )
                self._restart_from_checkpoint()
                if self.rank == 0:
                    self._kill_and_restart_simulations()
            self.__set_expected_batches_samples_watermark()

            self._server_online()

            if self._tb_logger is not None:
                self._tb_logger.close()
                if self.dl_config.get("convert_log_to_df", False):
                    convert_tb_logs_to_df(self.rank)

            self._server_finalize()

        except ServerError as e:
            logger.exception(e)
            raise e

    @override
    def _server_online(self) -> None:
        """Initiates data collection, and
        directs the custom methods for acting on collected data."""

        # put server receive on a separate thread.
        # should not be accesse by user
        self.__receiver_thread.start()
        try:
            self.train()
        except TrainingError as exc:
            self._catch_error = True
            logger.exception(f"Exception was raised in the training thread: \n {exc}")
            if self.no_fault_tolerance:
                self._server_finalize(exit_=1)
                sys.exit(1)

    @override
    def _server_finalize(self, exit_: int = 0):
        """Finalizes the server operations.

        ### Parameters
        - **exit_** (`int`, optional): The exit status code indicating
        the outcome of the server's operations.
        Defaults to 0, which signifies a successful termination."""

        if self.__receiver_thread.is_alive():
            self.__receiver_thread.join(timeout=1.0)

        self.comm.Barrier()
        self.__stop_pinger_thread()

        self.__dataset.signal_reception_over()

        super()._server_finalize(exit_)

    def __signal_end_of_reception(self) -> None:
        """Unsets the reception when all data has been received,
        and notifies `MelissaIterableDataset` to stop the batch formation."""

        self._is_receiving = False
        self.__dataset.signal_reception_over()
        logger.debug("Signal end of reception.")

    @override
    def _receive(self) -> None:
        """ "Handles data coming from the server object."""

        try:
            self._is_receiving = True
            while not self._all_done():
                start = time.time()
                data = self.poll_sockets()

                if data is not None and isinstance(data, SimulationData):
                    logger.debug(
                        f"Rank {self.rank}>> "
                        f"sim-id={data.simulation_id}, "
                        f"time-step={data.time_step} received."
                    )
                    self._buffer.put(data)
                    self.current_sample_count += 1
                    self._tb_logger.log_scalar(  # type: ignore
                        "put_time", time.time() - start, self.current_sample_count
                    )

                    if self.current_sample_count % 10000 == 0:
                        consumed, _ = self.get_memory_info_in_gb()
                        self._tb_logger.log_scalar(  # type: ignore
                            "memory_consumed", consumed, self.current_sample_count
                        )
            # endwhile
            self.comm.Barrier()
            self.__signal_end_of_reception()
            # ping until the training loop is done.
            self.__start_pinger_thread()

        except ReceptionError as exc:
            self._catch_error = True
            logger.exception(f"Exception was raised in the receiving thread: \n {exc}")
            if self.no_fault_tolerance:
                self.__signal_end_of_reception()
                logger.warning(
                    f"Rank {self.rank}>> Training will stop once the buffers are empty."
                )
                self._server_finalize(exit_=1)
                sys.exit(1)

    @override
    def _process_partial_data_reception(
        self, simulation: Simulation, simulation_data: PartialSimulationData
    ) -> None:
        """Partial data has to be assembled for `DeepMelissaServer`.
        Do not perform anything."""
        return None

    @override
    def _process_complete_data_reception(
        self, simulation: Simulation, simulation_data: PartialSimulationData
    ) -> SimulationData:

        # extract actual data from `PartialSimulationData` object.
        all_fields_data: Dict[str, Any] = {
            key: val.data
            for key, val in simulation.get_data(
                simulation_data.client_rank, simulation_data.time_step
            ).items()
            if isinstance(val, PartialSimulationData)
        }

        # dereference `received_simulation_data` as we will put the returned data in the buffer.
        simulation.clear_data(simulation_data.client_rank, simulation_data.time_step)

        if not self.__first_completion:
            self.__first_completion = True
            _, total = self.get_memory_info_in_gb()
            expected_buffer_consumption = (32 / 8) * self.buffer_size
            expected_buffer_consumption = expected_buffer_consumption * sum(
                v.size for v in all_fields_data.values()
            )
            expected_buffer_consumption /= 1024**3
            if expected_buffer_consumption / total < 0.2:
                logger.warning(
                    f"Rank {self.rank}>> [Suggestion] Buffer size can be increased. "
                    f"Buffer/Main memory={expected_buffer_consumption:.2f}/{total:.2f} GB"
                )

        return SimulationData(
            simulation_data.simulation_id,
            simulation_data.time_step,
            all_fields_data,
            simulation.parameters,
        )

    @override
    def _validate_data(self, simulation_data: PartialSimulationData) -> bool:
        """Validates the simulation data."""

        sim_id, field, time_step = (
            simulation_data.simulation_id,
            simulation_data.field,
            simulation_data.time_step,
        )
        group_id = self._get_group_id_by_simulation(sim_id)

        # handle termination messages
        if field == "termination":
            logger.info(
                f"Rank {self.rank}>> [Termination] sim-id={sim_id}, "
                f"total time-steps={time_step} received as expected."
            )
            # modify the time steps received accordingly
            if self.nb_expected_batches == 0:
                self.nb_time_steps += time_step
                self._groups[group_id].simulations[sim_id].nb_time_steps = time_step
                logger.info(f"Rank {self.rank}>> sim-id={sim_id} finished sending.")
                self.nb_finished_simulations += 1
            return False

        # apply validation checks
        if field not in self.fields:
            if field != "termination":
                logger.warning(
                    f'Rank {self.rank}>> [Bad] sim-id={sim_id}, field="{field}"'
                )
            return False

        return super()._validate_data(simulation_data)

    @override
    def _write_final_report(self) -> None:

        total_batches = self.comm.allreduce(
            self.dataset.get_sample_number() // self.batch_size,
            op=MPI.SUM
        )
        if self.rank == 0:
            logger.info(f" - Number of global batches: {total_batches}")
        super()._write_final_report()

    def __set_expected_batches_samples_watermark(self) -> None:
        """Computes and sets the expected samples and batches per server process."""

        # standard case where nb_time_steps is given in the config file
        if self.nb_time_steps > 0:
            # ensure watermark is sufficient
            self.__check_water_mark()

            # Account for possible accumulated shift
            self.nb_expected_time_steps = (
                self.nb_clients // self.comm_size
            ) * self.nb_time_steps
            self.nb_expected_batches = (
                self.nb_expected_time_steps // self.batch_size * self.pseudo_epochs
            )

            if (
                self.pseudo_epochs > 1
                and self.buffer_size != self.nb_expected_time_steps
            ):
                logger.warning(
                    "User tried using `pseudo_epochs` with `buffer_size` smaller than expected "
                    "samples. Setting `buffer_size` to number of expected time steps."
                    f"({self.nb_expected_time_steps})."
                )
                self.buffer_size = self.nb_expected_time_steps

            logger.info(
                f"Expecting {self.nb_expected_time_steps} "
                f"samples across {self.nb_expected_batches} batches."
            )
        # when `nb_time_steps` is not known a priori
        else:
            logger.info("Number of expected samples a priori unknown.")
            self.nb_expected_batches = 0

    def __check_water_mark(self) -> None:
        """Ensures there are sufficient samples to reach the `per_server_watermark`."""

        total_time_steps = self.nb_time_steps * self.nb_clients
        samples_per_server = total_time_steps // self.comm_size
        if not self.dl_config["per_server_watermark"] <= samples_per_server:
            raise UnsupportedConfiguration(
                "Insufficient samples to reach `per_server_watermark`. "
                "please increase `nb_time_steps`, "
                "or decrease `per_server_watermark`."
            )

    def other_processes_finished(self, batch_idx: int) -> bool:
        """Checks if other server processes have finished emptying their buffers.

        ### Parameters
        - **batch_idx** (`int`): The current batch number being processed.

        ### Returns
        - **`bool`**: if all other server processes have emptied their buffers."""

        logger.debug(
            f"Rank {self.rank}>> batch-idx={batch_idx}, "
            f"expected-batches >= {self.nb_expected_batches}"
        )

        # ensure self.__dataset._is_receiving is in sync across all server
        # processes.
        data_available: bool = self._synchronize_data_availability()

        # in case of pseudo_offline training, we want to avoid a
        # server timeout so we ping the launcher with time_monitor
        if not data_available:
            # at this point the total number of expected samples should be known
            # and used to update the value of self.nb_expected_batches
            if self.nb_expected_batches == 0:
                # per client number of expected time-steps
                self.nb_time_steps //= self.nb_clients
                self.__set_expected_batches_samples_watermark()
            logger.debug(
                f"Rank {self.rank}>> One of the server processes finished receiving at "
                f"batch_idx={batch_idx}, "
                f"expected-batches >= {self.nb_expected_batches}"
            )

        return not data_available

    def _synchronize_data_availability(self) -> bool:
        """Coordinates the dataset data availability status across all
        server processes. This usually requires a library
        specific all_reduce function (e.g. `dist.all_reduce` in pytorch).

        Default behaviour is to check whether the buffer is empty across all MPI ranks.
        If at least one rank, finishes, then stop the training for all ranks."""

        local_status = int(self.dataset.has_data)
        global_status = self.comm.allreduce(local_status, op=MPI.SUM)

        return global_status == self.comm_size

    @override
    def checkpoint_state(self) -> None:
        """Checkpoint the current state of the server."""

        if self.no_fault_tolerance:
            return

        self._save_base_state()
        # serialize the self._buffer.queue and then pickle it
        with open(f"checkpoints/{self.rank}/buffer_state.pkl", "wb") as file_:
            cloudpickle.dump(self._buffer.save_state(), file_)

    @override
    def _restart_from_checkpoint(self) -> None:
        """Restarts the server object from a checkpoint."""

        self._load_base_state()

        if (
            not os.path.exists("checkpoints/model.pt")
            or not os.path.exists(f"checkpoints/{self.rank}/buffer_state.pkl")
            or not os.path.exists(f"checkpoints/{self.rank}/buffer_state.pkl")
        ):
            raise UnsupportedConfiguration(
                f"Rank {self.rank}>> No checkpoints and/or buffer state were found."
                "Please make sure that fault-tolerance is set."
            )

        logger.info(f"Rank {self.rank}>> Restarting from checkpoint.")
        with open(f"checkpoints/{self.rank}/buffer_state.pkl", "rb") as file_:
            state = cloudpickle.load(file_)
            self._buffer.load_from_state(state)

        # lib specific loading method (torch vs tf)
        self._load_model_from_checkpoint()

    @abstractmethod
    def process_simulation_data(self, data: SimulationData, config_dict: dict) -> Any:
        """Transforms data while creating batches with `MelissaIterableDataset`.
        See `SimulationData` for usage of attributes associated with the received data.

        ### Parameters
        - **data** (`SimulationData`): The data message received from the simulation
        (pulled from the buffer).
        - **config_dict** (`Dict[str, Any]`): A dictionary containing configuration settings.

        ### Returns
        - **`Any`**: Transformed data before creating a batch from it."""
        raise NotImplementedError("This method must be implemented in a subclass.")

    def validation(self, batch_idx: int):
        """Predefined validation loop agnostic of frameworks."""

        logger.info(f"Rank {self.rank} Running validation at batch_idx={batch_idx}")
        self._on_validation_start(batch_idx)
        for v_batch_idx, v_batch in enumerate(self._valid_dataloader):
            self.validation_step(v_batch, v_batch_idx, batch_idx)
        self._on_validation_end(batch_idx)

    def train(self) -> None:
        """Predefined training loop agnostic of frameworks."""

        try:
            valid_found = self._valid_dataloader is not None
            if not valid_found:
                logger.warning(
                    f"Rank {self.rank}>> [Ignorable] Attribute `valid_dataloader` must be set "
                    "to perform validation."
                )

            self._on_train_start()
            for batch_idx, batch in enumerate(self._train_dataloader):
                batch_idx += self.batch_offset
                logger.debug(f"Rank {self.rank}>> Training on batch-idx={batch_idx}")
                if self.other_processes_finished(batch_idx):
                    logger.info(
                        f"Rank {self.rank}>> At least one other process has finished. "
                        "Break training loop."
                    )
                    break

                self._on_batch_start(batch_idx)
                self.training_step(batch, batch_idx)
                self._on_batch_end(batch_idx)
                # TODO: Multi-GPU validation (not a priority)
                # it should be framework specific thing. and may require overriding
                # this entire if block
                if (
                    self.rank == 0
                    and valid_found
                    and batch_idx > 0
                    and (batch_idx + 1) % self.nb_batches_update == 0
                ):
                    self.validation(batch_idx)

                self._checkpoint(batch_idx)
            # end training loop
            self._on_train_end()
        except TrainingError as exc:
            raise exc

    @abstractmethod
    def _setup_environment_slurm(self) -> None:
        """Sets up the unique Distributed Data Parallel (DDP) environment using SLURM
        as per the recommendations from: [Jean-Zay Documentation]
        (http://www.idris.fr/eng/jean-zay/gpu/jean-zay-gpu-torch-multi-eng.html)"""
        raise NotImplementedError("This method must be implemented in a subclass.")

    @abstractmethod
    def prepare_training_attributes(self) -> Tuple[Any, Any]:
        """Required to configure server's `self.model` and `self.optimizer` attributes,
        preparing them for initialization.

        ### Returns
        - `Tuple[Any, Any]`:
            - **model** (`Any`): Instantiated model object.
            - **optimizer** (`Any`): Instantiated optimizer object."""

        raise NotImplementedError(
            "This method must be implemented in a subclass. "
            "Parent classes rely on `self.model`, and `self.optimizer` "
            "which are set from the return values of this method."
        )

    @abstractmethod
    def checkpoint(self, batch_idx: int, path: str = "checkpoints"):
        """The method called to initiate full tree checkpointing. This is
        specific to `torch` or `tensorflow` server."""
        raise NotImplementedError("This method must be implemented in a subclass.")

    def _checkpoint(self, batch_idx: int, path: str = "checkpoints"):
        """Checkpointing at specific interval. The interval defaults `nb_batches_update`."""
        if batch_idx > 0 and (batch_idx + 1) % self.checkpoint_interval == 0:
            self.checkpoint(batch_idx, path)

    @abstractmethod
    def _load_model_from_checkpoint(self):
        """Library specific model loading function.  This is
        specific to `torch` or `tensorflow` server."""
        raise NotImplementedError("This method must be implemented in a subclass.")


def rank_zero_only(fn_: Callable) -> Callable:
    """Function that can be used as a decorator to enable a function/method
    being called only on rank 0. Inspired by pytorch_lightning"""

    rank, _ = get_rank_and_num_server_proc()

    @wraps(fn_)
    def wrapped_fn(*args: Any, **kwargs: Any) -> Optional[Any]:
        if rank == 0:
            return fn_(*args, **kwargs)
        return None

    return wrapped_fn
