from enum import Enum


class FrameworkType(Enum):
    """Enum based on framework.

    - DEFAULT = 0
    - TORCH = 1
    - TENSORFLOW = 2"""
    DEFAULT = 0
    TORCH = 1
    TENSORFLOW = 2
