"""This script defines `ExperimentalDeepMelissaActiveSamplingServer` which is a base class
for active sampling implementation."""

# pylint: disable=R0902,W0106

import os
import logging
import threading
from typing import Any, Dict, List, Tuple, Type, Union
from typing_extensions import override
import time
import cloudpickle

from melissa.scheduler import job
from melissa.server.base_server import UnsupportedConfiguration
from melissa.server.deep_learning import DeepMelissaServer
from melissa.server.deep_learning import active_sampling
from melissa.server.deep_learning.active_sampling.exceptions import ActiveSamplingError
from melissa.server.deep_learning.active_sampling.parameters import ExperimentBreeder
from melissa.server.parameters import ParameterSamplerType, ParameterSamplerClass

logger = logging.getLogger(__name__)


class ExperimentalDeepMelissaActiveSamplingServer(DeepMelissaServer):
    """Server for active sampling over a predefined number of clients, resampling
    clients that have not been submitted by the launcher. This server is currently
    limited to a single-rank server environments.

    - Performs periodic resampling to update simulation parameters.
    - Ensures efficient resource use by managing simulation jobs through a job margin.
    - Utilizes threading for asynchronous resampling events.
    - Tracks batches and offsets for resampling processes.

    ### Parameters
    - **config_dict** (`Dict[str, Any]`): A dictionary containing configuration settings for
    initializing the server.

    ### Attributes
    - **ac_config** (`Dict[str, Any]`): Configuration dictionary for active sampling parameters.
    - **ac_nn_threshold** (`int`): Number of neural network updates before triggering resampling.
    - **ac_sim_threshold** (`int`): Minimum number of simulations remaining to allow resampling.
    - **ac_skip_nb_batches** (`int`): Number of training batches to skip before triggering
    resampling.
    - **ac_window_size** (`int`): Specified sliding window size. Used for triggering resampling
    when these many simulations have finished. Default is -1 and, therefore, it will satisfy each
    trigger.
    - **__resample_event** (`threading.Event`): Event to signal resampling readiness.
    - **__resampler_thread** (`threading.Thread`): Thread managing the resampling process.
    - **nb_batches_per_resampling** (`List[int]`): List tracking batch counts between resamplings.
    - **offset_sim_ids_per_resampling** (`List[int]`): Simulation id offsets per resampling phase.
    - **max_breeding_count** (`int`): Limit on the number of simulations bred during resampling.
    - **__job_limit** (`int`): Maximum number of jobs the launcher can manage concurrently.
    - **__job_margin** (`int`): Margin used for preventing launcher-server inconsistencies.
    - **start_sim_id** (`int`): Starting simulation id for the current resampling phase."""

    def __init__(self, config_dict: Dict[str, Any]) -> None:

        super().__init__(config_dict)

        assert self.comm_size == 1, "Parallel server is not implemented for active sampling yet."

        # overriding the type of sampler
        self.ac_config: Dict[str, Any] = config_dict.get("active_sampling_config", {})
        if self.ac_config:
            self.ac_nn_threshold: int = self.ac_config.get("nn_updates", -1)
            self.ac_sim_threshold: int = self.ac_config.get("non_resampling_threshold", 50)
            self.ac_skip_nb_batches: int = self.ac_config.get("skip_nb_batches", 0)
            # default is -1
            self.ac_window_size: int = active_sampling.get_sliding_window_size()
            self.__resample_event: threading.Event = active_sampling.get_resampling_event()
            self.__resampler_thread: threading.Thread = threading.Thread(
                name="resampler-thread",
                target=self.__update_generator
            )
            self.nb_batches_per_resampling: List[int] = [0]
            self.offset_sim_ids_per_resampling: List[int] = [0]
            self.max_breeding_count: int = self.ac_config.get("breed_limit", -1)
            assert self.max_breeding_count <= self.nb_clients

            self.__job_limit: int = config_dict["launcher_config"]["job_limit"] - 1
            # The margin ensures that we do not have any incosistency with the launcher
            # it acts as a displacement/start-point after the current maximum simulation id
            # identified at a given instance. But, uncertain of the value.
            # It really depends on how slow the simulations are
            # slower they are, more we can reduce the margin.
            # and thus, have more scripts available for resampling.
            self.__job_margin: int = min(1000, self.__job_limit)
            if "slurm" in config_dict["launcher_config"]["scheduler"]:
                self.__job_margin = self.__job_margin // 2

            # starting simulation id for the current resampling.
            self.start_sim_id: int = 0
        else:
            self._catch_error = True
            raise UnsupportedConfiguration(
                f"Rank {self.rank}>> Active sampling configuration options."
            )

    @property
    def job_margin(self) -> int:
        """A jump to skip future scripts that could overlap
        i.e avoid inconsistencies while resampling."""
        return self.__job_margin

    @property
    def _get_current_starting_id(self) -> int:
        """Returns the simulation id which is a starting
        point when choosing resampling scripts."""
        return self.start_sim_id

    @override
    def _on_train_start(self):
        """Set the `TensorboardLogger` for the `active_sampling` module."""
        active_sampling.set_tb_logger(self.tb_logger)
        super()._on_train_start()

    @override
    def _on_batch_end(self, batch_idx) -> None:
        """Triggers the periodic resampling phase."""
        self.periodic_resampling(batch_idx)
        super()._on_batch_end(batch_idx)

    @override
    def set_parameter_sampler(self,
                              sampler_t: Union[ParameterSamplerType, Type[ParameterSamplerClass]],
                              **kwargs) -> None:
        """Sets the defined parameter sampler type. This dictates how parameters are sampled
        for experiments. This sampler type can either be pre-defined or customized
        by inheriting a pre-defined sampling class.

        This method is overridden for active sampling. It will first ensure if `sampler_t`
        is either `ParameterSamplerType.DEFAULT_BREED` or is a subclass to `ExperimentBreeder` and
        call `active_sampling.make_parameter_sampler`, Otherwise it will default to using
        regular (non-breed) samplers. This is especially useful when the users want to toggle
        between different samplers types without changing their codes.

        ### Parameters
        - **sampler_t** (`Union[ParameterSamplerType, Type[ParameterSamplerClass]]`):
            - `ParameterSamplerType`: Enum specifying pre-defined samplers.
            - `Type[ParameterSamplerClass]`: A class type to instantiate.
        - **kwargs** (`Dict[str, Any]`): Dictionary of keyword arguments.
        Useful to pass custom parameter as well as strict parameter such as
        `l_bounds`, `u_bounds`, `apply_pick_freeze`, `second_order`, `seed=0`, etc."""

        # if not provided by the users
        if "nb_params" not in kwargs:
            kwargs["nb_params"] = self.nb_parameters
        if "nb_sims" not in kwargs:
            kwargs["nb_sims"] = self.nb_clients
        if "apply_pick_freeze" in kwargs:
            del kwargs["apply_pick_freeze"]
        if "second_order" in kwargs:
            del kwargs["second_order"]
        # following is just renaming
        non_breed_sampler_t = kwargs.get("non_breed_sampling_strategy")
        if non_breed_sampler_t:
            kwargs["non_breed_sampler_t"] = ParameterSamplerType[non_breed_sampler_t]
            del kwargs["non_breed_sampling_strategy"]

        if (
            sampler_t is ParameterSamplerType.DEFAULT_BREED
            or (
                isinstance(sampler_t, type)
                and issubclass(sampler_t, ExperimentBreeder)
            )
        ):
            self._parameter_sampler = active_sampling.make_parameter_sampler(sampler_t, **kwargs)
        else:
            super().set_parameter_sampler(sampler_t, **kwargs)

    @override
    def checkpoint_state(self) -> None:
        """Checkpoints the current state of the server."""

        super().checkpoint_state()

        if self.rank == 0:
            metadata = {
                "offset_sim_ids_per_resampling": self.offset_sim_ids_per_resampling,
                "nb_batches_per_resampling": self.nb_batches_per_resampling
            }
            _temp = {}
            if os.path.exists(f'checkpoints/{self.rank}/metadata.pkl'):
                with open(f'checkpoints/{self.rank}/metadata.pkl', "rb") as f:
                    _temp = cloudpickle.load(f)
            _temp.update(metadata)

            active_sampling.checkpoint_state()

            with open(f'checkpoints/{self.rank}/metadata.pkl', 'wb') as f:
                cloudpickle.dump(_temp, f)

    @override
    def _restart_from_checkpoint(self) -> None:
        """Restarts the server object from a checkpoint."""

        super()._restart_from_checkpoint()

        metadata = {}
        with open(f'checkpoints/{self.rank}/metadata.pkl', 'rb') as f:
            metadata = cloudpickle.load(f)
        self.offset_sim_ids_per_resampling = metadata["offset_sim_ids_per_resampling"]
        self.nb_batches_per_resampling = metadata["nb_batches_per_resampling"]
        logger.info(f"METADATA: {metadata}")

        active_sampling.restart_from_checkpoint()

    def periodic_resampling(self, batch_idx: int) -> None:
        """Periodically triggers active resampling given the batch id and configured thresholds.

        It performs the following tasks:

        - Ensures resampling starts beyond specified `ac_skip_nb_batches` batches.
        - Ensures resampling starts when at least `sliding_window_size` number of
        simulations have finished to obtain fitnesses of correct length.
        - Resampling occurs when `batch_idx` is divisible by `ac_nn_threshold` and
        the server is ready to resample the next generation.
        - Calculates `start_sim_id` considering conflicts with currently running simulations.
        - Ensures resampling stops if remaining simulations fall below `ac_sim_threshold`.
        - Adjusts `max_breeding_count` to prevent exceeding the available simulations.
        - Launches a new thread to asynchronously trigger resampling and pauses for a second.

        ### Parameters
        - **batch_idx** (`int`): The current batch index."""

        if (
            batch_idx > self.ac_skip_nb_batches
            and self.nb_finished_simulations > self.ac_window_size
            and self.ac_nn_threshold > 0
            and batch_idx % self.ac_nn_threshold == 0
            and self.__is_resampling_ready()
        ):
            non_resample_window = self.nb_submitted_simulations - self.ac_sim_threshold
            # to obtain the maximum sim-id
            self.start_sim_id = self.__launcher_state_max_id() + self.job_margin

            if self.start_sim_id >= non_resample_window:
                # too few scripts left to update
                return

            if self.max_breeding_count > 0:
                is_last_resampling = \
                    self.start_sim_id + self.max_breeding_count >= non_resample_window
                # resample till the last sim-id
                if is_last_resampling:
                    self.max_breeding_count = -1

            print(f"About to trigger at batch_idx={batch_idx} start_sim_id={self.start_sim_id} "
                  f"max_breed_count={self.max_breeding_count}"
                  f"server max_sim_id={self._get_most_recent_sim_id()}", flush=True)

            # launch a thread for resampling to avoid stalling the training loop
            threading.Thread(
                target=active_sampling.trigger_sampling,
                kwargs={
                    "start_sim_id": self.start_sim_id,
                    "max_breeding_count": self.max_breeding_count
                }
            ).start()
            time.sleep(1 if self.max_breeding_count == -1 else 0.1)

    def __update_batches_per_resampling(self) -> None:
        """Updates the record of the number of batches processed since the last resampling phase.

        **Note:** _This is not an exact value when using the `Reservoir` buffer type as the batches
        keep forming as long as there is data available in the buffers._"""

        # Not an accurate value as the training never stops in case if Reservoir is used.
        nb_batches_so_far = self.dataset.get_sample_number() // self.batch_size
        self.nb_batches_per_resampling.append(nb_batches_so_far)

    def get_offset_sim_ids_per_resampling(self) -> List[int]:
        """Retrieves a list of offset simulation ids per resampling phase.

        ### Returns
        - `List[int]`: A list of offsets."""
        return self.offset_sim_ids_per_resampling

    def get_batch_count(self, phase_id: int = -1) -> int:
        """Calculates the number of batches processed for a resampling phase.

        ### Parameters
        - **phase_id** (`int`): A generation/phase id of the resampling generation.
        (Default is -1 i.e the most recent.)

        ### Returns
        - `int`: Number of batches processed."""
        return self.nb_batches_per_resampling[phase_id] - \
            self.nb_batches_per_resampling[phase_id - 1]

    def get_batch_counts_per_resampling(self) -> List[int]:
        """Calculates the number of batches processed during each resampling phase.

        ### Returns
        - `List[int]`: A list containing number of batches per resampling phase."""
        return list(
            map(
                self.get_batch_count,
                range(1, len(self.nb_batches_per_resampling))
            )
        )

    @override
    def _server_online(self) -> None:
        """Starts `__resampler_thread` before running the main server steps."""

        self.__resampler_thread.start()
        super()._server_online()

    @override
    def _server_finalize(self, exit_: int = 0):
        """Sets the `__resample_event`
        to join `__resampler_thread`, if it was waiting on it.

        Finalizes the server operations.

        ### Parameters
        - **exit_ (`int`, optional)**: The exit status code indicating
        the outcome of the server's operations.
        Defaults to 0, which signifies a successful termination."""

        self.__resample_event.set()  # if waiting
        self.__resampler_thread.join(timeout=5)
        super()._server_finalize(exit_)

    def __is_resampling_ready(self) -> bool:
        """Checks if the server is ready to trigger the resampling phase.

        - This method determines whether the server is ready for resampling by evaluating if
        there are enough remaining simulations (above a threshold) to justify initiating
        the resampling process.
        - The readiness condition depends on the difference between the total submitted simulations
        and the finished simulations, adjusted by the job limit, and compared to a
        predefined threshold.

        ### Returns
        - `bool`: if resampling is ready."""

        is_ready = False
        if active_sampling.exception() is None:
            remaining = self.nb_submitted_simulations - \
                (self.nb_finished_simulations + self.__job_limit)
            is_ready = remaining > self.ac_sim_threshold
        return is_ready

    def __update_generator(self) -> None:
        """Manages the active resampling process for the training server.
        This method waits for the resampling trigger and handles the entire resampling loop.
        If the minimum simulation criteria are met, it updates the parameter generator and
        resubmits the simulations scripts with updated parameters."""

        # local
        def __update_state():
            self.__update_batches_per_resampling()
            self.__resample_event.clear()
            nb_batches = self.get_batch_count()
            logger.info(f"Rank {self.rank}>> Model trained on (at least) nb_batches={nb_batches} "
                        "before current resampling.")

        try:
            while True:
                logger.info(f"Rank {self.rank}>> Waiting for resampling trigger.")
                self.__resample_event.wait()

                if self.nb_finished_simulations == self.nb_submitted_simulations:
                    __update_state()
                    return

                if not self.__is_resampling_ready():
                    __update_state()
                    logger.warning(f"Rank {self.rank}>> New scripts will not be generated "
                                   "as minimum simulation criteria was not met.")
                    return

                self._update_parameter_generator()
                max_sim_id = self._get_most_recent_sim_id()

                if max_sim_id is None:
                    __update_state()
                    return

                with self.consistency_lock:
                    first_id, last_id = \
                        self.__resubmit_simulations_with_updated_parameters()
                    __update_state()
                    logger.info(f"Rank {self.rank}>> Scripts of sim-ids from "
                                f"{first_id} to {last_id - 1} "
                                "have been updated.")
            # endwhile
        except ActiveSamplingError as e:
            self._catch_error = True
            logger.exception(str(e))

    def __resubmit_simulations_with_updated_parameters(self) -> Tuple[int, int]:
        """Resubmits simulations with updated parameters for active sampling.

        - Determines the range of simulation ids to resubmit.
        - Resubmits a set of simulations up to `max_breeding_count`.
        - Calls `_generate_client_scripts` for each simulation id in the
        determined range to create client scripts with updated parameters.
        - Tracks the starting simulation id for this resampling phase in
        `offset_sim_ids_per_resampling`.

        ### Returns
        - `Tuple[int, int]`: A tuple containing the starting and ending simulation ids
        of the resubmitted simulations."""

        first_id = self._get_current_starting_id
        last_id = first_id + self.max_breeding_count
        if self.max_breeding_count < 0:
            last_id = self.nb_submitted_simulations
        for sim_id in range(first_id, last_id):
            self._generate_client_scripts(sim_id, 1, create_new_group=True)

        self._clear_launcher_view()
        self.offset_sim_ids_per_resampling.append(first_id)

        return (first_id, last_id)

    def __launcher_state_max_id(self) -> int:
        """Retrieves the maximum simulation id currently in a running state within the launcher.

        - Ensures that the method identifies the highest job id in a "RUNNING" state.
        - Considers the fragmented state of the launcher,
        **[(4, W), (5, W), (6, R), (7, W), (8, R), || (9, W), (10, W), ...]** where jobs are
        "WAITING" or "RUNNING."
        - The method picks the index beyond the last running simulation id.

        ### Returns
        - `int`: The maximum simulation id that is currently running."""

        already_seen = set()
        max_id = self._get_most_recent_sim_id()
        view = self._get_launcher_view()
        for job_id, job_state in view:
            if job_id not in already_seen and job_state == job.State.RUNNING:
                max_id = max(max_id, job_id)
            already_seen.add(job_id)

        return max_id
