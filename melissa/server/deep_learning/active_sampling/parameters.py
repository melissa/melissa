"""This script defines the `ExperimentBreeder` base class and `DefaultBreeder`
which uses our breeding approach to sample the parameters."""

# pylint: disable=C0103,R0901,R0902,W0221,W0613,W0238

import logging
from typing import (
    Union, Any, List, Dict, Tuple,
    Iterable, Optional, Callable
)

import numpy as np
from numpy.typing import DTypeLike, NDArray
from scipy.stats import multivariate_normal
import matplotlib
import matplotlib.pyplot as plt

from melissa.server.deep_learning.active_sampling.exceptions import BreedError
from melissa.server.parameters import (
    StaticExperiment,
    RandomUniformSamplerMixIn,
    HaltonSamplerMixIn,
    LatinHypercubeSamplerMixIn,
    ParameterSamplerType
)
from melissa.server.deep_learning.active_sampling.breed_utils import get_fitnesses
from melissa.server.deep_learning.tensorboard import TensorboardLogger

logger = logging.getLogger(__name__)

# avoids a warning from matplotlib
# when we run it under a NOT main thread.
matplotlib.use('agg')


class ExperimentBreeder(RandomUniformSamplerMixIn,
                        HaltonSamplerMixIn,
                        LatinHypercubeSamplerMixIn,
                        StaticExperiment):
    """A base class for breeding and sampling experimental parameters. It initializes the parameters
    based on the chosen sampling method and logs relevant experiment data.

    Inherits from multiple mix-ins for sampling and static experiment management.

    **When inheriting this class, override `sample()` and optionally, `draw()`.**

    ### Parameters
    - **non_breed_sampler_t** (`ParameterSamplerType`, default=`RANDOM_UNIFORM`):
    The type of parameter sampler to use. Can be `RANDOM_UNIFORM`, `HALTON`, or `LHS`.
    **Note that the first generation will be populated from this sampler type.**

    ### Attributes
    - **checkpoint_data** (`Dict[str, Any]`): Stores experiment checkpoint data.
    - **tb_logger** (`TensorboardLogger`): Logger for tracking experiment metrics in TensorBoard.
    """

    def __init__(
        self,
        nb_params: int,
        nb_sims: int,
        l_bounds: List[Union[float, int]],
        u_bounds: List[Union[float, int]],
        seed: Optional[int] = None,
        dtype: DTypeLike = np.float32,
        non_breed_sampler_t: ParameterSamplerType = ParameterSamplerType.RANDOM_UNIFORM,
    ) -> None:

        StaticExperiment.__init__(
            self, nb_params, nb_sims, l_bounds, u_bounds, seed, dtype
        )

        if non_breed_sampler_t is ParameterSamplerType.HALTON:
            HaltonSamplerMixIn.__init__(self)
        elif non_breed_sampler_t is ParameterSamplerType.LHS:
            LatinHypercubeSamplerMixIn.__init__(self)
        else:
            RandomUniformSamplerMixIn.__init__(self)

        self.checkpoint_data: Dict[str, Any] = {}
        self.tb_logger: Optional[TensorboardLogger] = None

    def get_non_breed_samples(self, nb_samples: int = -1) -> NDArray:
        """Returns the `nb_samples` using the default (non-breed) sampler."""
        return self.sample(nb_samples)

    def next_parameters(self, **kwargs) -> Any:
        """This method is required for producing the next set of parameters i.e the next generation.
        It must be called only through `melissa.server.deep_learing.active_sampling` module and
        can be overridden.

        ### Parameters
        - **kwargs** (`Dict[str, Any]`): A keyword arguments for custom preprocessing.

        ### Returns
        - `Any`: A set of parameters for the next generation."""
        return self.sample(self.nb_sims)

    def set_tb_logger(self, tb_logger: TensorboardLogger) -> None:
        """Sets the Tensorboard logger."""
        self.tb_logger = tb_logger


class DefaultBreeder(ExperimentBreeder):
    """A class that extends `ExperimentBreeder` for managing breeding experiments with more advanced
    control over sampling and breeding parameters.

    ### Parameters
    - **sigma** (`Union[float, Iterable]`, default=0.005): Covariance initialization for breeding.
    - **start** (`float`, default=0.15): Starting breeding ratio.
    - **end** (`float`, default=0.75): Ending breeding ratio.
    - **breakpoint** (`int`, default=10): Number of steps in the breeding ratio transition.
    - **use_true_mixing** (`bool`, default=False): Use true mixing for breeding.
    - **log_extra** (`bool`, default=False): Log extra information for debugging.
    - **scatter_function** (`Callable[[NDArray], Tuple[str, NDArray, str, NDArray]]`,
    default=(lambda x: ("x0", x[:,0], "x1", x[:,1])): Scatter function that takes one NDarray
    `self.parameters`
    of shape `(nb_sims, nb_params)` and returns two Ndarays of shape (nb_sims,) for plotting
    scatter on X-Y axis labeled with provided strings.
    - **device** (`str`, default="cpu"): Device for computation, e.g., "cpu" or "cuda".

    ### Attributes
    - **sigma_opt** (`float`): Optimal minimum covariance value for breeding.
    - **covs** (`NDArray[np.float32]`): Covariance matrix for each simulation.
    - **Rs** (`NDArray`): Linearly spaced breeding ratios for each breeding step.
    - **R_i** (`int`): Current breeding ratio index.
    - **R** (`float`): Current breeding ratio.
    - **oob_factor** (`float`): Factor by which the covariance decreases
    when the child is out-of-bounds.
    - **max_oob_count** (`int`): Maximum allowed attempts for
    out-of-bounds children.
    - **oob_count** (`List[List[int]]`): List of out-of-bounds count for each simulation.
    - **parameters_is_bred** (`NDArray[np.bool_]`): Boolean array indicating whether a
    simulation's parameters have been bred."""

    def __init__(
        self,
        nb_params: int,
        nb_sims: int,
        l_bounds: List[Union[float, int]],
        u_bounds: List[Union[float, int]],
        seed: Optional[int] = None,
        dtype: DTypeLike = np.float32,
        non_breed_sampler_t: ParameterSamplerType = ParameterSamplerType.RANDOM_UNIFORM,
        sigma: Union[float, Iterable] = 0.005,
        start: float = 0.15,
        end: float = 0.75,
        breakpoint: int = 10,  # breakpoint is a python keyword too.
        use_true_mixing: bool = False,
        log_extra: bool = False,
        scatter_function: Callable[[NDArray], Tuple[str, NDArray, str, NDArray]] = (
            lambda x: ("x0", x[:, 0], "x1", x[:, 1])
        ),
    ) -> None:

        super().__init__(
            nb_params, nb_sims, l_bounds, u_bounds, seed, dtype, non_breed_sampler_t
        )

        self.sigma_opt: float = 0.05
        self._sigma_init(sigma)
        self.covs: NDArray[np.float32] = \
            np.full((self.nb_sims, self.nb_params), self.sigma).astype(np.float32)
        self.Rs: NDArray = np.linspace(start, end, max(breakpoint, 2), endpoint=True)
        self.R_i: int = 0
        self.R: float = start
        self.oob_factor: float = 0.3  # 3-sigma rule
        self.max_oob_count: int = 5
        self.oob_count: List[List[int]] = [[0]] * self.nb_sims
        self.parameters_is_bred: NDArray[np.bool_] = np.full(self.nb_sims, False)
        self.use_true_mixing: bool = use_true_mixing
        self.log_extra: bool = log_extra
        self.scatter_function: Callable[
            [NDArray], Tuple[str, NDArray, str, NDArray]
        ] = scatter_function

    def set_tb_logger(self, tb_logger: TensorboardLogger) -> None:
        """Sets the Tensorboard logger."""
        super().set_tb_logger(tb_logger)
        if self.log_extra:
            fig, axes = plt.subplots(1, 1, figsize=(8, 8))
            label_X, data_X, label_Y, data_Y = self.scatter_function(self.parameters)
            axes.set_xlabel(label_X)
            axes.set_ylabel(label_Y)
            axes.scatter(data_X, data_Y, c="red", s=10)
            axes.set_title("Initial parameters")
            self.plot_limits_xy = (axes.get_xlim(), axes.get_ylim())
            if self.tb_logger is not None:
                self.tb_logger.log_figure(
                    "Parents_vs_children", fig, close=True
                )

    def checkpoint_state(self) -> None:
        """Saves the current state for checkpointing."""

        self.checkpoint_data.update({
            "R_index": self.R_i,
            "oob_count": self.oob_count,
            "covariances": self.covs,
            "parameters_is_bred": self.parameters_is_bred
        })

        super().checkpoint_state()

    def restart_from_checkpoint(self) -> None:
        """Restores the state from a checkpoint."""

        super().restart_from_checkpoint()

        self.R_i = self.checkpoint_data["R_index"]
        self.oob_count = self.checkpoint_data["oob_count"]
        self.covs = self.checkpoint_data["covariances"]
        self.parameters_is_bred = self.checkpoint_data["parameters_is_bred"]

    def get_breeding_status_per_parameter(self) -> NDArray[np.bool_]:
        """Returns a boolean array stating which parameter indices were bred."""
        return self.parameters_is_bred

    def _sigma_init(self, sigma: Union[float, Iterable]) -> None:
        """Initializes the `sigma` values for the sampling process,
        ensuring they are within acceptable bounds.

        The function checks the type and size of `sigma`:

        - If `sigma` is a float, it initializes an array of the same size as
        the number of parameters (`nb_params`).
        - If `sigma` is an array-like object, it verifies that its size
        matches the number of parameters (`nb_params`), otherwise raises an assertion error.
        - The function then ensures that `sigma` values are within a valid range,
        scaling them if necessary to be within bounds defined by the parameter's upper and lower
        limits (`u_bounds` and `l_bounds`).

        ### Parameters
        - **sigma** (`Union[float, Iterable]`):
        The value(s) to initialize the sigma. This can either be a single float value
        (applied to all parameters) or an array-like object (list, tuple, or NDArray)
        specifying the sigma for each parameter.

        ### Raises
        - `RuntimeError`:
        If `sigma` is neither a float nor an iterable."""

        if isinstance(sigma, float):
            sigma = np.full((self.nb_params,), sigma)
        elif isinstance(sigma, (np.ndarray, list, tuple)):  # array_like but not string
            sigma = np.array(sigma).ravel()
            msg = (
                f"The size of array-like `sigma` must be equal to number "
                f"of dimensions of PDE domain: {sigma.size} != {self.nb_params}"
            )
            assert sigma.shape[0] == self.nb_params, msg
        else:
            raise BreedError(
                "Argument `sigma` for `samplers.Breed` must be one of `float` or `iterable`. "
                f"Given type was {type(sigma)}."
            )

        dim_scales = self.u_bounds - self.l_bounds
        max_sigma = dim_scales / 8
        if not np.logical_and(
            max_sigma >= sigma,  # sigma is not too big
            sigma >= (dim_scales * self.sigma_opt)  # sigma is not too small (or not set)
        ).all():
            self.sigma = np.where(sigma > max_sigma, max_sigma, sigma)
            self.sigma = np.where(
                sigma < dim_scales * self.sigma_opt, dim_scales * self.sigma_opt, sigma
            )
            logger.info(
                f"WARNING (samplers.Breed): Given value of sigma ({sigma}) "
                f"is updated to {self.sigma}"
            )
        else:
            self.sigma = sigma

    def next_parameters(  # type: ignore
        self, start_sim_id: int, max_breeding_count: int = -1
    ) -> None:
        """Override the parent class sampling method with custom breed-specific arguments.

        This method calculates the fitness for each simulation, selects breeding candidates,
        and returns a new set of parameters based on the custom breeding algorithm.

        ### Parameters
        - **start_sim_id** (`int`): The starting simulation id from which breeding should begin.
        - **max_breeding_count** (`int`, optional):
        The maximum number of breed iterations. (Default is -1 i.e all remaining parameters).

        **Note**: Unlike the parent, this method does not return anything as `__breed_algorithm`
        method directly modifies `parameters` attribute of the class."""

        fitness_per_sim, sim_ids = get_fitnesses()
        self.__breed_algorithm(
            fitness_per_sim, sim_ids, start_sim_id, max_breeding_count
        )

    def reset_index(self) -> None:
        """Placeholder method. `__breed_algorithm` updates `_current_idx` value directly."""
        return

    def __breed_algorithm(
        self,
        fitness_per_sim: Union[NDArray, list],
        sim_ids: Union[NDArray, list],
        start_sim_id: int,
        max_breeding_count: int = -1,
    ) -> None:
        """Breeding algorithm to generate new parameters based on simulation fitness.

        This method selects parent simulations based on their fitness scores, performs the breeding
        process to generate new parameters, and updates the breeding statistics. It also logs
        various statistics regarding the breeding process with `TensorboardLogger`.

        ### Parameters
        - **fitness_per_sim** (`Union[NDArray, list]`):
        Fitness values corresponding to each simulation.
        - **sim_ids** (`Union[NDArray, list]`): List or array of simulation ids.
        - **start_sim_id** (`int`): The starting simulation id for breeding.
        - **max_breeding_count** (`int`, optional): The maximum number of parameters to
        consider after `start_sim_id`.
        (Default is -1 i.e all remaining parameters)."""

        logger.info(f"Current breeding ratio={self.R:.3f}")
        fitness_per_sim = np.array(fitness_per_sim, dtype=np.float32)
        fitness = fitness_per_sim - fitness_per_sim.min()
        distribution = (fitness / fitness.sum()).ravel()

        self._current_idx = start_sim_id
        current_final_sim_id = (
            self.nb_sims
            if max_breeding_count < 0
            else start_sim_id + max_breeding_count
        )
        nb_children = current_final_sim_id - start_sim_id

        child_idx_list = np.arange(start_sim_id, current_final_sim_id)

        try:
            if self.use_true_mixing:
                parent_idx_list = np.random.choice(
                    sim_ids,
                    size=round(nb_children * self.R),
                    p=distribution.ravel()
                )
            else:
                parent_idx_list = np.random.choice(
                    sim_ids,
                    size=nb_children,
                    p=distribution.ravel()
                )
        except BreedError as e:
            logger.exception(f"Active sampling breed sim_ids={sim_ids}")
            logger.exception(f"Active sampling breed fitness={fitness_per_sim}")
            logger.exception(f"Active sampling breed loss dist={distribution}")
            raise BreedError(e) from e

        self.__set_children(parent_idx_list, child_idx_list)
        logger.info(
            f"Bred samples={self.parameters_is_bred[child_idx_list].sum()}/{nb_children}"
        )

        if self.log_extra and self.tb_logger:
            self.tb_logger.log_scalar(
                "Ratio_sampler/Empirical",
                self.parameters_is_bred[child_idx_list].sum() / nb_children,
                self.R_i,
            )
            self.tb_logger.log_scalar("Ratio_sampler/Expected", self.R, self.R_i)

            fig, axes = plt.subplots(1, 1, figsize=(9, 8))
            label_X, data_X, label_Y, data_Y = self.scatter_function(self.parameters)
            axes.set_xlabel(label_X)
            axes.set_ylabel(label_Y)
            axes.scatter(
                data_X[child_idx_list],
                data_Y[child_idx_list],
                c=np.where(self.parameters_is_bred[child_idx_list], "b", "g"),
                alpha=0.5,
                s=15,
            )
            scat = axes.scatter(
                data_X[sim_ids],
                data_Y[sim_ids],
                c=fitness_per_sim,
                cmap="Reds",
                marker="v",
                s=30,
                edgecolors="k",
                linewidths=0.3,
            )
            axes.set_title("blue - proposal, green - uniform, red - parents")
            fig.colorbar(scat, label="fitness (deltaloss)")
            xlim, ylim = (axes.get_xlim(), axes.get_ylim())
            self.plot_limits_xy = (
                (
                    min(xlim[0], self.plot_limits_xy[0][0]),
                    max(xlim[1], self.plot_limits_xy[0][1]),
                ),
                (
                    min(ylim[0], self.plot_limits_xy[1][0]),
                    max(ylim[1], self.plot_limits_xy[1][1]),
                ),
            )
            axes.set_xlim(self.plot_limits_xy[0])
            axes.set_ylim(self.plot_limits_xy[1])
            self.tb_logger.log_figure(
                tag="Parents_vs_children", figure=fig, step=self.R_i, close=True
            )

        # _R_step
        self.R_i += 1
        if self.R_i < len(self.Rs):
            self.R = self.Rs[self.R_i]

    def __set_proposal_child(self, parent_id: int, child_id: int) -> None:
        parent = self.parameters[parent_id]
        parent_oob = 0
        while parent_oob < self.max_oob_count:
            mvn_sampler = multivariate_normal(mean=parent, cov=self.covs[parent_id])
            child = mvn_sampler.rvs(size=1)
            # TODO division by proposal
            # self.proposal[parent_id] = mvn_sampler.pdf(parent)
            # self.proposal[child_id] = mvn_sampler.pdf(child)
            child_dim_inside = np.squeeze(
                np.logical_and(child > self.l_bounds, child < self.u_bounds)
            )
            if child_dim_inside.all():
                break
            # if out of boundary - sample again with lower covariance
            parent_oob += 1
            self.covs[parent_id][~child_dim_inside] *= self.oob_factor
        else:
            # if too close to boundary, parent is the child
            child = np.copy(parent)

        self.covs[child_id] = self.covs[parent_id]
        self.oob_count[parent_id].append(parent_oob)
        self.parameters_is_bred[child_id] = True
        self.parameters[child_id] = child

    def __set_random_child(self, child_id: int) -> None:
        child = self.get_non_breed_samples(nb_samples=1).astype(np.float32)
        self.covs[child_id] = self.sigma
        self.parameters_is_bred[child_id] = False
        self.parameters[child_id] = child

    def __set_children(self, parent_idx_list, child_idx_list) -> None:
        """Unoptimized breeding process to generate child parameters by sampling from
        the parent parameters and their covariance, with an additional check
        for out-of-bounds conditions. If a child falls out of bounds, its covariance is reduced,
        and the child is re-sampled.

        This method uses `scipy.stats.multivariate_normal` to perform
        sampling per simulation iteratively.

        ### Parameters
        - **parent_idx_list** (`NDArray`): List of indices for the parent simulations.
        - **child_idx_list** (`NDArray`):
        List of indices for the child simulations that are being bred."""

        random_parent_count = 0
        proposal_parent_idx_list = []
        if self.use_true_mixing:
            nb_total = len(child_idx_list)
            nb_proposal = len(parent_idx_list)
            nb_random = nb_total - nb_proposal
            idx_proposal, idx_random = 0, 0
            idx_total = 0
            # debug_string = []
            while idx_total < nb_total:
                child_id = child_idx_list[idx_total]
                if idx_proposal < nb_proposal and np.random.uniform(0, 1) < self.R:
                    # debug_string.append('1')
                    parent_id = parent_idx_list[idx_proposal]
                    self.__set_proposal_child(parent_id, child_id)
                    idx_proposal += 1
                    idx_total += 1
                    proposal_parent_idx_list.append(parent_id)
                elif idx_random < nb_random:
                    # debug_string.append('0')
                    self.__set_random_child(child_id)
                    idx_random += 1
                    idx_total += 1
                    random_parent_count += 1
            # debug_string = '\n'.join(''.join(debug_string[i * 50:(i + 1) * 50])
            # for i in range((len(debug_string) // 50 ) + 1))
            # print(debug_string)
            # logger.info(f'checking shuffle: \n {debug_string}')
        else:
            for child_id, parent_id in zip(child_idx_list, parent_idx_list):
                if np.random.uniform(0, 1) < self.R:
                    self.__set_proposal_child(parent_id, child_id)
                    proposal_parent_idx_list.append(parent_id)
                else:
                    self.__set_random_child(child_id)
                    random_parent_count += 1

        if self.log_extra:
            fig, ax = plt.subplots(1, 2, figsize=(16, 5))
            ax[0].set_title("Distribution of chosen parents")
            left_id, right_id = \
                min(proposal_parent_idx_list), max(proposal_parent_idx_list)
            ax[0].hist(
                proposal_parent_idx_list,
                bins=int(right_id - left_id) + 1,
                range=(left_id, right_id + 1),
                align="left",
                edgecolor="black",
            )
            ax[0].set_xlabel("Simulation id")
            ax[0].set_ylabel("Number times chosen")
            ax[0].set_axisbelow(True)
            ax[0].grid()

            ax[1].set_title('Distribution of "family" size')
            counts = np.unique(proposal_parent_idx_list, return_counts=True)[1]
            left_id, right_id = min(counts), max(counts)
            ax[1].hist(
                counts,
                bins=int(right_id - left_id) + 1,
                range=(left_id, right_id + 1),
                align="left",
                edgecolor="black",
            )
            ax[1].set_xlabel("...with that number of children")
            ax[1].set_ylabel("Number of parents...")
            ax[1].set_axisbelow(True)
            ax[1].grid()
            if self.tb_logger is not None:
                self.tb_logger.log_figure(
                    "Parents_statistics_distribution", fig, self.R_i, close=True
                )
