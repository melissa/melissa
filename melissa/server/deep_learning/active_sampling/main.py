"""`melissa.server.deep_learning.active_learning.main` module defines methods useful
for performing active sampling parameter generation,
checkpointing, triggering resampling events."""

# pylint: disable=W0603

import logging
import threading
from typing import Union, Type, Any, TypeVar, Optional

import numpy as np
from numpy.typing import NDArray

from melissa.server.deep_learning.tensorboard import TensorboardLogger
from melissa.server.parameters import ParameterSamplerType
from melissa.server.deep_learning.active_sampling.exceptions import ActiveSamplingError
from melissa.server.deep_learning.active_sampling.breed_utils import (
    initialize_container_with_sliding_window
)
from melissa.server.deep_learning.active_sampling.parameters import (
    ExperimentBreeder,
    DefaultBreeder
)


logger = logging.getLogger(__name__)

# Define a type variable for ExperimentBreeder or its subclasses
T = TypeVar('T', bound='ExperimentBreeder')

__SAMPLER: Any = None
__RESAMPLE_EVENT: threading.Event = threading.Event()
__EXCEPTION: Optional[Exception] = None


def make_parameter_sampler(sampler_t: Union[ParameterSamplerType, Type[T]],
                           non_breed_sampler_t: Optional[ParameterSamplerType] = None,
                           sliding_window_size: Optional[int] = None,
                           **kwargs) -> ExperimentBreeder:
    """Creates and returns an instance of a parameter sampler based on the specified sampler type.

    This method supports predefined sampler types from `ParameterSamplerType` and custom
    sampler classes derived from `ExperimentBreeder`. It can also handle the creation of breeding
    samplers, with optional configurations for non-breeding samplers and sliding window size for
    breeding.

    ### Parameters:
    - **sampler_t** (`Union[ParameterSamplerType, Type[T]]`):
        - `ParameterSamplerType`: An enum value representing a predefined parameter sampler type
        (`DEFAULT_BREED` only).
        - `Type[T]`: A custom sampler class that should inherit from
        `ExperimentBreeder` or a subclass of it.
    - **non_breed_sampler_t** (`Optional[ParameterSamplerType]`):
        The sampler type used for non-breeding simulations. Defaults to `RANDOM_UNIFORM`,
        if not specified.
    - **sliding_window_size** (`Optional[int]`):
        The size of the sliding window for breeding (only relevant when using breeding samplers).
        Must be specified when using breeding algorithms.
    - **kwargs**: Additional keyword arguments passed to the constructor of the chosen sampler.

    ### Returns:
    - `ExperimentBreeder`: An instance of the selected parameter sampler.

    ### Raises:
    - `NotImplementedError`: If the provided `sampler_t` is not a valid `ParameterSamplerType`
    or a subclass of `ExperimentBreeder`.
    - `AssertionError`: If a breeding sampler is selected and `sliding_window_size`
    is not provided."""

    msg_window = "Window size for the container must be provided when using Breed-based samplers."

    global __SAMPLER
    if non_breed_sampler_t is None:
        non_breed_sampler_t = ParameterSamplerType.RANDOM_UNIFORM

    if (
        isinstance(sampler_t, ParameterSamplerType)
        and sampler_t is ParameterSamplerType.DEFAULT_BREED
    ):

        __SAMPLER = DefaultBreeder(
            non_breed_sampler_t=non_breed_sampler_t,
            **kwargs
        )

        assert sliding_window_size is not None, msg_window
        initialize_container_with_sliding_window(sliding_window_size)

    elif isinstance(sampler_t, type) and issubclass(sampler_t, ExperimentBreeder):
        __SAMPLER = sampler_t(
            non_breed_sampler_t=non_breed_sampler_t,
            **kwargs
        )
        if issubclass(sampler_t, DefaultBreeder):
            assert sliding_window_size is not None, msg_window
            initialize_container_with_sliding_window(sliding_window_size)

    else:
        raise NotImplementedError(
            "User must provide an `ParameterSamplerType.DEFAULT_BREED` or "
            "provide a custom sampler object inheriting "
            "the `ExperimentBreeder` class.")

    return __SAMPLER


def get_current_parameters() -> Optional[NDArray]:
    """Retrieves the current parameters from the active parameter sampler.

    This method checks if a parameter sampler (`__SAMPLER`) is active and, if so,
    calls its `get_current_parameters` method to fetch the current parameter values,
    If no active sampler is present, it returns `None`.

    ### Returns
    - `Optional[NDArray]`:
    The current parameters as a NumPy array (if a sampler is active)
    `None` if no sampler is set."""

    if __SAMPLER:
        return __SAMPLER.get_current_parameters()
    return None


def get_breeding_status_per_parameter() -> Optional[NDArray[np.bool_]]:
    """Retrieves the breeding status of each parameter in the active parameter sampler.

    This method checks if the active parameter sampler is an instance of `DefaultBreeder`.
    If so, it calls the `get_breeding_status_per_parameter` method from the `DefaultBreeder`
    instance to get the breeding status of each parameter. If no active sampler or an incompatible
    sampler is present, it returns `None`.

    ### Returns
    - `Optional[NDArray[np.bool_]]`:
    An array indicating the breeding status (True for bred, False for non-bred)
    of each parameter if a `DefaultBreeder` is active, or `None` if no compatible sampler is set."""

    if __SAMPLER and isinstance(__SAMPLER, DefaultBreeder):
        return __SAMPLER.get_breeding_status_per_parameter()
    return None


def trigger_sampling(**kwargs) -> bool:
    """Triggers the active sampling process to update parameters based on the given arguments.

    This method checks if a resampling event is already in progress. If not, it triggers the
    active sampling process by calling the `_update_parameters` method with the
    provided keyword arguments. It also sets a resample event flag to prevent overlapping
    sampling events. In case of an error during sampling, the exception is logged, and subsequent
    triggers are disabled.

    ### Parameters
    - `**kwargs`: Keyword arguments passed to the `_update_parameters` method.

    ### Returns
    - `bool`: if sampling was successfully triggered.

    ### Raises
    - `ActiveSamplingError`: If any error occurs during the sampling process."""

    global __EXCEPTION

    try:
        if __RESAMPLE_EVENT.is_set():
            logger.warning("A resampling event occured while "
                           "the previous event is still in progress. "
                           "Make sure to increase the threshold to avoid this situation.")
            return False
        logger.info("Active sampling triggered.")
        _update_parameters(**kwargs)
        __RESAMPLE_EVENT.set()
        logger.info("Scripts will be modified with newly sampled parameters.")
        return True
    except ActiveSamplingError as e:
        __EXCEPTION = e
        logger.exception(str(e))
        logger.warning("Active sampling will be disabled for subsequent triggers."
                       " Scripts will not be updated.")
    return False


def get_sampler() -> ExperimentBreeder:
    """Returns the current sampler instance of `ExperimentBreeder`."""
    return __SAMPLER


def checkpoint_state() -> None:
    """Calls checkpointing method of the sampler to store the parameters
    as well as other breed statistics."""

    if __SAMPLER:
        __SAMPLER.checkpoint_state()


def restart_from_checkpoint() -> None:
    """Calls to load from checkpointing method of
    the sampler that stores the parameters
    as well as other breed statistics."""

    if __SAMPLER:
        __SAMPLER.restart_from_checkpoint()


def get_resampling_event() -> threading.Event:
    """Returns the `threading.Event` associated with resampling triggers."""
    return __RESAMPLE_EVENT


def _update_parameters(**kwargs) -> None:
    """Updates the parameters in the active parameter sampler.

    This method checks if there is an active parameter sampler (`__SAMPLER`).
    If so, it resets the index of the sampler, retrieves the next set of parameters using
    the `next_parameters` method, and then updates the parameters using `set_parameters`
    if new parameters are provided.

    ### Parameters
    - `**kwargs`: Keyword arguments passed to the `next_parameters` method."""

    if __SAMPLER:
        __SAMPLER.reset_index()
        new_params = __SAMPLER.next_parameters(**kwargs)
        if new_params is not None:
            __SAMPLER.set_parameters(new_params)


def set_tb_logger(tb_logger: TensorboardLogger) -> None:
    """Assign the existing `TensorboardLogger` object for logging information while breeding."""
    if __SAMPLER:
        __SAMPLER.set_tb_logger(tb_logger)


def exception() -> Optional[Exception]:
    """Returns exception to be raised from the server."""
    return __EXCEPTION


def __free() -> None:
    """Deallocate the sampler."""

    if __SAMPLER:
        del __SAMPLER
