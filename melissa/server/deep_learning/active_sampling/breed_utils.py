"""`melissa.server.deep_learning.active_sampling.breed_utils` defines all the helper functions
required for active sampling breed algorithm."""

# pylint: disable=W0603

import threading
from collections import deque
from typing import (
    Union, Dict, List, Tuple, Set,
    Deque, Protocol, Callable, Iterable
)

import numpy as np
from numpy.typing import NDArray
from iterative_stats.iterative_moments import IterativeMoments


class TensorType(Protocol):
    """A static type protocol for `record_increments`."""
    item: Callable


class LastN:
    """Maintains the last `n` unique values in the order of insertion.

    ### Attributes
    - **n** (`int`): Maximum number of unique values to keep.
    - **queue** (`deque`): Stores the ordered unique values.
    - **set** (`set`): Tracks the unique values for fast membership checks."""

    def __init__(self, n: int = 1) -> None:

        self.n: int = n
        self.queue: Deque[int] = deque()
        self.set_: Set[int] = set()

    def insert(self, value: int) -> None:
        """Inserts a value, maintaining the order and uniqueness."""

        if value in self.set_:
            self.queue.remove(value)
        elif len(self.queue) == self.n:
            oldest = self.queue.popleft()
            self.set_.remove(oldest)
        self.queue.append(value)
        self.set_.add(value)

    def get_values(self) -> list:
        """Returns the current list of values in the queue."""

        return list(self.queue)


__N: int = -1
__LASTN: LastN = LastN()
__LOCK: threading.Lock = threading.Lock()
# WARNING: do not combine the (sim_id, tstep) as a tuple key
MomentsType = Dict[int, Dict[int, IterativeMoments]]
__MOMENT_1: MomentsType = {}
RecordType = Union[List, NDArray, Iterable[TensorType]]


def get_moment() -> MomentsType:
    return __MOMENT_1


def get_sliding_window() -> LastN:
    return __LASTN


def get_sliding_window_size():
    return __N


def initialize_container_with_sliding_window(n: int) -> None:
    """Initializes the `LastN` container."""

    global __N, __LASTN
    __N = n
    __LASTN = LastN(__N)


def weighted_average(values: Union[List, NDArray]) -> NDArray:
    """Computes the weighted average of the input values using a logarithmic weighting scheme.

    ### Parameters
    - **values** (`Union[List, NDArray]`):
    A list or array of numerical values to compute the weighted average.

    ### Returns
    - `NDArray`: An array containing the weighted average values."""

    values = np.array(values, dtype=np.float32)
    n = len(values)
    idx = np.arange(n)
    alpha = np.log(n - idx + 2)
    return (values * alpha) / n


def calculate_delta_loss(loss_per_sample_in_batch: NDArray) -> NDArray:
    """Calculates delta loss values for every sample in batch
    - **loss_per_sample_in_batch** (`NDArray`): an array of losses per sample in a batch.

    ### Returns
    - `NDArray`: An array containing delta loss values calculated per sample in a batch.
    """

    avg = loss_per_sample_in_batch.mean()
    std = loss_per_sample_in_batch.std()
    return np.where(
        loss_per_sample_in_batch > avg, (loss_per_sample_in_batch - avg) / std, 0.0
    )


def _record_increment(sim_id: int, t_step: int, value: float) -> None:
    """Records the delta loss for a given simulation ID and time step,
    updating its mean iteratively based on occurance.

    ### Parameters
    - **sim_id** (`int`): The ID of the simulation.
    - **t_step** (`int`): The current time step in the simulation."""

    with __LOCK:
        if sim_id not in __MOMENT_1:
            __MOMENT_1[sim_id] = {}
        if t_step not in __MOMENT_1[sim_id]:
            __MOMENT_1[sim_id][t_step] = IterativeMoments(max_order=1, dim=1)
        __MOMENT_1[sim_id][t_step].increment(value)

        __LASTN.insert(sim_id)


def record_increments(sim_ids: RecordType, time_ids: RecordType, values: RecordType):
    """Records delta losses for multiple simulation IDs and time steps,
    iteratively updating their means based on occurrence.

    This function processes multiple simulation records in batch,
    converting inputs to raw numerical values before passing them
    to `_record_increment`.

    ### Parameters
    - **sim_ids** (`RecordType`): A collection of simulation IDs.
    - **time_ids** (`RecordType`): A collection of time steps corresponding to `sim_ids`.
    - **values** (`RecordType`): A collection of loss values to record.

    Each element in `sim_ids`, `time_ids`, and `values` must be convertible
    to an `int` or `float`."""

    def raw_val(x):
        x = x.item() if hasattr(x, "item") else x
        assert isinstance(x, (int, float))
        return x

    for j, t, v in zip(sim_ids, time_ids, values):
        _record_increment(raw_val(j), raw_val(t), raw_val(v))


def get_fitnesses(
    weighted: bool = False, lastN: bool = True
) -> Tuple[List[float], List[int]]:
    """Computes the fitness values for recent simulations based on their mean delta loss.

    ### Parameters
    - **weighted** (`bool`, optional): computes a weighted average of delta losses;
    otherwise, computes a simple mean. (Default is `False`).

    ### Returns
    - `Tuple[List[float], List[int]]`:
        - `List[float]`: A list of fitness values (averaged or weighted delta losses).
        - `List[int]`: A list of simulation IDs corresponding to the fitness values."""

    last_sim_ids: list = __LASTN.get_values() if lastN else list(__MOMENT_1.keys())
    last_averages: list = []

    with __LOCK:
        for sim_id in last_sim_ids:
            temp = []
            for t_step in __MOMENT_1[sim_id].keys():
                temp.append(
                    # average by batches
                    __MOMENT_1[sim_id][t_step].get_mean()[0]
                )
            # average by timestep
            last_averages.append(weighted_average(temp) if weighted else np.mean(temp))

        return last_averages, last_sim_ids
