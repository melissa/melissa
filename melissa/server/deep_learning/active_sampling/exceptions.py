class ActiveSamplingError(Exception):
    """Exception during active sampling server phase."""


class BreedError(Exception):
    """Exception associated with breeding algorithm."""
