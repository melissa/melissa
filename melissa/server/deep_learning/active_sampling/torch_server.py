"""This script defines `ExperimentalTorchActiveSamplingServer` which is simply a `TorchServer`
with active sampling functionalities."""

# pylint: disable=W0246

from typing import Dict, Any
from melissa.server.deep_learning.torch_server import TorchServer
from melissa.server.deep_learning.active_sampling.active_sampling_server import (
    ExperimentalDeepMelissaActiveSamplingServer
)


class ExperimentalTorchActiveSamplingServer(TorchServer,
                                            ExperimentalDeepMelissaActiveSamplingServer):
    """Extension of `TorchServer` for active sampling."""
    def __init__(self, config_dict: Dict[str, Any]):
        super().__init__(config_dict)
