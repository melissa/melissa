from typing import Optional, Dict, Any, Callable

from melissa.server.deep_learning import FrameworkType
from melissa.server.deep_learning.reservoir import BaseQueue
from melissa.server.deep_learning.dataset.base_dataset import (
    MelissaIterableDataset, GeneralDataLoader
)

__all__ = [
    "MelissaIterableDataset",
    "FrameworkType",
    "make_dataset",
    "make_dataloader"
]


def make_dataset(framework_t: FrameworkType,
                 buffer: BaseQueue,
                 tb_logger: Any,
                 config_dict: Dict[str, Any],
                 transform: Callable) -> MelissaIterableDataset:

    """Factory function to create datasets based on the specified deep learning framework.

    This function initializes and returns a dataset object for either PyTorch or TensorFlow
    based on the provided framework type.

    ### Parameters
    - **framework_t** (`FrameworkType`): The type of framework (`DEFAULT`, `TORCH` or `TENSORFLOW`).
    - **buffer** (`BaseQueue`): The data buffer to be used by the dataset.
    - **tb_logger** (`Any`): A logger for TensorBoard metrics.
    - **config_dict** (`Dict[str, Any]`): Configuration dictionary for the dataset.
    - **transform** (`Callable`): A transformation function to process data before yielding it.

    ### Returns
    - `MelissaIterableDataset`: A dataset object compatible with the specified framework."""

    if framework_t is FrameworkType.DEFAULT:
        dataset_class = MelissaIterableDataset
    if framework_t is FrameworkType.TORCH:
        from melissa.server.deep_learning.dataset.torch_dataset import TorchMelissaIterableDataset
        dataset_class = TorchMelissaIterableDataset
    if framework_t is FrameworkType.TENSORFLOW:
        from melissa.server.deep_learning.dataset.tf_dataset import TfMelissaIterableDataset
        dataset_class = TfMelissaIterableDataset

    return dataset_class(buffer, config_dict, transform, tb_logger)


def make_dataloader(framework_t: FrameworkType,
                    iter_dataset: MelissaIterableDataset,
                    batch_size: int,
                    collate_fn: Optional[Callable] = None,
                    num_workers: int = 0,
                    **extra_torch_dl_args) -> Any:
    """Factory function to create dataloader based on the specified deep learning framework.

    ### Parameters
    - **framework_t** (`FrameworkType`): The type of framework (`DEFAULT`, `TORCH` or `TENSORFLOW`).
    - **iter_dataset** (`MelissaIterableDataset`): An iterable dataset that streams data via its
    `__iter__` method.
    - **batch_size** (`int`): Number of samples per batch.
    - **collate_fn** (`Callable`, optional): A function to combine multiple samples into a batch.
    Defaults to `None`, which creates batches as lists of samples.
    - **num_workers** (`int`, optional): Number of worker threads for parallel data loading.
    Defaults to `0` (no threading).
    - **extra_torch_dl_args** (`Dict[str, Any]`, optional): Extra `kwargs` for
    `torch.utils.data.DataLoader`.

    ### Returns
    - `Union[GeneralDataLoader, torch.utils.data.DataLoader, tensorflow.data.Dataset]`:
    An iterable for training over batches.

    ### Raises
    - `RuntimeError`if the specified framework is not found."""

    if framework_t is FrameworkType.DEFAULT:
        return GeneralDataLoader(iter_dataset, batch_size, collate_fn, num_workers)
    if framework_t is FrameworkType.TORCH:
        from melissa.server.deep_learning.dataset.torch_dataset import (
            TorchMelissaIterableDataset,
            as_torch_dataloader
        )
        assert isinstance(iter_dataset, TorchMelissaIterableDataset)
        return as_torch_dataloader(
            iter_dataset, batch_size, collate_fn, num_workers, **extra_torch_dl_args
        )
    if framework_t is FrameworkType.TENSORFLOW:
        from melissa.server.deep_learning.dataset.tf_dataset import (
            TfMelissaIterableDataset,
            as_tensorflow_dataset
        )
        assert isinstance(iter_dataset, TfMelissaIterableDataset)
        return as_tensorflow_dataset(iter_dataset, batch_size, collate_fn)

    raise RuntimeError
