"""This script defines iterable datasets for pytorch and tensorflow servers."""

# pylint: disable=W0223,E0601

import time
import queue
import threading
import logging

from typing import Union, List, Dict, Any, Callable, Optional, Iterable

from melissa.server.simulation import SimulationData
from melissa.server.deep_learning.reservoir import BaseQueue, Empty
from melissa.server.deep_learning.tensorboard import TensorboardLogger


logger = logging.getLogger(__name__)


class MelissaIterableDataset:
    """A dataset class designed to handle streaming simulation data through a buffer,
    with optional data transformations and logging capabilities.

    ### Parameters
    - **buffer** (`BaseQueue`):
        The buffer used for storing and retrieving streaming data.
    - **config_dict** (`dict`, optional):
        Configuration dictionary for initializing dataset-specific parameters.
        Defaults to an empty dictionary.
    - **transform** (`Callable`, optional):
        A callable transformation function to apply to the data samples. Defaults to `None`.
    - **tb_logger** (`TensorboardLogger`, optional):
        A logger for tracking dataset operations via TensorBoard. Defaults to `None`.

    ### Attributes
    - **buffer** (`BaseQueue`):
        Holds the data samples in a queue for processing.
    - **__tb_logger** (`Optional[TensorboardLogger]`):
        Logs dataset-related events or metrics for TensorBoard visualization.
    - **_is_receiving** (`bool`):
        Indicates whether the dataset is currently receiving data from the buffer.
    - **sample_number** (`int`):
        Tracks the number of samples processed.
    - **config_dict** (`Dict[str, Any]`):
        Stores configuration settings for the dataset.
    - **__transform** (`Callable`, optional):
        Holds the transformation function, if provided.
    - **__transform_lock** (`threading.Lock`):
        Ensures thread-safe application of the transformation function."""
    def __init__(self,
                 buffer: BaseQueue,
                 config_dict: Optional[Dict[str, Any]] = None,
                 transform: Optional[Callable] = None,
                 tb_logger: Optional[TensorboardLogger] = None) -> None:

        self.buffer: BaseQueue = buffer
        self.__tb_logger: Optional[TensorboardLogger] = tb_logger
        self._is_receiving: bool = True
        self.sample_number = 0
        self.config_dict: Optional[Dict[str, Any]] = config_dict
        self.__transform: Optional[Callable] = transform
        self.__transform_lock: threading.Lock = threading.Lock()

    @property
    def transform_lock(self):
        return self.__transform_lock

    @property
    def has_data(self) -> bool:
        """Returns if the server is still receiving and the buffer is not empty."""
        return self._is_receiving or not self.buffer.empty()

    def get_sample_number(self) -> int:
        """Returns the total sample count
        that were pulled from the buffer and processed. Useful for logging."""
        return self.sample_number

    def signal_reception_over(self):
        """Called after reception is done to flush the remaining
        elements from the buffer."""

        self._is_receiving = False
        self.buffer.signal_reception_over()

    def wrapped_transform(self, *args, **kwargs):
        with self.__transform_lock:
            return self.__transform(*args, **kwargs)

    def __iter__(self):
        """Infinite iterator which will always try to pull from the
        buffer as long as the buffer is not empty or the server
        is still receiving data."""

        while self.has_data:
            try:
                start_get = time.time()
                items: Union[SimulationData, List[SimulationData]] = self.buffer.get(timeout=1)
                end_get = time.time()
                if self.__transform and items:
                    data = self.wrapped_transform(items, self.config_dict)
                elif isinstance(items, list):
                    data = [item.data for item in items]
                else:
                    data = items.data
                # compute samples per second
                elapsed_time = time.time() - start_get
                samples_per_second = 1.0 / elapsed_time if elapsed_time > 0 else 0.0
                self.sample_number += 1

                if self.__tb_logger:
                    self.__tb_logger.log_scalar(
                        "samples_per_second", samples_per_second, self.sample_number
                    )
                    self.__tb_logger.log_scalar("get_time", end_get - start_get, self.sample_number)
                    self.__tb_logger.log_scalar("buffer_size", len(self.buffer), self.sample_number)
                yield data
            except Empty:
                logger.warning("Buffer empty but still receiving.")
                time.sleep(1)  # necessary in case multiple rounds
                continue


class GeneralDataLoader:
    """A general-purpose data loader designed to handle streaming datasets
    with optional multi-threaded loading and batch collation.

    This class supports datasets like `MelissaIterableDataset` that provide
    infinite or streaming data. It enables efficient batching and parallel
    data loading while ensuring compatibility with custom collation functions.

    ### Parameters
    - **dataset** (`MelissaIterableDataset`):
        An iterable dataset that streams data via its `__iter__` method.
    - **batch_size** (`int`):
        Number of samples per batch.
    - **collate_fn** (`Callable`, optional):
        A function to combine multiple samples into a batch. Defaults to `None`,
        which creates batches as lists of samples.
    - **num_workers** (`int`, optional):
        Number of worker threads for parallel data loading. Defaults to `0` (no threading).
    - **drop_last** (`bool`, optional):
        Whether to drop the last incomplete batch. Defaults to `True`.

    ### Attributes
    - **dataset** (`MelissaIterableDataset`):
        The dataset being wrapped for batching and loading.
    - **batch_size** (`int`):
        Size of each batch produced by the data loader.
    - **collate_fn** (`Optional[Callable]`):
        The function used to collate samples into batches.
    - **num_workers** (`int`):
        Number of worker threads for parallel data loading.
    - **drop_last** (`bool`):
        Indicates if incomplete batches are dropped.
    - **_queue** (`queue.Queue`):
        An internal buffer to hold preloaded samples during multi-threaded loading.
    - **_stop_event** (`threading.Event`):
        A flag to signal worker threads to stop loading data.
    - **_threads** (`List[threading.Thread]`):
        List of worker threads for parallel data loading."""
    def __init__(self,
                 dataset: MelissaIterableDataset,
                 batch_size: int,
                 collate_fn: Optional[Callable] = None,
                 num_workers: int = 0,
                 drop_last: bool = True) -> None:

        self.dataset = dataset
        self.batch_size = batch_size
        self.collate_fn = collate_fn
        self.num_workers = num_workers
        self.drop_last = drop_last

        # for multi-threaded loading (if num_workers > 0)
        self._queue: queue.Queue = queue.Queue(maxsize=10 * batch_size)
        self._stop_event: threading.Event = threading.Event()
        self._threads: List[threading.Thread] = []

    def _worker_loop(self) -> None:
        """Worker thread loop to fetch data from the dataset and enqueue it."""

        for item in self.dataset:
            if self._stop_event.is_set():
                break
            self._queue.put(item)

    def __iter__(self) -> Iterable:
        """Iterate over the dataset and yield batches."""

        # start worker threads for parallel loading
        if self.num_workers > 0:
            self._stop_event.clear()
            self._threads = [
                threading.Thread(target=self._worker_loop, daemon=True)
                for _ in range(self.num_workers)
            ]
            for thread in self._threads:
                thread.start()

        batch = None
        try:
            while True:
                # fetch data from the dataset or the worker queue
                if self.num_workers > 0:
                    item = self._queue.get()
                else:
                    item = next(iter(self.dataset))

                if batch is None:
                    batch = [[] for _ in item]
                # making the first dimension equal to the number of returns
                # from `process_simulation_data` defined by the user
                for i, it in enumerate(item):
                    batch[i].append(it)  # type: ignore

                if len(batch[0]) == self.batch_size:  # type: ignore
                    if self.collate_fn:
                        yield self.collate_fn(batch)
                    else:
                        yield batch
                    batch = None

        except StopIteration:
            pass
        finally:
            # handle any leftover batch
            if batch and not self.drop_last:
                if self.collate_fn:
                    yield self.collate_fn(batch)
                else:
                    yield batch

            # stop worker threads if running
            if self.num_workers > 0:
                self._stop_event.set()
                for thread in self._threads:
                    thread.join()

    def __len__(self) -> None:
        """`__len__` is not supported for infinite datasets."""
        raise TypeError("`__len__` is not supported for infinite datasets.")
