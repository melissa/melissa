from typing import Optional, Dict, Any, Callable

from torch.utils.data import IterableDataset, DataLoader

from melissa.server.deep_learning.tensorboard import TensorboardLogger
from melissa.server.deep_learning.reservoir import BaseQueue
from melissa.server.deep_learning.dataset import MelissaIterableDataset


class TorchMelissaIterableDataset(MelissaIterableDataset, IterableDataset):
    """A dataset class designed to integrate Melissa's iterable dataset functionality
    with PyTorch's `IterableDataset`.

    This class enables seamless usage of Melissa's streaming simulation data
    within PyTorch-based deep learning workflows."""
    def __init__(self,
                 buffer: BaseQueue,
                 config_dict: Optional[Dict[str, Any]] = None,
                 transform: Optional[Callable] = None,
                 tb_logger: Optional[TensorboardLogger] = None) -> None:

        MelissaIterableDataset.__init__(self, buffer, config_dict, transform, tb_logger)
        IterableDataset.__init__(self)


def as_torch_dataloader(iter_dataset: TorchMelissaIterableDataset,
                        batch_size: int,
                        collate_fn: Optional[Callable] = None,
                        num_workers: int = 0,
                        **extra_torch_dl_args):
    """Creates a torch DataLoader using the iterable dataset.

    - **iter_dataset** (`TorchMelissaIterableDataset`): An iterable dataset that streams
    data via its `__iter__` method.
    - **batch_size** (`int`): Number of samples per batch.
    - **collate_fn** (`Callable`, optional): A function to combine multiple samples into a batch.
    Defaults to `None`, which creates batches as lists of samples.
    - **num_workers** (`int`, optional): Number of worker threads for parallel data loading.
    Defaults to `0` (no threading).
    - **extra_torch_dl_args** (`Dict[str, Any]`, optional): Extra `kwargs` for
    `torch.utils.data.DataLoader`.

    ### Returns
    - `torch.utils.data.DataLoader`: A torch dataloader instance for training over batches.
    """
    return DataLoader(
        iter_dataset,
        batch_size,
        collate_fn=collate_fn,
        num_workers=num_workers,
        **extra_torch_dl_args
    )
