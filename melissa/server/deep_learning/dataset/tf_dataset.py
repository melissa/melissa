import tensorflow as tf
from typing import Dict, Any, Callable, Optional

from melissa.server.deep_learning.reservoir import BaseQueue
from melissa.server.deep_learning.tensorboard import TensorboardLogger
from melissa.server.deep_learning.dataset import MelissaIterableDataset


class TfMelissaIterableDataset(MelissaIterableDataset):
    """A TensorFlow-compatible extension of the MelissaIterableDataset.

    This class adapts the MelissaIterableDataset to work seamlessly with
    TensorFlow pipelines. It serves as a bridge between the Melissa
    distributed data system and TensorFlow, ensuring compatibility
    and ease of use."""
    def __init__(
        self,
        buffer: BaseQueue,
        config_dict: Optional[Dict[str, Any]] = None,
        transform: Optional[Callable] = None,
        tb_logger: Optional[TensorboardLogger] = None,
    ) -> None:
        MelissaIterableDataset.__init__(self, buffer, config_dict, transform, tb_logger)


def as_tensorflow_dataset(iter_dataset: TfMelissaIterableDataset,
                          batch_size: int,
                          collate_fn: Optional[Callable] = None) -> tf.data.Dataset:
    """Converts the iterable dataset into a TensorFlow `tf.data.Dataset`.

    This method utilizes TensorFlow's `from_generator` functionality to
    wrap the current iterable dataset into a `tf.data.Dataset`, allowing
    integration with TensorFlow's data processing pipelines.

    ### Parameters
    - **iter_dataset** (`TfMelissaIterableDataset`): An iterable dataset instance
    defining `__iter__` method.
    - **batch_size** (`int`): Batch size for the iterable.
    - **collate_fn** (`Callable`, optional):
    A function to combine multiple samples into a batch.

    ### Returns
    - `tf.data.Dataset`: A TensorFlow dataset with elements
    structured as `(features, labels)`. Both features and labels are of type
    `tf.float32` with dynamic shapes (`None`)."""

    dataset = tf.data.Dataset.from_generator(
        iter_dataset.__iter__,
        output_types=(tf.float32, tf.float32),
        output_shapes=((None,), (None,)),
    ).batch(batch_size)

    if collate_fn:
        dataset = dataset.map(collate_fn)

    return dataset
