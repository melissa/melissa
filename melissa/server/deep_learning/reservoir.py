"""This script defines all the buffer classes."""

import logging
import random
import threading
from collections import Counter, deque
from dataclasses import dataclass
from enum import Enum
from queue import Empty, Full
from time import time
from typing import (
    Any, Dict, Deque,
    List, Optional,
    Tuple, Union
)
from numpy.typing import NDArray

from melissa.server.simulation import SimulationData
from melissa.types import QueueProtocol

logger = logging.getLogger(__name__)


@dataclass
class Sample:
    """A Sample represents an item contained in the reservoir.
    It is associated to simulation data and seen counter."""

    data: SimulationData
    seen: int = 0


class PutGetMetric:
    """A class to monitor the balance between puts and gets in the reservoir."""
    def __init__(self, val: int) -> None:

        self.val: int = val
        self.increment_lock: threading.Lock = threading.Lock()

    def inc(self, val: int) -> None:
        """Increments the counter."""
        with self.increment_lock:
            self.val = self.val + val

    def dec(self, val: int) -> None:
        """Decrements the counter."""
        with self.increment_lock:
            self.val = self.val - val


class NotEnoughData(Empty):
    "Not enough data in the queue"


class BaseQueue:
    """A queue inspired by the Lib/queue.py,
    but with only the functionality needed, hence no task_done feature.
    See https://github.com/python/cpython/blob/main/Lib/queue.py#L28"""
    def __init__(self, maxsize: int = 0) -> None:

        self.maxsize: int = maxsize
        self.mutex: threading.RLock = threading.RLock()
        self.not_empty: threading.Condition = threading.Condition(self.mutex)
        self.not_full: threading.Condition = threading.Condition(self.mutex)
        self._init_queue()
        self.put_get_metric: PutGetMetric = PutGetMetric(0)

    def _init_queue(self) -> None:
        """Initialize the queue with a specific data-structure.
        Unique to subclasses."""
        self.queue: Deque = deque()

    def save_state(self) -> Dict[str, Any]:
        """Returns the current state of the queue."""
        return {"queue": self.queue}

    def load_from_state(self, state: Dict[str, Any]) -> None:
        """Loads the queue from the previous state."""
        self.queue = state["queue"]

    def _size(self) -> int:
        """Returns the current size of the queue."""
        return len(self.queue)

    def _is_sampling_ready(self) -> bool:
        return self._size() > 0

    def __len__(self) -> int:
        with self.mutex:
            return self._size()

    def empty(self) -> bool:
        with self.mutex:
            return self._size() == 0

    def _is_full(self) -> bool:
        return self._size() >= self.maxsize

    def _on_full(self, block, timeout) -> bool:
        """Handles the behavior when the queue is full,
        blocking the operation or raising an exception.

        ### Parameters
        - **block** (`bool`): Indicates whether the operation should block
        until the queue is not full.
        - **timeout** (`float`): The maximum time (in seconds) to wait if blocking is enabled.

        ### Returns
        - `bool`: Always returns `True` if the operation blocks successfully and
        the queue is no longer full."""

        if not block:
            raise Full
        self.not_full.wait(timeout)
        return True

    def _get(self) -> Union[List[SimulationData], SimulationData]:
        """Returns the `SimulationData` object from the queue."""
        return self.queue.popleft()

    def _put(self, item: SimulationData) -> None:
        """Inserts `SimulationData` object in the queue."""
        self.queue.append(item)

    def get(self,
            block: bool = True,
            timeout: Optional[float] = None) -> Union[List[SimulationData], SimulationData]:
        """Retrieves an item from the queue, blocking if necessary until data is available.

        ### Parameters
        - **block** (`bool`, default=`True`):
            - If `True`, the method will block until data is available or the timeout expires.
            - If `False`, raises `NotEnoughData` if data is not immediately available.
        - **timeout** (`Optional[float]`):
            - Maximum time (in seconds) to wait for data if `block` is `True`.
            - If `None`, waits indefinitely.
            - Must be a non-negative number if provided.

        ### Returns
        - `Union[List[SimulationData], SimulationData]`: The retrieved item from the queue.

        ### Raises
        - `NotEnoughData`: If the queue does not have enough data to retrieve:
            - When `block` is `False` and data is unavailable.
            - When `block` is `True` and the timeout expires.
        - `ValueError`: If `timeout` is a negative number."""

        with self.not_empty:
            if not block:
                if not self._is_sampling_ready():
                    raise NotEnoughData
            elif timeout is None:
                while not self._is_sampling_ready():
                    self.not_empty.wait()
            elif timeout < 0:
                raise ValueError("'timeout' must be a non-negative number")
            else:
                endtime = time() + timeout
                while not self._is_sampling_ready():
                    remaining = endtime - time()
                    if remaining <= 0.0:
                        raise NotEnoughData
                    self.not_empty.wait(remaining)
            item = self._get()
            self.not_full.notify()
            self.put_get_metric.dec(1)

            return item

    def put(self,
            item: SimulationData,
            block: bool = True,
            timeout: Optional[float] = None) -> None:
        """Adds an item to the queue, blocking if necessary until space is available.

        - If the queue has a finite `maxsize` and is full:
            - If `block` is `False`, checks whether the item can be
            conditionally added based on the `_on_full` method.
            - If `block` is `True`, waits until space is available or the timeout expires.
        - Adds the item to the queue if space is available or the `_on_full` method allows it.
        - Notifies waiting threads if the queue is no longer empty after the addition.

        ### Parameters
        - **item** (`SimulationData`): The item to be added to the queue.
        - **block** (`bool`, default=`True`):
            - If `True`, the method will block until space becomes available or the timeout expires.
            - If `False`, raises `Full` if the queue is full.
        - **timeout** (`Optional[float]`):
            - Maximum time (in seconds) to wait for space in the queue if `block` is `True`.
            - If `None`, waits indefinitely.
            - Must be a non-negative number if provided.

        ### Raises
        - `Full`:
            - If the queue is full, `block` is `False`, and the item cannot be conditionally added.
            - If the queue is full, `block` is `True`, and the timeout expires.
        - `ValueError`: If `timeout` is a negative number."""

        add_item = True  # Do we actually add the item to the queue
        with self.not_full:
            if self.maxsize > 0:
                if not block:
                    if self._is_full():
                        add_item = self._on_full(block=block, timeout=timeout)
                elif timeout is None:
                    while self._is_full():
                        add_item = self._on_full(block=block, timeout=timeout)
                elif timeout < 0:
                    raise ValueError("'timeout' must be a non-negative number")
                else:
                    endtime = time() + timeout
                    while self._is_full():
                        remaining = endtime - time()
                        if remaining <= 0.0:
                            raise Full
                        add_item = self._on_full(block=block, timeout=remaining)
            if add_item:
                self._put(item)
                self.put_get_metric.inc(1)
            if self._is_sampling_ready():
                self.not_empty.notify()

    def compute_buffer_statistics(self) -> Tuple[NDArray, NDArray]:
        """Not needed."""
        raise NotImplementedError


class CounterMixin:
    """A Mixin to track the number of times each sample has been seen in the reservoir.

    This class adds counting functionality to the base class, using a `Counter` to track
    how many times each sample has been added to or retrieved from the reservoir."""
    def __init__(self):

        self.seen_ctr = Counter()

    def put(self,
            item: SimulationData,
            block: bool = True,
            timeout: Optional[float] = None) -> None:
        """Adds an item to the reservoir and tracks how many times it has been seen."""

        watched_item = Sample(item)
        super().put(watched_item, block, timeout)  # type: ignore

    def _get_with_eviction(self,
                           index: Union[List, int]
                           ) -> Union[List[SimulationData], SimulationData]:
        """Retrieves items from the reservoir, evicting them if necessary,
        and updates their seen counts."""

        items: Union[List[Sample], Sample] = super()._get_with_eviction(  # type: ignore
            index
        )
        if isinstance(items, list):
            item_data = []
            item_seen = []
            for item in items:
                item.seen += 1
                item_seen.append(item.seen)
                item_data.append(item.data)
            self.seen_ctr += Counter(item_seen)
            return item_data
        self.seen_ctr += Counter([items.seen + 1])

        return items.data

    def _get_without_eviction(self,
                              index: Union[List, int]
                              ) -> Union[List[SimulationData], SimulationData]:
        """Retrieves items from the reservoir without eviction, updating their seen counts."""

        items: Union[List[Sample], Sample] = super()._get_without_eviction(  # type: ignore
            index
        )
        if isinstance(items, list):
            for item in items:
                item.seen += 1
            return [item.data for item in items]
        items.seen += 1
        return items.data

    def _get(self) -> Union[List[SimulationData], SimulationData]:
        """Retrieves a single item from the reservoir and updates its seen count."""

        if hasattr(super(), "_get_without_eviction"):
            return super()._get()  # type: ignore

        item: Sample = super()._get()  # type: ignore
        self.seen_ctr += Counter([item.seen + 1])
        return item.data

    def __repr__(self) -> str:
        s = super().__repr__()
        return f"{s}: {self.seen_ctr}"


class ReceptionDependant:
    """A Mixin to signal when the reception of data is over.

    It is particularly useful for cases where actions depend on the completion
    of data reception, such as emptying or processing the contents of a reservoir. It
    provides a method to indicate that the reception process is complete, allowing other
    parts of the system to act accordingly.

    ### Attributes:
    - **_is_reception_over** (`bool`): A flag indicating whether data reception is over."""
    def __init__(self) -> None:

        self._is_reception_over = False

    def signal_reception_over(self) -> None:
        self._is_reception_over = True


class SamplingDependant:
    """A Mixin to receive the signal that the reservoir is ready for sampling.
    Espcieally useful with thresholds."""

    def _is_sampling_ready(self) -> bool:
        return True


class ThresholdMixin(SamplingDependant, ReceptionDependant):
    """A Mixin that blocks operations when not enough data is available in the container,
    based on a threshold.

    This Mixin is used to manage situations where certain operations should be blocked or delayed
    until the data in the container reaches a specified threshold. It extends the functionality
    of both `SamplingDependant` and `ReceptionDependant` mixins, and adds a threshold-based
    control mechanism for readiness.

    ### Attributes
    - **threshold** (`int`): The minimum number of items required in the container
    for sampling to proceed."""
    def __init__(self, threshold: int) -> None:

        ReceptionDependant.__init__(self)
        self.threshold = threshold

    def _is_sampling_ready(self: QueueProtocol) -> bool:
        """Determines if sampling is ready based on the threshold
        and available data in the container."""

        is_ready = super()._is_sampling_ready() and (self._size() > self.threshold)  # type: ignore
        return is_ready

    def set_threshold(self, t: int) -> None:
        """Sets a new threshold for when sampling is considered ready."""
        self.threshold = t

    def signal_reception_over(self: QueueProtocol) -> None:
        """Signals that the reception of data is complete,
        sets the threshold to 0, and notifies any waiting processes."""
        with self.mutex:
            super().signal_reception_over()  # type: ignore
            self.set_threshold(0)
            self.not_empty.notify()


class ReadingWithoutEvictionMixin(ReceptionDependant):
    """A Mixin that retrieves data without eviction until data reception is over.

    This Mixin allows data to be retrieved from the container without eviction,
    but once data reception is marked as complete, it switches to eviction-based retrieval.
    It extends the `ReceptionDependant` Mixin to track the reception status."""

    def _get_from_index(self: QueueProtocol,
                        index: Union[list, int]) -> Union[List[SimulationData], SimulationData]:
        """Retrieves data from the container either with or without eviction,
            depending on whether the reception process has been completed."""

        if self._is_reception_over:
            return self._get_with_eviction(index)
        return self._get_without_eviction(index)


class RandomQueue(BaseQueue):
    """A queue that randomly selects items from the container and evicts them upon retrieval.

    This queue operates by randomly selecting an item from the list container. Upon reading,
    the selected item is removed (evicted) from the queue. The queue supports a configurable
    number of pseudo-epochs to control the number of times the process of eviction and item
    retrieval is repeated."""
    def __init__(self, maxsize: int, pseudo_epochs: int = 1) -> None:

        super().__init__(maxsize)
        self.pseudo_epochs = pseudo_epochs

    def _init_queue(self) -> None:
        """Initializes the internal list (queue)."""
        self.queue: List[SimulationData] = []  # type: ignore

    def _get_index(self) -> int:
        """Randomly selects an index from the queue."""
        index = random.randrange(self._size())
        return index

    def _get_without_eviction(self,
                              index: Union[list, int]
                              ) -> Union[List[SimulationData], SimulationData]:
        """Retrieves items without eviction."""

        if isinstance(index, int):
            return self.queue[index]
        if isinstance(index, list):
            return [self.queue[i] for i in index]
        return SimulationData(0, 0, {}, [])

    def _get_with_eviction(self,
                           index: Union[list, int]
                           ) -> Union[List[SimulationData], SimulationData]:
        """Retrieves items and evicts them."""

        if isinstance(index, int):
            item = self.queue[index]
            del self.queue[index]
            return item
        if isinstance(index, list):
            items = [self.queue[i] for i in index]
            for i in index:
                del self.queue[i]
            return items

    def _get_from_index(self,
                        index: Union[List, int]
                        ) -> Union[List[SimulationData], SimulationData]:
        """Retrieves data based on the current pseudo-epoch setting."""

        if self.pseudo_epochs <= 1:
            return self._get_with_eviction(index)
        return self._get_without_eviction(index)

    def _get(self) -> Union[List[SimulationData], SimulationData]:
        """Retrieves a random item from the queue and evicts it, if necessary."""

        index = self._get_index()
        return self._get_from_index(index)


class ReservoirQueue(ReadingWithoutEvictionMixin, RandomQueue):
    """The `ReservoirQueue` supports adding items to the queue, evicting items when the queue
    reaches its maximum size, and retrieving items without eviction during sampling."""
    def __init__(self, maxsize: int) -> None:

        ReadingWithoutEvictionMixin.__init__(self)
        RandomQueue.__init__(self, maxsize)
        self.put_ctr: int = 0

    def _evict(self, index: int) -> None:
        """Removes an item from the queue at the specified index."""
        del self.queue[index]

    def _on_full(self, block, timeout) -> bool:
        """Handles eviction when the queue is full."""

        index = random.randrange(self.put_ctr)
        if index < self.maxsize:
            self._evict(index)
            return True
        return False

    def put(self, item, block=True, timeout=None) -> None:
        """Adds an item to the queue, evicting items as needed."""

        with self.mutex:
            RandomQueue.put(self, item, block, timeout)
            self.put_ctr += 1


class RandomEvictOnWriteQueue(ReadingWithoutEvictionMixin, RandomQueue):
    """A queue that overwrites random samples that have already been seen.

    This queue implements a variant of reservoir sampling where items are randomly evicted
    when the queue reaches its maximum size. The key difference is that once an item has been
    "seen", it can be overwritten randomly, ensuring that older items are eventually evicted
    while still maintaining randomness in the selection.

    The queue maintains two lists: `not_seen` for items that have not been processed yet,
    and `seen` for items that have already been processed. Items are added to the `not_seen`
    list and moved to the `seen` list once they are accessed.

    ### Attributes
    - **not_seen** (`list`): A list holding items that have not yet been accessed.
    - **seen** (`list`): A list holding items that have already been accessed."""
    def __init__(self, maxsize: int) -> None:

        ReadingWithoutEvictionMixin.__init__(self)
        RandomQueue.__init__(self, maxsize)

    def _init_queue(self) -> None:
        """Initializes `seen` and `not_seen` lists."""
        self.not_seen: list = []
        self.seen: list = []

    def save_state(self) -> Dict[str, List]:
        """Saves the current state of the queue, including the `not_seen` and `seen` lists."""
        return {"not_seen": self.not_seen, "seen": self.seen}

    def load_from_state(self, state: Dict[str, List]) -> None:
        """Loads the queue state from a saved state dictionary."""

        self.not_seen = state["not_seen"]
        self.seen = state["seen"]

    def __repr__(self) -> str:
        """Returns a string representation of the queue, showing the size of the
        `not_seen` and `seen` lists."""

        return (
            f"{self.__class__.__name__}:"
            f"not yet seen samples {len(self.not_seen)}, already seen {len(self.seen)}"
        )

    def _size(self) -> int:
        """Returns the total size of the queue (sum of `not_seen` and `seen`)."""
        return len(self.not_seen) + len(self.seen)

    def _evict(self, index) -> None:
        """Evicts the item at the given index from the `seen` list."""
        del self.seen[index]

    def _on_full(self, block, timeout) -> bool:
        """Handles eviction when the queue is full, randomly evicting
        an item from the `seen` list."""

        if not block:
            if len(self.seen) == 0:
                raise Full
        else:
            if len(self.seen) == 0:
                timed_in = self.not_full.wait(timeout)
                if not timed_in:
                    return True
        index = random.randrange(len(self.seen))
        self._evict(index)
        return True

    def _get_with_eviction(self,
                           index: Union[list, int]
                           ) -> Union[List[SimulationData], SimulationData]:
        """Retrieves an item by index, evicting it if necessary."""

        if isinstance(index, int):
            if index < len(self.not_seen):
                item = self.not_seen[index]
                del self.not_seen[index]
            else:
                index = index - len(self.not_seen)
                item = self.seen[index]
                del self.seen[index]
            return item

        if isinstance(index, list):
            not_seen_index = sorted([i for i in index if i < len(self.not_seen)], reverse=True)
            seen_index = sorted(
                [i - len(self.not_seen) for i in index if i >= len(self.not_seen)], reverse=True
            )
            items = [self.not_seen[i] for i in not_seen_index]
            items += [self.seen[i] for i in seen_index]
            for i in not_seen_index:
                del self.not_seen[i]
            for i in seen_index:
                del self.seen[i]
            return items

    def _get_without_eviction(self,
                              index: Union[list, int]
                              ) -> Union[List[SimulationData], SimulationData]:
        """Retrieves an item without eviction, moving it from
        `not_seen` to `seen` if accessed."""

        if isinstance(index, int):
            if index < len(self.not_seen):
                item = self.not_seen[index]
                del self.not_seen[index]
                self.seen.append(item)
            else:
                index = index - len(self.not_seen)
                item = self.seen[index]
            return item

        if isinstance(index, list):
            not_seen_index = [i for i in index if i < len(self.not_seen)]
            seen_index = [i - len(self.not_seen) for i in index if i >= len(self.not_seen)]
            items = []
            for i in not_seen_index:
                item = self.not_seen[i]
                items.append(item)
                del self.not_seen[i]
                self.seen.append(item)
            items += [self.seen[i] for i in seen_index]
            return items

    def _put(self, item) -> None:
        """Adds an item to the `not_seen` list."""
        self.not_seen.append(item)

    def all_seen(self) -> bool:
        """Returns `True` if there are no more items in the `not_seen` list,
        indicating that all items have been processed."""
        return len(self.not_seen) == 0


class BatchGetMixin(SamplingDependant, ReceptionDependant):
    """A Mixin that retrieves data from the reservoir in batches instead of individual sampling.

    This class provides functionality to retrieve multiple items (a batch) from the reservoir
    instead of retrieving them one at a time. It ensures that the reservoir has enough items
    to serve a batch, and if the reception of data is over, it limits the batch size to the
    remaining number of items in the reservoir.

    ### Attributes
    - **batch_size** (`int`): The desired number of items to retrieve in each batch."""
    def __init__(self, batch_size: int) -> None:

        ReceptionDependant.__init__(self)
        self.batch_size = batch_size

    def _is_sampling_ready(self: QueueProtocol) -> bool:
        """Checks if the reservoir has enough items to serve a full batch, considering
        the batch size and whether data reception is over."""

        is_ready = super()._is_sampling_ready()  # type: ignore
        if self._is_reception_over:
            # No more data will arrive, we may not be able to serve batch_size data
            return is_ready
        return is_ready and (self._size() >= self.batch_size)

    def _get(self: QueueProtocol) -> Union[List[SimulationData], SimulationData]:
        """Retrieves a batch of items from the reservoir. The number of items returned
        is limited by the batch size, and it ensures that the batch is randomly selected
        from the available items in the reservoir."""

        if not self._is_reception_over:
            population = self.batch_size
        else:
            population = min(self.batch_size, self._size())
        indices = sorted(random.sample(range(self._size()), k=population), reverse=True)
        items = self._get_from_index(indices)
        return items


class FIFO(CounterMixin, ReceptionDependant, BaseQueue):
    """ First In First Out (FIFO) Queue.

    A queue implementation that follows the FIFO principle, where the first item added
    is the first one to be retrieved. This queue also supports counting the number of
    times each item has been seen (via `CounterMixin`) and can signal when data reception
    is over (via `ReceptionDependant`)."""
    def __init__(self, maxsize: int = 0) -> None:

        CounterMixin.__init__(self)
        ReceptionDependant.__init__(self)
        BaseQueue.__init__(self, maxsize)


class FIRO(CounterMixin, ThresholdMixin, RandomQueue):
    """First In Random Out (FIRO) Queue.

    A queue implementation that combines elements of FIFO (First In) and random eviction
    (Random Out). It follows a First In strategy to decide which item is added first,
    but when retrieving, items are evicted randomly from the queue.
    The queue also supports counting the number of times each item has been seen
    (via `CounterMixin`) and can block operations when the threshold is not met
    (via `ThresholdMixin`).

    ### Attributes
    - **threshold** (`int`): The minimum number of items required in the queue to
    proceed with sampling.
    - **pseudo_epochs** (`int`): The number of epochs. Defaults to 1."""
    def __init__(self, maxsize: int, threshold: int, pseudo_epochs: int = 1) -> None:

        assert threshold <= maxsize
        CounterMixin.__init__(self)
        RandomQueue.__init__(self, maxsize, pseudo_epochs)
        ThresholdMixin.__init__(self, threshold)


class Reservoir(CounterMixin, ThresholdMixin, RandomEvictOnWriteQueue):
    """First In Random Out (FIRO) with eviction on write.

    This queue implementation combines the First In strategy (FIFO) with
    random eviction upon writing. It behaves like a traditional FIFO queue for insertion,
    but when the queue reaches its capacity, it randomly evicts one of the previously added
    elements to make room for the new item. The class also tracks the number of times
    each item has been seen (via `CounterMixin`) and enforces a threshold for sampling
    (via `ThresholdMixin`).

    ### Attributes
    - **maxsize** (`int`): The maximum size of the queue. Once the size exceeds this limit,
    eviction occurs.
    - **threshold** (`int`): The minimum number of items required in the queue to allow sampling."""
    def __init__(self, maxsize: int, threshold: int) -> None:

        assert threshold <= maxsize
        CounterMixin.__init__(self)
        ThresholdMixin.__init__(self, threshold)
        RandomEvictOnWriteQueue.__init__(self, maxsize)


class BatchReservoir(BatchGetMixin, Reservoir):
    """A queue implementing a batch version of the Reservoir sampling algorithm.

    This class extends the `Reservoir` queue to support batch sampling.
    It combines the functionality of the Reservoir sampling algorithm
    (with random eviction on write) with the ability to retrieve
    data in batches rather than individual samples.
    It ensures that the queue never exceeds a specified maximum size and
    maintains a threshold for sampling readiness.

    ### Attributes
    - **threshold** (`int`): The minimum number of items required in the queue to allow sampling.
    - **batch_size** (`int`): The number of items to retrieve in each batch."""
    def __init__(self, maxsize: int, threshold: int, batch_size: int) -> None:

        assert threshold <= maxsize
        assert batch_size <= maxsize
        Reservoir.__init__(self, maxsize, threshold)
        BatchGetMixin.__init__(self, batch_size)


class BufferType(Enum):
    """Enum representing the different types of buffer strategies available for sampling.

    - FIFO = 0
    - FIRO = 1
    - Reservoir = 2
    - BatchReservoir = 3"""
    FIFO = 0
    FIRO = 1
    Reservoir = 2
    BatchReservoir = 3


def make_buffer(buffer_size: int,
                buffer_t: BufferType = BufferType.FIFO,
                per_server_watermark: Optional[int] = None,
                pseudo_epochs: Optional[int] = None,
                batch_size: Optional[int] = None) -> BaseQueue:
    """Factory function to create different types of buffers based on the specified buffer type.

    This function initializes and returns a buffer (queue) object based on the given buffer size
    and type. The available buffer types are defined by the `BufferType` enum, and each type
    has specific requirements. The function raises a `ValueError` if the necessary parameters
    are not provided for the selected buffer type.

    ### Parameters
    - **buffer_size** (`int`): The maximum size of the buffer.
    - **buffer_t** (BufferType, optional): The type of buffer to create.
    Default is `BufferType.FIFO`.
    - **per_server_watermark** (`int`, optional): A threshold used in some buffer types
    (e.g., `Reservoir`, `FIRO`, and `BatchReservoir`).
    - **pseudo_epochs** (`int`, optional): A parameter for `FIRO` buffer type
    to define the number of epochs for the buffer.
    - **batch_size** (`int`, optional):A parameter for `BatchReservoir` buffer type
    to defien the size of a batch.

    ### Returns
    `BaseQueue`: A buffer object of the specified type."""

    if not buffer_t:
        logger.warning("Buffer type is not specified. Defaulting to `FIFO`.")
        buffer_t = BufferType.FIFO

    if buffer_t is BufferType.FIFO:
        return FIFO(buffer_size)

    if buffer_t is BufferType.FIRO:
        if per_server_watermark is None or pseudo_epochs is None:
            raise ValueError("`per_server_watermark` and `pseudo_epochs`"
                             " are required for `FIRO` buffers.")
        return FIRO(buffer_size, per_server_watermark, pseudo_epochs)

    if buffer_t is BufferType.Reservoir:
        if per_server_watermark is None:
            raise ValueError("`per_server_watermark` is required for Reservoir buffers.")
        return Reservoir(buffer_size, per_server_watermark)

    if buffer_t is BufferType.BatchReservoir:
        if per_server_watermark is None or batch_size is None:
            raise ValueError("`per_server_watermark` and `batch_size`"
                             " are required for `BatchReservoir` buffers.")
        return BatchReservoir(buffer_size, per_server_watermark, batch_size)

    raise ValueError(f"Unknown buffer type: '{buffer_t}'. "
                     "Must be `FIFO`, `FIRO`, `Reservoir`, or `BatchReservoir`.")
