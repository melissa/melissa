"""This script extends `DeepMelissaServer` and implements `TFServer`"""

# pylint: disable=E1101,E0401

import os
import logging
from typing_extensions import override
from typing import Any, Dict, List

import tensorflow as tf
import tensorflow.distribute as tfd
try:
    from tensorflow.distribute.cluster_resolver import SlurmClusterResolver  # type: ignore
except ModuleNotFoundError:
    pass

from melissa.server.deep_learning.frameworks import FrameworkType
from melissa.server.deep_learning.base_dl_server import DeepMelissaServer

logger = logging.getLogger(__name__)


class TFServerDeviceError(Exception):
    """Exception for distributed tensorflow server errors."""


class TFServer(DeepMelissaServer):
    """`TFServer` manages and trains a TensorFlow model in a distributed setting.

    This class handles the following tasks:

    - Sets up the distributed environment for multi-GPU or CPU training using TensorFlow's
    `MultiWorkerMirroredStrategy`.
    - Configures the environment based on available GPUs or falls back to CPU-only training.
    - Initializes the distributed training strategy using cluster resolvers for SLURM, OAR,
    or local clusters.
    - Synchronizes data availability to ensure smooth gradient aggregation during
    distributed training."""
    def __init__(self, config_dict: Dict[str, Any]) -> None:
        super().__init__(config_dict)
        self._framework_t: FrameworkType = FrameworkType.TENSORFLOW
        self.__strategy: tfd.MultiWorkerMirroredStrategy = tfd.MultiWorkerMirroredStrategy()

    @property
    def strategy(self) -> tfd.MultiWorkerMirroredStrategy:
        """Returns initialized `tensorflow.distribute.MultiWorkerMirroredStrategy` instance."""
        return self.__strategy

    def __configure_visible_devices(self, device_type: str = "GPU") -> List:
        """Sets the visible device(s) for the current process based on the rank.

        This method ensures that each process sees only the GPU corresponding
        to its local rank, facilitating distributed GPU training.

        ### Parameters
        - **device_type** (`str`, optional): The type of device to configure (default is `"GPU"`).

        ### Returns
        - `List`: A list of physical devices of the specified type found on the machine."""

        physical_devices = tf.config.list_physical_devices(device_type)
        if physical_devices:
            local_rank = self.rank % len(physical_devices)
            tf.config.set_visible_devices(physical_devices[local_rank], device_type)

        return physical_devices

    def __initialize_slurm_cluster(self) -> "SlurmClusterResolver":
        """Initializes the cluster using Slurm environment variables for multi-node training.

        It uses the `tensorflow.distribute.cluster_resolver.SlurmClusterResolver`
        to set up a distributed cluster environment, configuring GPU usage and
        networking based on Slurm settings.

        ### Returns
        - `tensorflow.distribute.cluster_resolver.SlurmClusterResolver`: A cluster resolver
        configured for the current Slurm setup."""

        return SlurmClusterResolver(
            port_base=12345,
            gpus_per_node=self.comm_size // int(os.environ['SLURM_NNODES']),
            auto_set_gpu=False
        )

    def __initialize_oar_cluster(self) -> "SlurmClusterResolver":
        """Initializes the cluster configuration using OAR environment variables.

        This method sets up the distributed cluster for environments managed by OAR,
        extracting the node list from the `OAR_NODEFILE` and configuring the
        Slurm-compatible cluster resolver.

        ### Returns
        - `tensorflow.distribute.cluster_resolver.SlurmClusterResolver`:
        A cluster resolver configured for OAR-based multi-node training."""

        os.environ['SLURM_PROCID'] = str(self.rank)
        with open(os.environ['OAR_NODEFILE']) as my_file:
            host_list = list(set(my_file.read().splitlines()))
        return SlurmClusterResolver(
            jobs={'worker': self.comm_size},
            port_base=12345,
            gpus_per_node=self.comm_size // len(host_list),
            gpus_per_task=1,
            tasks_per_node={host: self.comm_size // len(host_list) for host in host_list},
            auto_set_gpu=False
        )

    def __initialize_local_cluster(self) -> "SlurmClusterResolver":
        """Initializes the cluster for a local, single-node distributed training setup.

        This method configures the `tensorflow.distribute.cluster_resolver.SlurmClusterResolver`
        for local environments without external cluster managers like Slurm or OAR,
        using the local hostname as the node.

        ### Returns
        - `tensorflow.distribute.cluster_resolver.SlurmClusterResolver`:
        A cluster resolver configured for local distributed training."""

        os.environ['SLURM_PROCID'] = str(self.rank)
        return SlurmClusterResolver(
            jobs={'worker': self.comm_size},
            port_base=12345,
            gpus_per_node=None,
            gpus_per_task=1,
            tasks_per_node={os.uname()[1]: self.comm_size},
            auto_set_gpu=False
        )

    def __initialize_strategy(self, cluster_resolver: "SlurmClusterResolver") -> None:
        """Initializes the distributed training strategy using NCCL for multi-GPU communication.

        This method configures the `MultiWorkerMirroredStrategy` with the given cluster resolver
        and sets the communication options to use the NCCL backend for efficient GPU-based
        inter-process communication.

        ### Parameters
        - **cluster_resolver** (`tensorflow.distribute.cluster_resolver.SlurmClusterResolver`):
        The cluster resolver that defines the distributed cluster configuration."""

        communication_options = tfd.experimental.CommunicationOptions(
            implementation=tfd.experimental.CommunicationImplementation.NCCL
        )
        self.__strategy = tfd.MultiWorkerMirroredStrategy(
            cluster_resolver=cluster_resolver,
            communication_options=communication_options
        )

    def __fallback_to_cpu_strategy(self) -> None:
        """Provides a fallback strategy for CPU-only distributed training.

        This method ensures that distributed training can proceed when no GPUs are available.
        If multiple CPUs are detected, it raises an error since TensorFlow cannot distribute
        workloads across multiple CPUs without GPUs.

        ### Raises
        - `TFServerDeviceError`: If multiple CPUs are detected."""

        if len(tf.config.list_physical_devices('CPU')) > 1:
            raise TFServerDeviceError(
                "TensorFlow cannot be distributed on multiple non-GPU devices."
            )
        if len(tf.config.list_physical_devices('CPU')) == 1:
            logger.info("Default `MultiWorkerMirroredStrategy` with CPU.")
            self.__strategy = tfd.MultiWorkerMirroredStrategy()

    @override
    def _setup_environment_slurm(self) -> None:
        return None

    @override
    def setup_environment(self) -> None:
        """Configures the environment for distributed training using GPUs or CPUs.

        This method sets up GPU visibility, initializes the appropriate cluster
        (SLURM, OAR, or local), and configures the distribution strategy for
        multi-worker training. If no GPUs are available, it falls back to CPU-based
        distributed training.

        ### Behavior
        - Detects and configures GPU devices for the current process.
        - Initializes the cluster using SLURM, OAR, or a local setup.
        - Sets up a `MultiWorkerMirroredStrategy` using NCCL for GPU communication.
        - Falls back to CPU strategy if no GPUs are found.

        ### Raises
        - **TFServerDeviceError**: Raised when no GPUs are available for distributed training."""

        try:
            physical_devices = self.__configure_visible_devices('GPU')

            if not physical_devices:
                raise TFServerDeviceError("No GPU found")

            if 'SLURM_NODELIST' in os.environ:
                logger.info("Slurm cluster initialization")
                cluster_resolver = self.__initialize_slurm_cluster()
            elif 'OAR_NODEFILE' in os.environ:
                logger.info("OAR cluster initialization")
                cluster_resolver = self.__initialize_oar_cluster()
            else:
                logger.info("Local cluster initialization")
                cluster_resolver = self.__initialize_local_cluster()

            logger.info(f"Rank {self.rank}>> physical-devices={physical_devices}")
            logger.info(
                f"Rank {self.rank}>> visible-devices={tf.config.get_visible_devices('GPU')}"
            )

            self.__initialize_strategy(cluster_resolver)
        except TFServerDeviceError as e:
            logger.info(f"SLURM, OAR, and local cluster initialization failed with exception: {e}")
            self.__fallback_to_cpu_strategy()

    @override
    def _load_model_from_checkpoint(self) -> None:
        """Loads tensorflow `self.model` and `self.optimizer` attributes
        from the last checkpoint."""

        step = tf.Variable(0, trainable=False)
        with self.__strategy.scope():
            restore = tf.train.Checkpoint(
                step=step,
                optimizer=self.optimizer,
                model=self.model
            )
            restore.read("checkpoints/model.pt")

        self.batch_offset = step.numpy()

    @override
    def checkpoint(self,
                   batch_idx: int = 0,
                   path: str = "checkpoints") -> None:
        """The method called to initiate full tree checkpointing.
        Saves `self.model` and `self.optimizer` states."""

        with self.buffer.mutex:

            self.checkpoint_state()
            if self.rank == 0:
                # tensorflow checkpoint
                ckpt = tf.train.Checkpoint(
                    step=tf.Variable(batch_idx, trainable=False),
                    optimizer=self.optimizer,
                    model=self.model
                )

                ckpt.write(f'{path}/model.pt')
