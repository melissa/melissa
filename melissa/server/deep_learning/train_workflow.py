import logging
from typing import Any
from abc import ABC, abstractmethod


logger = logging.getLogger("melissa")


class TrainingWorkflowMixin(ABC):
    """Provides a structure for overriding training, validation, and hook methods."""

    # ==================================================================================================
    #                                       USER-DEFINED METHODS
    # ==================================================================================================
    @abstractmethod
    def training_step(self, batch: Any, batch_idx: int, **kwargs) -> None:
        """Defines the logic for a single training step.

        ### Parameters
        - **batch** (`Any`): A single batch of data.
        - **batch_idx** (`int`): The index of the batch."""

        raise NotImplementedError

    # @abstractmethod
    def validation_step(self, batch: Any, valid_batch_idx: int, batch_idx: int, **kwargs) -> None:
        """Defines the logic for a single validation step.

        ### Parameters
        - **batch** (`Any`): A single batch of validation data.
        - **valid_batch_idx** (`int`): The index of the validation batch.
        - **batch_idx** (`int`): The index of the batch.

        ### Returns
        - `Dict[str, Any]`: Output from the validation step."""

    def on_train_start(self) -> None:
        """Hook called at the start of training."""

    def on_train_end(self) -> None:
        """Hook called at the end of training."""

    # note that `batch_idx` is always referring to the training batch index
    def on_batch_start(self, batch_idx: int) -> None:
        """Hook called at the start of batch iteration."""

    def on_batch_end(self, batch_idx: int) -> None:
        """Hook called at the end of batch iteration."""

    def on_validation_start(self, batch_idx: int) -> None:
        """Hook called at the start of validation."""

    def on_validation_end(self, batch_idx: int) -> None:
        """Hook called at the end of validation."""

    # ==================================================================================================
    # METHODS TO BE CALLED IN THE TRAINING LOOP THAT DO FIXED THINGS ON THE SERVER-SIDE
    # BEFORE CALLING THE USER-DEFINED METHODS.
    # ==================================================================================================

    def _on_train_start(self) -> None:
        self.on_train_start()

    def _on_train_end(self) -> None:
        self.on_train_end()

    def _on_batch_start(self, batch_idx: int) -> None:
        self.on_batch_start(batch_idx)

    def _on_batch_end(self, batch_idx: int) -> None:
        self.on_batch_end(batch_idx)

    def _on_validation_start(self, batch_idx: int) -> None:
        self.on_validation_start(batch_idx)

    def _on_validation_end(self, batch_idx: int) -> None:
        self.on_validation_end(batch_idx)
