"""This script extends `DeepMelissaServer` and implements `TorchServer`, as well as
`ExperimentalDeepMelissaActiveSamplingServer` and implements `ExperimentalTorchActiveSamplingServer`
for active sampling."""

import os
import logging
from typing import Any, Dict
from typing_extensions import override

import torch
import torch.distributed as dist
from torch.nn.parallel import DistributedDataParallel as DDP
import torch.utils.data
import cloudpickle

from melissa.server.deep_learning.frameworks import FrameworkType
from melissa.utility.idr_torch import SlurmEnvironment
from melissa.server.deep_learning.base_dl_server import DeepMelissaServer
from melissa.utility.networking import is_port_in_use

logger = logging.getLogger(__name__)


class TorchServer(DeepMelissaServer):
    """`TorchServer` for managing and training a PyTorch model in a distributed setting.
    It performs the following tasks:

    - Sets up the distributed environment for multi-GPU or CPU training using PyTorch's DDP.
    - Wraps the model in `torch.nn.parallel.DistributedDataParallel` (DDP) for distributed training.
    - Manages model and optimizer state, including loading and saving checkpoints.
    - Synchronizes data availability across processes to avoid deadlocks during
    distributed training."""

    def __init__(self, config_dict: Dict[str, Any]) -> None:

        super().__init__(config_dict)
        self.device: str = "cpu"
        self._framework_t = FrameworkType.TORCH

    @property
    def unwrapped_model(self) -> torch.nn.Module:
        """Returns `torch.nn.Module` object of the original model given by the user
        before DDP wrapping. Useful for checkpointing custom state as well as calling user-defined
        methods belonging to `self.model`. As the server wraps the given model with
        `torch.nn.parallel.DistributedDataParallel` which will set `module` attribute of its own.
        Thus, returning `self.model.module` or `self.model`"""
        return self.model.module if hasattr(self.model, "module") else self.model

    @override
    def _on_train_start(self) -> None:
        self.model.train()
        super()._on_train_start()

    @override
    def _on_validation_start(self, batch_idx: int) -> None:
        self.model.eval()
        super()._on_validation_start(batch_idx)

    @override
    def _on_validation_end(self, batch_idx: int) -> None:
        self.model.train()
        super()._on_validation_end(batch_idx)

    @override
    @torch.no_grad()
    def validation(self, batch_idx: int) -> None:
        super().validation(batch_idx)

    @override
    def setup_environment(self) -> None:
        """Configures the environment for distributed GPU or CPU training using
        PyTorch's `torch.distributed` package.

        This method sets up the master address and port for inter-process communication,
        determines the appropriate device (GPU or CPU), selects the communication backend,
        and initializes the distributed process group. It also initializes a TensorBoard logger
        for tracking training metrics.

        ### Behavior
        - If multiple GPUs are available and match the communication size (`self.comm_size`),
        the `nccl` backend is used with GPU devices.
        - If GPUs are unavailable or insufficient, the `gloo` backend is used with CPU devices.

        ### Raises
        - `RuntimeError`: If `torch.distributed.init_process_group` fails during initialization."""

        os.environ["MASTER_ADDR"] = os.getenv("MASTER_ADDR", "127.0.0.1")
        master_port = 29500
        if self.rank == 0:
            attempts = 10
            i = 0
            while is_port_in_use(master_port) and i < attempts:
                logger.warning(
                    f"Rank {self.rank}>> MASTER_PORT={master_port} "
                    "for torch.distributed is already being used. Trying another..."
                )
                master_port += 1
                i += 1

            if i == attempts:
                logger.error(
                    f"{self.rank}>> Could not find an available MASTER_PORT after "
                    f"{attempts} attempts."
                )
                raise RuntimeError

        master_port = self.comm.bcast(master_port, root=0)
        os.environ["MASTER_PORT"] = str(master_port)

        self.comm.Barrier()
        logger.info(
            f"Rank {self.rank}>> torch.distributed will use "
            f"env://{os.environ['MASTER_ADDR']}:{os.environ['MASTER_PORT']}"
        )
        backend = "none"

        if (
            torch.cuda.is_available()
            and torch.cuda.device_count() >= self.comm_size
        ):
            world_size = torch.cuda.device_count()
            self.device = f"cuda:{self.rank}"
            backend = 'nccl'
        else:
            world_size = self.comm_size
            self.device = f"cpu:{self.rank}"
            backend = 'gloo'

        logger.info(f"Rank {self.rank}>> backend=\"{backend}\", "
                    f"device=\"{self.device}\", world-size={world_size}")

        dist.init_process_group(
            backend, rank=self.rank, world_size=world_size
        )

    @override
    def _setup_environment_slurm(self) -> None:
        """Configures the multi-node distributed data parallel (DDP) environment using SLURM
        for GPU-based training (using Jean-zay recommendations).

        It retrieves information from the SLURM environment and sets up
        the local rank, world size, and device for each process.
        It also initializes the distributed process group with the appropriate backend.

        ### Behavior
        - If GPUs are available, sets the local GPU device and configures the DDP environment
        using the `nccl` backend.
        - If no GPUs are found, raises a `RuntimeError` with a descriptive error message.

        ### Raises
        - **RuntimeError**: If no GPUs are available or the SLURM environment is
        not set up correctly for DDP."""

        slurm_env = SlurmEnvironment()

        if torch.cuda.is_available():
            torch.cuda.set_device(slurm_env.local_rank)
            # world size depends on the number of gpus and not the task given
            # as we breed with an extra task. this step is necessary.
            gpu_world_size = len(slurm_env.gpu_ids)
            self.device = f"cuda:{slurm_env.local_rank}"
            self.idr_rank = slurm_env.local_rank
            backend = 'nccl'
            logger.info(f"Rank {self.rank}>> backend=\"{backend}\", "
                        f"device=\"{self.device}\", world-size={gpu_world_size}")
            dist.init_process_group(
                backend,
                init_method="env://",
                rank=slurm_env.rank,
                world_size=gpu_world_size
            )
        else:
            logger.error("Using `setup_slurm_ddp` requires GPU reservations. No GPU found.")
            raise RuntimeError

    def wrap_model_ddp(self, model: torch.nn.Module) -> DDP:
        """Wraps the model in DistributedDataParallel (DDP) for multi-GPU training.

        Depending on the setup (SLURM or local CUDA), this method wraps the model
        in DDP using the appropriate device(s).

        ### Parameters
        - **model** (`torch.nn.Module`): Instantiated torch model.
        ### Returns
        - `torch.nn.parallel.DistributedDataParallel`:
        The model wrapped in DDP for distributed training."""

        device_ids: Any = None
        if not self.setup_slurm_ddp and "cuda" in self.device:
            device_ids = [self.device]
        elif self.setup_slurm_ddp:
            device_ids = [self.idr_rank]

        return DDP(model, device_ids)

    @override
    def _server_finalize(self, exit_: int = 0) -> None:
        """
        Finalizes the server operations by calling
        `torch.distributed.destroy_process_group`.

        ### Parameters
        - **exit_ (int, optional)**: The exit status code indicating
        the outcome of the server's operations.
        Defaults to 0, which signifies a successful termination."""

        if dist.is_initialized():
            dist.destroy_process_group()

        super()._server_finalize(exit_)

    @override
    def _synchronize_data_availability(self) -> bool:
        """Coordinates dataset to be sure there are data available to be processed.
        This is to avoid any deadlock in `torch.distributed.all_reduce` of the gradients."""

        if dist.is_initialized():
            _status = torch.tensor(
                int(self.dataset.has_data),
                dtype=int,  # type: ignore
                device=self.device
            )
            dist.all_reduce(_status, op=dist.ReduceOp.SUM)
            return _status.item() == self.comm_size

        return super()._synchronize_data_availability()

    @override
    def _load_model_from_checkpoint(self) -> None:
        """Loads torch `self.model` and `self.optimizer` attributes
        from the last checkpoint.

        ### Returns
        - `bool`: if data is available on all processes."""

        with open('checkpoints/net_arch.pkl', 'rb') as f:
            self.model = cloudpickle.load(f)

        self.model.to(self.device)
        self.model: DDP = self.wrap_model_ddp(self.model)

        # torch 2.6 requires explicit `weights_only=False`
        checkpoint = torch.load("checkpoints/model.pt", weights_only=False)
        self.optimizer = checkpoint["optimizer"]
        self.model.load_state_dict(checkpoint["model_state_dict"])
        self.optimizer.load_state_dict(checkpoint["optimizer_state_dict"])
        self.batch_offset = checkpoint["batch"]

    @override
    def checkpoint(self,
                   batch_idx: int = 0,
                   path: str = "checkpoints") -> None:
        """The method called to initiate full tree checkpointing.
        Saves `self.model` and `self.optimizer` states."""

        with self.buffer.mutex:
            self.checkpoint_state()

            if self.rank == 0:
                with open(f'{path}/net_arch.pkl', 'wb') as f:
                    cloudpickle.dump(self.unwrapped_model, f)

                torch.save(
                    {
                        "optimizer": self.optimizer,
                        "batch": batch_idx,
                        "model_state_dict": self.model.state_dict(),
                        "optimizer_state_dict": self.optimizer.state_dict(),
                    },
                    f"{path}/model.pt",
                )
