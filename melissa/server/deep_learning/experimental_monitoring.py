# type: ignore

import time
import logging
from collections import Counter
from typing import Union, Protocol, Optional

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import TwoSlopeNorm

from melissa.server.deep_learning.reservoir import BaseQueue, Reservoir
from melissa.server.deep_learning.tensorboard import TensorboardLogger
from melissa.server.deep_learning.train_workflow import TrainingWorkflowMixin
from melissa.server.parameters import BaseExperiment
from melissa.server.deep_learning.active_sampling import (
    get_fitnesses,
    DefaultBreeder
)


logger = logging.getLogger("melissa")


class ExperimentalMonitoringProtocol(Protocol):
    """A Protocol class that declares attributes from the server classes that are required
    by the `ExperimentalMonitoringMixin` class."""

    # only for static type checking
    dl_config: dict
    experimental_monitoring: bool
    nb_time_steps: int
    nb_clients: int
    nb_batches_update: int
    _buffer: Union[BaseQueue, Reservoir]
    _parameter_sampler: Optional[Union[BaseExperiment, DefaultBreeder]]
    _tb_logger: Optional[TensorboardLogger]


class ExperimentalMonitoringMixin(ExperimentalMonitoringProtocol, TrainingWorkflowMixin):

    def _on_train_start(self):
        self.experimental_monitoring = self.dl_config.get("experimental_monitoring", False)
        self.on_train_start()

    def _on_batch_end(self, batch_idx):
        """Hook called at the end of batch iteration."""

        self.on_batch_end(batch_idx)

        if (
            self.experimental_monitoring
            and isinstance(self._buffer, Reservoir)
        ):
            try:
                self.__plot_active_sampling_specific_figures(batch_idx)
            except Exception as e:
                logger.exception(e)
                logger.exception(
                    "Experimental plotting has not been tested across different situations."
                )

    def __plot_active_sampling_specific_figures(self, batch_idx: int):
        """Plots buffer and parameters sampling related plots using `tb_logger`."""
        assert self._tb_logger is not None
        assert isinstance(self._buffer, Reservoir)

        start = time.time()

        # # # # Buffer statistics GENERAL
        def find_limits(arr, f=10):
            # function to have nice xlim in plot
            first_element = 0
            first_is_found = False
            last_element = 0
            last_is_found = False
            for i in range(1, len(arr)):
                if not first_is_found and arr[i] - arr[i - 1] != 1:
                    first_element = i - 1
                    first_is_found = True
                if not last_is_found and arr[-i] - arr[-i - 1] != 1:
                    last_element = -i
                    last_is_found = True
                if first_is_found and last_is_found:
                    break
            if last_element == 0 or arr[first_element] != first_element:
                # 9 10 11 12 13 ... -> 0 10
                # 5 6 7 12 13 -> 7 12 -> 0 20
                return 0, (arr[last_element] // f + 1) * f
            else:
                # 0 1 2 9 10 12 13 -> 2 12 -> 0 20
                return (arr[first_element] // f) * f, (arr[last_element] // f + 1) * f

        # matrix t_i_to_count has seen_count values
        # 0.5 for not seen, None for not in buffer
        t_i_to_count = np.full((self.nb_time_steps, self.nb_clients), np.nan)
        for sample in self._buffer.seen:
            t_i_to_count[sample.data.time_step, sample.data.simulation_id] = sample.seen
        for sample in self._buffer.not_seen:
            t_i_to_count[sample.data.time_step, sample.data.simulation_id] = 0.5
        t_i_in_buffer = ~np.isnan(t_i_to_count)
        i_in_buffer = np.any(
            t_i_in_buffer,
            axis=0,
        )
        t_i_not_seen = t_i_to_count == 0.5  # different_size
        t_i_seen = np.logical_and(t_i_in_buffer, ~t_i_not_seen)

        is_breed_study = isinstance(self._parameter_sampler, DefaultBreeder)
        if is_breed_study:
            assert isinstance(self._parameter_sampler, DefaultBreeder)  # mypy throws errors
            assert (
                len(self._parameter_sampler.parameters_is_bred) == t_i_to_count.shape[1]
            )
            i_from_proposal = self._parameter_sampler.parameters_is_bred.reshape(1, -1)
            t_i_proposal_seen = np.logical_and(t_i_seen, i_from_proposal)
            with np.errstate(divide="ignore"):
                # if seen buffer is empty, we divide by zero but we do not care
                self._tb_logger.log_scalars(
                    main_tag="Ratio",
                    step=batch_idx,
                    tag_scalar_dict={
                        "buffer_seen_jt_cnt": t_i_to_count[t_i_proposal_seen].sum()
                        / t_i_to_count[t_i_seen].sum(),
                        "buffer_seen_jt": t_i_proposal_seen.sum() / t_i_seen.sum(),
                        "buffer_not_seen_jt": (t_i_not_seen * i_from_proposal).sum()
                        / (t_i_not_seen).sum(),
                        "buffer_all_j": (i_in_buffer * i_from_proposal).sum()
                        / (i_in_buffer).sum(),
                        "sampler_parameters": i_from_proposal.mean(),
                        "sampler_R": self._parameter_sampler.R,
                    },
                )
            # proposal indicated by negative values
            t_i_to_count[t_i_in_buffer * i_from_proposal] *= -1

        # values for nice plots colorbars and moving limits
        if (batch_idx % (self.nb_batches_update * 5)) == 0:
            self.vmax = 2
        try:
            self.vmax = max(self.vmax, abs(t_i_to_count[t_i_in_buffer]).max())
            prop_max = t_i_to_count[t_i_in_buffer].min()
            rand_max = t_i_to_count[t_i_in_buffer].max()
        except ValueError:
            # can happen if buffer is empty
            prop_max = -1
            rand_max = 1
        xlim = find_limits(
            np.where(t_i_in_buffer.sum(0) != self.nb_time_steps)[0], f=50
        )

        # plot of the buffer state as a matrix image
        fig, ax = plt.subplots(1, 1, figsize=(11, 10))
        im = ax.imshow(
            t_i_to_count,
            norm=TwoSlopeNorm(
                vmin=-self.vmax if is_breed_study else -0.01, vmax=self.vmax, vcenter=0
            ),
            cmap="coolwarm",
            aspect=(xlim[1] - xlim[0]) / self.nb_time_steps,
            origin="lower",
        )
        y_ticks = [
            0,
            self.nb_time_steps // 4,
            self.nb_time_steps // 2,
            self.nb_time_steps - 1,
        ]
        ax.set_yticks(y_ticks, y_ticks)
        ax.set_ylabel("Timestep")
        ax.set_xlim(xlim[0] - 0.5, xlim[1] + 0.5)
        ax.set_xlabel("Simulation id")
        plt.subplots_adjust(top=0.90, bottom=0.07)

        cb2ax = fig.add_axes((0.91, 0.45, 0.02, 0.05))  # not seen
        cb3ax = fig.add_axes((0.91, 0.55, 0.02, 0.33))  # random
        fig.colorbar(
            im,
            cax=cb3ax,
            boundaries=np.linspace(0.5, self.vmax, num=10),
            ticks=[1, self.vmax, rand_max],
            label="Random\n#seen",
        )
        fig.colorbar(im, cax=cb2ax, boundaries=(-0.45, 0.45), ticks=[0])
        if is_breed_study:
            cb1ax = fig.add_axes((0.91, 0.07, 0.02, 0.33))  # proposal
            cb1ax.yaxis.set_inverted(True)
            fig.colorbar(
                im,
                cax=cb1ax,
                boundaries=np.linspace(-self.vmax, -0.5, num=10),
                ticks=[-1, -self.vmax, prop_max],
                label="Proposal\n#seen",
            )
            cb1ax.set_yticklabels(
                [tick.get_text()[1:] for tick in cb1ax.get_yticklabels()]
            )

        self._tb_logger.log_figure(
            "Buffer_representation", fig, batch_idx, close=True
        )

        # # # # Statistics of seen distribution

        # initialise at first batch and do not store history of more than X batches
        if batch_idx % (self.nb_batches_update * 20) == 0:
            # list of dicts per batch seen_cnt : freq
            self.seen_counts_hist_per_batch = []
            # list of lists [batch, #seen, #not_seen]
            self.num_seen_not_seen_per_batch = []

        self.seen_counts_hist_per_batch.append(Counter(t_i_to_count[t_i_seen].ravel()))
        if len(self.seen_counts_hist_per_batch[-1]) == 0:
            # happens when seen is empty (consumption <= production)
            self.seen_counts_hist_per_batch[-1][1.0] = 0
        self.num_seen_not_seen_per_batch.append(
            [batch_idx, t_i_seen.sum(), t_i_not_seen.sum()]
        )
        # plot every X batches
        if (batch_idx + 1) % self.nb_batches_update == 0:
            fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(18, 6))

            tmp = np.array(self.num_seen_not_seen_per_batch, dtype=np.float64)
            tmp[:, 1:] /= tmp[:, 1:].sum(1, keepdims=True)  # convert to ratio
            # stacked bars
            ax1.bar(tmp[:, 0], tmp[:, 1], label="#seen", width=0.95)
            ax1.bar(
                tmp[:, 0],
                tmp[:, 2],
                label="#not_seen",
                bottom=tmp[:, 1],
                width=1,
            )
            ax1.legend()
            ax1.set_title(
                "Buffer distribution <seen> vs <not_seen> (disregard repeats) over time"
            )
            ax1.set_ylabel("proportion")
            ax1.set_xlabel("batch")

            # preparing the plot of history of histograms
            # preparing y-axis limits
            left_lim, right_lim = 10000, -10000
            for counter in self.seen_counts_hist_per_batch:
                left_lim = min(left_lim, min(counter))  # -10
                right_lim = max(right_lim, max(counter))  # 10
            histogram_per_batch = np.full(
                (int(right_lim - left_lim + 1), len(self.seen_counts_hist_per_batch)),
                np.nan,
            )
            for i, counter in enumerate(self.seen_counts_hist_per_batch):
                for k, v in counter.items():
                    histogram_per_batch[int(k - left_lim), i] = v  # -10: 2 -> 0, i = 2

            im = ax2.imshow(
                histogram_per_batch,
                origin="lower",
                cmap="viridis_r",
                aspect=(histogram_per_batch.shape[1] / histogram_per_batch.shape[0]),
            )
            ax2.set_title("seen_count values distribution over time")
            ax2.set_ylabel(
                "seen_count values"
                + ("\nproposal --- 0 --- random" if is_breed_study else "")
            )
            ax2.set_xlabel("batch")
            ax2.set_yticks(
                [0, -left_lim, right_lim - left_lim], [left_lim, 0, right_lim]
            )
            fig.colorbar(im, ax=ax2, label="frequency of seen_count")

            self._tb_logger.log_figure(
                "Buffer_seen_count", fig, batch_idx, close=True
            )

        # # # # BREED ONLY
        if is_breed_study:
            assert isinstance(self._parameter_sampler, DefaultBreeder)  # mypy throws errors
            # # # # misc
            self._tb_logger.log_scalar(
                "R_i",
                self._parameter_sampler.R_i,
                batch_idx,
            )

            # # # # Buffer statistics
            fig, axis = plt.subplots(1, 1, figsize=(8, 5))
            axis.set_title(
                (
                    "Buffer distribution, #timesteps per sim_id.\n"
                    "Green for random-seen, Blue for proposal-seen, Gray for not seen.\n"
                )
            )
            axis.set_xlabel("simulation id")
            axis.set_ylabel("number of timesteps in buffer")
            axis.bar(
                np.where(i_in_buffer)[0],
                t_i_seen.sum(axis=0)[i_in_buffer],
                width=1,
                color=np.where(i_from_proposal[0, i_in_buffer], "blue", "green")[0],
            )
            axis.bar(
                np.where(i_in_buffer)[0],
                t_i_not_seen.sum(axis=0)[i_in_buffer],
                width=1,
                color="gray",
                bottom=t_i_seen.sum(axis=0)[i_in_buffer],
            )
            axis.set_ylim(0, 105)
            axis.set_axisbelow(True)
            axis.grid()
            self._tb_logger.log_figure(
                "Histogram_buffer_distribution", fig, batch_idx, close=True
            )

            # # # # Fitness statistics, ESS
            last_N_fitnesses, last_N_simids = get_fitnesses(lastN=True)
            #  all_fitnesses, all_simids = get_fitnesses(lastN=False)

            fits = np.array(last_N_fitnesses)
            try:
                fits /= fits.sum()
                self._tb_logger.log_scalar(
                    "ESS", (fits.sum() ** 2) / (fits**2).sum(), batch_idx
                )
            except Exception:
                fits -= fits.min()
                fits /= fits.sum()
                self._tb_logger.log_scalar(
                    "ESS", (fits.sum() ** 2) / (fits**2).sum(), batch_idx
                )
            if batch_idx % (self.nb_batches_update * 10) == 0:
                self.fitness_stats_per_batch = []
                self.lastN_sims_per_batch = []

            self.fitness_stats_per_batch.append(fits)
            self.lastN_sims_per_batch.append(last_N_simids)
            num_bins = 20
            if (batch_idx + 1) % self.nb_batches_update == 0:
                # fitness distribution dynamic over iterations
                left_lim = min(
                    min(x[x != 0.0]) if (x != 0.0).sum() != 0 else 0.0
                    for x in self.fitness_stats_per_batch
                )
                right_lim = max(max(x) for x in self.fitness_stats_per_batch) + 1e-8
                bins = np.linspace(left_lim, right_lim, num_bins + 1)

                if left_lim != 0.0:
                    bins = np.logspace(
                        np.log10(left_lim), np.log10(right_lim), num_bins + 1
                    )
                else:
                    bins = np.logspace(
                        np.log10(1e-10), np.log10(right_lim), num_bins + 1
                    )
                full_hist = np.zeros((num_bins, len(self.fitness_stats_per_batch)))
                for i, values in enumerate(self.fitness_stats_per_batch):
                    hists = np.histogram(values, bins=bins, density=True)[0]
                    full_hist[:, i] = hists / hists.sum()
                full_hist[full_hist == 0] = np.nan

                # lastN dynamic over iterations
                min_j = min(min(x) for x in self.lastN_sims_per_batch)
                max_j = max(max(x) for x in self.lastN_sims_per_batch)

                full_map = np.full(
                    (max_j - min_j + 1, len(self.lastN_sims_per_batch)), np.nan
                )
                for i in range(len(self.lastN_sims_per_batch)):
                    for bi, j in enumerate(self.lastN_sims_per_batch[i]):
                        full_map[j - min_j, i] = self.fitness_stats_per_batch[i][bi]

                fig, (ax, axx) = plt.subplots(
                    1, 2, figsize=(19, 7), width_ratios=(2, 1)
                )
                im = ax.imshow(
                    full_hist,
                    cmap="viridis_r",
                    origin="lower",
                    aspect="auto",
                    vmin=0,
                    vmax=1,
                )
                ax.set_title(
                    f"Fitness distribution histogram over time (last {len(fits)} simulations)"
                )
                ax.set_xlabel("Batch")
                ax.set_ylabel("Values")
                ax.set_yticks(
                    np.arange(num_bins + 1)[::3],
                    [f"{x:.5f}" for x in bins[::3] + (bins[1] - bins[0]) / 2],
                )
                fig.colorbar(im, ax=ax, label="Density (normalised per batch)")

                im = axx.imshow(
                    full_map, cmap="magma_r", origin="lower", aspect="auto", vmin=0
                )
                axx.set_title("LastN progression over time")
                axx.set_xlabel("Batch")
                axx.set_ylabel("Simulation id")
                axx.set_yticks([0, max_j - min_j + 1], [min_j, max_j])
                fig.colorbar(im, ax=axx, label="Fitness value (not normalised)")

                self._tb_logger.log_figure(
                    "Fitness_distribution", fig, batch_idx, True
                )
        print(f"Time spent on monitoring post-batch: {time.time() - start:.3} s")
