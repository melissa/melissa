"""This script defines `SensitivityAnalysisServer` class."""

import sys
import logging
from pathlib import Path
from dataclasses import dataclass
from typing_extensions import override
from typing import Any, Dict, List, Optional, Set, Tuple, Type, Union, Callable

import cloudpickle
import numpy as np
from numpy.typing import NDArray
from mpi4py import MPI
import rapidjson

from iterative_stats.iterative_moments import IterativeMoments
from iterative_stats.sensitivity.sensitivity_martinez import IterativeSensitivityMartinez
from melissa.launcher import message
from melissa.server.main import ServerError
from melissa.server.base_server import ReceptionError, BaseServer, MPI2NP_DT
from melissa.server.parameters import ParameterSamplerType, ParameterSamplerClass
from melissa.server.simulation import PartialSimulationData, Simulation


logger = logging.getLogger(__name__)


@dataclass
class FieldMetadata:
    """A class to store and manage metadata for a field.

    ### Parameters
    - **size (`int`)**: The number of local vectors i.e client ranks.

    ### Attributes
    - **local_vect_sizes (`NDArray`)**: An array containing the local vector
    sizes for each process.
    - **global_vect_size** (`int`): The total global vector size, calculated as the sum."""

    local_vect_sizes: NDArray
    global_vect_size: int

    def __init__(self, size: int):
        self.local_vect_sizes = np.zeros(size, dtype=MPI2NP_DT["int"])

    def compute_global_size(self):
        """Computes the global vector size by summing."""
        self.global_vect_size = int(np.sum(self.local_vect_sizes))


class SensitivityAnalysisServer(BaseServer):
    """`SensitivityAnalysisServer` class extends the `BaseServer` class and provides specialized
    functionalities for sensitivity analysis. The primary tasks of this class include:

    - Generating parameters and scripts using pick-freeze sampling.
    - Calculating statistical moments with the `IterativeSensitivityMartinez` method.
    - Overriding or redefining abstract methods.

    ### Parameters
    - **config_dict** (`Dict[str, Any]`):
    A dictionary containing configuration settings for initializing
    the sensitivity analysis server.

    ### Attributes
    - **sobol_op** (`bool`): Indicates if Sobol sensitivity analysis is enabled.
    - **second_order** (`bool`): Flag to activate second order for parameter sampling
    during pick-freeze.
    - **__mean** (`bool`): Flag for computing the mean as part of the statistical analysis.
    - **__variance** (`bool`): Flag for computing the variance as part of the statistical analysis.
    - **__skewness** (`bool`): Flag for computing the skewness as part of the statistical analysis.
    - **__kurtosis** (`bool`): Flag for computing the kurtosis as part of the statistical analysis.
    - **__seen_ranks** (`Set[int]`): Set of ranks corresponding to clients that have been processed.
    - **__checkpoint_count** (`int`): Counter for the number of checkpoints performed.
    - **__checkpoint_interval** (`int`): Interval at which checkpoints are taken,
    specified in the configuration.
    - **__max_order** (`int`): The maximum statistical moment order to compute
    (For example, mean = 1, variance = 2, etc.).
    - **__melissa_moments** (`Dict[tuple, IterativeMoments]`): Dictionary to store statistical
    moments for each field, rank, and time step.
    - **__pick_freeze_matrix** (`List[List[Union[int, float]]]`): Matrix to
    freeze parameters for Sobol computations(used if Sobol analysis is enabled).
    - **__melissa_sobol** (`Dict[tuple, IterativeSensitivityMartinez]`): Dictionary to store
    Sobol sensitivity indices for each field, rank, and time step, if enabled."""
    def __init__(self, config_dict: Dict[str, Any]) -> None:

        super().__init__(config_dict)

        if self.nb_time_steps == 0:
            raise RuntimeError(
                "[ERROR] For SA study `nb_time_steps` must be set by the user."
            )

        Path('./results/').mkdir(parents=True, exist_ok=True)
        sa_config: Dict[str, Any] = config_dict["sa_config"]

        self.sobol_op = sa_config.get("sobol_indices", False)
        self.second_order = self.sobol_op and sa_config.get("second_order", False)
        self._check_group_size()

        self.__mean = sa_config.get("mean", False)
        self.__variance = sa_config.get("variance", False)
        self.__skewness = sa_config.get("skewness", False)
        self.__kurtosis = sa_config.get("kurtosis", False)

        self.__seen_ranks: Set[int] = set()  # list of seen client ranks
        self.__checkpoint_count: int = 0
        self.__checkpoint_interval: int = sa_config["checkpoint_interval"]

        # Instantiate the melissa statistical data structures
        self.__max_order: int = 0
        # {(field, clt_rank, t): StatisticalMoments}}
        self.__melissa_moments: Dict[Tuple[str, int, int], IterativeMoments] = {}
        if self.sobol_op:
            self.__pick_freeze_matrix: List[List[Union[int, float]]] = []
            # {(field, clt_rank, t): IterSobolMartinez}}
            self.__melissa_sobol: Dict[Tuple[str, int, int], IterativeSensitivityMartinez] = {}

        if self.__kurtosis:
            self.__max_order = 4
        elif self.__variance:
            self.__max_order = 3
        elif self.__variance:
            self.__max_order = 2
        elif self.__mean:
            self.__max_order = 1
        else:
            self.__max_order = 0

        # only calling it to handle the situation.
        self.__unimplemented_stats(sa_config)

    @property
    def melissa_moments(self) -> Dict[Tuple[str, int, int], IterativeMoments]:
        return self.__melissa_moments

    @property
    def melissa_sobol(self) -> Dict[Tuple[str, int, int], IterativeSensitivityMartinez]:
        return self.__melissa_sobol

    # keeping it modularized for better code management.
    def __unimplemented_stats(self, sa_config):
        """No implementation available for the following yet."""

        self.__min = sa_config.get("min", False)
        self.__max = sa_config.get("max", False)
        self.__threshold_exceedance = sa_config.get("threshold_exceedance", False)
        self.__threshold_values = sa_config.get("threshold_values", [0.7, 0.8])
        self.__quantiles = sa_config.get("quantiles", False)
        self.__quantile_values = sa_config.get(
            "quantile_values", [0.05, 0.25, 0.5, 0.75, 0.95]
        )

        if self.__min or self.__max:
            logger.warning("min max not implemented")
        if self.__threshold_exceedance:
            logger.warning("threshold not implemented")
        if self.__quantiles:
            logger.warning("quantiles not implemented")

    @override
    def _check_group_size(self) -> None:
        """Based on sobol, validates the given group size,
        and updates the number of clients."""

        if self.sobol_op:
            self.group_size = self.nb_parameters + 2
            self.nb_groups = self.nb_clients
            self.nb_clients = self.group_size * self.nb_groups
        elif (
            not self.sobol_op
            and self.group_size > 1
            and self.nb_clients % self.group_size != 0
        ):
            logger.error("Incorrect group_size, please remove or adjust this option.")
            self._catch_error = True

# =====================================Parameter generation=====================================

    def __draw_from_pick_freeze(self) -> List:
        """Retrieves a single row from the pick-freeze matrix and builds it, if empty.

        ### Returns
        - `List`: A list representing a single row of parameters."""

        if len(self.__pick_freeze_matrix) == 0:
            # logger.debug(f"Rank {self.rank}>> Build pick-freeze matrix")
            self.__build_pick_freeze_matrix()
        return self.__pick_freeze_matrix.pop(0)

    def __build_pick_freeze_matrix(self):
        """Builds the pick-freeze matrix for one group."""

        try:
            self.__pick_freeze_matrix = super()._get_next_parameters()
        except StopIteration:
            self._update_parameter_generator()
            self.__pick_freeze_matrix = super()._get_next_parameters()

    @override
    def _get_next_parameters(self) -> List:
        """Retrieves the next parameters for,
        - Group: if `sobol_op` is active, it uses the pick-freeze matrix.
        - Client: it retrieves parameters from the parent implementation.

        ### Returns
        - `List`: A row of parameters."""

        return self.__draw_from_pick_freeze() if self.sobol_op \
            else super()._get_next_parameters()

    @override
    def set_parameter_sampler(self,
                              sampler_t: Union[ParameterSamplerType, Type[ParameterSamplerClass]],
                              **kwargs) -> None:
        if not self.offline_mode:
            kwargs.update({
                "apply_pick_freeze": self.sobol_op,
                "second_order": self.sobol_op and self.second_order
            })
        super().set_parameter_sampler(sampler_t, **kwargs)

# =====================================Parameter generation=====================================

    @override
    def _receive(self):
        """Handles data from the server."""

        try:
            self._is_receiving = True
            received_samples = 0
            while not self._all_done():
                status = self.poll_sockets()
                if status is not None:
                    if isinstance(status, PartialSimulationData):
                        logger.debug(
                            f"Rank {self.rank}>> "
                            f"sim-id={status.simulation_id}, "
                            f"time-step={status.time_step} received."
                        )
                        received_samples += 1

                        # compute the statistics on the received data
                        self._compute_stats(status)
                        self.checkpoint_state()

            self._is_receiving = False
        except ReceptionError as e:
            logger.exception(f"Exception was raised in the receiving thread: \n {e}")
            if self.no_fault_tolerance:
                self.server_finalize(exit_=1)
                sys.exit(1)

    @override
    def _server_online(self):
        """Steps to perform while the server is online."""
        self._receive()

    @override
    def _server_offline(self):
        """Optional. Post processing steps."""
        self._melissa_write_stats()

    @override
    def start(self):
        """The main entrypoint for the server events."""

        try:
            if not self._restart:
                self._launch_first_groups()

            if self._restart:
                # the reinitialization from checkpoint occurs here
                logger.info(
                    f"Rank {self.rank}>> Continuing from checkpoint restart-count={self._restart}"
                )
                self._restart_from_checkpoint()
                if self.rank == 0:
                    self._kill_and_restart_simulations()

            self.setup_environment()
            self._server_online()
            self._server_offline()
            self._server_finalize()
        except ServerError as e:
            logger.exception(e)
            raise e

    @override
    def _process_partial_data_reception(self,
                                        _: Simulation,
                                        simulation_data: PartialSimulationData
                                        ) -> PartialSimulationData:
        return simulation_data

    @override
    def _process_complete_data_reception(self,
                                         simulation: Simulation,
                                         simulation_data: PartialSimulationData
                                         ) -> PartialSimulationData:

        simulation.clear_data(simulation_data.client_rank, simulation_data.time_step)
        return simulation_data

    def __get_cached_sobol_data(self,
                                pdata: PartialSimulationData) -> Union[bool, Optional[NDArray]]:
        """Caches time steps received from each simulation in a group for Sobol sampling.

        ### Parameters
        - **pdata** (`PartialSimulationData`): The data message received from the simulation.

        ### Returns
        - **Union[bool, Optional[NDArray]]**:
            - `NDArray` if all timesteps for a specific group are available.
            - `False` otherwise."""

        group_id = self._get_group_id_by_simulation(pdata.simulation_id)
        current_group = self._groups[group_id]
        current_group.cache(pdata)
        np_data = current_group.get_cached(
            pdata.field, pdata.client_rank, pdata.time_step
        )

        return np_data if len(np_data) == self.group_size else False

    def _compute_stats(self, pdata: PartialSimulationData) -> None:
        """Computes statistics iteratively and Sobol sensitivity indices, if `sobol_op` is set.
        - Initializes `IterativeMoments` and `IterativeSensitivityMartinez` objects
        per new combination of field, client rank, and time step.
        - Handles Sobol calculations by caching received data for a specific group.

        ### Parameters
        - **pdata** (`PartialSimulationData`): The data message received from the simulation."""

        self.__seen_ranks.add(pdata.client_rank)
        current_key = (pdata.field, pdata.client_rank, pdata.time_step)
        if current_key not in self.__melissa_moments:
            self.__melissa_moments[current_key] = IterativeMoments(
                self.__max_order,
                dim=pdata.data_size
            )
            if self.sobol_op:
                self.__melissa_sobol[current_key] = IterativeSensitivityMartinez(
                    nb_parms=self.nb_parameters,
                    dim=pdata.data_size
                )

        if not self.sobol_op:
            np_data = pdata.data.reshape(-1, pdata.data_size)
            self.__melissa_moments[current_key].increment(np_data[0])
        else:
            np_data = self.__get_cached_sobol_data(pdata)
            if isinstance(np_data, np.ndarray):
                self.__melissa_moments[current_key].increment(np_data[0])
                # increment the sobol data structure
                self.__melissa_sobol[current_key].increment(np_data)
                # increment the moments with the second solution
                self.__melissa_moments[current_key].increment(np_data[1])

                del np_data

    def __gather_data(self,
                      local_vect_sizes: NDArray[np.int32],
                      d_buffer: NDArray[np.float64]
                      ) -> NDArray[np.float64]:
        """Gathers data from all ranks to rank 0 using MPI's Gatherv function.

        ### Parameters:
        - **local_vect_sizes (`NDArray[np.int32]`)**: An array containing the size
        of the data vector per rank.
        - **d_buffer (`NDArray[np.float64]`)**: An array containing the local data to be gathered.

        ### Returns:
        - `NDArray[np.float64]`: An array with the gathered data at rank 0."""

        offsets = [0] + list(np.cumsum(local_vect_sizes))[:-1]
        # TODO: mpi4py version 4+ has Gatherv_init which removes initialization overhead
        # for persistent gather calls.
        self.comm.Gatherv(
            d_buffer[:local_vect_sizes[self.rank]],
            [d_buffer, local_vect_sizes, offsets, MPI.DOUBLE],
            root=0
        )
        return d_buffer

    def __gather_and_write_moments(self,
                                   field: str,
                                   global_vect_size: int,
                                   local_vect_sizes: NDArray[np.int32],
                                   stat_type: str,
                                   values_fn: Callable) -> None:
        """Gathers data from all ranks based on the specified statistical type,
        and writes the results.

        ### Parameters:
        - **field** (`str`): The field for which the data is to be gathered.
        - **global_vect_size (`int`)**: The size of the global vector.
        - **local_vect_sizes (`NDArray[np.int32]`)**: An array containing the size
        of the data vector per rank.
        - **stat_type (`str`)**: A string specifying the type of statistics to gather
        (called per moment. For example, `mean`).
        - **values_fn (`Callable`)**: A function that takes `__melissa_moments` object
        which calls `get_stat_type()` already defined.
        (called per moment. For example, `lambda m: m.get_mean()`)."""

        d_buffer = np.zeros(global_vect_size)
        self.comm.Barrier()

        for t in range(self.nb_time_steps):
            file_name = f"./results/results.{field}_{stat_type}." \
                f"{str(t + 1).zfill(len(str(self.nb_time_steps)))}"

            temp_offset = 0
            for rank in range(self.client_comm_size):
                key = (field, rank, t)
                values = values_fn(self.__melissa_moments[key])

                if np.size(values) > 0:
                    last_offset = temp_offset + np.size(values)
                    d_buffer[temp_offset:last_offset] = values
                    temp_offset = last_offset

            d_buffer = self.__gather_data(local_vect_sizes, d_buffer)
            if self.rank == 0:
                np.savetxt(file_name, d_buffer)
                logger.info(f"file name: {file_name}")

    def __gather_and_write_sobol(self,
                                 field: str,
                                 global_vect_size: int,
                                 local_vect_sizes: NDArray[np.int32]) -> None:
        """Gathers sobol data from all ranks, and writes the results.

        ### Parameters:
        - **field** (`str`): The field for which the data is to be gathered.
        - **global_vect_size (`int`)**: The size of the global vector.
        - **local_vect_sizes (`NDArray[np.int32]`)**: An array containing the size
        of the data vector per rank."""

        d_buffer_a = np.zeros(global_vect_size)
        d_buffer_b = np.zeros(global_vect_size)
        self.comm.Barrier()

        for param in range(self.nb_parameters):
            for t in range(self.nb_time_steps):
                file_name_b = f"./results/results.{field}_sobol{str(param)}." \
                    f"{str(t + 1).zfill(len(str(self.nb_time_steps)))}"

                file_name_a = f"./results/results.{field}_sobol_tot{str(param)}." \
                    f"{str(t + 1).zfill(len(str(self.nb_time_steps)))}"

                temp_offset = 0
                for rank in range(self.client_comm_size):
                    key = (field, rank, t)
                    pearson_b = self.__melissa_sobol[key].pearson_B[:, param]
                    pearson_a = self.__melissa_sobol[key].pearson_A[:, param]

                    if (
                        np.size(pearson_b) > 0
                        and np.size(pearson_a) == np.size(pearson_b)
                    ):
                        last_offset = temp_offset + np.size(pearson_b)
                        d_buffer_b[temp_offset:last_offset] = pearson_b
                        d_buffer_a[temp_offset:last_offset] = pearson_a
                        temp_offset = last_offset

                d_buffer_b = self.__gather_data(local_vect_sizes, d_buffer_b)
                d_buffer_a = self.__gather_data(local_vect_sizes, d_buffer_a)

                if self.rank == 0:
                    np.savetxt(file_name_b, d_buffer_b)
                    logger.info(f"file name: {file_name_b}")
                    np.savetxt(file_name_a, d_buffer_a)
                    logger.info(f"file name: {file_name_a}")

    def _melissa_write_stats(self):
        """Gathers and writes all results to `results/` folder."""

        # turn server monitoring off
        if self.rank == 0:
            snd_msg = self._encode_msg(message.StopTimeoutMonitoring())
            self._launcherfd.send(snd_msg)

        # brodcast client_comm_size to all server ranks
        client_comm_size: int = self.client_comm_size
        if self.rank == 0:
            self.comm.bcast(client_comm_size, root=0)
        else:
            client_comm_size = self.comm.bcast(client_comm_size, root=0)
        self.client_comm_size = client_comm_size
        logger.info(f"Rank {self.rank}>> gathered client-comm-size={self.client_comm_size}")

        # update __melissa_moments and __melissa_sobol with missing client ranks
        # these are just placeholders and do not contribute to the results
        unseen_ranks = set(range(self.client_comm_size)) - self.__seen_ranks
        for field in self.fields:
            for client_rank in unseen_ranks:
                for t in range(self.nb_time_steps):
                    key = (field, client_rank, t)
                    self.__melissa_moments[key] = IterativeMoments(
                        self.__max_order,
                        dim=0
                    )
                    if self.sobol_op:
                        self.__melissa_sobol[key] = IterativeSensitivityMartinez(
                            nb_parms=self.nb_parameters,
                            dim=0
                        )
        # avoiding code repetitions
        stat2values_fn: Dict[str, Tuple[bool, Callable]] = {
            "mean": (self.__mean, lambda m: m.get_mean()),
            "variance": (self.__variance, lambda m: m.get_variance()),
            "skewness": (self.__skewness, lambda m: m.get_skewness()),
            "kurtosis": (self.__kurtosis, lambda m: m.get_kurtosis())
        }

        # compute the vector size across all client ranks
        # and gather them to calculate the global size for every server rank.
        # this is done across all fields
        # and finally gather results for all moments and sobol
        field_metadata: Dict[str, FieldMetadata] = {}
        for field in self.fields:
            field_metadata[field] = FieldMetadata(self.comm_size)
            vect_size = np.zeros(1, dtype=MPI2NP_DT["int"])
            for client_rank in range(self.client_comm_size):
                key = (field, client_rank, 0)
                vect_size += np.size(self.__melissa_moments[key].get_mean())

            self.comm.Allgather(
                [vect_size, MPI.INT],
                [field_metadata[field].local_vect_sizes, MPI.INT]
            )

            field_metadata[field].compute_global_size()
            local_vect_sizes = field_metadata[field].local_vect_sizes
            global_vect_size = field_metadata[field].global_vect_size

            logger.info(
                f"Rank {self.rank}>> field=\"{field}\", "
                f"local-vect-size={vect_size[-1]}, "
                f"global-vect-size={global_vect_size}"
            )
            for stat_type, (condition, values_fn) in stat2values_fn.items():
                if condition:
                    self.__gather_and_write_moments(
                        field,
                        global_vect_size,
                        local_vect_sizes,
                        stat_type,
                        values_fn
                    )

            if self.sobol_op:
                self.__gather_and_write_sobol(field, global_vect_size, local_vect_sizes)

    @override
    def checkpoint_state(self) -> None:
        """Checkpoint moments and sobol information."""

        if self.no_fault_tolerance or not self.__checkpoint_interval:
            return

        self.__checkpoint_count += 1
        if self.__checkpoint_count % self.__checkpoint_interval != 0:
            return

        logger.info("Checkpointing state")
        self._save_base_state()

        stats_metadata = {
            "seen_ranks": list(self.__seen_ranks),
            "nb_time_steps": self.nb_time_steps
        }

        # logger.info(f"Checkpointing moments {self.__melissa_moments}")
        with open(f"checkpoints/{self.rank}/melissa_moments.pkl", 'wb') as f:
            cloudpickle.dump(self.__melissa_moments, f)

        if self.sobol_op:
            with open(f"checkpoints/{self.rank}/melissa_sobol.pkl", 'wb') as f:
                cloudpickle.dump(self.__melissa_sobol, f)

        with open(f"checkpoints/{self.rank}/stats_metadata.json", 'w') as f:
            rapidjson.dump(stats_metadata, f)

    @override
    def _restart_from_checkpoint(self) -> None:
        """Loads from the last checkpoint, in case of a restart."""

        self._load_base_state()

        with open(f"checkpoints/{self.rank}/melissa_moments.pkl", 'rb') as f:
            self.__melissa_moments = cloudpickle.load(f)

        if self.sobol_op:
            with open(f"checkpoints/{self.rank}/melissa_sobol.pkl", 'rb') as f:
                self.__melissa_sobol = cloudpickle.load(f)

        with open(f"checkpoints/{self.rank}/stats_metadata.json", 'r') as f:
            stats_metadata = rapidjson.load(f)

        self.__seen_ranks = set(stats_metadata["seen_ranks"])
        self.nb_time_steps = stats_metadata["nb_time_steps"]
