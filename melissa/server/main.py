"""This script is the main Melissa server entrypoint."""

import argparse
import importlib.util
import sys
import logging
import os
from pathlib import Path
from typing import Any, Dict

import rapidjson

from melissa.launcher.__main__ import validate_config, CONFIG_PARSE_MODE

from melissa.server.base_server import BaseServer
from melissa.utility.logger import configure_logger, get_log_level_from_verbosity
from melissa.utility.networking import get_rank_and_num_server_proc

from mpi4py import MPI
rank = MPI.COMM_WORLD.Get_rank()
nb_proc_server = MPI.COMM_WORLD.Get_size()

logger = logging.getLogger(__name__)


class ServerError(Exception):
    """Server specific exception."""


def main() -> None:
    """
    Initializes and starts the Melissa server.

    ### Overview
    This function is the entry point for the Melissa server. It is **not intended to be called
    directly by users** but is invoked by the `melissa-launcher`.

    ### Steps Performed
    1. Parses command-line arguments, including flags like `--config_name` to
    locate the project directory.
    2. Resolves the appropriate custom server class, if specified in the configuration.
    3. Instantiates the server using the user-provided configuration.
    4. Starts the server by calling `server.start()`.

    ### Usage
    This function is typically invoked through the server shell script `server.sh`:

    ```
    exec python -m melissa --config_name /path/to/config
    ```"""

    parser = argparse.ArgumentParser(
        prog="melissa", description="Melissa"
    )

    # CLI flags
    parser.add_argument(
        "--project_dir",
        help="Directory to all necessary files:\n"
        "   client.sh\n"
        "   server.sh\n"
        "   config.py\n"
        "   data_generator\n"
        "   CustomServer.py"
    )

    parser.add_argument(
        "--config_name",
        help="Defaults to `config` but user can change"
        "the search name by indicating it with this flag",
        default=None
    )

    args = parser.parse_args()
    if not args.config_name:
        conf_name = 'config'
    else:
        conf_name = args.config_name

    # load the config into a python dictionary
    with open(Path(args.project_dir) / f"{conf_name}.json") as json_file:
        config_dict = rapidjson.load(json_file, parse_mode=CONFIG_PARSE_MODE)

    config_dict['user_data_dir'] = Path('user_data')

    # ensure user passed the necessary information for software configuration
    args, config_dict = validate_config(args, config_dict)

    rank, _ = get_rank_and_num_server_proc()

    # set server log level
    sconfig = config_dict['study_options']  # this will become server_config
    log_level = get_log_level_from_verbosity(sconfig.get("verbosity", 3))
    restart_count = int(os.environ.get("MELISSA_RESTART", 0))
    if restart_count:
        app_str = f"_restart_{restart_count}"
    else:
        app_str = ""
    configure_logger(f"melissa_server_{rank}{app_str}.log", log_level)

    # Resolve the server
    myserver = get_resolved_server(args, config_dict)
    try:
        myserver.initialize_connections()
        myserver.start()
    except ServerError as msg:
        logger.exception(f"Server failed with msg {msg}.")
        myserver.close_connection(1)


def safe_join(base_path: str, new_path: str) -> Path:
    """Safely joins a base path and a new path, ensuring the result is an expanded absolute path.

    ### Parameters
    - **base_path (str)**: The base directory path.
    - **new_path (str)**: The path to join with the base path.

    ### Returns
    - **Path**: An expanded and absolute path combining `base_path` and `new_path`."""

    new_path = os.path.expandvars(new_path)
    new_path_ = Path(new_path)
    if new_path_.is_absolute():
        return new_path_
    return Path(base_path) / new_path_


def get_resolved_server(args: argparse.Namespace,
                        config_dict: Dict[str, Any]) -> BaseServer:
    """Resolves and returns a server object based on the provided configurations.

    ### Parameters
    - **args (argparse.Namespace)**: Parsed command-line arguments containing
    server-related options.
    - **config_dict (Dict[str, Any])**: Configuration dictionary with server settings.

    ### Returns
    - `BaseServer`: An instance of the resolved server class, initialized with the
    provided configurations.

    ### Raises
    - `ImportError`: If the server file or class cannot be imported.
    - `AttributeError`: If the specified server class is not found in the module.
    - `Exception`: If there is an issue with initializing the server class."""

    # Resolve the server file name and path
    server_file_name = config_dict.get("server_filename", "server.py")
    server_path = safe_join(args.project_dir, server_file_name)
    server_module_name = Path(server_path).stem
    # Import the server module dynamically
    spec_server = importlib.util.spec_from_file_location(server_module_name, server_path)
    if not (spec_server and spec_server.loader):
        logger.warning("Unable to import server module from path.")
        raise ImportError(f"Failed to import server module from {server_path}.")

    sys.path.append(str(Path(server_path).parent))
    server = importlib.util.module_from_spec(spec_server)
    spec_server.loader.exec_module(server)

    # Resolve the server class name
    server_class_name = config_dict.get("server_class", "MyServer")
    try:
        MyServerClass = getattr(server, server_class_name)
    except AttributeError:
        raise AttributeError(
            f"Server class '{server_class_name}' not found in '{server_file_name}'."
        )

    return MyServerClass(config_dict)
