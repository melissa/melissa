"""This script defines `FaultTolerance` class to monitor failed jobs."""
from typing import Dict, List
import time
import logging
from melissa.server.simulation import Group

logger = logging.getLogger(__name__)


class FaultToleranceError(Exception):
    """Fault tolerance specific exception."""


class FaultTolerance:
    """This class represents a Fault-Tolerance object with attributes and mechanisms for handling
    simulation crashes and delays.

    ### Parameters
    - **ft_off** (`bool`): Indicates whether fault tolerance is enabled or disabled.
    - **max_delay** (`float`): Maximum simulation walltime allowed before timing out.
    - **crashes_before_redraw** (`int`): Number of accepted failures before resampling inputs.
    - **nb_group** (`int`): Total number of groups involved in the simulation.

    ### Attributes
    - **ft_off**: The fault-tolerance switch.
    - **max_delay**: The maximum simulation walltime.
    - **crashes_before_redraw**: The threshold for accepted failures before triggering input
    resampling.
    - **restart_group** (`dict`): A dictionary identifying which groups require relaunching, either
    with or without resampling.
    - **failed_id** (`list`): A list of unique ids corresponding to failed groups.
    - **nb_group**: The total count of groups."""
    def __init__(self,
                 ft_off: bool,
                 max_delay: float,
                 crashes_before_redraw: int,
                 nb_group: int) -> None:

        self.ft_off = ft_off
        self.max_delay = max_delay
        self.crashes_before_redraw = crashes_before_redraw
        self.restart_group: Dict[int, bool] = {}
        self.failed_id: List[int] = []
        self.nb_group: int = nb_group

    # def checkpointing(self) -> None:
    #     """
    #     this function needs to be designed with the following
    #     features:
    #     - save NN model [DL] or Stats [SA]
    #     - save buffer [DL]
    #     - save simulations dictionary [DL, SA]
    #     """

    # def restart(self) -> None:
    #     """
    #     this function needs to be designed with the following
    #     features:
    #     - import NN model [DL] or Stats [SA]
    #     - import buffer [DL]
    #     - import simulations dictionary [DL, SA]
    #     """

    def handle_failed_group(self, group_id: int, group: Group) -> bool:
        """Handles the reaction to failed simulations for a specific group by performing necessary
        actions such as marking failures and determining if additional steps are needed.

        ### Parameters
        - **group_id** (`int`): The unique identifier of the failed group.
        - **group** (`Group`): An instance of the `Group` class representing
        the group that has failed.

        ### Returns
        - **bool**: if the failure was handled successfully."""

        return_bool: bool = False
        group.nb_failures += 1

        if group.nb_failures > self.crashes_before_redraw:
            logger.warning(f"[Fault Tolerance] group-id/sim-id={group_id} failed too many times.")
            return_bool = True
        else:
            logger.warning(
                f"[Fault Tolerance] group-id/sim-id={group_id} failed {group.nb_failures} times."
            )
            return_bool = False

        self.append_failed_id(group_id)
        return return_bool

    def check_time_out(self, groups: Dict[int, Group]) -> bool:
        """Verifies if any simulation has timed out by performing the following actions:
        - Creates a list of simulation ids that have timed out.
        - Updates the number of failures.
        - Creates a dictionary to relaunch all timed-out simulations.
        - Attempts to update the list of unique failed ids.
        - Returns a boolean indicating if any simulation has timed out.

        ### Parameters
        - **groups** (`Dict[int, Group]`): A dictionary where keys are group ids and values are
        `Group` instances representing each simulation group.

        ### Returns
        - **bool**: if any simulation has timed out or not."""

        timed_out_ids: List[int] = []

        for group_id, grp in groups.items():
            for _, sim in grp.simulations.items():
                if (
                    sim.last_message is not None
                    and not sim.finished()
                    and time.time() - sim.last_message
                    > self.max_delay
                    and group_id not in timed_out_ids
                ):
                    timed_out_ids.append(group_id)

        for group_id in timed_out_ids:
            logger.warning(f"group-id/sim-id={group_id} has timed-out.")
            groups[group_id].nb_failures += 1
            for sim in list(groups[group_id].simulations.values()):
                sim.last_message = None
            self.restart_group[group_id] = (
                groups[group_id].nb_failures > self.crashes_before_redraw
            )
            self.append_failed_id(group_id)

        return len(self.restart_group) > 0

    def append_failed_id(self, group_id: int):
        """Attempts to update the list of unique failed group IDs. If an error occurs during the
        update process, an exception is raised.

        ### Parameters
        - **group_id** (`int`): The unique identifier of the group that has failed."""
        if group_id not in self.failed_id:
            self.failed_id.append(group_id)
        if self.ft_off:
            raise FaultToleranceError(
                f"Fault-Tolerance is off, group {group_id} "
                "failure will cause the server to abort"
            )
        if len(self.failed_id) == self.nb_group:
            raise FaultToleranceError(
                "All groups failed please make sure: \n"
                "- the path to the executable is correct, \n"
                "- the number of expected time steps is correct, \n"
                "- the simulation walltime was well estimated."
            )
