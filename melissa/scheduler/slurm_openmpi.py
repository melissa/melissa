#!/usr/bin/python3

# Copyright (c) 2020-2022, Institut National de Recherche en Informatique et en Automatique (Inria)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import logging
import os
from typing import List, Tuple
from pathlib import Path

from melissa.utility.process import ArgumentList, Environment

from .scheduler import Options
from .slurm import SlurmScheduler

logger = logging.getLogger(__name__)


class OpenmpiSlurmScheduler(SlurmScheduler):

    def _sanity_check_impl(self, options: Options) -> List[str]:
        args = options.raw_arguments
        es = []

        for a in args:
            e = None
            if "do-not-launch" in a:
                e = f"remove `{a}` argument"
            elif a in ["-N", "-c", "-n", "--n", "-np"]:
                e = f"remove `{a}` argument"

            if e is not None:
                es.append(e)

        return es

    def _submit_job_impl(
        self, commands: List[ArgumentList],
        env: Environment, options: Options,
        name: str,
        uid: int
    ) -> Tuple[ArgumentList, Environment]:

        # job submission with mpirun MPMD syntax
        # by mixing slurm and openmpi submit functions
        sbatch_env = os.environ.copy()
        sbatch_env.update(env)

        output_filename = f"./stdout/job.{uid}.{name}.out"
        error_filename = f"./stdout/job.{uid}.{name}.err"

        propagated_options = [
            f"--output={output_filename}",
            f"--error={error_filename}"
        ]

        # get #SBATCH options from scheduler_arg options:
        # -> sbatch options (e.g. -N or --nodes) correspond to JOB values
        job_options: List[str] = []
        env_args: List[str] = []
        job_options = options.raw_arguments

        sbatch_options = propagated_options + job_options

        # serialize sbatch options
        def options2str(options: str) -> str:
            return "#SBATCH " + options

        sbatch_options_str = [options2str(o) for o in sbatch_options]

        # assemble mpirun arguments
        ompi_env = os.environ.copy()
        for key in sorted(env.keys()):
            ompi_env[key] = env[key]
            env_args += ["-x", key]

        # get mpirun options from scheduler_command_options:
        # -> mpirun options (e.g. -n NTASKS, --oversubscribe, -x MELISSA_COUPLING=1)
        #    where per-tasks options are SIMULATION values
        ompi_options = options.sched_cmd_opt

        ompi_commands: List[str] = []
        for i, cmd in enumerate(commands):
            ompi_cmd = (
                ompi_options
                + env_args
                + ["--"]
                + cmd
                + ([":"] if i + 1 < len(commands) else [])
            )

            ompi_commands = ompi_commands + ompi_cmd

        sched_cmd = options.sched_cmd
        if not sched_cmd:
            raise ValueError("the executable command cannot be empty with this scheduler")

        # write srun calls to file
        Path('./sbatch').mkdir(parents=True, exist_ok=True)
        sbatch_script_filename = f"./sbatch/sbatch.{uid}.sh"
        sbatch_script = ["#!/bin/sh"] \
            + [f"# sbatch script for job {name}"] \
            + sbatch_options_str \
            + [""] \
            + [f"exec {sched_cmd} \\"] \
            + [" ".join(ompi_commands)]

        sbatch_script_str_noeol = "\n".join(sbatch_script)

        with open(sbatch_script_filename, "w") as f:
            print(sbatch_script_str_noeol, file=f)

        sbatch_call = (
            ["sbatch"]
            + ["--parsable"]
            + ["--job-name={:s}".format(name)]
            + [sbatch_script_filename]
        )

        return sbatch_call, sbatch_env
