#!/usr/bin/python3

# Copyright (c) 2020-2022, Institut National de Recherche en Informatique et en Automatique (Inria)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from typing import List


def break_comma2str(node_list: str, node_list_str: List[str]) -> List[str]:
    """
    parses list of comma separated strings
    """
    return node_list_str + [arg for arg in node_list.split(",")]


def break_hyphen2str(node_list: str, node_list_str: List[str]) -> List[str]:
    """
    extracts list of numbers separated by hyphen characters
    """
    return node_list_str + [
        str(arg) for arg in range(int(node_list.split("-")[0]), int(node_list.split("-")[-1]) + 1)
    ]


def break_bracket2list(bracketed_text: str) -> List[str]:
    """
    extracts list of numbers from numbers between brackets and possibly
    comma or hyphen separated
    """
    list_of_numbers_str: List[str] = []
    # treat comma separated arguments possibly containing hyphens
    if "," in bracketed_text:
        raw_list_of_numbers = break_comma2str(bracketed_text, [])
        for arg in raw_list_of_numbers:
            if "-" in arg:
                list_of_numbers_str = break_hyphen2str(arg, list_of_numbers_str)
            else:
                list_of_numbers_str.append(arg)
    # treat hyphen separated arguments
    else:
        list_of_numbers_str = break_hyphen2str(bracketed_text, list_of_numbers_str)
    return list_of_numbers_str


def break2str(node_list: str) -> List[str]:
    """
    turns SLURM list of nodes into a Python consistent list of strings
    - node_list [input] string obtained from SLURM_NODELIST
    - node_list_str [output] list of strings containing all node names
    """
    node_list_str: List[str] = []
    while True:
        # if arguments with brackets
        if "[" in node_list:
            idx_start_br = node_list.find("[")
            idx_end_br = node_list.find("]")
            # extract node numbers between brackets
            br_str = node_list[idx_start_br + 1 : idx_end_br]
            # process string before brackets
            pre_node_list = node_list[:idx_start_br]
            if "," in pre_node_list:
                # latest preceding comma
                last_comma_idx = len(pre_node_list) - 1 - pre_node_list[::-1].index(",")
                # get listed node name
                node_sublist_name = node_list[last_comma_idx + 1 : idx_start_br]
                # all nodes before comma can be treated with break_comma2str
                node_list_str = break_comma2str(pre_node_list[:last_comma_idx], node_list_str)
            else:
                # get the bracket listed node name
                node_sublist_name = node_list[:idx_start_br]
            # treat brackets arguments
            node_list_str += [node_sublist_name + arg for arg in break_bracket2list(br_str)]
            # remove treated part from node list
            try:
                node_list = (
                    node_list[idx_end_br + 1 :]
                    if node_list[idx_end_br + 1] != ","
                    else node_list[idx_end_br + 2 :]
                )
            # empty str if remaining list is empty
            except IndexError:
                node_list = ""
        # if only comma separated arguments
        else:
            break
    if "," in node_list:
        node_list_str = break_comma2str(node_list, node_list_str)
    elif node_list != "":
        node_list_str.append(node_list)
    return node_list_str
