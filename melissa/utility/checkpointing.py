"""Checkpointing related helper functions."""
import os


def checkpoint_available():
    """Check if checkpoint exists."""
    return os.path.exists("checkpoints/restart_metadata.json")
