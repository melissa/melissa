#!/usr/bin/python3

# Copyright (c) 2021-2022, Institut National de Recherche en Informatique et en Automatique (Inria)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""This module defines a `Timer` class for sending a signal signals
to a socket with a timed loops."""

import select
import socket

from melissa.utility import time
from melissa.utility.networking import Socket


class Timer:
    """
    Timer class that runs a timed loop, sending a signal through a socket after
    a specified interval if no activity is detected on the socket.

    Attributes:
        _fd (Socket): The socket file descriptor to monitor.
        _interval (time.Time): The interval duration to wait for activity.
    """

    def __init__(self, fd: Socket, interval: time.Time) -> None:
        """
        Initializes the Timer with a socket file descriptor and an interval.

        Args:
            fd (Socket): The socket to monitor.
            interval (time.Time): The time interval to wait for activity.

        Raises:
            AssertionError: If the interval is not greater than zero.
        """
        assert interval.total_seconds() > 0

        self._fd = fd
        self._interval = interval

    def run(self) -> None:
        """
        Starts the timer and waits for activity on the socket. If no activity
        is detected within the interval, a null byte is sent through the socket
        to avoid generating a SIGPIPE. If the socket is closed, it handles the
        BrokenPipeError gracefully.

        If activity is detected on the socket, the method returns.

        Raises:
            BrokenPipeError: If the socket is closed before sending.
        """
        while True:
            rs, _, _ = select.select(
                [self._fd], [], [], self._interval.total_seconds()
            )

            if rs == []:
                # No activity detected, send a signal to prevent SIGPIPE
                try:
                    self._fd.send(b'\0', socket.MSG_NOSIGNAL)
                except BrokenPipeError:
                    pass
            else:
                assert rs[0] == self._fd
                return
