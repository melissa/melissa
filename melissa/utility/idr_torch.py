#!/usr/bin/env python
# coding: utf-8


"""Fetching SLURM environment for torch distributed training."""

import os
from dataclasses import dataclass, field
from typing import List
from melissa.scheduler.slurm_parser import break2str


@dataclass
class SlurmEnvironment:
    rank: int = field(init=False)
    local_rank: int = field(init=False)
    size: int = field(init=False)
    cpus_per_task: int = field(init=False)
    hostnames: List[str] = field(init=False)
    gpu_ids: List[str] = field(init=False)
    nnodes: int = field(init=False)
    ntasks_per_node: int = field(init=False)
    master_addr: str = field(init=False)
    master_port: str = field(init=False)

    def __post_init__(self):
        self.rank = int(os.environ.get('SLURM_PROCID', 0))
        self.local_rank = int(os.environ.get('SLURM_LOCALID', 0))
        self.size = int(os.environ.get('SLURM_NTASKS', 0))
        self.cpus_per_task = int(os.environ.get('SLURM_CPUS_PER_TASK', 0))
        self.hostnames = break2str(os.environ.get('SLURM_JOB_NODELIST', ","))
        self.gpu_ids = os.environ.get('SLURM_STEP_GPUS', "")
        if self.gpu_ids != "":
            self.gpu_ids = self.gpu_ids.split(",")

        self.nnodes = int(os.environ.get("SLURM_NNODES", 0))
        self.ntasks_per_node = int(os.environ.get("SLURM_NTASKS_PER_NODE", -1))

        if self.gpu_ids != "" and len(self.hostnames) > 1:
            # Setting MASTER_ADDR and MASTER_PORT
            self.master_addr = self.hostnames[0]
            self.master_port = str(12345 + int(min(self.gpu_ids) if self.gpu_ids else 0))

            # Update environment variables for distributed training
            os.environ['MASTER_ADDR'] = self.master_addr
            os.environ['MASTER_PORT'] = self.master_port
