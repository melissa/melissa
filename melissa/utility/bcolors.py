"""Class for colored standard outputs."""
from enum import Enum


class TextColor(Enum):
    """Class for colored standard outputs."""
    HEADER = '\033[95m'       # Light Magenta
    OKBLUE = '\033[94m'       # Light Blue
    OKCYAN = '\033[96m'       # Light Cyan
    OKGREEN = '\033[92m'      # Light Green
    WARNING = '\033[93m'      # Light Yellow
    FAIL = '\033[91m'         # Light Red
    ENDC = '\033[0m'          # Reset to default color
    BOLD = '\033[1m'          # Bold text
    UNDERLINE = '\033[4m'     # Underlined text
    BLACK = '\033[30m'        # Black text
    RED = '\033[31m'          # Red text
    GREEN = '\033[32m'        # Green text
    YELLOW = '\033[33m'       # Yellow text
    BLUE = '\033[34m'         # Blue text
    MAGENTA = '\033[35m'      # Magenta text
    CYAN = '\033[36m'         # Cyan text
    WHITE = '\033[37m'        # White text
    BG_BLACK = '\033[40m'     # Black background
    BG_RED = '\033[41m'       # Red background
    BG_GREEN = '\033[42m'     # Green background
    BG_YELLOW = '\033[43m'    # Yellow background
    BG_BLUE = '\033[44m'      # Blue background
    BG_MAGENTA = '\033[45m'   # Magenta background
    BG_CYAN = '\033[46m'      # Cyan background
    BG_WHITE = '\033[47m'     # White background

    def __str__(self):
        """Prints values alone."""
        return str(self.value)
