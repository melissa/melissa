"""This module contains classes for creating plots."""

import matplotlib as mpl
import numpy as np

import matplotlib.pyplot as plt


class DynamicHistogram:
    """
    A class to create and update a dynamic histogram plot.
    Attributes:
    -----------
    cmap : matplotlib.colors.Colormap
        The colormap used for the histogram.
    bins : int
        The number of bins for the histogram.
    fig : matplotlib.figure.Figure
        The figure object for the plot.
    axes : matplotlib.axes._subplots.AxesSubplot
        The axes object for the plot.
    first : int
        The index of the first histogram step shown.
    current : int
        The current step index.
    show_last : int
        The number of last histogram steps to show. If 0, show all steps.
    cbar : matplotlib.colorbar.Colorbar
        The colorbar for the plot.
    Methods:
    --------
    __init__(self, title='Histogram', cmap='magma', bins=50, size=(8,5), show_last=0):
        Initializes the DynamicHistogram with the given parameters.
    add_histogram_step(self, data):
        Adds a new step to the histogram with the given data.
    """

    def __init__(self, title='Histogram', cmap='magma', bins=50, size=(8, 5), show_last=0):
        """
        Initializes the plot with the given parameters.
        Parameters:
        title (str): The title of the plot. Default is 'Histogram'.
        cmap (str): The colormap to be used for the plot. Default is 'magma'.
        bins (int): The number of bins for the histogram. Default is 50.
        size (tuple): The size of the figure in inches. Default is (8, 5).
        show_last (int): The number of last iterations to show. Default is 0.

        Attributes:
        cmap (Colormap): The colormap instance.
        bins (int): The number of bins for the histogram.
        fig (Figure): The figure object.
        axes (Axes): The axes object.
        first (int): The first iteration index.
        current (int): The current iteration index.
        show_last (int): The number of last iterations to show.
        cbar (Colorbar): The colorbar object.
        """

        self.cmap = plt.get_cmap(cmap)
        self.bins = bins
        self.fig, self.axes = plt.subplots(1, 1, figsize=size)
        self.axes.set_title(title)
        self.axes.set_xlabel('Value')
        self.axes.set_ylabel('Log-Density')
        self.first = 0
        self.current = 0
        self.show_last = show_last
        self.cbar = self.fig.colorbar(
            mpl.cm.ScalarMappable(cmap=self.cmap),
            ax=self.axes, orientation='vertical', label='Iteration')

    def add_histogram_step(self, data):
        # 1. update counter
        self.current += 1

        # 2. add histogram
        self.axes.hist(data, bins=self.bins, histtype='stepfilled', log=True)

        # 3. update colors
        for i, color in enumerate(map(self.cmap, np.linspace(0, 1, self.current - self.first))):
            self.axes.patches[i].set_facecolor(color)

        # 4. update xlim
        xmin, xmax = min(data), max(data)
        if (self.axes.get_xlim()[0] / xmin) <= 0.5 or xmin < self.axes.get_xlim()[0]:
            self.axes.set_xlim(xmin, None)
        if (xmax / self.axes.get_xlim()[1]) <= 0.5 or xmax > self.axes.get_xlim()[1]:
            self.axes.set_xlim(None, xmax)

        # 5. when theres enough artists - remove first
        if self.show_last != 0 and self.current > self.show_last:
            self.axes.patches[0].remove()
            self.first = self.current - self.show_last

        # 6. update colorbar limit
        self.cbar.mappable.set_norm(mpl.colors.Normalize(self.first, self.current))
