"""Interfaces for MixIn classes defined in server scripts."""

import threading
from typing import Protocol, Any, Union, List


class QueueProtocol(Protocol):
    not_empty: threading.Condition
    mutex: threading.RLock
    batch_size: int
    _is_reception_over: bool

    def _get_with_eviction(self, index: Union[List[int], int]) -> Any:
        ...

    def _get_without_eviction(self, index: Union[List[int], int]) -> Any:
        ...

    def _is_sampling_ready(self) -> bool:
        ...

    def _size(self) -> int:
        ...

    def _get_from_index(self, index: Union[List, int]) -> Any:
        ...

    def set_threshold(self, t: int) -> None:
        ...
