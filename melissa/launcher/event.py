#!/usr/bin/python3

# Copyright (c) 2021-2022, Institut National de Recherche en Informatique et en Automatique (Inria)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from typing import Generic, List, TypeVar, Optional

from melissa.utility.networking import ConnectionId
from melissa.utility.process import Process

from . import action
from melissa.utility.message import Message

JobT = TypeVar("JobT")


class Event:
    pass


class ActionFailure(Event):
    def __init__(self, ac: action.Action, error: Exception):
        self.action = ac
        self.error = error


class ConnectionShutdown(Event):
    def __init__(self, cid: Optional[ConnectionId]) -> None:
        self.cid = cid


class HttpRequest_(Event):
    pass


class JobCancellation(Generic[JobT], Event):
    def __init__(self, jobs: List[JobT]) -> None:
        self.jobs = jobs


class JobSubmission(Generic[JobT], Event):
    def __init__(self, sub: action.Action, job: JobT) -> None:
        self.submission = sub
        self.job = job


class JobUpdate(Generic[JobT], Event):
    def __init__(self, jobs: List[JobT]) -> None:
        self.jobs = jobs


class MessageReception(Event):
    def __init__(self, cid: ConnectionId, message: Message) -> None:
        self.cid = cid
        self.message = message


class NewConnection(Event):
    def __init__(self, cid: ConnectionId) -> None:
        self.cid = cid


# this event is not supposed to be consumed by the state machine
class ProcessCompletion_(Event):
    def __init__(self, ac: action.Action, uid: int, process: "Process[str]"):
        assert process.poll() is not None
        self.action = ac
        self.id = uid
        self.process = process


class Signal(Event):
    def __init__(self, signo: int) -> None:
        self.signo = signo


class Timeout(Event):
    pass


# this event is used to handle postponed job
class JobPostponing(Event):
    pass
