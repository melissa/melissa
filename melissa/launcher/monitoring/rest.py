#!/usr/bin/python3

# Copyright (c) 2022, Institut National de Recherche en Informatique et en Automatique (Inria),
#                     Poznan Supercomputing and Networking Center (PSNC)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from collections import namedtuple
import rapidjson
import logging
import re
from http import HTTPStatus
from http.server import BaseHTTPRequestHandler, HTTPServer
from typing import Any, Dict, List, Tuple

from melissa.launcher import __version__ as version

from melissa.launcher import event
from melissa.launcher.action import Action
from melissa.launcher.event import Event
from melissa.launcher.state_machine import State

logger = logging.getLogger(__name__)


Job = namedtuple("Job", ["id", "unique_id", "state"])


class RestHttpHandler(BaseHTTPRequestHandler):
    """Incoming request handler for REST API"""

    def __init__(self, request, client_address, server: HTTPServer) -> None:
        # ensure the base class constructor is called AFTER initializing all
        # properties
        self.server_version = "melissa-launcher/%s" % version
        super().__init__(request, client_address, server)

    def log_error(self, format: str, *args, **kwargs) -> None:
        logger.error(format, *args, **kwargs)

    def log_message(self, format: str, *args) -> None:
        logger.debug(format, *args)

    def _respond(self, data: Dict[str, Any]) -> None:
        response_str = rapidjson.dumps(data).encode() + b"\n"
        self.send_response(HTTPStatus.OK)
        self.send_header("content-type", "application/json")
        self.send_header("content-length", str(len(response_str)))
        self.end_headers()
        self.wfile.write(response_str)

    def do_GET(self) -> None:
        """Callable triggered everytime HTTP GET
        request hits the server.
        """
        forbidden = True
        if "token" not in self.headers:
            logger.debug("GET refused: no token in header")
        elif self.headers["token"] != self.server._token:  # type: ignore
            logger.debug("GET refused: wrong token")
        else:
            forbidden = False

        if forbidden:
            self.send_response(HTTPStatus.FORBIDDEN)
            self.end_headers()
            return

        m = re.fullmatch("/jobs", self.path)
        if m is not None:
            self._respond({"jobs": [j for j in self.server._jobs.keys()]})  # type: ignore
            return

        m = re.fullmatch("/jobs/([0-9]+)", self.path)
        if m is not None:
            uid = int(m.group(1))
            jobs = [j for j in self.server._jobs.keys() if j == uid]  # type: ignore
            assert len(jobs) <= 1
            if len(jobs) == 1:
                self._respond(self.server._jobs[jobs[0]])  # type: ignore
                return

        self.send_response(HTTPStatus.NOT_FOUND)
        self.end_headers()


class RestHttpServer(HTTPServer):
    def __init__(
        self, server_address: Tuple[str, int], token: str, state: State
    ) -> None:
        super().__init__(server_address, RestHttpHandler)
        self._token = token
        self._state = state
        self._jobs: Dict[int, Dict[str, Any]] = {}

    def notify(
        self, ev: Event, new_state: State, actions: List[Action]
    ) -> None:
        self._state = new_state

        if isinstance(ev, event.JobSubmission):
            self._jobs[ev.job.unique_id()] = {
                'id': int(ev.job.id()),
                'unique_id': int(ev.job.unique_id()),
                'state': str(ev.job.state())
            }

        if isinstance(ev, event.JobUpdate):
            for job in ev.jobs:
                self._jobs[job.unique_id()]['state'] = str(job.state())
