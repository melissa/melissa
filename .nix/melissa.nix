{
    pkgs,
    src
}:
pkgs.stdenv.mkDerivation {
    
    inherit src;

    name = "melissa-base";
    version = "1.0.0";

    nativeBuildInputs = with pkgs; [
        python312Packages.python
        python312Packages.pip
        python312Packages.setuptools
        cmake
        pkg-config
    ];
    
    cmakeFlags = [
        "-DINSTALL_ZMQ=OFF"
    ];

    buildInputs = with pkgs; [
        libgcc
        gfortran
        mpi
        zeromq
    ];
}
