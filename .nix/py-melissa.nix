{
    pkgs,
    src,
    mpi4py
}:
pkgs.python3Packages.buildPythonPackage rec {
# pkgs.stdenv.mkDerivation rec {
    
    inherit src;

    name = "py-melissa-binaries";
    version = "1.0.0";
    format = "pyproject";

    # pkgs.config.allowUnfree = true;
    # pkgs.config.cudaSupport = true;
    # pkgs.config.cudaVersion = "12";

    iterative-stats = pkgs.python3Packages.buildPythonPackage rec {
        pname = "iterative_stats";
        version = "0.1.1";
        format = "pyproject";
        src = pkgs.python3Packages.fetchPypi {
            inherit pname version;
            sha256 = "sha256-wr5gRecgqn/1yNu80B0ILRtm8sKoYTrYJVKFNePOBDY=";
        };
        propagatedBuildInputs = with pkgs.python3Packages; [
            pyyaml
            numpy
            poetry-core
            setuptools
        ];
        doCheck = true;
    };

    mainDependencies = with pkgs.python312Packages; [
        python
        pip
        setuptools
        python
        numpy
        scipy
        pyzmq
        python-rapidjson
        jsonschema
        cloudpickle
        mpi4py
        iterative-stats
        psutil
    ];

    deepLearningDependencies = with pkgs.python312Packages; [
        matplotlib
        pandas
        tensorboard
        distutils
        tensorflow-bin
        torch
    ];

    developerDependencies = with pkgs.python312Packages; [
        flake8
        mypy
        pytest
        pytest-mock
        python-markdown-math
        mkdocs
        mkdocs-material
        coverage
        types-requests
        plotext
        debugpy
    ];

    propagatedBuildInputs = with pkgs; [
        python312Packages.poetry-core
        python312Packages.setuptools
        zlib
    ];

    buildInputs = mainDependencies ++
        deepLearningDependencies ++
        developerDependencies;
}
