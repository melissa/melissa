let
    pkgs = import <nixpkgs> {
        overlays = [ (import ./mpi4py-overlay.nix) ];
    };
in
pkgs.mkShell rec {

    name = "melissa-devshell";
    src = ./..;

    melissaBuild = import ./melissa.nix { 
    	inherit pkgs;
	    inherit src;
    };
    
    pyMelissaBuild = import ./py-melissa.nix {
        inherit pkgs;
        inherit src;
        mpi4py = pkgs.mpi4py;
    };
    
	devShellDependencies = with pkgs; [
        pkg-config
        bash-completion
        cmake
        libgcc
        gfortran
        mpi
        zeromq
        git
        curl
        vim
        neovim
    ];
    
    propagatedBuildInputs =
    # ======Developer shell deps======
        devShellDependencies ++
        pyMelissaBuild.developerDependencies ++
    # ================================
        pyMelissaBuild.mainDependencies ++ 
        pyMelissaBuild.deepLearningDependencies ++ [
        melissaBuild
        pyMelissaBuild
    ];

    libsToFind = pkgs.lib.strings.makeLibraryPath [
        pkgs.libgcc
        pkgs.mpi
        pkgs.libz
        melissaBuild
    ];

    shellHook = ''
        export CMAKE_PREFIX_PATH="${melissaBuild}";
        export LD_LIBRARY_PATH="${libsToFind}";
        export CC=$(which gcc)
        export CXX=$(which g++)
        export MPICC=$(which mpicc)
        export MELISSA_ROOT="${src}";
        cat ${src}/.nix/devshell.msg
        source ${melissaBuild}/bin/melissa_set_env.sh
        export EDITABLE_SRC=$(dirname $PWD)
        export PYTHONPATH="$EDITABLE_SRC:$PYTHONPATH"
    '';
}
