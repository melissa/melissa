self: super: {
        mpi4py = super.python312Packages.mpi4py.overrideAttrs (oldAttrs: {
            version = "3.1.6";
            src = super.fetchPypi {
            pname = "mpi4py";
            version = "3.1.6";
            sha256 = "sha256-yPpiXg+SsILvlVv7UvGfpmkdKSc9fXETXSlaoUPe5ss=";
        };
    });
}