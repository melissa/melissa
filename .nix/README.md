# Testing Melissa within Nix

## Overview
The `default.nix` file provides a Nix shell or build environment for development. This is the quickest way to test Melissa Study locally.

**Note:** These Nix files do **not** provide any GPU support.

## Installation and Development Setup
Nix installs Melissa using the current source directory, meaning the entry points for Melissa binaries such as `melissa-launcher`, `melissa-server`, etc., will point to the Melissa modules inside the `/nix/store`.

However, as developers, we prefer to have Melissa installed in an editable mode, similar to `pip install -e .`. This is not possible within `/nix/store`.

### Workaround
To enable an editable installation, we update the `PYTHONPATH` to point to a desired source directory (`EDITABLE_SRC`), where we develop the Melissa module. This allows us to access the desired source directory directly.

**Note:** If you modify the `api/` folder, which is not installed via `pip install` but rather through CMake, simply re-entering the shell will automatically update `melissa_api`.

## Running Melissa
To enter a `nix-shell` with Melissa installed, run:

```bash
# from melissa/
nix-shell .nix/default.nix
```

That's it! You are now in a `nix-shell` environment with Melissa installed.

## Running the Heat-PDE Example
### Step 1: Build the Executables
Run the following commands to build the executables:

```bash
cmake -S examples/heat-pde/executables -B examples/heat-pde/executables/build
make -C examples/heat-pde/executables/build
```

### Step 2: Launch Melissa
Fix the paths in the config file, then run:

```bash
melissa-launcher --config examples/heat-pde-dl/config_mpi.json
```
