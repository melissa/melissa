# ZeroMQ Configuration #

# Option to install ZeroMQ from source
option(INSTALL_ZMQ "Enable to download and build ZeroMQ from source" OFF)

if(INSTALL_ZMQ)
  include(${CMAKE_ROOT}/Modules/ExternalProject.cmake)

  set(ZMQ_VERSION "4.3.5")
  # Configure ExternalProject to download and build ZeroMQ
  ExternalProject_Add(
    ZeroMQ
    URL "https://github.com/zeromq/libzmq/releases/download/v${ZMQ_VERSION}/zeromq-${ZMQ_VERSION}.tar.gz"
    PREFIX ${CMAKE_CURRENT_BINARY_DIR}/ZeroMQ
    CMAKE_ARGS
      -DCMAKE_INSTALL_PREFIX=${CMAKE_INSTALL_PREFIX}
      -DCMAKE_INSTALL_LIBDIR=${CMAKE_INSTALL_PREFIX}/lib
      -DZMQ_BUILD_TESTS=OFF
  )

  # Set include and library paths for the built ZeroMQ
  set(ZeroMQ_INCLUDE_DIR "${CMAKE_INSTALL_PREFIX}/include")
  set(ZeroMQ_LIBRARY "${CMAKE_INSTALL_PREFIX}/lib/${CMAKE_SHARED_LIBRARY_PREFIX}zmq${CMAKE_SHARED_LIBRARY_SUFFIX}")

  message(STATUS "ZeroMQ will be downloaded and built from source.")
else()
  # Use pkg-config to locate ZeroMQ
  find_package(PkgConfig REQUIRED)

  # Locate ZeroMQ include directory and library
  pkg_check_modules(PC_ZeroMQ QUIET libzmq)

  find_path(
    ZeroMQ_INCLUDE_DIR
    NAMES zmq.h
    PATHS ${PC_ZeroMQ_INCLUDE_DIRS} "${PC_ZeroMQ_PREFIX}/include" "${ZeroMQ_ROOT}/include" "/usr/include"
    NO_DEFAULT_PATH
  )

  find_library(
    ZeroMQ_LIBRARY
    NAMES zmq
    PATHS ${PC_ZeroMQ_LIBRARY_DIRS} "${ZeroMQ_ROOT}/lib"
    NO_DEFAULT_PATH
  )

  # Optional: Ensure the located ZeroMQ is the correct version (example: >= ${ZMQ_VERSION})
  # TODO: Add version checking logic if required

  if(ZeroMQ_INCLUDE_DIR AND ZeroMQ_LIBRARY)
    message(STATUS "ZeroMQ found: ${ZeroMQ_LIBRARY}")
  else()
    message(FATAL_ERROR "ZeroMQ not found. Please install ZeroMQ or enable INSTALL_ZMQ.")
  endif()
endif()
