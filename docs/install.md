# Installation

While Melissa was originally designed for use on supercomputers, it can also be installed and run on a local machine. In fact, users are encouraged to begin with a local installation to familiarize themselves with the framework before deploying it at scale.

Melissa's installation process is divided into two parts due to its code structure:

- API Installation: The API, written in C and wrapped for other languages, is compiled and installed using CMake.
- Launcher and Server Installation: The launcher, server components, and their dependencies are built and installed using pip.

The optimal installation method depends on the user's environment and requirements. This tutorial provides step-by-step guidance for each approach, helping users choose the most suitable method for their needs.

!!! danger "Availability"
    Melissa is currently supported for linux systems only. Windows and MacOS are unsupported. Although, it is possible to create a docker cluster with SLURM. (see [Creating a SLURM-docker cluster](creating-a-slurm-docker-cluster.md)).

## Before building Melissa

The following dependencies must be installed before building Melissa:

- CMake 3.15+
- GNU Make
- A C99 compiler
- MPI (OpenMPI 3+)
- Python 3.9+
- ZeroMQ 4.1.5+

On Debian based systems with root access, these dependencies can be installed via:

```bash
sudo apt-get install \
  cmake \
  build-essential \
  libopenmpi-dev \
  python3 \
  python3-dev \
  python3-venv \
  libzmq3-dev
```

!!! Note
    If the [module system](https://hpc-wiki.info/hpc/Modules) is not available or if it does not contain all the required dependencies, this procedure can easily be supplemented (or substituted) with other package managers (_e.g._ Spack or `conda`).

In addition, Melissa has three levels of Python dependencies that will be installed with `pip` at build:

| **Description**                                                 | **File**                                                                                                                |
|-----------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------|
| Dependencies sufficient to run the sensitivity analysis server. | [requirements.txt](https://gitlab.inria.fr/melissa/melissa/-/blob/develop/requirements.txt)                             |
| Dependencies required to run a deep-surrogate training.         | [requirements_deep_learning.txt](https://gitlab.inria.fr/melissa/melissa/-/blob/develop/requirements_deep_learning.txt) |
| Additional dependencies for development purposes.               | [requirements_dev.txt](https://gitlab.inria.fr/melissa/melissa/-/blob/develop/requirements_dev.txt)                     |                                       |

## Quick Installation

The following guidelines outline how to manually install `melissa` module using `pip` and Melissa's client API using CMake.

Clone from its INRIA GitLab repository:

```bash
git clone https://gitlab.inria.fr/melissa/melissa.git
cd melissa
```

### Install `melissa` module

#### Installing dependencies
**Manually:** If you prefer to manage dependencies manually (e.g., using `~/.local`, Docker images, an existing environment, etc.), the requirement files are available in the repository.
```bash
pip install --upgrade pip                         # upgrade pip
pip install -r requirements.txt \                 # base dependencies (for sensitivity analysis)
            -r requirements_deep_learning.txt \   # deep learing dependencies (optional)
            -r requirements_dev.txt               # developer dependencies (optional)

pip install --no-deps .                           # install melissa module
```
!!! Warning
    If the user has already installed the dependencies, always pass `--no-deps`, especially while installing `melissa` with `--target` option.

**Using `pyproject.toml`:** The latest implementation specifies dependencies in `pyproject.toml`, and they are installed along with `melissa` module.
```bash
pip install .                                     # base dependencies (for sensitivity analysis)
# OR
pip install .[dl]                                 # base dependencies + deep learning dependencies
# OR
pip install .[dev]                                # base dependencies + developer dependencies
# OR
pip install .[all]                                # base dependencies + deep learning dependencies + developer dependencies
``` 

!!! Note
    As a Melissa developer, specify `--editable` flag with `pip install` to have more flexibility with development.


!!! Important
    Users can install the `melissa` module and its binary entry points in one of two ways:

    - **Local Environment Installation:** Use `pip install --user .` to install the module into `~/.local`
    - **Custom Location Installation:** Specify a target directory with `pip install --target=/path/to/install .`
    This method is particularly recommended when working within a `conda` environment to prevent potential installation issues. *One can also maintain multiple melissa installations in several folders specified using `--target`* and avoid potentional conflicts.


### Install Client API with CMake

Finally, the API can be built as follows:

```bash
# from melissa/
cmake -DCMAKE_INSTALL_PREFIX=install/ -S . -B build
make -C build -j 12
make -C build install
```

As evidenced by the following table, users installing on a local machine may benefit from including the `-DINSTALL_ZMQ=ON` flag.

| CMake option             | Default value | Possible values | Description |
| --                       | --            | -- | -- |
| `-DCMAKE_INSTALL_PREFIX` | `./install`  | any location on the file system | Melissa installation directory |
| `-DINSTALL_ZMQ`          | `OFF`         | `ON`, `OFF` | Download, build, and install ZeroMQ |


!!! Danger
    If `CMAKE_INSTALL_PREFIX` and `pip install --target` directories are the same, then perform pip installation prior CMake as `--target` overwrites the specified folder.


After a successful installation:

```bash
# from melissa/
source .venv/bin/activate # activate venv if created earlier
source melissa_set_env.sh # Update environment variables
melissa-launcher -h       # print melissa-launcher help
```

_Refer to the generic environment [`melissa_set_env.sh`](#the-initialization-script) script._


### Spack Installation

- It is recommended that users familiarize themselves with managing compilers, environments, etc in Spack before installing the Melissa packages. [Spack documentation: _Getting Started_](https://spack.readthedocs.io/en/latest/getting_started.html)

- Add Melissa in an environment using system's default compiler:
  ```bash
  spack env create melissa-env
  spacktivate melissa-env
  spack add melissa@target            # adds the client API
  spack add py-melissa-core@target    # adds the melissa python module with basic/minimal dependencies 
  spack spec                          # shows what will be installed
  spack install                       # actually installs the packages
  ```

- Load Melissa environment:
  ```bash
  spacktivate melissa-env
  melissa-launcher -h
  ```

#### Variants for `py-melissa-core`
By default, `py-melissa-core` installs base dependencies along with `tensorflow` (required for TensorBoard logging).

1. `+torch` installs `py-torch`
2. `+tf` installs `py-tensorflow`
3. `+cuda cuda_arch=xx` must be given with `+torch` or `+tf` to install these frameworks with CUDA support.

For example, `spack install py-melissa-core +torch +cuda cuda_arch=70` will install PyTorch CUDA with compute capability of 70 (supports V100 GPUs).


!!! Warning
    Although very flexible, Spack can cause a lot of files to be created hence saturating inodes quota. To alleviate this constraint, users can force Spack to use cluster provided packages as explained below.

### Spack and modules

If you have installed Melissa on a supercomputer which works with modules, you may want to have Melissa build depending on one of such module. Typically, the MPI module optimized for your supercomputer, here is a basic example on how to proceed:

* Load the module:
  ```console
  $ module load openmpi/4.0.5

  Loading openmpi/4.0.5
  Loading requirement: intel-compilers/19.1.3
  ```

* Tell Spack to search for installed (so-called "external") packages:
  ```console
  $ spack external find

   ==> The following specs have been detected on this system and added to /home/user/.spack/packages.yaml
   openmpi@4.0.5
  ```

* Afterwards the modules can be unloaded:
  ```bash
  module purge
  ```

* The module may have been compiled with a specific compiler. Add the compiler to Spack:
  ```console
   $ spack compiler find

   ==> Added 1 new compiler to /home/user/.spack/linux/compilers.yaml
    intel@19.1.3.304
   ==> Compilers are defined in the following files:
    /home/user/.spack/linux/compilers.yaml
  ```

* Spack can only recognize a small number of packages automatically and in many cases it is advisable or even necessary to edit the list of available packages manually. The list of packages can be found at `~/.spack/packages.yaml`. If you perform these modifications, you must find out:
    * the path to the package or the relevant module(s) to load.
    * the Spack package name (e.g., the Spack package name of CMake is `cmake`)
    * the package version,
    * and the compiler that was used.

  The compiler dependency is very important because in the author's experience different compiler modules may depend on different versions of essential libraries such as `libc` or `binutils`. Using a package that was built with one compiler while a different compiler module is loaded may lead to crashes in the best case and silent data corruption or a waste of supercomputer CPU hours in the worst case.

  An example with Intel MPI:
  ```yaml
  packages:
    gmake:
      externals:
      - spec: gmake@4.2.1
        prefix: /usr
    intel-mpi:
      externals:
      - spec: intel-mpi@2019.5.281%intel@19.0.5.281
        modules: [intel-all/2019.5]
  ```

* In some environments existing packages must be used; they should never be built by Spack. For example on supercomputers, MPI must often be linked against libraries of the computers' batch scheduler for otherwise, MPI applications cannot be launched. You can prohibit building packages from scratch. Example:
  ```yaml
  packages:
    mpi:
      buildable: false
  ```

* Build Melissa with specific compiler and package dependencies:
  ```bash
   spack install melissa py-melissa-core %intel@19.1.3.304 ^openmpi@4.0.5
  ```

## Advanced Installation
### Example cluster

On a supercomputer while working with modules, it is important that you always load the same modules that were used while installation.

For example, On [Grid5000]() can load,

```bash
module load \
    python/3.10.8_gcc-10.4.0 \
    cmake/3.23.3_gcc-10.4.0 \
    cuda/12.0.0_gcc-10.4.0 \
    cudnn/8.4.0.27-11.6_gcc-10.4.0 \
    openmpi/4.1.5_gcc-10.4.0 \
    gcc/10.4.0_gcc-10.4.0
```

Afterwards, you must follow [Quick Installation](#quick-installation) section. But you must make sure to load the same modules again while running a study.


### Jean-zay

On the other hand, the Python dependencies may be constrained by the available packages. On [Jean-Zay](http://www.idris.fr/eng/jean-zay/index.html) for instance, `torch` and `tensorflow` versions optimized for GPU execution are provided in their own `conda` environments and with Python versions different from the default one 
on the head-node.

```bash
module purge                             # purge all pre-loaded modules
module load pytorch-gpu                  # switch to torch-gpu Python environment
```

Install the Melissa binaries and missing dependencies in `~/.local` **from a compile-node that has network access**:

```bash
# from melissa/
pip3 install --user --no-cache-dir -r requirements.txt -r requirements_deep_learning.txt 
pip3 install --target=install/ --no-deps --e .
```

_We use `--no-deps` to install Melissa separately in `--target=install/` while keeping dependencies in `~/.local`. This ensures that dependencies remain consistent across multiple Melissa module installations._

!!! Important
    Follow proper [guidelines](http://www.idris.fr/eng/jean-zay/gpu/jean-zay-gpu-python-env-eng.html) when installing python packages with `pip`. Here, we must pass `--user` as the loaded `conda` environment is locked.


### The initialization script

Due to the structure of Melissa, environment variables must be set consistently with the way the software is built. To ensure this, Melissa relies on an automatic environment setup script `melissa_set_env.sh` that is created at build by CMake from `melissa_set_env.sh.in`:

```bash
#! /bin/bash

if [ -z "$MELISSA_ENV_INITIALIZED" ]; then
    export MELISSA_INSTALL_PREFIX=@CMAKE_INSTALL_PREFIX@
    export MELISSA_ROOT=@PROJECT_SOURCE_DIR@

    INSTALL_LIB="$MELISSA_INSTALL_PREFIX/lib"
    INSTALL_PATH="$MELISSA_INSTALL_PREFIX/bin"

    if [ -z "$PATH" ]; then
    export PATH="$INSTALL_PATH"
    else
    export PATH="$INSTALL_PATH:$PATH"
    fi
    if [ -z "$LD_LIBRARY_PATH" ]; then
    export LD_LIBRARY_PATH="$INSTALL_LIB"
    else
    export LD_LIBRARY_PATH="$INSTALL_LIB:$LD_LIBRARY_PATH"
    fi

    if [ -d "$HOME/.local" ]; then 
        export PATH="$HOME/.local/bin:$PATH"
        export LD_LIBRARY_PATH="$HOME/.local/lib:$LD_LIBRARY_PATH"
    fi

    PY_VERSION="python@Python3_VERSION_MAJOR@.@Python3_VERSION_MINOR@"
    if [ -z "$PYTHONPATH" ]; then
        export PYTHONPATH="$MELISSA_INSTALL_PREFIX/lib/$PY_VERSION/site-packages"
    else
        export PYTHONPATH="$MELISSA_INSTALL_PREFIX/lib/$PY_VERSION/site-packages:$PYTHONPATH"
    fi

    export PYTHONPATH="@CMAKE_SOURCE_DIR@:$HOME/.local/lib/$PY_VERSION/site-packages:$PYTHONPATH"
    export MELISSA_ENV_INITIALIZED=1
else
    echo "Melissa environment is already initialized."
fi
```

`melissa_set_env.sh` is copied both in `CMAKE_INSTALL_PREFIX/bin` and in `melissa`.

Melissa ensures that this file is sourced every time a new server or client job is executed. This piece of code serves two main purposes:

- Ensure that the API sources and executables are added to `LD_LIBRARY_PATH` which is required in order to compile the Melissa instrumented solver.

- Update `PATH` and `PYTHONPATH` with the Melissa Python executables and required packages.

!!! Note
    By default, the client and server job scripts (_i.e._ `client.sh` and `server.sh`) will try to source `melissa_set_env.sh` by running:
    ```bash
    . /path/to/melissa/melissa_set_env.sh
    ```
    which corresponds to the last created script. In case multiple versions coexist, the targeted version and its sourcing script can be selected explicitly by adding the following lines to the config file:
    ```json
    "client_config": {
        "melissa_client_env": "/path/to/melissa/<installation_folder>",
    },
    "server_config": {
        "melissa_server_env": "/path/to/melissa/<installation_folder>",
    }
    ```

<!-- #### Dependency propagation during execution

As explained in the [Building a new use-case](./new-use-case.md) tutorial, specific dependencies and environment variables must be set using the `preprocessing_commands` lists in the user configuration file. These bash commands are executed before the data generator executable, on each individual job node. Therefore, they need to prepare the node for the data generator and the `melissa-server`. For example, loading modules to prepare for a data generator would set `client/server_config` to look as follows:
```json
    "client_config": {
        "executable_command": "/path/to/melissa/examples/heat-pde/executables/build/heatc",
        "preprocessing_commands":[
                    "module -v load zeromq/4.2.5",
                    "module -v load openmpi/4.0.2-cuda"
        ]
    },
    "server_config": {
        "preprocessing_commands":[
                    "module load -v pytorch-gpu/py3/1.10.0"
        ]
    }
```

See [`examples/heat-pde`](https://gitlab.inria.fr/melissa/melissa/-/tree/develop/examples/heat-pde) and [`examples/lorenz`](https://gitlab.inria.fr/melissa/melissa/-/tree/develop/examples/lorenz) for more configuration examples. -->

