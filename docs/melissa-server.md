# Melissa Server

The Melissa server serves as the central coordinator of the Melissa ecosystem. It:  

- Establishes and manages communication between the server, launcher, and clients.  
- Processes all incoming data from clients via the Melissa API.  
- Instructs the launcher to schedule new client instances.  
- Detects failures and re-launches clients when needed.

![image](assets/server-class-hierarchy.drawio.png)

## Base Server

Base server handles,

- Connections with the Launcher
- Parameter Sampling
- Client Script Generation
- Basic Monitoring with the help of the Launcher
- General Checkpointing of Server Metadata
- Fault Tolerance

As shown above, `BaseServer` serves as the primary parent class in the inheritance hierarchy. Each subclass specializes in logistics, and management of the reception, and the main processing. _For instance, `DeepMelissaServer` utilizes round-robin communication to gather client data into a buffer for training, whereas `SensitivityAnalysisServer` employs NxM communication to conduct real-time statistical analysis of field data._

### Understanding `FaultTolerance` Class

Melissa’s elastic and fault-tolerant design enables it to handle client job failures effectively through mechanisms at both the launcher and server levels. At the server level, the `FaultTolerance` class is responsible for managing client failures. It is instantiated as the `ft` object within `BaseServer` and provides multiple methods to detect and react to failures.  

- **Detecting Failures:** When the launcher identifies a client job failure, it notifies the server via the `launcherfd` socket. The server can then restart the failed client group with the same or newly generated inputs.
- **Monitoring Timeouts:** The server uses an internal `timerfd` socket to track client job timeouts. Every `simulation_timeout` seconds, it checks for non-finished clients that haven’t sent messages since the last verification.  

#### Handling Different Failure Types  

- **Non-Deterministic Failures:** If failures are random, simply restarting the affected clients should be sufficient.  

- **Deterministic Failures:** Incorrect study configurations can cause widespread client failures that result in the study being aborted. Alternatively, if failures stem from input values that prevent convergence, the system employs a retry strategy: after `crashes_before_redraw` failures, clients are restarted with new, randomly sampled inputs.


!!! Note
    If `fault_tolerance` is set to `false`, the mechanism is disabled. Failures will still be detected, but instead of restarting failed instances, the study will stop immediately. Additionally, `fault_tolerance` is closely linked to the server's checkpointing mechanism, which is further explained in the [Fault Tolerance](fault-tolerance.md) section.

### Understanding the Different Communication Protocols  

A Melissa server is designed to collect data from instrumented clients, which send their data through `ZeroMQ` sockets. However, since clients operate within a different `MPI_COMM_WORLD` than the server, data transfer requires a structured approach. The sensitivity-analysis and deep-learning servers process data differently, leading to the use of two distinct communication protocols:  

- **Round-robin:** Each simulation gathers data on its rank 0, and for each time step, the targeted server rank increments sequentially until all ranks are used, at which point the cycle restarts from rank 0. This protocol is ideal for the deep-learning server, as deep-surrogates typically predict solutions across the entire computational domain at each time step, requiring the server to have access to the full solution for each sample.  
!!! Note  
    When a time step is sent to a specific rank, all other ranks receive a message containing an empty data array, which the server automatically discards.  

- **NxM:** In this protocol, the client and server are parallelized over `N` and `M` ranks, respectively, ensuring that data is evenly distributed across all server ranks. This approach is well-suited for the sensitivity-analysis server, as statistics are computed independently for each mesh element, meaning the server does not require the full solution for each sample.  

The Melissa API determines which protocol to use based on the `sobol_op` and `learning` attributes of the server:  

| Server | `learning` | `sobol_op` | Protocol |  
|---------|-----------|------------|-----------|  
| Deep-learning | `2` | `0` | Round-robin |  
| Sensitivity-analysis (without Sobol) | `0` | `0` | NxM |  
| Sensitivity-analysis (with Sobol) | `0` | `1` | NxM |

The specific case where Sobol indices are computed (_i.e._ `sobol_op=1`) is discussed below.

## Sensitivity Analysis Server  
SA server handles,

- Reception Logic
- Iterative statistics Computation
- Checkpointing

Users must inherit the `SensitivityAnalysisServer` and set a parameter sampling strategy. [Building an SA Server](use-case/build-new-server.md#sensitivity-analysis-server) section walks through how to build your own SA server.

#### Key aspects
- SA server utilizes the `compute_stats` function to calculate the required statistics. These computations are now handled using the Python [`iterative-stats`](https://pypi.org/project/iterative-stats/) library, which provides efficient implementations of various iterative statistical methods.
- When a client calls `melissa_send` on a field, it transmits partial data instead of the entire time step. This partial data is stored in `PartialSimulationData` on the server side and passed directly to the `compute_stats` function, allowing independent computation without waiting for the full time step.

#### Understanding Sobol Indices Computation and the Pick-Freeze Method

The Sobol indices computation utilizes the pick-freeze method, which aims to generate two independent and `nb_parameters` correlated samples required by the Martinez formula. This method is thoroughly explained in Section 3 of [Terraz _et al._](https://hal.inria.fr/hal-01607479/document).

In Melissa, the procedure is handled by constructing groups of size `nb_parameters + 2`, with inputs sampled according to the pick-freeze method. First, two input lists are generated by calling the `draw` function of the sample generator twice. Then, the remaining `nb_parameters` sets of inputs are created by combining the first two sets. (Refer to `pick_freeze` [here](docstrings/parameters.md#melissaserverparameterspickfreezemixin))

!!! Warning
    The pick-freeze method assumes that the sampling function (in this case `draw`) provides independent uniformly distributed random variables and is hence not compatible with any [Design of Experiment](doe.md).


The **SA server has reimplemented the Sobol computation strategy**, which differs from the deprecated approach outlined below. In the updated method, each client in a group sends its time step to the server as usual. The server then caches these time steps based on the group of clients. Once all time steps for a specific group are received, the server assembles the data and passes it to the `compute_stats` function.

_By offloading Sobol caching on server-side, we reduce memory usage and avoid extra communication on clients. This can be helpful as server will have relatively more resources to manage caching._

!!! Note
    The following changes were made in the client-side API in `api/melissa_api.c` to adapt to the new strategy.
    ```diff
    -        global_data.sobol = global_data.rinit_tab[1];
    +        global_data.sobol = 0;
    ```

!!! Warning "Deprecated Sobol Computation Strategy"
    Previously, Each sample was first gathered on client 0 of the group before being sent to the server. This behavior was automatically triggered when `sobol_op=1` and `learning=0` conditions were met in the API. As a result, the solutions from the entire group were communicated on the server-side before being passed to the `compute_stats` function.

## Deep Learning Server
DL server handles,

- Reception Logic
- Configuration of buffer, dataset, dataloader, etc.
- Framework-agnostic training
- Checkpointing of model, optimizers, etc.

Users must inherit either the `TorchServer` or `TFServer` and set a parameter sampling strategy.  [Building a DL Server](use-case/build-new-server.md#deep-learning-server) section walks through how to build your own DL server.

#### Key aspects

##### Understanding Buffer/Reservoir
Online training architectures, like Melissa's, rely on a carefully managed "reservoir" or buffer. This buffer acts as a bridge between the client (data generator) and the dataset iterator. While clients continuously `put` new samples into the buffer, the dataset iterator `gets` samples to form training batches. The Melissa buffer effectively handles several key challenges:

- **Is the buffer sufficiently populated for diverse sampling?** The `per_server_watermark` parameter in `dl_config` controls this threshold. If the buffer lacks enough samples, the dataset iterator will wait before building training batches. Once the watermark is reached, sampling begins.  

- **How does the dataset iterator sample from the buffer?** Melissa provides three sampling methods:  
    - **FIFO:** A simple queue where the oldest sample is removed on read (`get`).  
    - **FIRO:** Instead of removing the oldest sample, this method randomly selects a sample for eviction (uniform sampling), also on read (`get`).  
    - **Reservoir:** Designed to reduce bias and prevent catastrophic forgetting, this buffer evicts samples on write. It retains samples as long as possible, replacing a randomly chosen sample only when full, ensuring diverse repetitions in online training.  

- **What happens when clients stop sending data, but the buffer still has samples?** By default, Melissa continues drawing samples until the buffer is emptied, or the required batches are formed. For the `Reservoir` buffer, which retains samples as long as possible, Melissa transitions from eviction on write to eviction on read when all clients finish sending data. This ensures the buffer is fully flushed at the end of training, preventing an infinite training loop.


Users can set the buffer based on the available resources as follows:

```json
{
    "dl_config": {
        "buffer": "Reservoir",
        "per_server_watermark": 500,
        "buffer_size": 5000
    }
}
```

##### Understanding Datasets & Dataloaders

- The dataset is initialized with the user-selected buffer and is responsible for feeding samples from the buffer to the training loop. This dataset inherits `MelissaIterableDataset` class which has a `__iter__` method implemented for yielding samples from the buffers. By default, the dataset stops yielding samples when the server is no longer receiving data from clients and the buffer holds fewer than `per_server_watermark` samples.  `TorchServer`, and `TfServer` use specific dataset types which inherit `MelissaIterableDataset`.

!!! Note  
    The default logic tracks both the number of batches seen and the expected number of batches. Once the server stops receiving data, it gradually reduces `per_server_watermark` to zero, ensuring the buffer is emptied. If enough batches have been processed, the training loop terminates, preventing potential deadlocks in distributed server processes.

- The dataloader is selected based on the framework in use:
    - `torch.utils.data.Dataloader` for `TorchServer`
    - `tf.data.Dataset.from_generator` for `TfServer`.

If no framework is specified, `DeepMelissaServer` defaults to `GeneralDataLoader`, which handles batch creation.


##### Understanding Training & Validation
Regardless of the framework, Melissa follows a standardized training loop where each section is highly customizable through specific training hooks.

```python
def train(self):
    ...
    self._on_train_start()
    for batch_idx, batch in enumerate(self._train_dataloader):

        batch_idx += self.batch_offset

        if self.other_processes_finished(batch_idx):
            break

        self._on_batch_start(batch_idx)
        self.training_step(batch, batch_idx)
        self._on_batch_end(batch_idx)

        if (
            self.rank == 0
            and self._valid_dataloader is not None
            and batch_idx > 0
            and (batch_idx + 1) % self.nb_batches_update == 0
        ):
            self.validation(batch_idx)

        self._checkpoint(batch_idx)
    # end training loop
    self._on_train_end()
    ...
```

`other_processes_finished` prevents deadlocks in multi-rank server training by synchronizing all ranks while ensuring each has data available from its buffer. If any rank completes its training, the loop breaks to maintain efficiency and avoid stalls.

Validation occurs on server rank 0 every `nb_batches_update` steps, but only if users manually set the `self.valid_dataloader` attribute in their custom server implementation. 

```python
    def validation(self, batch_idx: int):

        self._on_validation_start(batch_idx)
        for v_batch_idx, v_batch in enumerate(self._valid_dataloader):
            self.validation_step(v_batch, v_batch_idx, batch_idx)
        self._on_validation_end(batch_idx)

```

Users can override the `on_validation_*` hooks and `validation_step`, or modify the entire `validation` method if necessary.

!!! Important  
    In the `train` and `validation` methods, hooks with `_on` prefixes (e.g., `_on_train_start()`) execute predefined code specific to different server classes like `TorchServer` and `TfServer`. However, users should override the corresponding methods **without** the underscore (e.g., `on_train_start()`). The predefined hooks automatically call these user-defined methods, allowing customization while preserving built-in functionality.

##### Distributed Data Parallelism with `torch` and `tensorflow`

Deep surrogate training in Melissa leverages data parallelism, where each server rank has its own `reservoir` for batch extraction, which is then fed to the corresponding GPU. Each GPU also holds its own copy of the network architecture, and gradients are aggregated during each back-propagation step. This distributed training setup is supported by [`torch` DDP](https://pytorch.org/docs/stable/notes/ddp.html) and [`tensorflow` distributed training](https://www.tensorflow.org/guide/distributed_training), which manage inter-GPU communication. Proper environment setup is crucial for efficient operation, and in Melissa, this is handled through the `setup_environment` methods, following the IDRIS guidelines:

- [PyTorch: Multi-GPU and multi-node data parallelism](http://www.idris.fr/eng/jean-zay/gpu/jean-zay-gpu-torch-multi-eng.html)
- [TensorFlow: Multi-GPU and multi-node data parallelism](http://www.idris.fr/eng/jean-zay/gpu/jean-zay-gpu-tf-multi-eng.html)

!!! Warning
    With `torch`, tensors and operations can be explicitly assigned to a specific device using the [`to` method](https://pytorch.org/docs/stable/generated/torch.Tensor.to.html). However, in `tensorflow`, process/GPU pinning is more challenging when using the [`MultiWorkerMirroredStrategy`](https://www.tensorflow.org/api_docs/python/tf/distribute/MultiWorkerMirroredStrategy). When setting up the distributed environment, the [`SlurmClusterResolver`](https://www.tensorflow.org/api_docs/python/tf/distribute/cluster_resolver/SlurmClusterResolver) method uses `slurm` environment variables and `nvidia-smi` by default to gather information about the hosts, tasks, nodes, and available GPUs. To ensure each process only sees its designated GPU, the [`set_visible_devices`](https://www.tensorflow.org/api_docs/python/tf/config/set_visible_devices) method is used to restrict visibility to the corresponding process ID. This is handled automatically when `auto_set_gpu=True`, but may fail if the `CUDA` environment is initialized before the `SlurmClusterResolver` call. In Melissa, the environment variable is set in `setup_environment` as follows:

    ```python
    physical_devices = tf.config.list_physical_devices('GPU')
    local_rank = self.rank % len(physical_devices)
    tf.config.set_visible_devices(physical_devices[local_rank], 'GPU')
    ```

##### Validation with a pseudo offline approach

The server offers a `pseudo_epochs` setting in `dl_config` for small-scale prototype validation, shifting Melissa from online to pseudo-offline training. This allows users to aggregate all client samples before training, mimicking offline learning while using the basic `FIRO` buffer. During training, the system samples from the buffer to create `pseudo_epochs` worth of batches. However, this does not guarantee each data point is seen exactly `pseudo_epochs` times. Instead, the total batch count follows `(nb_time_steps * nb_clients / batch_size) * pseudo_epochs`, with uniform random sampling from the full dataset. Users can enable this by setting `pseudo_epochs` in `dl_config`. By default, it is set to 1, maintaining standard online training behavior.

!!! Warning
    The `pseudo_epochs` feature has been retained but has not been tested in the latest implementation.