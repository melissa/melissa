# Creating a dataloader
::: melissa.server.deep_learning.dataset.make_dataloader
    options:
        show_root_heading: true

# Iterable Dataset Classes

### melissa.server.deep_learning.dataset.GeneralDataLoader
::: melissa.server.deep_learning.dataset.GeneralDataLoader

### melissa.server.deep_learning.dataset.torch_dataset.as_torch_dataloader
::: melissa.server.deep_learning.dataset.torch_dataset.as_torch_dataloader

### melissa.server.deep_learning.dataset.tf_dataset.as_tensorflow_dataset
::: melissa.server.deep_learning.dataset.tf_dataset.as_tensorflow_dataset