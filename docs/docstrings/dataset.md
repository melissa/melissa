# Creating a dataset
::: melissa.server.deep_learning.dataset.make_dataset
    options:
        show_root_heading: true

# Iterable Dataset Classes

### melissa.server.deep_learning.dataset.MelissaIterableDataset
::: melissa.server.deep_learning.dataset.MelissaIterableDataset

### melissa.server.deep_learning.dataset.torch_dataset.TorchMelissaIterableDataset
::: melissa.server.deep_learning.dataset.torch_dataset.TorchMelissaIterableDataset

### melissa.server.deep_learning.dataset.tf_dataset.TfMelissaIterableDataset
::: melissa.server.deep_learning.dataset.tf_dataset.TfMelissaIterableDataset