# Server Class

### melissa.server.base_server.BaseServer
::: melissa.server.base_server.BaseServer

### melissa.server.base_server.ServerStatus
::: melissa.server.base_server.ServerStatus

# Exceptions

### melissa.server.base_server.UnsupportedProtocol
::: melissa.server.base_server.UnsupportedProtocol

### melissa.server.base_server.ReceptionError
::: melissa.server.base_server.ReceptionError

