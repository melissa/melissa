# Parameter types and pre-defined classes

### melissa.server.parameters.ParameterSamplerType
::: melissa.server.parameters.ParameterSamplerType

### melissa.server.parameters.RandomUniform
::: melissa.server.parameters.RandomUniform

### melissa.server.parameters.HaltonGenerator
::: melissa.server.parameters.HaltonGenerator

### melissa.server.parameters.LHSGenerator
::: melissa.server.parameters.LHSGenerator


# Creating a parameter sampler
::: melissa.server.parameters.make_parameter_sampler
    options:
        show_root_heading: true


# Base classes
These parent classes can be used for creating a custom parameter sampler.
The inherited server must set the sampler via `set_parameter_sampler` method.

### melissa.server.parameters.BaseExperiment
::: melissa.server.parameters.BaseExperiment

### melissa.server.parameters.StaticExperiment
::: melissa.server.parameters.StaticExperiment

### melissa.server.parameters.SobolBaseExperiment
::: melissa.server.parameters.SobolBaseExperiment

## MixIn Classes

### melissa.server.parameters.PickFreezeMixIn
::: melissa.server.parameters.PickFreezeMixIn

### melissa.server.parameters.RandomUniformSamplerMixIn
::: melissa.server.parameters.RandomUniformSamplerMixIn

### melissa.server.parameters.QMCSamplerMixIn
::: melissa.server.parameters.QMCSamplerMixIn

### melissa.server.parameters.HaltonSamplerMixIn
::: melissa.server.parameters.HaltonSamplerMixIn

### melissa.server.parameters.LatinHypercubeSamplerMixIn
::: melissa.server.parameters.LatinHypercubeSamplerMixIn
