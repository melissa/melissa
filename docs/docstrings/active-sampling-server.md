# Active Sampling Server Classes

### melissa.server.deep_learning.active_sampling.active_sampling_server
### ExperimentalDeepMelissaActiveSamplingServer
::: melissa.server.deep_learning.active_sampling.active_sampling_server.ExperimentalDeepMelissaActiveSamplingServer

### melissa.server.deep_learning.active_sampling.torch_server
### ExperimentalTorchActiveSamplingServer
::: melissa.server.deep_learning.active_sampling.torch_server.ExperimentalTorchActiveSamplingServer