# Creating a TensorBoard Logger
::: melissa.server.deep_learning.tensorboard.make_tb_logger
    options:
        show_root_heading: true

# TensorBoard Logger Classes

### melissa.server.deep_learning.tensorboard.base_logger.TensorboardLogger
::: melissa.server.deep_learning.tensorboard.base_logger.TensorboardLogger

### melissa.server.deep_learning.tensorboard.torch_logger.TorchTensorboardLogger
::: melissa.server.deep_learning.tensorboard.torch_logger.TorchTensorboardLogger

### melissa.server.deep_learning.tensorboard.tf_logger.TfTensorboardLogger
::: melissa.server.deep_learning.tensorboard.tf_logger.TfTensorboardLogger

# Converting Logs to Pandas DataFrame
::: melissa.server.deep_learning.tensorboard.base_logger.convert_tb_logs_to_df
    options:
        show_root_heading: true
