# Buffer Types

### melissa.server.deep_learning.reservoir.BufferType
::: melissa.server.deep_learning.reservoir.BufferType


# Creating a buffer
::: melissa.server.deep_learning.reservoir.make_buffer
    options:
        show_root_heading: true

# Buffer Classes

### melissa.server.deep_learning.reservoir.FIFO
::: melissa.server.deep_learning.reservoir.FIFO

### melissa.server.deep_learning.reservoir.FIRO
::: melissa.server.deep_learning.reservoir.FIFO

### melissa.server.deep_learning.reservoir.Reservoir
::: melissa.server.deep_learning.reservoir.Reservoir

### melissa.server.deep_learning.reservoir.BatchReservoir
::: melissa.server.deep_learning.reservoir.BatchReservoir