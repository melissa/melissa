# Status Enums
### melissa.server.simulation.SimulationDataStatus
::: melissa.server.simulation.SimulationDataStatus

### melissa.server.simulation.SimulationStatus
::: melissa.server.simulation.SimulationStatus

# Simulation Data types
### melissa.server.simulation.SimulationData
::: melissa.server.simulation.SimulationData

### melissa.server.simulation.PartialSimulationData
::: melissa.server.simulation.PartialSimulationData

# Main Classes
### melissa.server.simulation.Simulation
::: melissa.server.simulation.Simulation

### melissa.server.simulation.Group
::: melissa.server.simulation.Group
