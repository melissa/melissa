# Melissa Client APIs

Melissa provides functionalities for sending data with,

- C
- Python
- Fortran & Fortran90


# C API

## `melissa_init`
Initializes the Melissa connection using an MPI communicator. Retrieves the rank and size of the communicator and calls `melissa_init_internal`.

### Parameters
- `field_name`: Field name to initialize.
- `vect_size`: Size of the local data vector.
- `comm`: MPI communicator.

## `melissa_init_f`
Wrapper function to initialize Melissa from Fortran. Converts an MPI Fortran communicator to a C communicator and calls `melissa_init`.

### Parameters
- `field_name`: Field name to initialize.
- `local_vect_size`: Pointer to local vector size (Fortran-style).
- `comm_fortran`: Pointer to MPI Fortran communicator.

## `melissa_send`
Sends a data vector to the Melissa server for the specified field. Used to transmit simulation results or data points during execution.

### Parameters

| Parameter     | Type            | Description                                |
|---------------|-----------------|--------------------------------------------|
| `field_name`  | `const char*`   | The name of the field to which data is sent.|
| `send_vect`   | `const double*` | Pointer to the data vector to be sent.      |

### Notes
- The data in `send_vect` should match the size expected by Melissa for the specified `field_name`.
- Ensure that `melissa_init` has been called prior to sending data.

## `melissa_finalize`
Finalizes the connection with the Melissa server, performing any necessary cleanup operations. This function ensures that all resources associated with the Melissa server are released properly.

### Notes
- Call this function after all data transfers to the Melissa server are complete.
- Should be called before the MPI environment is finalized (i.e., before `MPI_Finalize`).

## `melissa_init_internal`
Initializes the connection with the Melissa server. This function is responsible for setting up the communication necessary to send data to the Melissa library using the MPI framework.

### Parameters

| Parameter          | Type          | Description                                       |
|--------------------|---------------|---------------------------------------------------|
| `field_name`       | `const char*` | The name of the field to initialize.              |
| `local_vect_size`  | `int`         | The size of the local data vector to send.        |
| `comm_size`        | `int`         | The size of the MPI communicator.                 |
| `rank`             | `int`         | The MPI rank of the calling process.              |
| `comm`             | `MPI_Comm`    | The MPI communicator used for communication.      |

### Notes
- This function is `static`, meaning it is only accessible within the translation unit where it is defined.
- The MPI communicator (`comm`) must be initialized before calling this function.
- Proper MPI setup is required to ensure data is transmitted to the Melissa server.

# Python API
::: api.melissa_api


# Fortran(90) API

The Fortran API for Melissa is a thin interface to the underlying C API. It allows Fortran applications to interact seamlessly with the Melissa server by internally converting Fortran-style data structures, such as MPI communicators, into their C counterparts.

#### `melissa_init`
- Acts as a wrapper for `melissa_init` in the C API.
- Converts the Fortran MPI communicator (`MPI_Fint`) to a C MPI communicator (`MPI_Comm`).

#### `melissa_send`
- Invokes the C function `melissa_send` for data transmission.

#### `melissa_finalize`
- Calls `melissa_finalize` to properly close the Melissa connection.

### Notes
- All core functionality is implemented in the C API, ensuring efficient communication and data handling.
- The Fortran API primarily handles type conversion and calling conventions to maintain compatibility with Fortran applications.
