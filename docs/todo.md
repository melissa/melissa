## TODO list

* Short-term and current developments are listed as [Merge Requests](https://gitlab.inria.fr/melissa/melissa/-/merge_requests).

* Medium-term code related tasks are listed in [Issues](https://gitlab.inria.fr/melissa/melissa/-/issues).

* Long-term Melissa development plans are listed in this [Issue](https://gitlab.inria.fr/melissa/melissa-sa/-/issues/157).