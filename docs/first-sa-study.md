# A first Sensitivity-Analysis study

This tutorial assumes that the user has gone through [Quick Installation](install.md#quick-installation) and has built Melissa.

## Heat-PDE use-case

This section demonstrates a core use case of Melissa: performing sensitivity analysis, which generates iteratively computed statistics using parallel clients and a central server. Each client acts as a data generator, utilizing a parallelized solver to simulate a heat diffusion equation.

To highlight Melissa's language compatibility, the same solver originally developed in Fortran90 ([heat.f90](https://gitlab.inria.fr/melissa/melissa/-/blob/develop/examples/heat-pde/executables/heat.f90) and [heat_utils.f90](https://gitlab.inria.fr/melissa/melissa/-/blob/develop/examples/heat-pde/executables/heat_utils.f90)) has been converted into a C example via subroutine binding. As a result, this tutorial will produce two executables (one in each language).


#### Use case presentation

In this example, a finite-difference parallel solver is used to solve the heat equation on a cartesian grid of size $N_x \times N_y$ and the time discretization $N_t$. The solver input variables are:

- $T_0$ the initial temperature across the domain,
- $T_1$, the left wall temperature (the other wall temperatures are set to zero).

By solving the heat equation for multiple sets of inputs, the purpose of this example is to perform a sensitivity-analysis of the solution to these temperatures.

By default, the use case is configured to accept only two input parameters (`nb_parameters = 2`). This limits the group size to `group_size = nb_parameters + 2` when computing Sobol indices. However, users can increase the number of input parameters to a maximum of 5 (temperature values), depending on the available resources.

### Making melissa visible
Assuming that you are in the project's root directory `melissa/`. Update the current shell:
```bash
source melissa_set_env.sh
```

If Melissa was installed via a package manager, there is no need to manually set up the environment. Simply loading the API package will automatically configure the paths as needed.

### Running the example

Next, move to the example folder and build the example code:
```bash
cd examples/heat-pde/
cmake -S executables/ -B executables/build
make -C executables/build
cd heat-pde-sa/
```

If the build is successful, three new executables should appear in the `executables/build` sub-directory:

```bash
-rwxr-xr-x  1 root root 37264 Feb 20 10:06 heatc
-rwxr-xr-x  1 root root 37104 Feb 20 10:06 heatf
-rwxr-xr-x  1 root root 37192 Feb 20 10:06 heat_no_melissac
```

The configuration file `config_<scheduler>.json` is used to configure the Melissa execution (_e.g._ parameter sweep, computed statistics, launcher options). It must be edited at least to update the path to the executable:
```json
    "client_config": {
        "executable_command": "/path/to/melissa/examples/heat-pde/executables/build/heatc",
        "command_default_args": ["100", "100", "100"]
    }
```

The example can be started with one of several batch schedulers supported by Melissa: `OpenMPI`, `slurm`, or `OAR`. It may be necessary to **pass additional arguments directly** to the batch scheduler for a successful example run. For example, starting with version 3, `OpenMPI` refuses to `oversubscribe` by default and requires the `--oversubscribe` option to have more processes than there are available CPUs. If you end running Melissa with `mpirun` on your local machine, it may require this option.

!!! Note
    In the configuration files, you will find an option `command_default_args: ["100", "100", "100"]` ($N_x$, $N_y$, $N_t$) specifying default command line arguments to the `executable_command`. The sampled parameters ($T_0$, $T_1$) will be attached beyond this part as further command line arguments.

For the tutorial, we use the `OpenMPI` scheduler and the default `config_mpi.json` file:

```bash
melissa-launcher --config_name /path/to/heat-pde-sa/config_mpi
```

_How to setup a configuration for the study is explained in_ [Configuration Structure](melissa-overview.md#configuration-structure).

!!! Note
    The problem may not be computationally challenging problem but simply due to the number of simulation processes and depending on the resources available to the user, the system may end up being oversubscribed. If so, specifying `--oversubscribe` likewise can be helpful.
    ```json
    "scheduler_arg_client": ["-n", "1","--timeout", "60", "--oversubscribe"],
    "scheduler_arg_server": ["-n", "1","--timeout", "3600", "--oversubscribe"]
    ```
    This will have for effect to submit every `mpirun` command with this option.

All results, log files, and a copy of the configuration file are stored in a dedicated directory named `STUDY_OUT`. If not explicitly specified in the configuration file, the output directory defaults to the format `melissa-YYYYMMDDTHHMMSS`, where `YYYYMMDD` represents the current date, and `THHMMSS` represents the local time in ISO 8601 basic format.

For each time step, field, and statistic, the Melissa server generates a file `results/results_*.txt` containing the statistical value at every grid point.

!!! Note
    If errors were encountered, find `melissa_*.log` files in the output directory. Some errors may not be logged in these files. In that case, users can look in `stdout/` to check stdout and stderr files for a given job. With `OpenMPI` runs, server job is always the first job i.e. `openmpi.0`.

After a successful run, the statistics can be turned into a small movie with the aid of the script `plot-results-sa.py`. For example, the command below will create a movie from the `mean` of the `temperature` over all time steps:

```bash
python3 plot-results-sa.py STUDY_OUT/results temperature mean
```

![GIF](assets/temperature_mean.gif)
