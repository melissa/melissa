#!/usr/bin/python3
# from typing import Callable, Any, Optional
import logging
import torch
import torch.utils.data
import numpy as np
from typing import Tuple, Dict, Any, List
from typing_extensions import override

from melissa.server.deep_learning.torch_server import TorchServer
from melissa.server.simulation import SimulationData
from melissa.server.parameters import BaseExperiment

logger = logging.getLogger("melissa")


class LorenzParameterGenerator(BaseExperiment):
    """A custom parameter sampling class inheriting `BaseExperiment`."""
    def __init__(self, r, sigma, beta, rho, tf, dt, **kwargs):
        super().__init__(**kwargs)
        # some attributes are unused.
        # but simply unpacking them here for the
        # super() which does not accept extra kwargs.
        self.sigma = sigma
        self.beta = beta
        self.rho = rho
        self.tf = tf
        self.dt = dt
        self.r = r

    @override
    def sample(self, nb_samples=1) -> np.ndarray:
        """Using `self.rng` generator to produce 3 values."""
        return self.rng.random(size=(nb_samples, 3)).squeeze()

    @override
    def draw(self) -> List[str]:
        x0 = -self.r // 2 + self.r * self.sample(1)
        return [
            "--x0 " + " ".join(map(str, list(x0))),
        ]


class LorenzServer(TorchServer):
    """Use-case specific server"""

    def __init__(self, config_dict: Dict[str, Any]):
        super().__init__(config_dict)

        # specific configuraiton for sampling
        self.sweep_params = config_dict["sweep_params"]
        self.set_parameter_sampler(
            sampler_t=LorenzParameterGenerator,
            l_bounds=[-2],
            u_bounds=[2],
            seed=123,
            dtype=np.float32,
            **self.sweep_params
        )

    @override
    def prepare_training_attributes(self):
        """Abstract method that must return model and optimizer."""

        model = self.wrap_model_ddp(
            self.MyModel(3, 512).to(self.device)
        )
        optimizer = torch.optim.Adam(
            model.parameters(),
            lr=1e-3,
            weight_decay=1e-4
        )
        return model, optimizer

    @override
    def on_train_start(self):
        self.criterion = torch.nn.MSELoss()
        self.learning_rate_scheduler = torch.optim.lr_scheduler.ExponentialLR(
            self.optimizer, 0.995
        )

    @override
    def training_step(self, batch, batch_idx, **kwargs):
        self.optimizer.zero_grad()
        x, y_target = batch
        x, y_target = normalize(x, y_target)
        x = x.float().to(self.device)
        y_target = y_target.float().to(self.device)
        y_pred = self.model(x)
        loss = self.criterion(y_pred, y_target)
        # Backprogation
        loss.backward()
        self.optimizer.step()
        global_batch_idx = batch_idx * self.comm_size
        self.tb_logger.log_scalar("Loss/train", loss.item(), global_batch_idx)

    @override
    def on_batch_end(self, batch_idx):
        if batch_idx > 0 and (batch_idx + 1) % self.nb_batches_update == 0:
            self.learning_rate_scheduler.step()
            lrs = []
            for grp in self.optimizer.param_groups:
                lrs.append(grp["lr"])
            self.tb_logger.log_scalar("lr", lrs[0], batch_idx)

    @override
    def process_simulation_data(self, msg: SimulationData, config_dict: dict):
        """Abstract method for transformation while batch creation."""

        # Set positions inputs: x(n), x(n-1)
        x_prev = msg.data["preposition"]
        x_next = msg.data["position"]

        input_train = x_prev
        output_train = (x_next - x_prev) / self.sweep_params['dt']

        return input_train, output_train

    class MyModel(torch.nn.Module):
        """
        The base model architecture. Use-cases can
        override if they wish to change model architecture.
        """

        def __init__(self, input_dim, n_features=16):
            super().__init__()
            self.input_dim = input_dim
            self.output_dim = input_dim
            self.n_features = n_features
            self.net = torch.nn.Sequential(
                torch.nn.Linear(self.input_dim, self.n_features),
                torch.nn.SiLU(),
                torch.nn.Linear(self.n_features, self.n_features),
                torch.nn.SiLU(),
                torch.nn.Linear(self.n_features, self.output_dim),
            )
            print(f"Model summary: {self.net}")

        def forward(self, x):
            y = self.net(x)
            return y


# Values computed beforehand
x_mu = np.array([0.29185456, 0.3032881, 24.386353], dtype=np.float32)
x_std = np.array([8.135049, 9.0794735, 8.009931], dtype=np.float32)
y_mu = np.array([0.11236616, 0.0756601, 1.1951811], dtype=np.float32)
y_std = np.array([40.364864, 64.21792, 75.33695], dtype=np.float32)


def x_normalize(x: np.ndarray) -> np.ndarray:
    x_normalized = (x - x_mu) / (x_std + np.array([1e-7], dtype=np.float32))
    return x_normalized


def y_normalize(y: np.ndarray) -> np.ndarray:
    y_normalized = (y - y_mu) / (y_std + np.array([1e-7], dtype=np.float32))
    return y_normalized


def normalize(x: np.ndarray, y: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    x_normalized = x_normalize(x)
    y_normalized = y_normalize(y)
    return x_normalized, y_normalized


def y_unormalize(y: np.ndarray) -> np.ndarray:
    y_unormalized = y * (y_std + np.array([1e-7], dtype=np.float32)) + y_mu
    return y_unormalized
