from typing import Dict, Any, Optional, Union
import os.path as osp
import logging
import numpy as np
import torch
import torch.utils.data
import typing
from typing_extensions import override

from melissa.server.parameters import ParameterSamplerType
from melissa.server.deep_learning import active_sampling
from melissa.server.deep_learning.active_sampling.torch_server import \
    ExperimentalTorchActiveSamplingServer
from melissa.server.simulation import SimulationData


logger = logging.getLogger("melissa")


class ACHeatPDEServerDL(ExperimentalTorchActiveSamplingServer):
    """
    Use-case specific server
    """

    def __init__(self, config_dict: Dict[str, Any]):

        super().__init__(config_dict)
        study_options = config_dict["study_options"]
        self.param_list = ["ic", "b1", "b2", "b3", "b4", "t"]
        self.mesh_size = study_options["mesh_size"]
        self.Tmin, self.Tmax = study_options['parameter_range']
        seed = study_options["seed"]

        # keep in mind that predefined parameter sampler accept only
        # known keyword argumets
        self.is_breed_study = config_dict["sampler_type"] == "breed"

        if self.is_breed_study:
            # conforming non-breed key before unpacking
            breed_params = self.ac_config.get("breed_params", dict())

            self.set_parameter_sampler(
                sampler_t=ParameterSamplerType.DEFAULT_BREED,
                **breed_params,
                seed=seed,
                l_bounds=[self.Tmin],
                u_bounds=[self.Tmax]
            )
        else:
            self.set_parameter_sampler(
                sampler_t=ParameterSamplerType.RANDOM_UNIFORM,
                seed=seed,
                l_bounds=[self.Tmin],
                u_bounds=[self.Tmax]
            )
        self.valid_dataloader = self.get_validation_dataloader()
        self.val_losses: list = []

    @override
    def prepare_training_attributes(self):
        class MyModel(torch.nn.Module):
            def __init__(
                    self,
                    input_features,
                    output_features,
                    output_dim,
                    nb_hidden_layers,
                    hidden_features,
            ):
                super().__init__()
                self.output_dim = output_dim
                self.output_features = output_features
                self.hidden_features = hidden_features
                self.layers = [
                    torch.nn.Linear(input_features, self.hidden_features),
                    torch.nn.ReLU()
                ]
                for i in range(nb_hidden_layers):
                    self.layers += [torch.nn.Linear(self.hidden_features, self.hidden_features),
                                    torch.nn.ReLU()]
                self.layers.append(
                    torch.nn.Linear(self.hidden_features, output_features * output_dim)
                )
                self.net = torch.nn.Sequential(*self.layers)

            def forward(self, x):
                y = self.net(x)
                return y

        model = MyModel(
            self.nb_parameters + 1,
            self.mesh_size * self.mesh_size,
            1,
            self.dl_config.get("hidden_num", 1),
            self.dl_config.get("hidden_size", 256)
        ).to(self.device)
        model = self.wrap_model_ddp(model)
        optimizer = torch.optim.Adam(
            model.parameters(), lr=self.dl_config.get("lr", 1e-3), weight_decay=1e-4
        )

        return model, optimizer

    @override
    def on_train_start(self):
        self.criterion = torch.nn.MSELoss(reduction="none")

    @override
    def training_step(self, batch, batch_idx, **kwargs):
        # Backprogation
        self.optimizer.zero_grad()
        x, y_target, sim_ids_list, time_step_list = batch
        x = x.to(self.device)
        y_target = y_target.to(self.device)
        y_pred = self.model(x)
        loss_per_sample = self.criterion(y_pred, y_target).mean(dim=1)
        batch_loss = loss_per_sample.mean()
        batch_loss.backward()
        grad_norm = self.get_grad_norm()
        self.optimizer.step()

        if self.is_breed_study:
            delta_losses = \
                active_sampling.calculate_delta_loss(loss_per_sample.detach().cpu().numpy())
            active_sampling.record_increments(sim_ids_list, time_step_list, delta_losses)

        self.tb_logger.log_scalar("Loss/train", batch_loss.item(), batch_idx)
        self.tb_logger.log_scalar("Misc/l2-norm-gradient", grad_norm, batch_idx)

    @override
    def on_validation_start(self, batch_idx):
        self.val_losses.append([])

    @override
    def validation_step(self, batch, valid_batch_idx, batch_idx, **kwargs):
        x, y_target = batch
        x = x.to(self.device)  # type: ignore
        x = self.normalize_inputs(x, is_torch=True)
        y_target = y_target.to(self.device)
        y_target = self.normalize(y_target, self.Tmin, self.Tmax)

        y_pred = self.model(x)
        batch_loss = self.criterion(y_pred, y_target).mean()
        self.val_losses[-1].append(batch_loss.item())

    @override
    def on_validation_end(self, batch_idx):
        self.tb_logger.log_scalar("Loss/valid", np.mean(self.val_losses[-1]), batch_idx)
        # Learning rate
        lrs = []
        for grp in self.optimizer.param_groups:
            lrs.append(grp["lr"])
            self.tb_logger.log_scalar("lr", lrs[0], batch_idx)

    @override
    def on_train_end(self):
        seen_counts = list(self.buffer.seen_ctr.elements())
        if seen_counts:
            self.tb_logger.log_histogram("seen", np.array(seen_counts))

        # checkpoint the final model
        self.checkpoint()

    @override
    def process_simulation_data(self, msg: SimulationData, config_dict: dict):

        sim_id = msg.simulation_id
        time_step = msg.time_step
        mesh = msg.data["temperature"].astype(np.float32)
        params = np.array(
            msg.parameters[-self.nb_parameters:] + [time_step],
            dtype=np.float32
        )

        x = self.normalize_inputs(params)
        x = torch.from_numpy(x)

        normed_y = self.normalize(mesh, self.Tmin, self.Tmax)
        y = torch.from_numpy(normed_y)

        return x, y, sim_id, time_step

# =================================================================================================
#                                   CUSTOM METHODS BELOW
# =================================================================================================

    def attach_timesteps(self, parameters, nt=None):
        parameters = torch.as_tensor(parameters)
        if nt is None:
            time_steps = torch.as_tensor(range(self.nb_time_steps)).reshape(-1, 1)
            repeated_parameters = parameters.repeat_interleave(self.nb_time_steps, dim=0)
            repeated_time_steps = time_steps.repeat(len(parameters), 1)
            return torch.cat((repeated_parameters, repeated_time_steps), dim=1)
        else:
            time_steps = torch.full((len(parameters), 1), fill_value=nt)
            return torch.cat((parameters, time_steps), dim=1)

    def normalize(self, tensor, min_val, max_val):
        norm_tensor = 2 * ((tensor - min_val) / (max_val - min_val)) - 1
        return norm_tensor

    def denormalize(self, tensor, min_val, max_val):
        denorm_tensor = ((tensor + 1) / 2) * (max_val - min_val) + min_val
        return denorm_tensor

    def get_grad_norm(self):
        total_norm = 0
        for param in self.model.parameters():
            if param.grad is not None:
                param_norm = param.grad.data.norm(2)
                total_norm += param_norm.item() ** 2
        return (total_norm ** 0.5)

    @typing.no_type_check
    def normalize_inputs(self, params: Union[np.ndarray, torch.Tensor],
                         t_step=None, is_torch=False) -> Union[np.ndarray, torch.Tensor]:
        if is_torch:
            if params.dim() == 1:
                params = params.unsqueeze(0)
        else:
            if params.ndim == 1:
                params = np.expand_dims(params, axis=0)

        if t_step is None:
            t_step = params[:, -1:]
        T = params[:, :-1]
        normed_nt = t_step / self.nb_time_steps
        normed_p = self.normalize(T, self.Tmin, self.Tmax)
        if is_torch:
            normed = torch.cat((normed_p, normed_nt), dim=1)
        else:
            normed = np.concatenate((normed_p, normed_nt), axis=1)
        if is_torch:
            if normed.size(0) == 1:
                return normed[0]
            return normed
        if normed.shape[0] == 1:
            return normed[0]
        return normed

    def get_validation_dataloader(self) -> Optional[torch.utils.data.DataLoader]:
        validation_input_filename = self.dl_config.get("validation_input_filename", None)
        validation_target_filename = self.dl_config.get("validation_target_filename", None)

        if validation_input_filename is None:
            return None

        validation_input_filename = osp.expandvars(validation_input_filename)
        validation_target_filename = osp.expandvars(validation_target_filename)

        valid_dataloader = None
        files_are_valid = (
            validation_input_filename and osp.exists(validation_input_filename)
        ) and (validation_target_filename and osp.exists(validation_target_filename))
        if files_are_valid:
            x_valid = np.load(validation_input_filename).astype(np.float32)
            y_valid = np.load(validation_target_filename).astype(np.float32)
            x_valid = x_valid.reshape(-1, 6)
            y_valid = y_valid.reshape(-1, self.mesh_size * self.mesh_size)
            logger.info(f"Validation inputs shape={x_valid.shape}")
            valid_dataset = torch.utils.data.TensorDataset(
                torch.from_numpy(x_valid), torch.from_numpy(y_valid)
            )
            valid_dataloader = torch.utils.data.DataLoader(
                valid_dataset, batch_size=self.batch_size, num_workers=0, shuffle=False
            )
            logger.info("Found valid dataset path for loss validation computation")
        else:
            logger.warning("Did not find valid dataset path for loss validation computation")

        return valid_dataloader
