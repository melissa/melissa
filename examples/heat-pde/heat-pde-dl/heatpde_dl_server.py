from typing import Dict, Any, Optional
from typing_extensions import override
import os.path as osp
import logging
import numpy as np
import torch
import torch.utils.data

from melissa.server.deep_learning.torch_server import TorchServer
from melissa.server.simulation import SimulationData
from melissa.server.parameters import ParameterSamplerType


logger = logging.getLogger("melissa")


class HeatPDEServerDL(TorchServer):
    """Use-case specific server"""

    def __init__(self, config_dict: Dict[str, Any]):
        super().__init__(config_dict)
        self.param_list = ["ic", "b1", "b2", "b3", "b4", "t"]
        study_options = self.config_dict["study_options"]
        self.mesh_size = study_options["mesh_size"]
        Tmin, Tmax = study_options['parameter_range']

        # Example of random uniform sampling
        self.set_parameter_sampler(
            sampler_t=ParameterSamplerType.RANDOM_UNIFORM,
            l_bounds=[Tmin],
            u_bounds=[Tmax],
            seed=123
        )
        self.valid_dataloader = self.get_validation_dataloader()

    @override
    def prepare_training_attributes(self):
        """Abstract method that must return model and optimizer."""

        model = self.wrap_model_ddp(
            self.MyModel(
                self.nb_parameters + 1,
                self.mesh_size * self.mesh_size,
                1
            ).to(self.device)
        )

        optimizer = torch.optim.Adam(
            model.parameters(),
            lr=self.dl_config.get("lr", 1e-3),
            weight_decay=1e-4
        )

        return model, optimizer

    @override
    def on_train_start(self):
        self.criterion = torch.nn.MSELoss()
        self.learning_rate_scheduler = torch.optim.lr_scheduler.ExponentialLR(
            self.optimizer, gamma=0.995
        )
        logger.info("Start Training")

    @override
    def on_batch_start(self, batch_idx):
        if self.dl_config["get_buffer_statistics"]:
            self.get_buffer_statistics(batch_idx)

    @override
    def training_step(self, batch, batch_idx, **kwargs):

        # Backprogation
        self.optimizer.zero_grad()
        x, y_target = batch
        x = x.to(self.device)
        y_target = y_target.to(self.device)
        y_pred = self.model(x)
        loss = self.criterion(y_pred, y_target)
        loss.backward()
        self.optimizer.step()
        self.learning_rate_scheduler.step()
        self.tb_logger.log_scalar("Loss/train", loss.item(), batch_idx)

    @override
    def on_validation_start(self, batch_idx):
        self.val_losses = []

    @override
    def validation_step(self, batch, valid_batch_idx, batch_idx, **kwargs):
        x, y_target = batch
        x = x.to(self.device)  # type: ignore
        y_target = y_target.to(self.device)
        # model evaluation
        y_pred = self.model(x)
        loss = self.criterion(y_pred, y_target)
        self.val_losses.append(loss.item())

    @override
    def on_validation_end(self, batch_idx):
        self.tb_logger.log_scalar("Loss/valid", np.mean(self.val_losses), batch_idx)
        # Learning rate
        lrs = []
        for grp in self.optimizer.param_groups:
            lrs.append(grp["lr"])
            self.tb_logger.log_scalar("lr", lrs[0], batch_idx)

    @override
    def on_train_end(self):
        logger.info(f"Rank {self.rank}>> Finished training.")
        seen_counts = list(self.buffer.seen_ctr.elements())
        if seen_counts:
            self.tb_logger.log_histogram("seen", np.array(seen_counts))

        # checkpoint the final model
        self.checkpoint()

    @override
    def process_simulation_data(self, msg: SimulationData, config_dict: dict):
        """Abstract method for transformation while batch creation."""

        field = "temperature"
        # cast msg.data to float32
        x = torch.from_numpy(
            np.array(msg.parameters[-self.nb_parameters:] + [msg.time_step], dtype=np.float32)
        )
        y = torch.from_numpy(msg.data[field].astype(np.float32))

        return x, y

    class MyModel(torch.nn.Module):
        def __init__(self, input_features, output_features, output_dim):
            super().__init__()
            self.output_dim = output_dim
            self.output_features = output_features
            self.hidden_features = 256
            self.net = torch.nn.Sequential(
                torch.nn.Linear(input_features, self.hidden_features),
                torch.nn.ReLU(),
                torch.nn.Linear(self.hidden_features, self.hidden_features),
                torch.nn.ReLU(),
                torch.nn.Linear(self.hidden_features, output_features * output_dim),
            )

        def forward(self, x):
            y = self.net(x)
            return y

    def get_validation_dataloader(self) -> Optional[torch.utils.data.DataLoader]:
        validation_input_filename = self.dl_config.get("validation_input_filename", None)
        validation_target_filename = self.dl_config.get("validation_target_filename", None)

        if validation_input_filename is None:
            return None

        validation_input_filename = osp.expandvars(validation_input_filename)
        validation_target_filename = osp.expandvars(validation_target_filename)

        valid_dataloader = None
        files_are_valid = (
            validation_input_filename and osp.exists(validation_input_filename)
        ) and (validation_target_filename and osp.exists(validation_target_filename))
        if files_are_valid:
            x_valid = np.load(validation_input_filename)
            y_valid = np.load(validation_target_filename)
            x_valid = x_valid.reshape(-1, 6)
            y_valid = y_valid.reshape(-1, self.mesh_size * self.mesh_size)
            assert x_valid.shape[0] == y_valid.shape[0]
            valid_dataset = torch.utils.data.TensorDataset(
                torch.from_numpy(x_valid), torch.from_numpy(y_valid)
            )
            valid_dataloader = torch.utils.data.DataLoader(
                valid_dataset, batch_size=self.batch_size, num_workers=0, shuffle=False
            )
            logger.info("Found valid dataset path for loss validation computation")
        else:
            logger.warning("Did not find valid dataset path for loss validation computation")

        return valid_dataloader

    def get_buffer_statistics(self, batch_idx: int):
        rows = len(self.buffer.queue)
        columns = int(self.nb_parameters) + 1
        stats_array = torch.zeros((rows, columns))
        for i in range(rows):
            item = self.buffer.queue[i]
            x, _ = self.process_simulation_data(item.data, self.config_dict)
            stats_array[i, :] = x

        std, mean = torch.std_mean(stats_array, dim=0)
        for std_v, mean_v, param in zip(std, mean, self.param_list):
            self.tb_logger.log_scalar(f"buffer_std/{param}", std_v, batch_idx)
            self.tb_logger.log_scalar(f"buffer_mean/{param}", mean_v, batch_idx)
