import logging
from typing import Dict, Any
from typing_extensions import override
import numpy as np
import tensorflow as tf
import os.path as osp

from melissa.server.simulation import SimulationData
from melissa.server.deep_learning.tf_server import TFServer
from melissa.server.parameters import ParameterSamplerType

logger = logging.getLogger("melissa")


class HeatPDEServerDL(TFServer):
    """
    Use-case specific server
    """

    def __init__(self, config_dict: Dict[str, Any]):
        super().__init__(config_dict)
        self.param_list = ["ic", "b1", "b2", "b3", "b4", "t"]
        study_options = self.config_dict["study_options"]
        self.mesh_size = study_options["mesh_size"]
        Tmin, Tmax = study_options['parameter_range']

        # Example of random uniform sampling
        self.set_parameter_sampler(
            sampler_t=ParameterSamplerType.RANDOM_UNIFORM,
            l_bounds=[Tmin],
            u_bounds=[Tmax],
            seed=123
        )
        self.valid_dataloader = self.get_validation_dataloader()

    @override
    def prepare_training_attributes(self):
        """Abstract method that must return model and optimizer."""

        with self.strategy.scope():
            model = self.MyModel(6, self.mesh_size * self.mesh_size, 1)

            learning_rate_scheduler = tf.keras.optimizers.schedules.ExponentialDecay(
                initial_learning_rate=self.dl_config.get("lr", 1e-3),
                decay_steps=1,
                decay_rate=0.995,
                staircase=True,
            )

            optimizer = tf.keras.optimizers.Adam(
                learning_rate=learning_rate_scheduler,
                weight_decay=1e-4,
            )

        return model, optimizer

    @override
    @tf.function
    def on_train_start(self):
        with self.strategy.scope():
            # define the loss
            self.criterion = tf.keras.losses.MeanSquaredError(
                reduction=tf.keras.losses.Reduction.NONE
            )
            self.compute_loss = lambda y_true, y_pred: tf.nn.compute_average_loss(
                self.criterion(y_true, y_pred),
                global_batch_size=self.batch_size * self.comm_size
            )

    @tf.function
    def distributed_training_step(self, batch):
        def step_fn(batch):
            x, y_true = batch
            with tf.GradientTape() as tape:
                # forward pass
                y_pred = self.model(x, training=True)
                loss = self.compute_loss(y_true, y_pred)
                # backward pass
                gradients = tape.gradient(loss, self.model.trainable_variables)
                self.optimizer.apply_gradients(zip(gradients, self.model.trainable_variables))
                return loss

        per_replica_losses = self.strategy.run(step_fn, args=(batch,))

        return self.strategy.reduce(
            tf.distribute.ReduceOp.SUM, per_replica_losses, axis=None)

    @override
    def training_step(self, batch, batch_idx):
        loss = self.distributed_training_step(batch)
        self.tb_logger.log_scalar("Loss/train", loss.numpy(), batch_idx)

    @override
    def on_validation_start(self, batch_idx):
        self.val_losses = []

    @override
    def validation_step(self, batch, valid_batch_idx, batch_idx, **kwargs):
        x, y_target = batch
        # model evaluation
        y_pred = self.model(x, training=False)
        loss = self.compute_loss(y_target, y_pred)
        self.val_losses.append(loss.numpy())

    @override
    def on_validation_end(self, batch_idx):
        self.tb_logger.log_scalar("Loss/valid", np.mean(self.val_losses), batch_idx)
        self.tb_logger.log_scalar("lr", self.optimizer.learning_rate.numpy(), batch_idx)

    @override
    def process_simulation_data(self, msg: SimulationData, config_dict: dict):
        """Abstract method for transformation while batch creation."""

        x = np.asarray(msg.parameters[-self.nb_parameters:] + [msg.time_step], dtype=np.float32)
        y = msg.data["temperature"]

        return x, y

    class MyModel(tf.keras.Model):
        def __init__(self, input_dim, output_dim, n_features=16):
            super().__init__()
            self.input_dim = input_dim
            self.output_dim = output_dim
            self.hidden_features = 256
            self.d1 = tf.keras.layers.Dense(
                self.hidden_features, activation=tf.keras.activations.swish)
            self.d2 = tf.keras.layers.Dense(
                self.hidden_features, activation=tf.keras.activations.swish)
            self.dout = tf.keras.layers.Dense(self.output_dim * n_features)
            print(f"Model summary: {self.summary}")

        def call(self, inputs):
            x1 = self.d1(inputs)
            x2 = self.d2(x1)
            return self.dout(x2)

    def get_validation_dataloader(self):
        validation_input_filename = self.dl_config.get("validation_input_filename", None)
        validation_target_filename = self.dl_config.get("validation_target_filename", None)
        valid_dataset = None
        files_are_valid = (
            validation_input_filename and osp.exists(validation_input_filename)
        ) and (validation_target_filename and osp.exists(validation_target_filename))
        if files_are_valid:
            x_valid = np.load(validation_input_filename)
            y_valid = np.load(validation_target_filename)
            x_valid = x_valid.reshape(-1, 6)
            y_valid = y_valid.reshape(-1, self.mesh_size * self.mesh_size)
            valid_dataset = tf.data.Dataset.from_tensor_slices(
                (x_valid, y_valid)
            ).batch(self.batch_size)
            logger.info("Found valid dataset path for loss validation computation")
        else:
            logger.warning("Did not find valid dataset path for loss validation computation")

        return valid_dataset

    def get_buffer_statistics(self, batch: int):
        rows = len(self.buffer.queue)
        columns = int(self.nb_parameters) + 1
        stats_array = np.zeros((rows, columns))
        for i in range(rows):
            # safety in case the buffer size changes during computation
            if i >= len(self.buffer.queue):
                rows = i - 1
                break
            item = self.buffer.queue[i]
            x, _ = self.process_simulation_data(item.data, self.config_dict)
            stats_array[i, :] = x

        mean = np.mean(stats_array[:rows], axis=0)
        std = np.std(stats_array[:rows], axis=0)
        for std_v, mean_v, param in zip(std, mean, self.param_list):
            self.tb_logger.log_scalar(f"buffer_std/{param}", std_v, batch)
            self.tb_logger.log_scalar(f"buffer_mean/{param}", mean_v, batch)
