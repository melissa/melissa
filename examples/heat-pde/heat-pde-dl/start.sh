#!/bin/bash

cd /path/to/melissa/examples/heat-pde/heat-pde-dl/offline-example/

for i in {1..20}
do
	python3 run_offline_study.py
done
