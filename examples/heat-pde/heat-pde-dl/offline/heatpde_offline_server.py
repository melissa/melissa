import logging
from typing import Dict, Any

from melissa.server.offline_server import OfflineServer
from melissa.server.parameters import ParameterSamplerType


logger = logging.getLogger("melissa")


class HeatPDEOfflineServer(OfflineServer):
    def __init__(self, config_dict: Dict[str, Any]):
        super().__init__(config_dict)
        Tmin, Tmax = config_dict["study_options"]["parameter_range"]
        # Example of random uniform sampling
        self.set_parameter_sampler(
            sampler_t=ParameterSamplerType.HALTON,
            l_bounds=[Tmin],
            u_bounds=[Tmax],
            seed=123
        )
