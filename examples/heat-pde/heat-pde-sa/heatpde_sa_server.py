import logging

from melissa.server.sensitivity_analysis import SensitivityAnalysisServer
from melissa.server.parameters import ParameterSamplerType
from typing import Dict, Any

logger = logging.getLogger("melissa")


class HeatPDEServerSA(SensitivityAnalysisServer):
    """
    Use-case specific server
    """

    def __init__(self, config_dict: Dict[str, Any]):
        super().__init__(config_dict)
        Tmin, Tmax = config_dict["study_options"]['parameter_range']

        # Example of random uniform sampling
        self.set_parameter_sampler(
            sampler_t=ParameterSamplerType.RANDOM_UNIFORM,
            l_bounds=[Tmin],
            u_bounds=[Tmax],
            seed=123
        )
