# ARG TAG=latest
# FROM ubuntu:$TAG
FROM python:3.10

# Metadata for CI/CD (versioning, maintainers, etc.)
LABEL org.opencontainers.image.authors="abhishek.purandare@inria.fr" \
      org.opencontainers.image.source="https://gitlab.inria.fr/melissa" \
      org.opencontainers.image.version="1.0.0"

# Set environment variables
ENV PATH=/home/docker/.local/bin:$PATH \
    TZ=Europe/Paris \
    DEBIAN_FRONTEND=noninteractive \
    OMPI_MCA_btl_vader_single_copy_mechanism=none \
    OMPI_MCA_rmaps_base_oversubscribe=1 \
    OMPI_MCA_mpi_yield_when_idle=1

# Set timezone and install essential dependencies in a single layer for caching
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ >/etc/timezone
RUN apt-get update --yes && apt-get install apt-transport-https curl --no-install-recommends --yes
RUN apt-get --no-install-recommends --yes install \
    vim \
    build-essential \
    cmake \
    gfortran \
    git \
    libboost-numpy-dev \
    libhdf5-openmpi-dev \
    libopenblas-dev \
    libopenmpi-dev \
    libzmq5-dev \
    pkg-config \
    libssl-dev \
    libffi-dev \
    python3-numpy && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

# Create non-root user with fixed UID for CI consistency
ARG userid=1001
RUN adduser --uid=$userid --shell=/bin/bash --gecos='Docker,,,,' --disabled-password docker && \
    echo "docker:docker" | chpasswd

ENV LD_LIBRARY_PATH=/lib:/usr/lib:/lib/x86_64-linux-gnu/openmpi/lib

# Switch to non-root user for security
USER docker

# Set working directory for the non-root user
WORKDIR /home/docker

# Install Python requirements
COPY requirements.txt requirements_dev.txt requirements_deep_learning.txt ./

ENV VIRTUAL_ENV=/home/docker/.venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

# Upgrade pip and install required Python packages in a single step
RUN . .venv/bin/activate && \
    python3 -m pip install --no-cache-dir --upgrade pip && \
    python3 -m pip install --no-cache-dir -r requirements.txt -r requirements_dev.txt && \
    python3 -m pip install --no-cache-dir tensorflow-cpu tensorboard>=2.10.0 matplotlib && \
    python3 -m pip install --no-cache-dir --index-url https://download.pytorch.org/whl/cpu torch 

# Final stage commands to ensure smooth CI/CD execution (optional)
ENTRYPOINT ["/bin/bash", "-l", "-c"]

# CI runner
CMD [ "/bin/bash" ]