###################################################################
#                            Melissa                              #
#-----------------------------------------------------------------#
#   COPYRIGHT (C) 2017  by INRIA and EDF. ALL RIGHTS RESERVED.    #
#                                                                 #
# This source is covered by the BSD 3-Clause License.             #
# Refer to the  LICENCE file for further information.             #
#                                                                 #
#-----------------------------------------------------------------#
#  Original Contributors:                                         #
#    Theophile Terraz,                                            #
#    Bruno Raffin,                                                #
#    Alejandro Ribes,                                             #
#    Bertrand Iooss,                                              #
###################################################################

# -*- coding: utf-8 -*-

"""This code loads the Melissa C API shared library (libmelissa_api.so) using
`np.ctypeslib.load_library`. It then defines C function prototypes for
`melissa_init`, `melissa_send`, and `melissa_finalize`,
specifying the argument types for each function using `ctypes`.
These prototypes enable interaction with the C API from Python.
"""

from mpi4py import MPI
import numpy as np
from numpy.typing import NDArray
import ctypes

__version__ = "@Melissa_VERSION_MAJOR@.@Melissa_VERSION_MINOR@.@Melissa_VERSION_PATCH@"


__all__ = [
    "melissa_init",
    "melissa_send",
    "melissa_finalize"
]

__MELISSA_C_API = np.ctypeslib.load_library(
    "libmelissa_api",
    "@CMAKE_INSTALL_PREFIX@/lib/libmelissa_api.so"
)

# C prototypes
c_char_ptr = ctypes.POINTER(ctypes.c_char)
c_void_ptr = ctypes.c_void_p
c_double_ptr = ctypes.POINTER(ctypes.c_double)
c_int_ptr = ctypes.POINTER(ctypes.c_int)

__MELISSA_C_API.melissa_init_f.argtypes = (c_char_ptr, c_int_ptr, c_int_ptr)

__MELISSA_C_API.melissa_send.argtypes = (c_char_ptr, c_double_ptr)

__MELISSA_C_API.melissa_finalize.argtypes = ()


def melissa_init(field_name: str,
                 local_vect_size: int,
                 comm: MPI.Intracomm) -> None:
    """Initializes a connection with the Melissa server.

    ### Parameters
    - **field_name** (`str`):
    The name of the field to initialize.

    - **local_vect_size** (`int`):
    A list representing the size of the local data vector.

    - **comm** (`MPI.Intracomm`):
    The MPI communicator used for communication with the Melissa server."""

    comm_f = comm.py2f()
    buff = ctypes.create_string_buffer(len(field_name) + 1)
    buff.value = field_name.encode()
    __MELISSA_C_API.melissa_init_f(
        buff,
        ctypes.byref(ctypes.c_int(local_vect_size)),
        ctypes.byref(ctypes.c_int(comm_f))
    )


def melissa_send(field_name: str,
                 send_vect: NDArray) -> None:
    """Sends data to the Melissa server for a specific field.

    ### Parameters
    - **field_name** (`str`): The name of the field to which the data is being sent.

    - **send_vect** (`NDArray`): A NumPy array containing the data vector
    to be sent to the server."""

    buff = ctypes.create_string_buffer(len(field_name) + 1)
    buff.value = field_name.encode()
    array = (ctypes.c_double * len(send_vect))(*send_vect)
    __MELISSA_C_API.melissa_send(buff, array)


def melissa_finalize() -> None:
    """Finalizes the connection with the server."""
    __MELISSA_C_API.melissa_finalize()
