# -*- coding: utf-8 -*-

###################################################################
#                            Melissa                              #
#-----------------------------------------------------------------#
#   COPYRIGHT (C) 2017  by INRIA and EDF. ALL RIGHTS RESERVED.    #
#                                                                 #
# This source is covered by the BSD 3-Clause License.             #
# Refer to the  LICENCE file for further information.             #
#                                                                 #
#-----------------------------------------------------------------#
#  Original Contributors:                                         #
#    Theophile Terraz,                                            #
#    Bruno Raffin,                                                #
#    Alejandro Ribes,                                             #
#    Bertrand Iooss,                                              #
###################################################################

import ctypes
import numpy as np


__version__ = "@Melissa_VERSION_MAJOR@.@Melissa_VERSION_MINOR@.@Melissa_VERSION_PATCH@"


__all__ = [
    "melissa_init",
    "melissa_send",
    "melissa_finalize",
    "melissa_send_horovod"
]


__MELISSA_C_API_NO_MPI = np.ctypeslib.load_library(
    "libmelissa_api",
    "@CMAKE_INSTALL_PREFIX@/lib/libmelissa_api.so"
)


# C prototypes
c_char_ptr = ctypes.POINTER(ctypes.c_char)
c_double_ptr = ctypes.POINTER(ctypes.c_double)

__MELISSA_C_API_NO_MPI.melissa_init_no_mpi.argtypes = (
    c_char_ptr,  # field_name
    ctypes.c_int  # vect_size
)


__MELISSA_C_API_NO_MPI.melissa_send_no_mpi.argtypes = (
    c_char_ptr,   # field_name
    c_double_ptr  # send_vect
)

__MELISSA_C_API_NO_MPI.melissa_finalize.argtypes = ()


def melissa_init(field_name, vect_size):
    buff = ctypes.create_string_buffer(len(field_name) + 1)
    buff.value = field_name.encode()
    __MELISSA_C_API_NO_MPI.melissa_init_no_mpi(
        buff,
        ctypes.c_int(vect_size)
    )
    pass


def melissa_send(field_name, send_vect):
    buff = ctypes.create_string_buffer(len(field_name) + 1)
    buff.value = field_name.encode()
    data = send_vect.astype(np.float64)
    array = data.ctypes.data_as(c_double_ptr)
    __MELISSA_C_API_NO_MPI.melissa_send(buff, array)


def melissa_finalize():
    __MELISSA_C_API_NO_MPI.melissa_finalize()


def melissa_send_horovod(field_name, send_vect):
    buff = ctypes.create_string_buffer(len(field_name) + 1)
    buff.value = field_name.encode()
    array = (ctypes.c_double * len(send_vect))(*send_vect)
    __MELISSA_C_API_NO_MPI.melissa_send_horovod(buff, array)
    pass
