from unittest.mock import MagicMock
import logging
import os
import struct
import argparse
import sys
from melissa.server.parameters import ParameterSamplerType
from melissa.server.main import get_resolved_server, validate_config
from melissa.server.base_server import BaseServer
from tests.conftest import log_has_re
from melissa.server.simulation import Group, Simulation
import glob


def get_naked_BaseServer(default_config) -> BaseServer:
    sys.argv = ['--project_dir examples/lorenz']
    parser = argparse.ArgumentParser()
    parser.add_argument('--project_dir', type=str, default='examples/lorenz')
    parser.add_argument("--server_class")

    args = parser.parse_args()

    args, config = validate_config(args, default_config)

    os.environ["MELISSA_FAULT_TOLERANCE"] = "OFF"
    os.environ["MELISSA_RESTART"] = "0"

    server = get_resolved_server(args, config)
    server.set_parameter_sampler(
        sampler_t=ParameterSamplerType.RANDOM_UNIFORM,
        l_bounds=[100],
        u_bounds=[400],
        second_order=False,
        apply_pick_freeze=False,
    )
    server._update_parameter_generator()

    return server


# def get_patched_BaseServer(mocker, default_config):
#     base_server = get_naked_BaseServer(mocker, default_config)
#     base_server.launcherfd = MagicMock()


def test_generate_client_scripts(default_config, caplog, tmp_path):
    with caplog.at_level(logging.DEBUG, logger="melissa"):
        server = get_naked_BaseServer(default_config)
        server.node_name = MagicMock()
        server._generate_client_scripts(first_id=1, nb_scripts=2)

        assert log_has_re(
            "Rank 0>> created client.1.sh",
            caplog
        )
        [os.remove(file) for file in glob.glob('client.*.sh')]


def test_handle_simulation_data_success(default_config, caplog):
    server = get_naked_BaseServer(default_config)
    server._groups[0] = Group(0)
    server._groups[0].simulations[0] = Simulation(0, 10, ['position'], None)
    # pack a fake message to test handle_simulation_data
    msg = struct.pack("4i128s6d", 8, 0, 2, 6, str.encode('position'), 1, 2, 3, 4, 5, 6)
    sim_data = server._handle_simulation_data(msg)

    assert sim_data


def test_handle_simulation_data_bad_time_step(default_config, caplog):
    server = get_naked_BaseServer(default_config)
    server._groups[0] = Group(0)
    server._groups[0].simulations[0] = Simulation(0, 10, ['position'], None)
    # pack a fake message to test handle_simulation_data
    msg = struct.pack("4i128s6d", -8, 0, 2, 6, str.encode('position'), 1, 2, 3, 4, 5, 6)
    server._handle_simulation_data(msg)

    assert log_has_re(
        r"Rank 0>> \[BAD\] sim-id=0, time-step=-8",
        caplog
    )


def test_handle_simulation_data_bad_field(default_config, caplog):
    server = get_naked_BaseServer(default_config)
    server._groups[0] = Group(0)
    server._groups[0].simulations[0] = Simulation(0, 10, ['position'], None)
    # pack a fake message to test handle_simulation_data
    msg = struct.pack("4i128s6d", 8, 0, 2, 6, str.encode('position2'), 1, 2, 3, 4, 5, 6)
    server._handle_simulation_data(msg)

    assert log_has_re(
        r'Rank 0>> \[BAD\] sim-id=0, field="position2"',
        caplog
    )


def test_handle_simulation_connection(default_config, caplog):
    server = get_naked_BaseServer(default_config)
    server._groups[0] = Group(0)
    server._groups[0].simulations[0] = Simulation(0, 10, ['position'], None)
    server._port_names = MagicMock()
    server._connection_responder = MagicMock()
    # pack a fake message to test handle_simulation_data
    msg = struct.pack("2i", 1, 1)

    ret = server._handle_simulation_connection(msg)

    assert "Connection" in ret
