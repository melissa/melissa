import logging
from melissa.server.sensitivity_analysis import SensitivityAnalysisServer

logger = logging.getLogger("melissa")


class OTServerSA(SensitivityAnalysisServer):
    """
    Use-case specific server
    """
    pass
