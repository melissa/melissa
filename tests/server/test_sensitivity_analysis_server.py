from unittest.mock import MagicMock
import logging
import numpy as np
import os
import argparse
import sys
from melissa.server.main import get_resolved_server, validate_config
from melissa.server.base_server import BaseServer
from melissa.server.simulation import PartialSimulationData, Group, Simulation
from melissa.server.parameters import ParameterSamplerType, make_parameter_sampler
import pytest


np.seterr(divide='ignore', invalid='print')


def get_naked_SensitivityAnalysisServer(default_sa_config) -> BaseServer:
    sys.argv = ["--project_dir tests/server"]
    parser = argparse.ArgumentParser()
    parser.add_argument("--project_dir", type=str, default="tests/server")
    parser.add_argument("--server_class")
    args = parser.parse_args()
    args, config = validate_config(args, default_sa_config)
    os.environ["MELISSA_FAULT_TOLERANCE"] = "OFF"
    os.environ["MELISSA_RESTART"] = "0"
    server = get_resolved_server(args, config)
    seed = config["study_options"]["seed"]
    server.set_parameter_sampler(
        sampler_t=ParameterSamplerType.RANDOM_UNIFORM,
        l_bounds=[100],
        u_bounds=[400],
        second_order=False,
        apply_pick_freeze=True,
        seed=seed
    )
    server._update_parameter_generator()

    return server


def model_ishigami(input_array):
    x1 = input_array[:, 0]
    x2 = input_array[:, 1]
    x3 = input_array[:, 2]
    # output formula from
    # https://openturns.github.io/openturns/latest/user_manual/_generated/openturns.MartinezSensitivityAlgorithm.html
    model_output = (
        np.sin(np.pi * x1)
        + 7 * np.square(np.sin(np.pi * x2))
        + 0.1 * (
            np.power(np.pi * x3, 4)
            * np.sin(np.pi * x1)
        )
    )
    return model_output


def test_sensitivity_analysis_server(default_sa_config, caplog, tmp_path):
    with caplog.at_level(logging.DEBUG, logger="melissa"):

        field = "field"
        time_step = 0
        client_rank = 0
        key = (field, client_rank, time_step)

        server = get_naked_SensitivityAnalysisServer(default_sa_config)
        server.node_name = MagicMock()

        # test check_group_size with sobol_op
        server._check_group_size()
        assert server.group_size == server.nb_parameters + 2
        assert server.nb_clients == server.group_size * server.nb_groups

        input_parameters = np.array([
            server._get_next_parameters()
            for _ in range(server.nb_clients)
        ])

        total_params = server.nb_clients * server.nb_parameters
        assert np.size(input_parameters) == total_params
        output_values = model_ishigami(input_parameters)
        output_values = output_values.reshape(
            server.nb_groups,
            server.group_size
        )
        # test compute_stats
        vec_1 = output_values[:, 0]
        vec_2 = output_values[:, 1]
        for group_id in range(server.nb_groups):
            server._groups[group_id] = Group(group_id, server.sobol_op)
            for i in range(server.group_size):
                sim_id = group_id * server.group_size + i
                server._groups[group_id].simulations[sim_id] = \
                    Simulation(sim_id, -1, [], [])
                server._compute_stats(
                    PartialSimulationData(
                        time_step=time_step,
                        simulation_id=sim_id,
                        client_rank=client_rank,
                        data_size=1,
                        field=field,
                        data=np.array(output_values[group_id, i])
                    )
                )

        vec = np.concatenate((vec_1, vec_2))

        mean = np.mean(vec)
        err = abs(server.melissa_moments[key].get_mean() - mean) / mean
        assert err < 1e-6

        variance = np.var(vec, ddof=1)
        err = abs(server.melissa_moments[key].get_variance() - variance) / variance
        assert err < 1e-3
        # sobol indices
        first_order_indices = [
            server.melissa_sobol[key].pearson_B[0, param]
            for param in range(server.nb_parameters)
        ]
        total_order_indices = [
            server.melissa_sobol[key].pearson_A[0, param]
            for param in range(server.nb_parameters)
        ]
        print(f"first order indices: {first_order_indices}")
        print(f"total order indices: {total_order_indices}")


def qmc_generator(sampler_t):
    # Test case 1: Initialization without bounds
    with pytest.raises(TypeError):
        _ = make_parameter_sampler(sampler_t, nb_params=2, nb_sims=10)

    # Test case 2: Different bounds for parameters
    obj = make_parameter_sampler(
        sampler_t,
        l_bounds=[0, 1, 2],
        u_bounds=[1, 2, 3],
        nb_params=3,
        nb_sims=10
    )

    count = 0

    for sample in obj.generator():
        assert len(sample) == 3
        assert 0 <= sample[0] <= 1
        assert 1 <= sample[1] <= 2
        assert 2 <= sample[2] <= 3
        count += 1
    assert count == 10


def test_halton():
    qmc_generator(ParameterSamplerType.HALTON)


def test_latin_hypercube():
    qmc_generator(ParameterSamplerType.LHS)
