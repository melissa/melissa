# This code is only compatible with direct mpirun output files in stdout folder
# FIXME
# Line 37: change the exec_pattern which is the pattern for the simulation executable.
# Line 66: change the filenames accordingly.

import re
import os
import argparse


def extract_floats_from_script(script_content):
    """Extracts the last five floating point numbers from the provided script content.

    ### Parameters
    - **script_content** (`str`): The content of the script file as a string.

    ### Returns
    - `list`: A list of the last five floating point numbers as strings."""

    float_pattern = r'\b\d+\.\d+\b'
    floats = re.findall(float_pattern, script_content)
    last_five_floats = floats[-5:]
    return last_five_floats


def extract_floats_from_err(err_content):
    """Extracts the last five floating point numbers from a specific line in
    the provided .err content.

    ### Parameters
    - **err_content** (`str`): The content of the .err file as a string.

    ### Returns
    - `list`: A list of the last five floating point numbers as strings
    if the specific line is found, otherwise an empty list."""

    exec_pattern = "*heatc"
    specific_line_pattern = r'\+ exec ' + exec_pattern + r'.*'
    match = re.search(specific_line_pattern, err_content)
    if match:
        specific_line = match.group(0)
        float_pattern = r'\b\d+\.\d+\b'
        floats = re.findall(float_pattern, specific_line)
        last_five_floats = floats[-5:]
        return last_five_floats
    return []


def args():
    parser = argparse.ArgumentParser(description="Patterns extraction for comparing the parameters "
                                     "of an OpenMPI active sampling study.")
    parser.add_argument('--nb-sims', type=int, required=True, help='The number of simulations.')
    parser.add_argument('--study-out', type=str, required=True, help='path to STUDY_OUT')
    args = parser.parse_args()

    return args.nb_sims, args.study_out


if __name__ == "__main__":
    nb_sims, study_out = args()
    stdout_dir = os.path.join(study_out, "stdout")
    client_scripts_dir = os.path.join(study_out, "client_scripts")
    all_consistent = True
    c = 0
    for i in range(1, nb_sims):
        err_file_path = os.path.join(stdout_dir, f"openmpi.{i}.err")
        script_file_path = os.path.join(client_scripts_dir, f"client.{i-1}.sh")

        if os.path.exists(err_file_path):
            with open(err_file_path) as err_file:
                floats_from_err = extract_floats_from_err(err_file.read())

            with open(script_file_path) as script_file:
                floats_from_script = extract_floats_from_script(script_file.read())

            inconsistency_found = False
            for float_err, float_script in zip(floats_from_err, floats_from_script):
                if float_err != float_script:
                    inconsistency_found = True
                    break

            if inconsistency_found:
                all_consistent = False
                print(f"sim-id={i-1} ran={floats_from_err}\tscript={floats_from_script}")
                c += 1

    if all_consistent:
        print("\nAll submissions are consistent!")
    else:
        print(f"\nTotal {c}/{nb_sims} simulations ran with previous generation parameters!")
