import sys
import os
import signal
import logging
from typing_extensions import override

from melissa.server.base_server import ReceptionError
from melissa.server.simulation import PartialSimulationData
p = os.path.expandvars("$MELISSA_ROOT/examples/heat-pde/heat-pde-sa")
assert os.path.exists(p)
sys.path.append(p)
from heatpde_sa_server import HeatPDEServerSA as OriginalServer  # type: ignore # noqa: E402

logger = logging.getLogger("melissa")


class HeatPDEServerSA(OriginalServer):

    # we override receive to inject a failure into it for testing checkpointing
    @override
    def _receive(self):
        """Handles data from the server."""

        try:
            self._is_receiving = True
            received_samples = 0
            while not self._all_done():
                status = self.poll_sockets()
                if status is not None:
                    if isinstance(status, PartialSimulationData):
                        logger.debug(
                            f"Rank {self.rank}>> "
                            f"sim-id={status.simulation_id}, "
                            f"time-step={status.time_step} received."
                        )
                        received_samples += 1

                        # simulate a failure
                        if received_samples % 500 == 0:
                            os.kill(os.getpid(), signal.SIGILL)
                        # compute the statistics on the received data
                        self._compute_stats(status)
                        self.checkpoint_state()

            self._is_receiving = False
        except ReceptionError as e:
            logger.exception(f"Exception was raised in the receiving thread: \n {e}")
            if self.no_fault_tolerance:
                self.server_finalize(exit_=1)
                sys.exit(1)
