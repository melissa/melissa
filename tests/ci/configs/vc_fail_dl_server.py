import sys
import os
import signal
import logging
from typing_extensions import override

p = os.path.expandvars("$MELISSA_ROOT/examples/heat-pde/heat-pde-dl")
assert os.path.exists(p)
sys.path.append(p)
from heatpde_dl_server import HeatPDEServerDL as OriginalServer  # type: ignore # noqa: E402

logger = logging.getLogger("melissa")


class HeatPDEServerDL(OriginalServer):
    """
    Use-case specific server
    """

    @override
    def on_batch_end(self, batch_idx):
        # simulating a failure
        if batch_idx == 10 and self.rank == 0 and self._restart == 0:
            logger.info(f"Rank {self.rank}>> Dying. On purpose.")
            os.kill(os.getpid(), signal.SIGILL)
