#!/bin/sh
#SBATCH --job-name=heatpde-slurm-semiglobal
#SBATCH --output=heatpde-semiglobal.out
#SBATCH --time=01:00:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=2

source $MELISSA_ROOT/melissa_set_env.sh

exec melissa-launcher --config_name vc_slurm_semiglobal.json