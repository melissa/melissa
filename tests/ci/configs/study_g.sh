#!/bin/sh
#SBATCH --job-name=heatpde-slurm-global
#SBATCH --output=heatpde-global.out
#SBATCH --time=01:00:00
#SBATCH --nodes=5
#SBATCH --exclusive
#SBATCH --ntasks-per-node=2

source $MELISSA_ROOT/melissa_set_env.sh

exec melissa-launcher --config vc_slurm_global.json