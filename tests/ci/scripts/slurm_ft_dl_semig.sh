#! /bin/bash

set -e

EXAMPLE_DIR=$CI_CONFIGS
CONFIG_FILE="vc_slurm_semiglobal.json"

cd $EXAMPLE_DIR
rm -rf STUDY_OUT
sbatch study_sg.sh
sleep 60
ls -l STUDY_OUT/checkpoints
tail $(ls STUDY_OUT/melissa_server_0_restart_*.log | sort -V | tail -n 1)
rm heatpde-semiglobal.out