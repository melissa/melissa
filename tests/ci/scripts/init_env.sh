#!/bin/bash
set -e

# Check if the environment is already initialized
if [ -z "$ENV_INITIALIZED" ]; then
    echo "Initializing environment..."

    # Set the Open MPI environment variables
    export OMPI_ALLOW_RUN_AS_ROOT=1
    export OMPI_ALLOW_RUN_AS_ROOT_CONFIRM=1
    export OMPI_MCA_btl="^openib,ofi"

    # Check if VIRTUAL_ENV is set
    if [ -z "$VIRTUAL_ENV" ]; then
        echo "Error: VIRTUAL_ENV is not set. Please set it to the path of your virtual environment."
        exit 1
    fi

    echo "Activating virtual environment: $VIRTUAL_ENV"
    source "$VIRTUAL_ENV/bin/activate"

    # If melissa_set_env.sh exists, source it
    if [ -f "$MELISSA_ROOT/melissa_set_env.sh" ]; then
        source "$MELISSA_ROOT/melissa_set_env.sh"
    fi

    echo "Environment initialized successfully!"

    export ENV_INITIALIZED=1
else
    echo "Environment already initialized."
fi
