#! /bin/bash

set -e



# SA study
export CONFIG_FILE="vc_slurm_openmpi_sa.json"
export EXAMPLE_DIR="$CI_CONFIGS"
source $CI_SCRIPTS/sa_study.sh

# DL study
export CONFIG_FILE="vc_slurm_openmpi_dl.json"
export EXAMPLE_DIR="$CI_CONFIGS"
source $CI_SCRIPTS/dl_study.sh
