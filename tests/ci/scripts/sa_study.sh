#! /bin/bash

set -e

echo "===================SENSITIVITY ANALYSIS STUDY START========================"

source $CI_SCRIPTS/init_env.sh

cd $EXAMPLE_DIR
rm -rf STUDY_OUT
sed -i "s|/path/to/melissa/|$MELISSA_ROOT/|g" $CONFIG_FILE
melissa-launcher --config $CONFIG_FILE
tail STUDY_OUT/melissa_server_0.log
ls STUDY_OUT/results | wc -l

if [ -d "$SA_RESULTS_DIR" ]; then
  python3 "$MELISSA_ROOT/tests/l2norm.py" STUDY_OUT/results "$SA_RESULTS_DIR" || { 
    echo "Ensure you have run this SA study with numpy seed set to 123 and 3 groups of 4 clients."; 
    echo "Otherwise, there might be some issues in the new codebase."; 
    exit 1; 
  }
fi

echo "=================SENSITIVITY ANALYSIS STUDY FINISHED======================"
