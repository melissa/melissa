#! /bin/bash

set -e

EXAMPLE_DIR=$CI_CONFIGS
CONFIG_FILE="$EXAMPLE_DIR/vc_slurm_sa.json"

source $CI_SCRIPTS/sa_study.sh

tail $(ls STUDY_OUT/melissa_server_0_restart_*.log | sort -V | tail -n 1)
