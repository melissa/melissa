#! /bin/bash

set -e

EXAMPLE_DIR=$CI_CONFIGS
CONFIG_FILE="vc_slurm_dl.json"

source $CI_SCRIPTS/dl_study.sh

tail $(ls STUDY_OUT/melissa_server_0_restart_*.log | sort -V | tail -n 1)