#!/bin/bash

# Exit immediately if a command exits with a non-zero status
set -e

source $CI_SCRIPTS/init_env.sh

echo "Building and installing Melissa..."

rm -rf $MELISSA_ROOT/build $MELISSA_ROOT/install
# Install Melissa dependencies into the specified directory
python3 -m pip install \
    --target="$MELISSA_ROOT/install" \
    --no-cache-dir \
    --no-deps \
    "$MELISSA_ROOT[all]"
    
# Configure and build Melissa
export PKG_CONFIG_PATH=$(pkg-config --variable pcfiledir libzmq)
cmake -DCMAKE_INSTALL_PREFIX="$MELISSA_ROOT/install" -DINSTALL_ZMQ=OFF -S "$MELISSA_ROOT" -B "$MELISSA_ROOT/build"
make -C "$MELISSA_ROOT/build" -j $(nproc)
make -C "$MELISSA_ROOT/build" install

# Source Melissa environment setup
if [ -f "$MELISSA_ROOT/melissa_set_env.sh" ]; then
    echo "Sourcing melissa_set_env.sh..."
    source "$MELISSA_ROOT/melissa_set_env.sh"
else
    echo "Error: $MELISSA_ROOT/melissa_set_env.sh not found."
    exit 1
fi

# Check the installed Melissa launcher
echo "MELISSA LAUNCHER BINARY:"
which melissa-launcher || { echo "Melissa launcher not found"; exit 1; }

echo "Building HeatPDE executables..."

rm -rf $HEATPDE_EXEC_DIR/build
# Configure and build HeatPDE executables
cmake -DCMAKE_PREFIX_PATH="$MELISSA_ROOT/install" -S "$HEATPDE_EXEC_DIR" -B "$HEATPDE_EXEC_DIR/build"
make -C "$HEATPDE_EXEC_DIR/build"

# List the build directory contents
echo "Listing build directory contents:"
ls -l "$HEATPDE_EXEC_DIR/build"

# Download SA study results ran on 3 groups each with 4 clients
# Parameters are sampled with numpy seed 123
# Compare each SA study using l2norm.py
FNAME="sa_sobol_results.npz"
echo "Downloading SA results for consistency checks:"

# Try downloading the file, but ignore network failures
if curl -o $FNAME $SA_RESULTS_URL; then
    echo "Download successful."
    python3 $MELISSA_ROOT/tests/numpy_compression.py --decompress $FNAME --output $SA_RESULTS_DIR
    ls -lt $SA_RESULTS_DIR
else
    echo "Warning: Download failed. Skipping SA results processing."
fi

echo "Build completed successfully!"
