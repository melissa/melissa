#! /bin/bash

set -e

EXAMPLE_DIR=$CI_CONFIGS
cd $EXAMPLE_DIR
rm -rf STUDY_OUT
sbatch study_g.sh
sleep 120
ls -l STUDY_OUT/checkpoints
tail $(ls STUDY_OUT/melissa_server_0_restart_*.log | sort -V | tail -n 1)
rm heatpde-global.out
