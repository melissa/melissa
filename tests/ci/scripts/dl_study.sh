#! /bin/bash

set -e

echo "===================DEEP LEARNING STUDY START========================"

source $CI_SCRIPTS/init_env.sh

cd $EXAMPLE_DIR
rm -rf STUDY_OUT
sed -i "s|/path/to/melissa/|$MELISSA_ROOT/|g" $CONFIG_FILE
melissa-launcher --config $CONFIG_FILE
tail STUDY_OUT/melissa_server_0.log
ls -l STUDY_OUT
ls -l STUDY_OUT/checkpoints

echo "=================DEEP LEARNING STUDY FINHISHED======================"
