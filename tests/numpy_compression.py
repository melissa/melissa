import os
import numpy as np
import argparse


def compress_text_files(folder_path, output_file):
    """
    Loads all text files in a given folder, reads numerical values,
    and compresses them into a .npz file.

    Parameters:
        folder_path (str): Path to the folder containing text files.
        output_file (str): Output filename for the compressed .npz file.
    """
    data_dict = {}

    for filename in os.listdir(folder_path):
        if filename.startswith("results_"):
            file_path = os.path.join(folder_path, filename)
            try:
                data = np.loadtxt(file_path)  # Load numerical values
                data_dict[filename] = data
            except Exception as e:
                print(f"Skipping {filename}: {e}")

    if data_dict:
        np.savez_compressed(output_file, **data_dict)
        print(f"Compressed {len(data_dict)} files into {output_file}")
    else:
        print("No valid text files found to compress.")


def decompress_npz(npz_file, output_folder):
    """
    Decompresses a .npz file and saves the data as text files.

    Parameters:
        npz_file (str): Path to the .npz file.
        output_folder (str): Folder where text files will be saved.
    """
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    data = np.load(npz_file)
    for filename, values in data.items():
        output_path = os.path.join(output_folder, filename)
        try:
            np.savetxt(output_path, values)
            print(f"Decompressed {filename} to {output_path}")
        except Exception as e:
            print(f"Skipping {filename}: {e}")


def main():
    parser = argparse.ArgumentParser(
        description="Compress or decompress text files using numpy .npz format."
    )
    parser.add_argument(
        "--compress", type=str, help="Path to the folder containing text files to compress."
    )
    parser.add_argument("--decompress", type=str, help="Path to the .npz file to decompress.")
    parser.add_argument("--output", type=str, required=True, help="Output file or folder path.")

    args = parser.parse_args()

    if args.compress:
        compress_text_files(args.compress, args.output)
    elif args.decompress:
        decompress_npz(args.decompress, args.output)
    else:
        print("Please specify either --compress or --decompress with the appropriate path.")


if __name__ == "__main__":
    main()
