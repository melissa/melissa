import os
import sys
import numpy as np
import argparse
from multiprocessing import Pool, cpu_count


def get_file_tuples(study_results_dir, reference_results_dir):
    for fname in os.listdir(reference_results_dir):
        f = os.path.join(reference_results_dir, fname)
        if os.path.isfile(f):
            yield (
                os.path.join(study_results_dir, fname),
                f
            )


def calculate_l2_norm(file1, file2):

    try:
        n1 = np.loadtxt(file1, dtype=np.float64)
        n2 = np.loadtxt(file2, dtype=np.float64)
    except FileNotFoundError:
        print(f"file={os.path.split(file1)[-1]}\tnot found")
        return 0.0
    first_n = min(len(n1), len(n2))
    l2_norm = np.sqrt(np.sum((n1[:first_n] - n2[:first_n]) ** 2))

    print(
        f"file={os.path.split(file1)[-1]}\t"
        f"l2-norm={l2_norm:.3e}", end="\n" if np.all(n1) else ", nz=all\n"
    )
    return l2_norm


def parallel_calculate_l2_norm(file_pairs, jobs):
    with Pool(processes=jobs) as pool:
        norms_list = pool.starmap(calculate_l2_norm, file_pairs)
    return norms_list


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Calculate L2 norms of files.")
    parser.add_argument("file1", help="First file or directory")
    parser.add_argument("file2", help="Second file or directory")
    parser.add_argument("--jobs", type=int, default=-1,
                        help="Number of CPU cores to use (-1 means all cores)")
    args = parser.parse_args()

    file1 = os.path.expandvars(args.file1.strip())
    file2 = os.path.expandvars(args.file2.strip())
    jobs = args.jobs

    if jobs == -1:
        jobs = cpu_count()  # Use all available cores

    print(f"Path 1: {file1}\nPath 2: {file2}\n")

    if os.path.isdir(file1) and os.path.isdir(file2):
        file_pairs = list(get_file_tuples(file1, file2))

        norms_list = parallel_calculate_l2_norm(file_pairs, jobs)

        print()
        total_err = np.mean(norms_list)
        if total_err > 1e-10:
            print("\n[ERROR] Some results produced higher errors.\n")
            sys.exit(1)
    elif os.path.isfile(file1) and os.path.isfile(file2):
        calculate_l2_norm(file1, file2)
    else:
        print("Error: Both inputs must be files or directories.")
        sys.exit(1)
